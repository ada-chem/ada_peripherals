# import libraries
import numpy as np
from PIL import Image
import matplotlib as mpl
from matplotlib import colors
import matplotlib.pyplot as plt
from matplotlib import colorbar
from matplotlib.pyplot import savefig

# open original image and convert to gray scale
img = Image.open('0 - Original.png').convert("L")
# set area coordinates [left, top, right, bottom]
coordAreas = np.matrix([[305, 185, 545, 425], [827, 185, 1067, 425],
                        [1337, 185, 1577, 425], [1837, 185, 2077, 425],
                        [2357, 185, 2597, 425], [305, 685, 545, 925],
                        [827, 685, 1067, 925], [1337, 685, 1577, 925],
                        [1837, 685, 2077, 925], [2357, 685, 2597, 925]])

# set number of samples
numberSample = 10
# set threshold and score boundaries
threshold = 0.6
scoreMax = 70
scoreMin = 30

# initialise canvas
fig = plt.figure(figsize=(14, 6))

# loop over each sample
for countSample in range(numberSample):
    # crop image
    imgCrop = img.crop((coordAreas.item(countSample, 0), coordAreas.item(countSample, 1),
                        coordAreas.item(countSample, 2), coordAreas.item(countSample, 3)))
    # save image in array
    imgArr = np.array(imgCrop)

    # initialise variables for normalisation bounds [0, 1]
    a = 0.0
    b = 1.0
    # initialise variables for current bounds [0, 255]
    imgdata_min = 0.0
    imgdata_max = 255.0
    # apply normalisation
    imgNorm = a + (((imgArr - imgdata_min) * (b - a)) / (imgdata_max - imgdata_min))

    # normalise image
    # (num. of block x, block size x, num. of block y, block size y)
    imgNorm.shape = (60, 4, 60, 4)
    imgNorm2 = np.mean(imgNorm, axis=3)
    imgNorm2 = np.mean(imgNorm2, axis=1)

    # apply threshold
    imgNormThresh = imgNorm2
    imgNormThresh[imgNorm2 > threshold] = 1
    imgNormThresh[imgNorm2 <= threshold] = 0

    # Estimate score
    # initialise total number of damaged blocks
    totalDamagedBlock = 0
    # find numbers of blocks in row and column
    row, column = imgNormThresh.shape
    # calculate total number of block
    totalBlock = row * column

    # calculate total number of damaged block
    for countRow in range(row):
        for countColumn in range(column):
            if imgNormThresh[countRow, countColumn] == 1:
                totalDamagedBlock = totalDamagedBlock + 1

    # calculate score (not damaged percentage)
    score = 100 - (totalDamagedBlock * 100 / totalBlock)
    print("Score = %d %%" % score)

    # define colormap (score-dependent)
    if score >= scoreMax:
        colormap = colors.ListedColormap(["white", "green"])
    elif score <= scoreMin:
        colormap = colors.ListedColormap(["white", "red"])
    else:
        colormap = colors.ListedColormap(["white", "blue"])

    # set bounds of colorbar
    bounds = [0.0, 0.5, 1.0]
    norm = colors.BoundaryNorm(bounds, colormap.N)
    # display image
    ax = fig.add_subplot(2, 5, countSample + 1)
    imgShow = ax.imshow(imgNormThresh, interpolation="nearest", cmap=colormap, norm=norm)
    # display score in textbox
    ax.text(1.5, 1.5, "%d %%" % score,
            bbox=dict(facecolor='white', alpha=1.0),
            fontdict=None, withdash=False, fontsize=14,
            horizontalalignment="left", verticalalignment="top")

# display threshold value
fig.suptitle("threshold @ %.1f" % threshold, fontsize=16)
# set colorbar location [left, bottom, width, height]
ax = fig.add_axes([0.15, 0.015, 0.7, 0.05])
# set discrete colors of colorbar
cmap = mpl.colors.ListedColormap(["red", "blue", "green"])
# set bounds for each discrete colors
bounds = [0.0, 0.333, 0.666, 1.0]
# apply custom properties for colorbar
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
# define colorbar
colorBar = mpl.colorbar.ColorbarBase(ax, cmap=cmap,
                                     norm=norm,
                                     boundaries=bounds,
                                     extendfrac="auto",
                                     spacing="uniform",
                                     ticks=[0.166, 0.50, 0.833],
                                     orientation="horizontal")
# set ticks labels
colorBar.ax.set_xticklabels([("$\leq$ %d %%" % scoreMin),
                             ("%d-%d %%" % (scoreMin, scoreMax)),
                             ("$\geq$ %d %%" % scoreMax)])
# set label size
colorBar.ax.tick_params(labelsize=14)

# save (and display) output image
savefig("outputFigure.eps", format="eps", bbox_inches="tight", transparent="True", dpi=100)
savefig("outputFigure.png", format="png", bbox_inches="tight", transparent="True", dpi=200)
# plt.tight_layout()
# plt.show()
