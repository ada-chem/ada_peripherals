# import libraries
import matplotlib.pyplot as plt
import numpy as np
from matplotlib import colors
from matplotlib.pyplot import savefig
from PIL import Image

# open original image and convert to gray scale
img = Image.open('0 - Original.png').convert("L")

# crop image
imgCrop = img.crop((833, 182, 1063, 412))
# save image in array
imgArr = np.array(imgCrop)

# initialise variables for normalisation bounds [0, 1]
a = 0.0
b = 1.0
# initialise variables for current bounds [0, 255]
imgdata_min = 0.0
imgdata_max = 255.0
# apply normalisation
imgNorm = a + (((imgArr - imgdata_min) * (b - a)) / (imgdata_max - imgdata_min))

# initialise canvas
plt.figure(figsize=(14, 3.5))
# show left image
plt.subplot(131)
plt.title("original image")
plt.imshow(imgNorm, "gray", interpolation="nearest", vmin=0.0, vmax=1.0)
plt.colorbar(fraction=0.046, pad=0.04)

# normalise image
# (num. of block x, block size x, num. of block y, block size y)
imgNorm.shape = (46, 5, 46, 5)
imgNorm2 = np.mean(imgNorm, axis=3)
imgNorm2 = np.mean(imgNorm2, axis=1)

# show middle image
plt.subplot(132)
plt.title("downsampling: 46x46 blocks")
plt.imshow(imgNorm2, "gray", interpolation="nearest", vmin=0.0, vmax=1.0)
plt.colorbar(fraction=0.046, pad=0.04)

# apply threshold
threshold = 0.6
imgNormThresh = imgNorm2
imgNormThresh[imgNorm2 > threshold] = 1
imgNormThresh[imgNorm2 <= threshold] = 0

# Estimate score
# initialise total number of damaged blocks
totalDamagedBlock = 0
# find numbers of blocks in row and column
row, column = imgNormThresh.shape
# calculate total number of block
totalBlock = row * column

# calculate total number of damaged block
for countRow in range(1, row):
    for countColumn in range(1, column):
        if imgNormThresh[countRow, countColumn] == 1:
            totalDamagedBlock = totalDamagedBlock + 1

# calculate score (not damaged percentage)
score = 100 - (totalDamagedBlock * 100 / totalBlock)
print("")
print("Score = %.1f %%" % score)
print("")

# show right image
plt.subplot(133)
# make a color map of fixed colors
if score >= 80:
    colormap = colors.ListedColormap(["white", "green"])
elif score <= 20:
    colormap = colors.ListedColormap(["white", "red"])
else:
    colormap = colors.ListedColormap(["white", "blue"])

# set bounds of colorbar
bounds = [0.0, 0.5, 1.0]
norm = colors.BoundaryNorm(bounds, colormap.N)
# display image
img = plt.imshow(imgNormThresh, interpolation="nearest", cmap=colormap, norm=norm)
# display score in textbox
plt.text(0.5, 0.5, "Score: %d %%" % score,
         bbox=dict(facecolor='white', alpha=1.0),
         fontdict=None, withdash=False, fontsize=18,
         horizontalalignment="left", verticalalignment="top")
plt.title("threshold @ %.1f" % threshold)
cbar = plt.colorbar(img, cmap=colormap, ticks=[0.25, 0.75], fraction=0.046, pad=0.04)
cbar.ax.set_yticklabels(["not damaged", "damaged"], fontsize=12,
                        rotation="vertical", verticalalignment="center")

# save (and display) output image
savefig("outputFigure.eps", format='eps', bbox_inches="tight", transparent="True", dpi=100)
savefig("outputFigure.png", format='png', bbox_inches="tight", transparent="True", dpi=200)
# plt.tight_layout()
# plt.show()
