# ------------------------------------------------------------
# *** analyse ***
# Author: Pierre Chapuis
# Date: Dec. 2018
# ------------------------------------------------------------

# import libraries
# from matplotlib.patches import Rectangle
from PIL import Image
# import matplotlib
import tensorflow as tf
# import matplotlib.pyplot as plt
import numpy as np
import os


"""
Description:
Single inference based on trained model.
Author:
Pierre Chapuis

Manual:
The following arguments are required when calling this function:
    * x_grid/y_grid (int): block size (in pixel). The image will be cut into sub-blocks.
    * x_img/y_img (int): raw image size (in pixel).
    * size_img (int): reshaping sub_block into this new size (in pixel, the best is to apply square shape).
    * img_path (string): location of raw image.
    * graph_path (string): location of graph path of the model.
    * check_path (string): location of checkpoint path of the model.
"""


# !!! IF GPU IS NOT TENSORFLOW COMPATIBLE !!!
# Preventing TensorFlow to display a warning message if GPU is not compatible.
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class AnalyseWithModel(object):
    def __init__(self, x_grid, y_grid, x_img, y_img, size_img, img_path, graph_path, check_path):
        self.path_img = img_path
        self.path_graph = graph_path
        self.path_check = check_path
        self.grid_x = x_grid
        self.grid_y = y_grid
        self.img_x = x_img
        self.img_y = y_img
        self.size = size_img
        self.x_range = 0
        self.y_range = 0
        self.image_list = []
        self.img = []
        self.feature = []

    # load image
    def load_image(self):
        # define number of blocks in one row and one column
        self.x_range = round(self.img_x / self.grid_x)
        self.y_range = round(self.img_y / self.grid_y)

        self.img = Image.open(self.path_img)
        self.image_list = [self.img]

        return self.x_range, self.y_range, self.image_list, self.img

    # cut image into sub-blocks
    def cut_image(self):
        # initialize real_image list
        real_image = []

        # loop over sub-blocks
        for x_coord in range(self.x_range):
            for y_coord in range(self.y_range):
                # define sub-block coordinates
                bbox = (x_coord * self.grid_x, y_coord * self.grid_y,
                        x_coord * self.grid_x + self.grid_x, y_coord * self.grid_y + self.grid_y)
                # crop image into sub-block, resize into square and convert into array
                img_crop = self.image_list[0].crop(bbox)
                if self.grid_x != self.size[0] or self.grid_y != self.size[1]:
                    img_resized = img_crop.resize(self.size, Image.LANCZOS)
                    image_in_array = np.array(img_resized)
                else:
                    image_in_array = np.array(img_crop)

                # append image to real_image list
                real_image.append(image_in_array)

        self.feature = np.asarray(real_image)

    # perform single inference to predict sample quality
    def start_model(self):
        tf.reset_default_graph()
        with tf.Session() as sess:
            # Load meta graph and restore weights and biases
            saver = tf.train.import_meta_graph(self.path_graph)
            saver.restore(sess, tf.train.latest_checkpoint(self.path_check))
            # get graph of the session
            graph = sess.graph

            # recover placeholders
            x_ph = graph.get_tensor_by_name("x_ph:0")
            keep_prob = graph.get_tensor_by_name("keep_prob:0")
            if_training = graph.get_tensor_by_name("if_training:0")
            logits = graph.get_tensor_by_name("output:0")

            # define inputs to feed the program
            feed_dict = {x_ph: self.feature, keep_prob: 1.0, if_training: 'false'}

            # perform prediction
            prediction = sess.run([logits], feed_dict=feed_dict)[0]
            # print prediction
            print(prediction)

            # # set plot font size
            # matplotlib.rcParams.update({'font.size': 16})
            # # convert raw image (not converted into square) to array
            # img_in_array = np.array(self.img)
            # # initialize figure canvas
            # plt.figure(figsize=(10, 7))
            # # get current axis
            # current_axis = plt.gca()
            # # plot image
            # plt.imshow(img_in_array)
            # plt.xlabel('Row # (pixel)')
            # plt.ylabel('Column # (pixel)')
            #
            # # loop over the sub-blocks and incrementing the prediction vector to display tagged areas
            # k = 0
            # blue_count = 0
            # red_count = 0
            # total_sub_block = self.x_range * self.y_range
            # for x in range(self.x_range):
            #     for y in range(self.y_range):
            #         # for y in range(0, range_y):
            #         if prediction[k][0] >= 0.80:
            #             current_axis.add_patch(Rectangle((x * self.grid_x, y * self.grid_y),
            #                                              self.grid_x, self.grid_y,
            #                                              alpha=0.25, facecolor='blue', edgecolor='black'))
            #             k = k + 1
            #             blue_count = blue_count + 1
            #         elif prediction[k][0] <= 0.20:
            #             current_axis.add_patch(Rectangle((x * self.grid_x, y * self.grid_y),
            #                                              self.grid_x, self.grid_y,
            #                                              alpha=0.25, facecolor='red', edgecolor='black'))
            #             k = k + 1
            #             red_count = red_count + 1
            #         else:
            #             current_axis.add_patch(Rectangle((x * self.grid_x, y * self.grid_y),
            #                                              self.grid_x, self.grid_y,
            #                                              alpha=0.25, facecolor='gray', edgecolor='black'))
            #             k = k + 1
            #
            # # estimate coverage percentage
            # percent_blue = round(blue_count * 100 / total_sub_block)
            # percent_red = round(red_count * 100 / total_sub_block)
            #
            # # display coverage in title
            # plt.title('Cracked areas ~ %s %%' % percent_red)
            # # display figure
            # plt.show()

            # close current session
            sess.close()


if __name__ == "__main__":
    # define image size
    img_x = 44
    img_y = 44
    # define block size
    grid_x = 44
    grid_y = 44
    # new block size (compressed in square shape)
    size = 44, 44

    # image path
    path_img = '/Users/pierrechapuis/Documents/PyCharm/UBC/' \
               'image_analysis/GUI_imageClassification/imageStack/test.jpg'
    path_check = '/Users/pierrechapuis/Documents/PyCharm/UBC/' \
                 'image_analysis/GUI_imageClassification/saveModelHere/'

    path_graph = path_check + 'model.ckpt.meta'

    # start analyse
    my_class = AnalyseWithModel(grid_x, grid_y, img_x, img_y, size, path_img, path_graph, path_check)
    my_class.load_image()
    my_class.cut_image()
    my_class.start_model()
