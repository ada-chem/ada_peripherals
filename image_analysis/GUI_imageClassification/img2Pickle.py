# ------------------------------------------------------------
# *** img2Pickle ***
# Author: Pierre Chapuis
# Date: Dec. 2018
# ------------------------------------------------------------

# import libraries
from PIL import Image
from random import shuffle
import glob
import pickle
import numpy as np
import os


"""
Description:
Load images for each class, apply target, shuffle generated lists and save data set in a pickle file.
Author:
Pierre Chapuis

Manual:
By calling this function, one needs to specify:
    * img_path (string): disk location where raw images are stored.
    * name_file (string): name of the upcoming pickle file.
    * block_size (int): size of the raw images (in pixel).
    * ratio_file (int): ratio of stored images, tagged as train, valid and test sets.
    * target: set to None if Single label classification. The script classified images by itself.
              If Multi-label classification, one need to load targets manually here.
    * type_class (string: S, M): defining classification type as Single or Multi-label(s).
"""


class ImgIntoPkl(object):
    def __init__(self, img_path, name_file, block_size, ratio_file, target=None, type_class='S'):
        # import image stack
        self.filelist = {}
        self.path = img_path

        for i in range(0, len(self.path)):
            self.filelist[i] = glob.glob(os.path.join(self.path[i], '*.jpg'))

        self.file_name = name_file
        self.size = block_size
        self.ratio = ratio_file
        self.class_type = type_class
        self.targets_multi = target

    def concatenate_img(self):
        # initialize targets
        targets = []
        # fill target vector
        if self.class_type == 'S':
            # get identity matrix
            identity = [[1, 0], [0, 1]]
            for j in range(0, len(self.filelist)):
                targets.append([])
                for i in range(0, len(self.filelist[j])):
                    targets[j].append(identity[j])
        elif self.class_type == 'M':
            targets = self.targets_multi

        # shuffle file lists
        for i in range(len(self.path)):
            shuffle(self.filelist[i])

        # initialize features
        features = []
        # fill feature matrices
        for j in range(len(self.filelist)):
            features.append([])
            for i in range(len(self.filelist[j])):
                image = Image.open(self.filelist[j][i])
                if self.size != image.size:
                    img_resized = image.resize(self.size, Image.LANCZOS)
                else:
                    img_resized = image
                image_in_array = np.array(img_resized)
                features[j].append(image_in_array)

        # make zip list
        zip_list = []
        for j in range(len(self.filelist)):
            # append and lock list
            zip_list.append(list(zip(targets[j], features[j])))

        # combine zip_list[]
        full_zip = []
        for j in range(len(zip_list)):
            for i in range(len(zip_list[j])):
                full_zip.append(zip_list[j][i])
        # shuffle list
        shuffle(full_zip)

        # initialize train/valid/test sets
        train_set = []
        valid_set = []
        test_set = []

        # fill  train/valid/test sets by respecting given ratios
        for i in range(0,
                       round(len(full_zip)*self.ratio[0]/100)):
            train_set.append(full_zip[i])
        for i in range(round(len(full_zip)*self.ratio[0]/100),
                       round(len(full_zip)*self.ratio[0]/100) + round(len(full_zip)*self.ratio[1]/100)):
            valid_set.append(full_zip[i])
        for i in range(round(len(full_zip)*self.ratio[0]/100) + round(len(full_zip)*self.ratio[1]/100),
                       len(full_zip)):
            test_set.append(full_zip[i])

        # shuffle sets
        shuffle(train_set)
        shuffle(valid_set)
        shuffle(test_set)

        # unzip lists
        train_targets, train_features = zip(*train_set)
        valid_targets, valid_features = zip(*valid_set)
        test_targets, test_features = zip(*test_set)

        # pack train feature/target together
        train_set = [train_features, train_targets]
        # pack valid feature/target together
        valid_set = [valid_features, valid_targets]
        # pack test feature/target together
        test_set = [test_features, test_targets]
        # pack train/valid/test sets together
        all_set = [train_set, valid_set, test_set]

        # open the file for writing
        file_object = open(self.file_name, 'wb')
        # append data to pickle file
        pickle.dump(all_set, file_object)
        # close file
        file_object.close()


if __name__ == "__main__":
    # set file paths (for each image class)
    file_path = ['/Users/pierrechapuis/Documents/PyCharm/UBC/'
                 'image_analysis/filmQuality/Samples/crack_48–48_0/',
                 '/Users/pierrechapuis/Documents/PyCharm/UBC/'
                 'image_analysis/filmQuality/Samples/crack_48–48_1/']

    # define name for pickle file
    file_name = "test.pkl"

    # set classifier type ('S' := single, 'M' := multi)
    class_type = 'S'

    # define targets if multi-label classification
    target_multi = None

    # set block size
    size = 48, 48
    # set train, valid, test ratios
    ratio = 60, 20, 20

    my_class = ImgIntoPkl(file_path, file_name, size, ratio, target_multi, class_type)
    my_class.concatenate_img()
