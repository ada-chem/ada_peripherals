# ------------------------------------------------------------
# *** Gui_main ***
# Author: Pierre Chapuis
# Date: Dec. 2018
# ------------------------------------------------------------

# import libraries
from sys import platform
from tkinter import *
from tkinter import filedialog
from tkinter import StringVar
from PIL import Image
from PIL import ImageTk
from random import randint
import os
import glob
import tkinter as tk
import findWorkspace
import autoCropImg
import full_pre_processing
import img2Pickle
import trainModel
import analyse


class StdoutRedirector(object):
    def __init__(self, text_widget):
        self.text_space = text_widget

    def write(self, string):
        self.text_space.insert('end', string)
        self.text_space.see('end')

    def flush(self):
        pass


class GUI(Frame):

    # declare variables in __init__
    def __init__(self, root):
        Frame.__init__(self, root)
        # declare self variables
        self.filepath = None
        self.filelist = []
        self.select_folder = None
        self.width_image = 0
        self.height_image = 0
        self.raw_image_canvas = None
        self.rect_coord = None
        self.cur_x = 0
        self.cur_y = 0
        self.loc_rect = None
        self.var = StringVar()
        self.first_var = StringVar()
        self.second_var = StringVar()
        self.pickle_var = StringVar()
        self.pickle_path = StringVar()
        self.path_file = StringVar()
        self.path_folder = StringVar()
        self.model_path = StringVar()
        self.single_image = StringVar()
        self.count_image = StringVar()
        self.size_4_pickle = [0, 0]
        self.x_image_size_entry = Entry()
        self.y_image_size_entry = Entry()
        self.tr_ratio_entry = Entry()
        self.va_ratio_entry = Entry()
        self.te_ratio_entry = Entry()
        self.var.set('path/to/folder/here')
        self.path_file.set('path/to/images/here')
        self.path_folder.set('path/to/folder/here')
        self.first_var.set('path/to/first/folder/here')
        self.second_var.set('path/to/second/folder/here')
        self.pickle_var.set('path/to/save/location/here')
        self.pickle_path.set('Pickle/file/location/here')
        self.model_path.set('model/location/here')
        self.single_image.set('single/image/here')
        self.count_image.set('N/A')
        self.var_rect = StringVar()
        self.var_block_width = StringVar()
        self.var_block_height = StringVar()
        self.available_width = 0
        self.available_height = 0
        self.count_width = 0
        self.count_height = 0
        self.available_size = [[] for _ in range(2)]
        self.image_count = 0
        self.displayed_image = None
        self.displayed_image_resized = None
        self.displayed_image_2_tk = None
        self.targets = []
        self.text_box = ''

        self.hyper_param = [IntVar(), IntVar(), DoubleVar(), IntVar(), IntVar(),
                            IntVar(), IntVar(), DoubleVar(), DoubleVar(), IntVar(),
                            IntVar(), IntVar(), IntVar(), IntVar(), IntVar()]
        self.hyper_param[0].set(100000000)
        self.hyper_param[1].set(900)
        self.hyper_param[2].set(0.001)
        self.hyper_param[3].set(30)
        self.hyper_param[4].set(3)
        self.hyper_param[5].set(8)
        self.hyper_param[6].set(2)
        self.hyper_param[7].set(0.5)
        self.hyper_param[8].set(0.3)
        self.hyper_param[9].set(48)
        self.hyper_param[10].set(42)
        self.hyper_param[11].set(1024)
        self.hyper_param[12].set(768)
        self.hyper_param[13].set(44)
        self.hyper_param[14].set(44)

        # set cursor type when hovering GUI buttons (depends on OS ver.)
        if platform == "darwin":
            self.cursor_type = 'pointinghand'
        elif platform == "win32":
            self.cursor_type = 'hand2'

        # define left and right main frames
        self.leftFrame = Canvas(mainFrame, width=536, height=580, highlightthickness=0, bg='white')
        self.leftFrame.grid(row=0, column=0, padx=(2, 5), pady=2)
        self.leftFrame.grid_propagate(False)
        self.rightFrame = Canvas(mainFrame, width=536, height=580, highlightthickness=0, bg='white')
        self.rightFrame.grid(row=0, column=1, padx=(5, 2), pady=2)
        self.rightFrame.grid_propagate(False)

        self.button_frame = None
        self.tag_image_canvas = None

        self.rescale_x_ratio = 0
        self.rescale_y_ratio = 0
        self.image_number = 0
        self.shift = IntVar()

        # set path for raw data
        self.rawData_path = Label(upperFirstFrame, textvariable=self.var,
                                  fg='black', bg='white', anchor='w')
        self.rawData_path.grid(row=0, column=1, padx=2, pady=2, sticky='nesw')

        # declare every buttons
        self.open_button = Button(upperFirstFrame, text='OPEN...', cursor=self.cursor_type, fg='#017CFE',
                                  bg='#ECECEC', highlightthickness=0, command=self.open_raw_data)
        self.open_button.grid(row=0, column=2, padx=10, pady=2)

        self.selectArea_button = Button(upperSecondFrame, text='Select area', cursor='X_cursor',
                                        highlightthickness=0, fg='#017CFE', bg='#ECECEC',
                                        command=self.display_select_area)
        self.selectArea_button.grid(row=0, column=0, padx=(80, 80), pady=18)

        self.autoCrop_button = Button(upperSecondFrame, text='Auto crop', cursor='X_cursor',
                                      highlightthickness=0, fg='#017CFE', bg='#ECECEC',
                                      command=self.display_auto_crop)
        self.autoCrop_button.grid(row=0, column=1, padx=(0, 80), pady=18)

        self.tags_button = Button(upperSecondFrame, text='Tags', cursor=self.cursor_type,
                                  highlightthickness=0, command=self.tag_images)
        self.tags_button.grid(row=0, column=2, padx=(0, 80), pady=18)

        self.transfo_button = Button(upperSecondFrame, text='Transformation', cursor=self.cursor_type,
                                     highlightthickness=0, command=self.apply_transfo)
        self.transfo_button.grid(row=0, column=3, padx=(0, 80), pady=18)

        self.pickle_button = Button(upperSecondFrame, text='Save Pickle file', cursor=self.cursor_type,
                                    highlightthickness=0, command=self.save_pickle)
        self.pickle_button.grid(row=0, column=4, padx=(0, 80), pady=18)

        self.train_button = Button(upperSecondFrame, text='Train model', cursor=self.cursor_type,
                                   highlightthickness=0, command=self.train_model)
        self.train_button.grid(row=0, column=5, padx=(0, 80), pady=18)

        self.help_button = Button(upperFirstFrame, text='Help...',
                                  cursor=self.cursor_type, command=self.popup_help)
        self.help_button.grid(row=0, column=3, padx=(49, 3), pady=2)

        # declare variables for drawing rectangle of selection
        self.value_entry = None
        self.x = self.y = 0
        self.rect = None
        self.start_x = None
        self.start_y = None
        self.list_line = []

        # set menubar
        self.menubar = Menu(master)
        # set File menu in menubar
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Exit", command=self.quit_app,
                                  accelerator="Control+q")
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        # set Help menu in menubar
        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="Help...", command=self.popup_help,
                                  accelerator="Control+h")
        self.helpmenu.add_command(label="About...", command=self.popup_about,
                                  accelerator="Control+a")
        self.menubar.add_cascade(label="Help", menu=self.helpmenu)

        # enable shortcut from everywhere on the gui.
        master.bind_all('<Control-Key-a>', self.popup_about)
        master.bind_all('<Control-Key-h>', self.popup_help)
        master.bind_all('<Control-Key-q>', self.quit_app)

        # enable menubar
        master.config(menu=self.menubar)

    def load_single_image(self):
        file_var = StringVar(master, name='filevar')

        # open dialog
        filepath = filedialog.askopenfilename(initialdir=os.getcwd(), title='Select image')
        # check filename not None
        if filepath:
            # set file name
            file_var.set(filepath)
            # get file path
            self.filepath = file_var.get()
            self.single_image.set(self.filepath)

            master.update_idletasks()

    # linked to 'OPEN' button (if pickle == False), to open raw images
    def open_raw_data(self, pickle=False, folder_num=None):
        if not pickle:
            self.image_number = 0
            folder_var = StringVar(master, name='filevar')

            # open dialog
            foldername = filedialog.askdirectory(initialdir=os.getcwd(), title='Select folder')
            # check filename not None
            if foldername:
                # set file name
                folder_var.set(foldername)
                # get file path
                self.filepath = folder_var.get()
                self.var.set(self.filepath)
                self.selectArea_button.config(cursor=self.cursor_type)

                master.update_idletasks()

        # if pickle == True, def. used for defining paths in 'Save Pickle file' tab
        if pickle:
            folder_var = StringVar(master, name='filevar')

            # open dialog
            foldername = filedialog.askdirectory(initialdir=os.getcwd(), title='Select folder')
            # check filename not None
            if foldername:
                # set file name
                folder_var.set(foldername)
                # get file path
                self.filepath = folder_var.get()

                # folder_num := path number (1st dataset, 2nd dataset and save location)
                if folder_num == 1:
                    self.first_var.set(self.filepath)
                    self.filelist = glob.glob(os.path.join(self.filepath, '*.jpg'))
                    self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.jpeg')))
                    self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPG')))
                    self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPEG')))
                    image = Image.open(self.filelist[0])
                    self.size_4_pickle[0], self.size_4_pickle[1] = image.size
                    self.x_image_size_entry.insert(0, self.size_4_pickle[0])
                    self.y_image_size_entry.insert(0, self.size_4_pickle[1])
                elif folder_num == 2:
                    self.second_var.set(self.filepath)
                elif folder_num == 3:
                    self.pickle_var.set(os.path.join(self.filepath, 'dataset.pkl'))

                master.update_idletasks()

    def open_pickle(self, action):
        if action == 'load_pickle':
            file_var = StringVar(master, name='filevar')

            # open dialog
            filepath = filedialog.askopenfilename(initialdir=os.getcwd(), title='Select Pickle file')
            # check filename not None
            if filepath:
                # set file name
                file_var.set(filepath)
                # get file path
                self.filepath = file_var.get()
                self.pickle_path.set(self.filepath)

                master.update_idletasks()

        if action == 'model_loc':
            folder_var = StringVar(master, name='filevar')

            # open dialog
            folderpath = filedialog.askdirectory(initialdir=os.getcwd(), title='Select model location')
            # check filename not None
            if folderpath:
                # set file name
                folder_var.set(folderpath)
                # get file path
                self.filepath = folder_var.get()
                self.model_path.set(self.filepath)

                master.update_idletasks()

    # saving procedure for different tabs
    def save_in(self, which_tab, **kwargs):
        # tab 'Display Select Area'
        if which_tab == 'd_s_a':
            save_loc = StringVar()

            # open dialog
            loc_name = filedialog.askdirectory(initialdir=os.getcwd(), title='Select save location')
            # check filename not None
            if loc_name:
                # set file name
                save_loc.set(loc_name)
                # get file path
                save_loc = save_loc.get() + '/'

                if self.loc_rect:
                    launch_fw = findWorkspace.FindRect(self.loc_rect, self.filepath, save_loc)
                    launch_fw.select()
                elif not self.loc_rect:
                    self.loc_rect = (0, 0, self.width_image, self.height_image)
                    launch_fw = findWorkspace.FindRect(self.loc_rect, self.filepath, save_loc)
                    launch_fw.select()

        # tab 'Display Auto Crop'
        elif which_tab == 'd_a_c':
            save_loc = StringVar()

            # open dialog
            loc_name = filedialog.askdirectory(initialdir=os.getcwd(), title='Select save location')
            # check filename not None
            if loc_name:
                # set file name
                save_loc.set(loc_name)
                # get file path
                save_loc = save_loc.get()

                x_size = kwargs.get('x_size', None)
                y_size = kwargs.get('y_size', None)

                if (x_size or y_size) == 0:
                    x_size = self.available_width
                    y_size = self.available_height

                launch_aci = autoCropImg.AutoCropImg(self.available_width, self.available_height,
                                                     abs(self.loc_rect[2] - self.loc_rect[0]),
                                                     abs(self.loc_rect[3] - self.loc_rect[1]),
                                                     (x_size, y_size), self.filepath, save_loc)
                launch_aci.crop()

    # display next image (for 1st tab), resized to fit the window, but with real displayed coordinates
    def next_image(self):
        filelist = glob.glob(os.path.join(self.filepath, '*.jpg'))
        filelist.extend(glob.glob(os.path.join(self.filepath, '*.jpeg')))
        filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPG')))
        filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPEG')))
        if self.image_number < len(filelist) - 1:
            self.image_number = self.image_number + 1
        else:
            self.image_number = 0

        image = Image.open(self.filelist[self.image_number])
        if self.width_image > self.height_image:
            h = self.width_image / self.height_image
            image_resized = image.resize((535, round(535 / h)))
            self.rescale_x_ratio = self.width_image / 535
            self.rescale_y_ratio = self.height_image / round(535 / h)
        elif self.width_image < self.height_image:
            w = self.height_image / self.width_image
            image_resized = image.resize((round(580 / w), 580))
            self.rescale_x_ratio = self.width_image / round(580 / w)
            self.rescale_y_ratio = self.height_image / 580
        else:
            image_resized = image.resize((535, 535))
            self.rescale_x_ratio = self.width_image / 535
            self.rescale_y_ratio = self.height_image / 535
        image_2_tk = ImageTk.PhotoImage(image=image_resized)
        # add image to canvas
        self.raw_image_canvas.delete(image)
        self.raw_image_canvas.image = image_2_tk
        self.raw_image_canvas.create_image(image_resized.width / 2, image_resized.height / 2,
                                           anchor='center', image=image_2_tk)

    # display previous image (for 1st tab), resized to fit the window, but with real displayed coordinates
    def previous_image(self):
        filelist = glob.glob(os.path.join(self.filepath, '*.jpg'))
        filelist.extend(glob.glob(os.path.join(self.filepath, '*.jpeg')))
        filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPG')))
        filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPEG')))
        if self.image_number > 0:
            self.image_number = self.image_number - 1
        else:
            self.image_number = len(filelist) - 1

        image = Image.open(self.filelist[self.image_number])
        if self.width_image > self.height_image:
            h = self.width_image / self.height_image
            image_resized = image.resize((535, round(535 / h)))
            self.rescale_x_ratio = self.width_image / 535
            self.rescale_y_ratio = self.height_image / round(535 / h)
        elif self.width_image < self.height_image:
            w = self.height_image / self.width_image
            image_resized = image.resize((round(580 / w), 580))
            self.rescale_x_ratio = self.width_image / round(580 / w)
            self.rescale_y_ratio = self.height_image / 580
        else:
            image_resized = image.resize((535, 535))
            self.rescale_x_ratio = self.width_image / 535
            self.rescale_y_ratio = self.height_image / 535
        image_2_tk = ImageTk.PhotoImage(image=image_resized)
        # add image to canvas
        self.raw_image_canvas.delete(image)
        self.raw_image_canvas.image = image_2_tk
        self.raw_image_canvas.create_image(image_resized.width / 2, image_resized.height / 2,
                                           anchor='center', image=image_2_tk)

    # For 2nd tab, to display sub-areas when clicking on the arrow buttons (allow only possible integers)
    def define_sub_block_size(self, state, way):
        # state -> w: width, h: height
        # way -> l: left, r: right
        if state == 'w':
            if way == 'l':
                if self.count_width < len(self.available_size[0]):
                    self.count_width = self.count_width + 1
                    self.available_width = self.available_size[0][self.count_width - 1]
                    self.var_block_width.set(' Sub-area width = ' + str(self.available_width) + ' px')
                else:
                    self.count_width = 1
                    self.available_width = self.available_size[0][self.count_width - 1]
                    self.var_block_width.set(' Sub-area width = ' + str(self.available_width) + ' px')
            elif way == 'r':
                if self.count_width > 0:
                    self.count_width = self.count_width - 1
                    self.available_width = self.available_size[0][self.count_width - 1]
                    self.var_block_width.set(' Sub-area width = ' + str(self.available_width) + ' px')
                else:
                    self.count_width = len(self.available_size[0])
                    self.available_width = self.available_size[0][self.count_width - 1]
                    self.var_block_width.set(' Sub-area width = ' + str(self.available_width) + ' px')
        elif state == 'h':
            if way == 'l':
                if self.count_height < len(self.available_size[1]):
                    self.count_height = self.count_height + 1
                    self.available_height = self.available_size[1][self.count_height - 1]
                    self.var_block_height.set(' Sub-area height = ' + str(self.available_height) + ' px')
                else:
                    self.count_height = 1
                    self.available_height = self.available_size[1][self.count_height - 1]
                    self.var_block_height.set(' Sub-area height = ' + str(self.available_height) + ' px')
            elif way == 'r':
                if self.count_height > 0:
                    self.count_height = self.count_height - 1
                    self.available_height = self.available_size[1][self.count_height - 1]
                    self.var_block_height.set(' Sub-area height = ' + str(self.available_height) + ' px')
                else:
                    self.count_height = len(self.available_size[1])
                    self.available_height = self.available_size[1][self.count_height - 1]
                    self.var_block_height.set(' Sub-area height = ' + str(self.available_height) + ' px')

        # delete previous image to be replaced by the new one
        if self.list_line:
            for i in self.list_line:
                self.raw_image_canvas.delete(i)

        # available width/height correspond to selected area size
        if self.available_width != 0 and self.available_height != 0:
            if self.start_x < self.cur_x:
                x1 = self.start_x
                x2 = self.cur_x
            else:
                x2 = self.start_x
                x1 = self.cur_x

            # create sub-areas (lines to make a grid) horizontal borders
            for k in range(x1, x2, round(self.available_width / self.rescale_x_ratio)):
                y1 = self.start_y
                y2 = self.cur_y
                line_id = self.raw_image_canvas.create_line(k, y1, k, y2, fill='white', width=1.5)
                self.list_line.append(line_id)

            if self.start_y < self.cur_y:
                y1 = self.start_y
                y2 = self.cur_y
            else:
                y2 = self.start_y
                y1 = self.cur_y

            # create sub-areas (lines to make a grid) vertical borders
            for k in range(y1, y2, round(self.available_height / self.rescale_y_ratio)):
                x1 = self.start_x
                x2 = self.cur_x
                line_id = self.raw_image_canvas.create_line(x1, k, x2, k, fill='white', width=1.5)
                self.list_line.append(line_id)

    # allow tag processing
    def tag_process(self, status):
        # set folder path
        if status == 'selectFolder':
            foldername = filedialog.askdirectory(initialdir=os.getcwd(), title='Select folder')
            if foldername:
                # get file path
                self.select_folder = foldername
                self.path_folder.set(self.select_folder)

                master.update_idletasks()

        # if 1st image (before 1st tag (Yes, No))
        if status == '1stLoad':
            self.image_count = 0

            foldername = filedialog.askdirectory(initialdir=os.getcwd(), title='Select folder')
            if foldername:
                # get file path
                self.filepath = foldername
                self.path_file.set(self.filepath)

                master.update_idletasks()

            self.filelist = glob.glob(os.path.join(self.filepath, '*.jpg'))
            self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.jpeg')))
            self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPG')))
            self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPEG')))
            self.displayed_image = Image.open(self.filelist[self.image_count])

            self.count_image.set(str(self.image_count + 1) + ' / ' + str(len(self.filelist)))

        # if 'Yes' button hit
        if status == 'yes':
            if self.image_count + 1 <= len(self.filelist):
                self.targets.append([0, 1])
                if self.image_count + 1 < len(self.filelist):
                    self.image_count = self.image_count + 1
                    self.displayed_image = Image.open(self.filelist[self.image_count])

                self.count_image.set(str(self.image_count + 1) + ' / ' + str(len(self.filelist)))

        # if 'No' button hit
        if status == 'no':
            if self.image_count + 1 <= len(self.filelist):
                self.targets.append([1, 0])
                if self.image_count + 1 < len(self.filelist):
                    self.image_count = self.image_count + 1
                    self.displayed_image = Image.open(self.filelist[self.image_count])

                self.count_image.set(str(self.image_count + 1) + ' / ' + str(len(self.filelist)))

        # if 'Back' button hit
        if status == 'back':
            if self.image_count > 0:
                self.image_count = self.image_count - 1
                self.displayed_image = Image.open(self.filelist[self.image_count])
                self.targets = self.targets[:-1]

                self.count_image.set(str(self.image_count + 1) + ' / ' + str(len(self.filelist)))

        # if 'Save' button hit
        if status == 'save':
            foldername = filedialog.askdirectory(initialdir=os.getcwd(), title='Select folder')
            if foldername:
                # get file path
                save_path = foldername
                # create a folder named 'dataset', with children named '0' and '1'
                os.mkdir(os.path.join(save_path, 'dataset'))
                os.mkdir(os.path.join(save_path, 'dataset', '0'))
                os.mkdir(os.path.join(save_path, 'dataset', '1'))

                for i in range(0, len(self.filelist)):
                    image_2_save = Image.open(self.filelist[i])
                    if self.targets[i] == [0, 1]:
                        image_2_save.save(os.path.join(save_path, 'dataset', '0', str(i) + '_0.jpg'),
                                          optimize=True)
                    elif self.targets[i] == [1, 0]:
                        image_2_save.save(os.path.join(save_path, 'dataset', '1', str(i) + '_1.jpg'),
                                          optimize=True)

        if ((status == 'yes') or (status == 'no') or (status == 'back') or (status == '1stLoad')) \
                and self.image_count + 1 <= len(self.filelist):
            ratio = self.displayed_image.height / 300
            calc_width = int(self.displayed_image.width / ratio)
            calc_height = 300

            if calc_width > 726:
                calc_width = 726
                ratio = self.displayed_image.width / 726
                calc_height = int(self.displayed_image.height / ratio)

            self.displayed_image_resized = self.displayed_image.resize((calc_width, calc_height))
            self.displayed_image_2_tk = ImageTk.PhotoImage(image=self.displayed_image_resized)
            self.tag_image_canvas.image = self.displayed_image_2_tk
            self.tag_image_canvas.create_image(726 / 2, 300 / 2,
                                               anchor='center', image=self.displayed_image_2_tk)

    # allow image pre-processing
    @staticmethod
    def apply_img_proc(select_folder, degree, mirror, shift_value):
        if select_folder:
            launch_fpp = full_pre_processing.FullPreProcessing(select_folder + '/',
                                                               degree,
                                                               mirror,
                                                               shift_value)

            # if rotation is enabled
            if (degree[0] or degree[1] or degree[2]) != 0:
                launch_fpp.rotating()
            # if mirroring is enabled
            if mirror == 1 or mirror == 2:
                launch_fpp.mirroring()
            # if shifting is enabled
            if shift_value != 0:
                launch_fpp.shifting()

    # allow to save dataset into a Pickle file, readable directly by Machine Learning model
    @staticmethod
    def apply_save_pickle(location, file_name, size, ratio, target=None, type_class='S'):
        if location:
            launch_i2p = img2Pickle.ImgIntoPkl(location,
                                               file_name,
                                               size,
                                               ratio,
                                               target,
                                               type_class)

            launch_i2p.concatenate_img()

    @staticmethod
    def start_training(dataset_path, savedmodel_path, loss_threshold,
                       train_iters, batch_size, learning_rate,
                       mlp_size, filter_size, conv_hidden, stride, loss_min):
        if dataset_path != 'Pickle/file/location/here' and savedmodel_path != 'model/location/here':
            launch_tm = trainModel.TrainWithModel(dataset_path, savedmodel_path, loss_threshold,
                                                  train_iters, batch_size, learning_rate,
                                                  mlp_size, filter_size, conv_hidden, stride, loss_min)

            tr_f, tr_t, va_f, va_t, te_f, te_t = launch_tm.load_dataset()
            completed_program = launch_tm.start_model(tr_f, tr_t)

            if completed_program:
                print('Done!')

    @staticmethod
    def analyse(grid_x, grid_y, img_x, img_y, size, path_img, path_graph, path_check):
        if path_check != 'model/location/here' and path_img != 'single/image/here':
            launch_a = analyse.AnalyseWithModel(grid_x, grid_y, img_x, img_y, size,
                                                path_img, path_graph, path_check)

            launch_a.load_image()
            launch_a.cut_image()
            launch_a.start_model()

    # set GUI for 1st tab
    def display_select_area(self):
        if self.filepath:
            self.autoCrop_button.config(cursor=self.cursor_type)
            self.leftFrame.destroy()
            self.rightFrame.destroy()

            self.leftFrame = Canvas(mainFrame, width=536, height=580,
                                    highlightthickness=0, bg='white')
            self.leftFrame.grid(row=0, column=0, padx=(2, 5), pady=2)
            self.leftFrame.grid_propagate(False)
            self.rightFrame = Canvas(mainFrame, width=536, height=580,
                                     highlightthickness=0, bg='white')
            self.rightFrame.grid(row=0, column=1, padx=(5, 2), pady=2)
            self.rightFrame.grid_propagate(False)

            # import images
            self.filelist = glob.glob(os.path.join(self.filepath, '*.jpg'))
            self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.jpeg')))
            self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPG')))
            self.filelist.extend(glob.glob(os.path.join(self.filepath, '*.JPEG')))
            # open image
            image = Image.open(self.filelist[self.image_number])
            self.width_image, self.height_image = image.size

            equal_width = 530

            if self.width_image > self.height_image:
                h = self.width_image / self.height_image
                image_resized = image.resize((equal_width, round(equal_width / h)))
                self.rescale_x_ratio = self.width_image / equal_width
                self.rescale_y_ratio = self.height_image / round(equal_width / h)
            elif self.width_image < self.height_image:
                w = self.height_image / self.width_image
                image_resized = image.resize((round(580 / w), 580))
                self.rescale_x_ratio = self.width_image / round(580 / w)
                self.rescale_y_ratio = self.height_image / 580
            else:
                image_resized = image.resize((equal_width, equal_width))
                self.rescale_x_ratio = self.width_image / equal_width
                self.rescale_y_ratio = self.height_image / equal_width

            self.raw_image_canvas = Canvas(self.leftFrame,
                                           width=image_resized.width, height=image_resized.height,
                                           bg='white', cursor="cross")
            self.raw_image_canvas.pack(anchor='center')
            self.raw_image_canvas.bind("<ButtonPress-1>", self.on_button_press)
            self.raw_image_canvas.bind("<B1-Motion>", self.on_move_press)
            self.raw_image_canvas.bind("<ButtonRelease-1>", self.on_button_release)

            image_2_tk = ImageTk.PhotoImage(image=image_resized)
            # add image to canvas
            self.raw_image_canvas.image = image_2_tk
            self.raw_image_canvas.create_image(image_resized.width / 2, image_resized.height / 2,
                                               anchor='center', image=image_2_tk)

            previous_button = Button(self.rightFrame, text='<', command=self.previous_image,
                                     cursor=self.cursor_type)
            previous_button.grid(row=0, column=0, padx=(130, 0), pady=50, sticky='n')
            previous_button.config(font=("TkDefaultFont", 20))

            skip_text = Label(self.rightFrame, text='SKIP')
            skip_text.grid(row=0, column=1, padx=(0, 0), pady=50, sticky='n')
            skip_text.config(font=("TkDefaultFont", 20))

            next_button = Button(self.rightFrame, text='>', command=self.next_image,
                                 cursor=self.cursor_type)
            next_button.grid(row=0, column=2, padx=(0, 0), pady=50, sticky='n')
            next_button.config(font=("TkDefaultFont", 20))

            self.rect_coord = Label(self.rightFrame, textvariable=self.var_rect,
                                    fg='white', bg='black', anchor='w', width=20)
            self.rect_coord.config(font=("TkDefaultFont", 22))
            self.rect_coord.grid(row=1, column=0, columnspan=3, padx=(130, 0), pady=(50, 50))
            self.var_rect.set('  area size: N/A')

            select_area_button = Button(self.rightFrame, text='Save area', cursor=self.cursor_type,
                                        command=lambda: self.save_in('d_s_a'))
            select_area_button.config(font=("TkDefaultFont", 18))
            select_area_button.grid(row=2, column=0, columnspan=3, padx=(130, 0), pady=(50, 50))

    # display GUI for 2nd tab
    def display_auto_crop(self):
        if self.filepath and self.raw_image_canvas is not None:
            self.raw_image_canvas.config(cursor="arrow")
            self.rightFrame.destroy()

            self.var_block_width.set(' Sub-area width = 0 px')
            self.var_block_height.set(' Sub-area height = 0 px')

            self.rightFrame = Canvas(mainFrame, width=536, height=580,
                                     highlightthickness=0, bg='white')
            self.rightFrame.grid(row=0, column=1, padx=(5, 2), pady=2)
            self.rightFrame.grid_propagate(False)

            if self.cur_x != 0:
                for x_size in range(1, round(abs(self.cur_x - self.start_x) * self.rescale_x_ratio)):
                    if round(abs(self.cur_x - self.start_x) * self.rescale_x_ratio) % x_size == 0:
                        self.available_size[0].append(x_size)

                for y_size in range(1, round(abs(self.cur_y - self.start_y) * self.rescale_y_ratio)):
                    if round(abs(self.cur_y - self.start_y) * self.rescale_y_ratio) % y_size == 0:
                        self.available_size[1].append(y_size)
            else:
                self.available_size[0].append(self.width_image)
                self.available_size[1].append(self.height_image)

            sub_block_width_text = Label(self.rightFrame, textvariable=self.var_block_width,
                                         width=20, anchor='w')
            sub_block_width_text.grid(row=0, column=0, padx=(100, 0), pady=(50, 0), sticky='w')
            sub_block_width_text.config(font=("TkDefaultFont", 20))

            up_width_button = Button(self.rightFrame, text='<', cursor=self.cursor_type,
                                     command=lambda: self.define_sub_block_size('w', 'r'))
            up_width_button.grid(row=0, column=1, padx=(10, 0), pady=(50, 0), sticky='e')
            up_width_button.config(font=("TkDefaultFont", 20))

            down_width_button = Button(self.rightFrame, text='>', cursor=self.cursor_type,
                                       command=lambda: self.define_sub_block_size('w', 'l'))
            down_width_button.grid(row=0, column=2, padx=(0, 0), pady=(50, 0), sticky='e')
            down_width_button.config(font=("TkDefaultFont", 20))

            sub_block_height_text = Label(self.rightFrame, textvariable=self.var_block_height,
                                          width=20, anchor='w')
            sub_block_height_text.grid(row=1, column=0, padx=(100, 0), pady=(50, 0), sticky='w')
            sub_block_height_text.config(font=("TkDefaultFont", 20))

            up_height_button = Button(self.rightFrame, text='<', cursor=self.cursor_type,
                                      command=lambda: self.define_sub_block_size('h', 'r'))
            up_height_button.grid(row=1, column=1, padx=(10, 0), pady=(50, 0), sticky='e')
            up_height_button.config(font=("TkDefaultFont", 20))

            down_height_button = Button(self.rightFrame, text='>', cursor=self.cursor_type,
                                        command=lambda: self.define_sub_block_size('h', 'l'))
            down_height_button.grid(row=1, column=2, padx=(0, 0), pady=(50, 0), sticky='e')
            down_height_button.config(font=("TkDefaultFont", 20))

            sub_block_resize_text = Label(self.rightFrame, text='Resize sub-blocks:              x',
                                          width=25, anchor='w')
            sub_block_resize_text.grid(row=2, column=0, columnspan=3,
                                       padx=(130, 0), pady=(50, 50), sticky='w')
            sub_block_resize_text.config(font=("TkDefaultFont", 16))

            x_size_entry = Entry(self.rightFrame)
            x_size_entry.grid(row=2, column=0, columnspan=3,
                              padx=(276, 0),
                              pady=(50, 50),
                              sticky='w')
            x_size_entry.config(width=4)
            x_size_entry.insert(0, str(0))

            y_size_entry = Entry(self.rightFrame)
            y_size_entry.grid(row=2, column=0, columnspan=3,
                              padx=(340, 0),
                              pady=(50, 50),
                              sticky='w')
            y_size_entry.config(width=4)
            y_size_entry.insert(0, str(0))

            select_area_button = Button(self.rightFrame, text='Save sub-areas',
                                        cursor=self.cursor_type,
                                        command=lambda: self.save_in('d_a_c',
                                                                     x_size=int(x_size_entry.get()),
                                                                     y_size=int(y_size_entry.get())))
            select_area_button.config(font=("TkDefaultFont", 18))
            select_area_button.grid(row=3, column=0, columnspan=3, padx=(130, 0), pady=(50, 50))

    # display GUI for third tab
    def tag_images(self):
        self.raw_image_canvas = None
        self.leftFrame.destroy()
        self.rightFrame.destroy()

        self.leftFrame = Canvas(mainFrame, width=336, height=580,
                                bg='white', highlightthickness=0)
        self.leftFrame.grid(row=0, column=0, padx=(2, 5), pady=2)
        self.leftFrame.grid_propagate(False)

        self.rightFrame = Canvas(mainFrame, width=736, height=580,
                                 bg='white', highlightthickness=0)
        self.rightFrame.grid(row=0, column=1, padx=(5, 2), pady=2)
        self.rightFrame.grid_propagate(False)

        instruction_title = Label(self.leftFrame, text='Instructions:', fg='white',
                                  bg='black', highlightthickness=0)
        instruction_title.grid(row=0, column=0, padx=110, pady=20)
        instruction_title.config(font=("TkDefaultFont", 18))

        instruction_text = Text(self.leftFrame, height=15, width=45, highlightthickness=0)
        instruction_text.grid(row=1, column=0, padx=15, pady=20)
        instruction_text.insert(END, "1) Load images\n"
                                     "2) Classify them using Yes/No buttons.\n"
                                     "\n"
                                     "Back to previous images is possible\n"
                                     "using the 'Back' button.\n"
                                     "\n"
                                     "Then Save the dataset into a Pickle file,\n"
                                     "or go to the next tab 'Transformation'."
                                     "\n\n\n"
                                     "Shortcuts:\n"
                                     "\n"
                                     "      Yes:  Y  or  Right arrow -keys\n"
                                     "      No:   N  or  Left arrow -keys\n"
                                     "      Back: BackSpace -key\n")

        start_button = Button(self.rightFrame, text='Load images', anchor='center',
                              cursor=self.cursor_type,
                              command=lambda: self.tag_process('1stLoad'))
        start_button.grid(row=0, column=0, columnspan=3, padx=(10, 0), pady=(0, 0), sticky='w')
        start_button.config(font=("TkDefaultFont", 20))

        image_path = Label(self.rightFrame, textvariable=self.path_file,
                           fg='#017CFE', bg='white', width=75, anchor='w')
        image_path.grid(row=1, column=0, padx=(10, 0), pady=5, sticky='nesw')

        self.tag_image_canvas = Canvas(self.rightFrame, width=726, height=300)
        self.tag_image_canvas.grid(row=2, column=0, columnspan=3,
                                   padx=(5, 5), pady=(10, 0), sticky='news')

        self.button_frame = Frame(self.rightFrame, width=742, height=190, bg='#017CFE')
        self.button_frame.grid(row=3, column=0, columnspan=3, padx=(0, 0), pady=(10, 0), sticky='news')
        self.button_frame.grid_propagate(False)

        no_button = Button(self.button_frame, text='No', width=4, height=2, cursor=self.cursor_type,
                           command=lambda: self.tag_process('no'))
        no_button.grid(row=0, column=0, padx=(190, 0), pady=(30, 0))
        no_button.config(font=("TkDefaultFont", 30))

        count = Label(self.button_frame, textvariable=self.count_image,
                      fg='white', bg='black', width=10, anchor='center')
        count.grid(row=0, column=1, padx=(50, 0), pady=(30, 0))

        yes_button = Button(self.button_frame, text='Yes', width=4, height=2, cursor=self.cursor_type,
                            command=lambda: self.tag_process('yes'))
        yes_button.grid(row=0, column=2, padx=(50, 0), pady=(30, 0))
        yes_button.config(font=("TkDefaultFont", 30))

        back_button = Button(self.button_frame, text='Back', width=4, cursor=self.cursor_type,
                             command=lambda: self.tag_process('back'))
        back_button.grid(row=1, column=0, columnspan=2, padx=(130, 0), pady=(30, 0))
        back_button.config(font=("TkDefaultFont", 20))

        stop_button = Button(self.button_frame, text='Save', width=4, cursor=self.cursor_type,
                             command=lambda: self.tag_process('save'))
        stop_button.grid(row=1, column=1, columnspan=2, padx=(110, 0), pady=(30, 0))
        stop_button.config(font=("TkDefaultFont", 20))

        # enable shortcuts
        master.bind('n', lambda event: self.tag_process(status='no'))
        master.bind('y', lambda event: self.tag_process(status='yes'))
        master.bind('<Left>', lambda event: self.tag_process(status='no'))
        master.bind('<Right>', lambda event: self.tag_process(status='yes'))
        master.bind('<BackSpace>', lambda event: self.tag_process(status='back'))

    # display GUI for fourth tab
    def apply_transfo(self):
        self.raw_image_canvas = None
        self.leftFrame.destroy()
        self.rightFrame.destroy()

        self.leftFrame = Canvas(mainFrame, width=352, height=580,
                                bg='white', highlightthickness=0)
        self.leftFrame.grid(row=0, column=0, padx=(2, 5), pady=2)
        self.leftFrame.grid_propagate(False)

        self.rightFrame = Canvas(mainFrame, width=720, height=580,
                                 bg='white', highlightthickness=0)
        self.rightFrame.grid(row=0, column=1, padx=(5, 2), pady=2)
        self.rightFrame.grid_propagate(False)

        instruction_title = Label(self.leftFrame, text='Instructions:', fg='white',
                                  bg='black', highlightthickness=0)
        instruction_title.grid(row=0, column=0, padx=110, pady=20)
        instruction_title.config(font=("TkDefaultFont", 18))

        instruction_text = Text(self.leftFrame, height=15, width=48, highlightthickness=0)
        instruction_text.grid(row=1, column=0, padx=15, pady=20)
        instruction_text.insert(END, "1) Load dataset.\n"
                                     "2) Select transformation types.\n"
                                     "3) Apply transformations.\n"
                                     "\n"
                                     "Tips:\n"
                                     "Be sure to transform square-shaped images.\n"
                                     "(height = width)\n")

        select_button = Button(self.rightFrame, text='Select folder', anchor='center',
                               cursor=self.cursor_type,
                               command=lambda: self.tag_process('selectFolder'))
        select_button.grid(row=0, column=0, columnspan=3, padx=(10, 0), pady=(0, 0), sticky='w')
        select_button.config(font=("TkDefaultFont", 20))

        folder_path = Label(self.rightFrame, textvariable=self.path_folder,
                            fg='#017CFE', bg='white', width=75, anchor='w')
        folder_path.grid(row=1, column=0, padx=(10, 0), pady=5, sticky='nesw')

        Label(self.rightFrame, text="Rotations:", font=(None, 16)).grid(row=2,
                                                                        padx=(50, 0),
                                                                        pady=(30, 0),
                                                                        sticky=W)
        rot_90 = IntVar()
        Checkbutton(self.rightFrame, text="90°", variable=rot_90,
                    onvalue=90, offvalue=0).grid(row=3,
                                                 padx=(200, 0),
                                                 pady=(5, 0),
                                                 sticky=W)
        rot_180 = IntVar()
        Checkbutton(self.rightFrame, text="180°", variable=rot_180,
                    onvalue=180, offvalue=0).grid(row=4,
                                                  padx=(200, 0),
                                                  pady=(5, 0),
                                                  sticky=W)
        rot_270 = IntVar()
        Checkbutton(self.rightFrame, text="270°", variable=rot_270,
                    onvalue=270, offvalue=0).grid(row=5,
                                                  padx=(200, 0),
                                                  pady=(5, 0),
                                                  sticky=W)

        Label(self.rightFrame, text="Mirroring:", font=(None, 16)).grid(row=6,
                                                                        padx=(50, 0),
                                                                        pady=(50, 0),
                                                                        sticky=W)
        mirror_type = IntVar()
        Radiobutton(self.rightFrame, text="None", variable=mirror_type, value=0).grid(row=7,
                                                                                      padx=(200, 0),
                                                                                      pady=(5, 0),
                                                                                      sticky=W)
        Radiobutton(self.rightFrame, text="Horizontal", variable=mirror_type, value=1).grid(row=8,
                                                                                            padx=(200, 0),
                                                                                            pady=(5, 0),
                                                                                            sticky=W)
        Radiobutton(self.rightFrame, text="Vertical", variable=mirror_type, value=2).grid(row=9,
                                                                                          padx=(200, 0),
                                                                                          pady=(5, 0),
                                                                                          sticky=W)

        Label(self.rightFrame, text="Shifting:", font=(None, 16)).grid(row=10,
                                                                       padx=(50, 0),
                                                                       pady=(50, 0),
                                                                       sticky=W)

        Checkbutton(self.rightFrame, text="Enter value here: ", variable=self.shift,
                    command=self.activate_check).grid(row=11,
                                                      padx=(200, 0),
                                                      pady=(5, 0),
                                                      sticky=W)

        self.value_entry = Entry(self.rightFrame)
        self.value_entry.grid(row=11,
                              padx=(350, 0),
                              pady=(5, 0),
                              sticky='w')
        self.value_entry.config(width=4)
        self.value_entry.insert(0, str(0))
        self.value_entry.config(state=DISABLED)

        apply_button = Button(self.rightFrame, text='Apply', width=5, cursor=self.cursor_type,
                              command=lambda: self.apply_img_proc(select_folder=self.select_folder,
                                                                  degree=[rot_90.get(),
                                                                          rot_180.get(),
                                                                          rot_270.get()],
                                                                  mirror=mirror_type.get(),
                                                                  shift_value=int(self.value_entry.get())))
        apply_button.grid(row=12, padx=(70, 0), pady=(45, 0))
        apply_button.config(font=("TkDefaultFont", 25))

    # display GUI for fifth tab
    def save_pickle(self):
        self.raw_image_canvas = None
        self.leftFrame.destroy()
        self.rightFrame.destroy()

        self.leftFrame = Canvas(mainFrame, width=372, height=580,
                                bg='white', highlightthickness=0)
        self.leftFrame.grid(row=0, column=0, padx=(2, 5), pady=2)
        self.leftFrame.grid_propagate(False)

        self.rightFrame = Canvas(mainFrame, width=700, height=580,
                                 bg='white', highlightthickness=0)
        self.rightFrame.grid(row=0, column=1, padx=(5, 2), pady=2)
        self.rightFrame.grid_propagate(False)

        instruction_title = Label(self.leftFrame, text='Instructions:', fg='white',
                                  bg='black', highlightthickness=0)
        instruction_title.grid(row=0, column=0, padx=110, pady=20)
        instruction_title.config(font=("TkDefaultFont", 18))

        instruction_text = Text(self.leftFrame, height=15, width=48, highlightthickness=0)
        instruction_text.grid(row=1, column=0, padx=15, pady=20)
        instruction_text.insert(END, "1) Load 1st dataset.\n"
                                     "2) Load 2nd dataset.\n"
                                     "3) Select save location.\n"
                                     "4) Keep default size or set custom value.\n"
                                     "5) Set train/valid/test set ratios.\n"
                                     "\n"
                                     "Tips:\n"
                                     "  ° Be sure to use square-shaped images.\n"
                                     "    (height = width)\n\n"
                                     "  ° Be sure to keep ratio sum to 100%.\n")

        first_folder_button = Button(self.rightFrame, text='Select 1st dataset:', anchor='center',
                                     cursor=self.cursor_type, width=14,
                                     command=lambda: self.open_raw_data(True, 1))
        first_folder_button.grid(row=0, column=0, padx=(10, 0), pady=(0, 0), sticky='w')
        first_folder_button.config(font=("TkDefaultFont", 20))

        first_folder_path = Label(self.rightFrame, textvariable=self.first_var,
                                  fg='#017CFE', bg='white', width=75, anchor='w')
        first_folder_path.grid(row=1, column=0, padx=(10, 0), pady=5, sticky='nesw')

        second_folder_button = Button(self.rightFrame, text='Select 2nd dataset:', anchor='center',
                                      cursor=self.cursor_type, width=14,
                                      command=lambda: self.open_raw_data(True, 2))
        second_folder_button.grid(row=2, column=0, padx=(10, 0), pady=(15, 0), sticky='w')
        second_folder_button.config(font=("TkDefaultFont", 20))

        second_folder_path = Label(self.rightFrame, textvariable=self.second_var,
                                   fg='#017CFE', bg='white', width=75, anchor='w')
        second_folder_path.grid(row=3, column=0, padx=(10, 0), pady=5, sticky='nesw')

        save_folder_button = Button(self.rightFrame, text='Select save location:', anchor='center',
                                    cursor=self.cursor_type, width=14,
                                    command=lambda: self.open_raw_data(True, 3))
        save_folder_button.grid(row=4, column=0, padx=(10, 0), pady=(15, 0), sticky='w')
        save_folder_button.config(font=("TkDefaultFont", 20))

        save_folder_path = Label(self.rightFrame, textvariable=self.pickle_var,
                                 fg='#017CFE', bg='white', width=75, anchor='w')
        save_folder_path.grid(row=5, column=0, padx=(10, 0), pady=5, sticky='nesw')

        image_size_label = Label(self.rightFrame, text='Image size:                  x',
                                 width=25, anchor='w')
        image_size_label.grid(row=6, column=0, padx=(100, 0), pady=(20, 0), sticky='w')
        image_size_label.config(font=("TkDefaultFont", 16))

        self.x_image_size_entry = Entry(self.rightFrame)
        self.x_image_size_entry.grid(row=6, column=0,
                                     padx=(206, 0),
                                     pady=(20, 0),
                                     sticky='w')
        self.x_image_size_entry.config(width=4)

        self.y_image_size_entry = Entry(self.rightFrame)
        self.y_image_size_entry.grid(row=6, column=0,
                                     padx=(270, 0),
                                     pady=(20, 0),
                                     sticky='w')
        self.y_image_size_entry.config(width=4)

        ratio_label = Label(self.rightFrame, text='train/valid/test ratio:'
                                                  '             % /'
                                                  '             % /'
                                                  '             %',
                            width=35, anchor='w')
        ratio_label.grid(row=7, column=0, padx=(100, 0), pady=(20, 0), sticky='w')
        ratio_label.config(font=("TkDefaultFont", 16))

        self.tr_ratio_entry = Entry(self.rightFrame)
        self.tr_ratio_entry.grid(row=7, column=0,
                                 padx=(259, 0),
                                 pady=(20, 0),
                                 sticky='w')
        self.tr_ratio_entry.insert(0, 60)
        self.tr_ratio_entry.config(width=3)
        self.va_ratio_entry = Entry(self.rightFrame)
        self.va_ratio_entry.grid(row=7, column=0,
                                 padx=(336, 0),
                                 pady=(20, 0),
                                 sticky='w')
        self.va_ratio_entry.config(width=3)
        self.va_ratio_entry.insert(0, 20)
        self.te_ratio_entry = Entry(self.rightFrame)
        self.te_ratio_entry.grid(row=7, column=0,
                                 padx=(413, 0),
                                 pady=(20, 0),
                                 sticky='w')
        self.te_ratio_entry.config(width=3)
        self.te_ratio_entry.insert(0, 20)

        save_button = Button(self.rightFrame, text='Save Pickle file', anchor='center',
                             cursor=self.cursor_type, width=12,
                             command=lambda: self.apply_save_pickle([str(self.first_var.get()),
                                                                     str(self.second_var.get())],
                                                                    str(self.pickle_var.get()),
                                                                    [int(self.x_image_size_entry.get()),
                                                                     int(self.y_image_size_entry.get())],
                                                                    [int(self.tr_ratio_entry.get()),
                                                                     int(self.va_ratio_entry.get()),
                                                                     int(self.te_ratio_entry.get())],
                                                                    target=None,
                                                                    type_class='S'))
        save_button.grid(row=8, column=0, padx=(250, 0), pady=(75, 0), sticky='w')
        save_button.config(font=("TkDefaultFont", 20))

    # display GUI for fourth tab
    def train_model(self):
        self.raw_image_canvas = None
        self.leftFrame.destroy()
        self.rightFrame.destroy()

        self.leftFrame = Canvas(mainFrame, width=372, height=580,
                                bg='white', highlightthickness=0)
        self.leftFrame.grid(row=0, column=0, padx=(2, 5), pady=2)
        self.leftFrame.grid_propagate(False)

        self.rightFrame = Canvas(mainFrame, width=700, height=580,
                                 bg='white', highlightthickness=0)
        self.rightFrame.grid(row=0, column=1, padx=(5, 2), pady=2)
        self.rightFrame.grid_propagate(False)

        instruction_title = Label(self.leftFrame, text='Instructions:', fg='white',
                                  bg='black', highlightthickness=0)
        instruction_title.grid(row=0, column=0, padx=110, pady=20)
        instruction_title.config(font=("TkDefaultFont", 18))

        instruction_text = Text(self.leftFrame, height=15, width=48, highlightthickness=0)
        instruction_text.grid(row=1, column=0, padx=15, pady=20)
        instruction_text.insert(END, "1) Load Pickle file.\n"
                                     "2) Select saved model location.\n"
                                     "3) Set hyper parameters:\n"
                                     "4) Start training (results in console).\n"
                                     "5) Open single image\n"
                                     "6) Set size parameters\n"
                                     "7) Run Analyse (results in console)\n"
                                     "\n"
                                     "Tips:\n"
                                     "  ° Console at the bottom will be updated\n"
                                     "    only when the stop condition is reached.\n"
                                     "    (see 'stop below' entry)\n\n"
                                     "  ° mlp := Multi Layer Perceptrons\n\n"
                                     "  ° conv. hidden := Convolutional Hidden Layer\n\n")

        load_dataset_button = Button(self.rightFrame, text='Select Pickle file:', anchor='center',
                                     cursor=self.cursor_type, width=14,
                                     command=lambda: self.open_pickle('load_pickle'))
        load_dataset_button.grid(row=0, column=0, padx=(10, 0), pady=(0, 0), sticky='w')
        load_dataset_button.config(font=("TkDefaultFont", 20))

        dataset_path = Label(self.rightFrame, textvariable=self.pickle_path,
                             fg='#017CFE', bg='white', width=75, anchor='w')
        dataset_path.grid(row=1, column=0, padx=(10, 0), pady=5, sticky='nesw')

        save_loc_button = Button(self.rightFrame, text='Select model path:', anchor='center',
                                 cursor=self.cursor_type, width=14,
                                 command=lambda: self.open_pickle('model_loc'))
        save_loc_button.grid(row=2, column=0, padx=(10, 0), pady=(15, 0), sticky='w')
        save_loc_button.config(font=("TkDefaultFont", 20))

        model_path = Label(self.rightFrame, textvariable=self.model_path,
                           fg='#017CFE', bg='white', width=75, anchor='w')
        model_path.grid(row=3, column=0, padx=(10, 0), pady=5, sticky='nesw')

        ratio_label_1 = Label(self.rightFrame, text='train iterations:'
                                                    '                                      batch size:',
                              width=45, anchor='w')
        ratio_label_1.grid(row=4, column=0, padx=(50, 0), pady=(20, 0), sticky='w')
        ratio_label_1.config(font=("TkDefaultFont", 16))

        ratio_label_2 = Label(self.rightFrame, text='learning rate:'
                                                    '                                         mlp size:',
                              width=45, anchor='w')
        ratio_label_2.grid(row=5, column=0, padx=(50, 0), pady=(10, 0), sticky='w')
        ratio_label_2.config(font=("TkDefaultFont", 16))

        ratio_label_3 = Label(self.rightFrame, text='filter size:'
                                                    '                                               '
                                                    'conv. hidden:',
                              width=45, anchor='w')
        ratio_label_3.grid(row=6, column=0, padx=(50, 0), pady=(25, 0), sticky='w')
        ratio_label_3.config(font=("TkDefaultFont", 16))

        ratio_label_4 = Label(self.rightFrame, text='stride:'
                                                    '                                                     '
                                                    'loss threshold:                     stop below:',
                              width=55, anchor='w')
        ratio_label_4.grid(row=7, column=0, padx=(50, 0), pady=(10, 0), sticky='w')
        ratio_label_4.config(font=("TkDefaultFont", 16))

        hp_0_entry = Entry(self.rightFrame, textvariable=self.hyper_param[0])
        hp_0_entry.grid(row=4, column=0,
                        padx=(165, 0),
                        pady=(20, 0),
                        sticky='w')
        hp_0_entry.config(width=10)

        hp_1_entry = Entry(self.rightFrame, textvariable=self.hyper_param[1])
        hp_1_entry.grid(row=4, column=0,
                        padx=(432, 0),
                        pady=(20, 0),
                        sticky='w')
        hp_1_entry.config(width=6)

        hp_2_entry = Entry(self.rightFrame, textvariable=self.hyper_param[2])
        hp_2_entry.grid(row=5, column=0,
                        padx=(165, 0),
                        pady=(10, 0),
                        sticky='w')
        hp_2_entry.config(width=6)

        hp_3_entry = Entry(self.rightFrame, textvariable=self.hyper_param[3])
        hp_3_entry.grid(row=5, column=0,
                        padx=(432, 0),
                        pady=(10, 0),
                        sticky='w')
        hp_3_entry.config(width=4)

        hp_4_entry = Entry(self.rightFrame, textvariable=self.hyper_param[4])
        hp_4_entry.grid(row=6, column=0,
                        padx=(165, 0),
                        pady=(20, 0),
                        sticky='w')
        hp_4_entry.config(width=3)

        hp_5_entry = Entry(self.rightFrame, textvariable=self.hyper_param[5])
        hp_5_entry.grid(row=6, column=0,
                        padx=(432, 0),
                        pady=(20, 0),
                        sticky='w')
        hp_5_entry.config(width=3)

        hp_6_entry = Entry(self.rightFrame, textvariable=self.hyper_param[6])
        hp_6_entry.grid(row=7, column=0,
                        padx=(165, 0),
                        pady=(10, 0),
                        sticky='w')
        hp_6_entry.config(width=3)

        hp_7_entry = Entry(self.rightFrame, textvariable=self.hyper_param[7])
        hp_7_entry.grid(row=7, column=0,
                        padx=(432, 0),
                        pady=(10, 0),
                        sticky='w')
        hp_7_entry.config(width=7)

        hp_8_entry = Entry(self.rightFrame, textvariable=self.hyper_param[8])
        hp_8_entry.grid(row=7, column=0,
                        padx=(606, 0),
                        pady=(10, 0),
                        sticky='w')
        hp_8_entry.config(width=7)

        start_button = Button(self.rightFrame, text='Start training', anchor='center',
                              cursor=self.cursor_type, width=12,
                              command=lambda: self.start_training(str(self.pickle_path.get()),
                                                                  str(self.model_path.get()),
                                                                  float(hp_7_entry.get()),
                                                                  int(hp_0_entry.get()),
                                                                  int(hp_1_entry.get()),
                                                                  float(hp_2_entry.get()),
                                                                  int(hp_3_entry.get()),
                                                                  int(hp_4_entry.get()),
                                                                  int(hp_5_entry.get()),
                                                                  int(hp_6_entry.get()),
                                                                  float(hp_8_entry.get())))

        start_button.grid(row=8, column=0, padx=(250, 0), pady=(15, 0), sticky='w')
        start_button.config(font=("TkDefaultFont", 20))

        load_image_button = Button(self.rightFrame, text='Load image to analyse:', anchor='center',
                                   cursor=self.cursor_type, width=16,
                                   command=self.load_single_image)
        load_image_button.grid(row=9, column=0, padx=(10, 0), pady=(20, 0), sticky='w')
        load_image_button.config(font=("TkDefaultFont", 20))

        image_path = Label(self.rightFrame, textvariable=self.single_image,
                           fg='#017CFE', bg='white', width=75, anchor='w')
        image_path.grid(row=10, column=0, padx=(10, 0), pady=5, sticky='nesw')

        text_1 = Label(self.rightFrame, text='grid size:                         x',
                       width=18, anchor='w')
        text_1.grid(row=11, column=0, padx=(50, 0), pady=(10, 0), sticky='w')
        text_1.config(font=("TkDefaultFont", 16))

        text_2 = Label(self.rightFrame, text='image size:                     x',
                       width=18, anchor='w')
        text_2.grid(row=12, column=0, padx=(50, 0), pady=(10, 0), sticky='w')
        text_2.config(font=("TkDefaultFont", 16))

        text_3 = Label(self.rightFrame, text='resizement:                     x',
                       width=18, anchor='w')
        text_3.grid(row=13, column=0, padx=(50, 0), pady=(10, 0), sticky='w')
        text_3.config(font=("TkDefaultFont", 16))

        hp_9_entry = Entry(self.rightFrame, textvariable=self.hyper_param[9])
        hp_9_entry.grid(row=11, column=0,
                        padx=(165, 0),
                        pady=(10, 0),
                        sticky='w')
        hp_9_entry.config(width=4)

        hp_10_entry = Entry(self.rightFrame, textvariable=self.hyper_param[10])
        hp_10_entry.grid(row=11, column=0,
                         padx=(240, 0),
                         pady=(10, 0),
                         sticky='w')
        hp_10_entry.config(width=4)

        hp_11_entry = Entry(self.rightFrame, textvariable=self.hyper_param[11])
        hp_11_entry.grid(row=12, column=0,
                         padx=(165, 0),
                         pady=(10, 0),
                         sticky='w')
        hp_11_entry.config(width=4)

        hp_12_entry = Entry(self.rightFrame, textvariable=self.hyper_param[12])
        hp_12_entry.grid(row=12, column=0,
                         padx=(240, 0),
                         pady=(10, 0),
                         sticky='w')
        hp_12_entry.config(width=4)

        hp_13_entry = Entry(self.rightFrame, textvariable=self.hyper_param[13])
        hp_13_entry.grid(row=13, column=0,
                         padx=(165, 0),
                         pady=(10, 0),
                         sticky='w')
        hp_13_entry.config(width=4)

        hp_14_entry = Entry(self.rightFrame, textvariable=self.hyper_param[14])
        hp_14_entry.grid(row=13, column=0,
                         padx=(240, 0),
                         pady=(10, 0),
                         sticky='w')
        hp_14_entry.config(width=4)

        analyse_button = Button(self.rightFrame, text='Start analysis', anchor='center',
                                cursor=self.cursor_type, width=12,
                                command=lambda: self.analyse(int(hp_9_entry.get()),
                                                             int(hp_10_entry.get()),
                                                             int(hp_11_entry.get()),
                                                             int(hp_12_entry.get()),
                                                             (int(hp_13_entry.get()),
                                                              int(hp_14_entry.get())),
                                                             str(self.single_image.get()),
                                                             os.path.join(str(self.model_path.get()),
                                                                          'model.ckpt.meta'),
                                                             str(self.model_path.get())))

        analyse_button.grid(row=12, column=0, padx=(400, 0), pady=(10, 0), sticky='w')
        analyse_button.config(font=("TkDefaultFont", 20))

        self.rightFrame.create_line(50, 374, 650, 374, fill='#017CFE', dash=(4, 4))
        # self.text_box = Text(self.rightFrame, wrap='word', height=11, width=50)
        # self.text_box.grid(row=9, column=0, columnspan=2, sticky='news', padx=(5, 5), pady=(15, 0))
        # self.text_box.config(highlightthickness=0, borderwidth=4, relief="ridge")
        # sys.stdout = StdoutRedirector(self.text_box)

    # define mouse event when drawing the rectangle:
    # when mouse is pressed
    def on_button_press(self, event):
        if self.rect:
            self.raw_image_canvas.delete(self.rect)
            self.x = self.y = 0
            self.rect = None
            self.start_x = None
            self.start_y = None

        # save mouse drag start position
        self.start_x = event.x
        self.start_y = event.y

        # create rectangle if not yet exist
        # if not self.rect:
        self.rect = self.raw_image_canvas.create_rectangle(self.x, self.y, 1, 1,
                                                           dash=(6, 6), outline='red', width='2')

    # when mouse is moved
    def on_move_press(self, event):
        self.cur_x, self.cur_y = (event.x, event.y)

        # expand rectangle as you drag the mouse
        self.raw_image_canvas.coords(self.rect, self.start_x, self.start_y, self.cur_x, self.cur_y)
        self.var_rect.set('  area size: ' +
                          str(round(abs(self.cur_x - self.start_x) * self.rescale_x_ratio)) +
                          ' x ' + str(round(abs(self.cur_y - self.start_y) * self.rescale_y_ratio)))
        master.update_idletasks()

        self.loc_rect = (round(abs(self.start_x) * self.rescale_x_ratio),
                         round(abs(self.start_y) * self.rescale_y_ratio),
                         round(abs(self.cur_x) * self.rescale_x_ratio),
                         round(abs(self.cur_y) * self.rescale_y_ratio))

    # when mouse is released
    def on_button_release(self, event):
        pass

    # activate/de-activate entry if checked/not checked
    def activate_check(self):
        if self.shift.get() == 1:
            self.value_entry.config(state=NORMAL)
        elif self.shift.get() == 0:
            self.value_entry.config(state=DISABLED)

        return

    # display popup window (About...)
    @staticmethod
    def popup_about(event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

        # define window for 'About...'
        popup = tk.Tk()
        # set 'About...' window properties (size and location: centered)
        popup_width = 220
        popup_height = 170
        popup_x = (master.winfo_screenwidth() / 2) - (popup_width / 2)
        popup_y = (master.winfo_screenheight() / 2) - (popup_height / 2)
        popup.geometry('%dx%d+%d+%d' % (popup_width, popup_height, popup_x, popup_y))
        popup.minsize(popup_width, popup_height)
        popup.maxsize(popup_width, popup_height)
        # set title and font
        popup.wm_title("About...")
        label_font = ('times', 14, 'bold')

        # add text to window
        text_about = tk.Label(popup,
                              text="Image Classification software\n"
                                   "(ver_2.0)\n\n"
                                   "Author: Pierre Chapuis\n"
                                   "Date: 2018/12/21")
        text_about.pack(side="top", fill="both", expand='yes')
        text_about.config(bg='black', fg='white')
        text_about.config(font=label_font)
        text_about.config(height=3, width=30)

        # add button 'OK'
        close_button = tk.Button(popup, text="Close", font=('times', 16, 'bold'),
                                 width=10, command=popup.destroy)
        close_button.pack(side="top", expand='yes')

        popup.mainloop()

    # display popup window (Help...)
    @staticmethod
    def popup_help(event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

            # define window for 'About...'
            popup = tk.Tk()
            # set 'About...' window properties (size and location: centered)
            popup_width = 700
            popup_height = 720
            popup_x = (master.winfo_screenwidth() / 2) - (popup_width / 2)
            popup_y = (master.winfo_screenheight() / 2) - (popup_height / 2)
            popup.geometry('%dx%d+%d+%d' % (popup_width, popup_height, popup_x, popup_y))
            popup.geometry('%dx%d+%d+%d' % (popup_width, popup_height, popup_x, popup_y))
            popup.minsize(popup_width, popup_height)
            popup.maxsize(popup_width, popup_height)
            # set title and font
            popup.wm_title("Help...")
            label_font = ('times', 14, 'bold')

            # add text to window
            text_infos = tk.Label(popup,
                                  text="\n"
                                       "° Introduction:\n"
                                       "    This program allows users to treat, classify and generate "
                                       "a dataset to be used later on a\n    Machine Learning model. "
                                       "Different tabs and buttons are accessible and described "
                                       "below.\n\n"
                                       "° Main header buttons:\n"
                                       "    * OPEN: open a folder containing raw images (jpg). "
                                       "When done, you can reach the first 2 tabs,\n      named "
                                       "\'Select area\' and \'Auto crop\'.\n\n"
                                       "    * HELP: open this window!\n\n"
                                       "° Tab explanations:\n"
                                       "    * Select area: after open a folder with \'OPEN\' button, use "
                                       "this window to keep the entire\n    image or select only a "
                                       "specific area which will be applied to all images in such "
                                       "folder.\n    Left-click on an image location then drag the "
                                       "rectangle to a select the desired area. Finally \n    release "
                                       "the mouse button. One can also use \'SKIP\' button to view next or"
                                       " previous images, and\n    save specific area into a folder"
                                       "using \'Save\' button. To crop the selection into sub-areas, "
                                       "go to\n    next tab named \'Auto crop\'.\n\n"
                                       "    * Auto crop: if a specific area is selected before in "
                                       "\'Select area\' tab, one can define sub-areas\n    using the "
                                       "arrow buttons, then set both width and height of them "
                                       "(resizement before saving\n    if needed). Finally one can save "
                                       "sub-areas into a folder using \'Save sub-areas\' button.\n\n"
                                       "    * Tags: this tab works independently and is used to classify "
                                       "images. See instruction on the left\n    pane for more "
                                       "information.\n\n"
                                       "    * Transformation: this tab works independently and is used to "
                                       "apply geometrical transformation\n    on images. This is useful to"
                                       " increase drastically a dataset. See instruction on the left "
                                       "pane for\n    more information.\n\n"
                                       "    * Save Pickle file:  this tab works independently and is used "
                                       "to save a classified dataset into\n    a Pickle file, which will "
                                       "be directly readable by the Machine Learning model. See "
                                       "instruction\n    on the left pane for more information.\n\n"
                                       "    *Train model:  this tab works independently and is used "
                                       "to train and save a model based on the\n    corresponding "
                                       "dataset. See instruction on the left pane for more information."
                                       "\n\n"
                                       "° Ada logo: never click on it! ;-)")
            text_infos.pack(side="top")
            text_infos.config(bg='white', fg='black', justify='left')
            text_infos.config(font=label_font)
            text_infos.config(height=0, width=80)

            # add button 'OK'
            close_button = tk.Button(popup, text="Close", font=('times', 16, 'bold'),
                                     width=10, command=popup.destroy)
            close_button.pack(side="bottom", expand='yes')

            popup.mainloop()

    # to quit the app. properly
    @staticmethod
    def quit_app(event=None):
        # prevent 'event not used' warning
        if event is None:
            pass
        sys.exit(0)


# if someone click on the Ada logo xD
def on_click_logo(event=None):
    # prevent 'event not used' warning
    if event is None:
        pass

    ada_click = ImageTk.PhotoImage(Image.open("Ada_Black.jpg"))
    ada_black = tk.Label(upperSecondFrame, image=ada_click, height=46, width=46)
    ada_black.grid(row=0, column=6, padx=(0, 0), pady=0)
    upperSecondFrame.after(250, ada_black.destroy)

    popup = tk.Tk()

    popup_width = 220
    popup_height = 170
    popup_x = (master.winfo_screenwidth() / 2) - (popup_width / 2)
    popup_y = (master.winfo_screenheight() / 2) - (popup_height / 2)
    popup.geometry('%dx%d+%d+%d' % (popup_width, popup_height, popup_x, popup_y))
    popup.minsize(popup_width, popup_height)
    popup.maxsize(popup_width, popup_height)
    # set title and font
    popup.wm_title("Ada said...")
    label_font = ('times', 14, 'bold')

    text = ["Don't click on me like that!", "Stop that, please!", "Are you kidding me?",
            "Let me alone...", "What the Hell?", "I can't believe it...",
            "May the force be with me...", "Why this frenetic clicks\non myself?",
            "I'm angry now...", "I'll propagate Skynet virus\nif you continue! (joke)"]
    text_num = randint(0, len(text) - 1)

    # add text to window
    text_ada = tk.Label(popup, text=text[text_num])
    text_ada.pack(side="top", fill="both", expand='yes')
    text_ada.config(bg='#017CFE', fg='white')
    text_ada.config(font=label_font)
    text_ada.config(height=3, width=30)

    # add button 'OK'
    close_button = tk.Button(popup, text="I'm sorry bud...", font=('times', 14),
                             width=15, height=2, command=popup.destroy)
    close_button.pack(side="top", expand='yes')

    popup.mainloop()


# create tkinter Gui
master = Tk()
# set titles
master.title('Image Classification')

# create first upper frame
upperFirstFrame = Frame(master, width=1100, height=50, bg='gray')
upperFirstFrame.grid(row=0, column=0, padx=2, pady=2)

# create title for raw data path
rawData_title = Label(upperFirstFrame, text='Open raw data:', fg='white', bg='gray')
rawData_title.grid(row=0, column=0, padx=2, pady=2)

upperFirstFrame.grid_columnconfigure(1, minsize=792)

# create second upper frame
upperSecondFrame = Canvas(master, width=1084, height=50, bg='gray')
upperSecondFrame.grid(row=1, column=0, padx=2, pady=2)
upperSecondFrame.grid_propagate(False)
upperSecondFrame.create_rectangle(73, 11, 315, 45, width=0, fill='black')
upperSecondFrame.create_rectangle(381, 11, 432, 45, width=0, fill='black')
upperSecondFrame.create_rectangle(498, 11, 616, 45, width=0, fill='black')
upperSecondFrame.create_rectangle(682, 11, 800, 45, width=0, fill='black')
upperSecondFrame.create_rectangle(866, 11, 962, 45, width=0, fill='black')
upperSecondFrame.create_line(35, 28, 1030, 28, arrow=tk.LAST,
                             fill='white', width=5, arrowshape=(30, 40, 10))

# add Ada logo
ada_image = ImageTk.PhotoImage(Image.open("Ada_Logo.jpg"))
ada_logo = tk.Label(upperSecondFrame, image=ada_image, height=46, width=46)
ada_logo.grid(row=0, column=6, padx=(0, 0), pady=0)
ada_logo.bind("<ButtonPress-1>", on_click_logo)

# define main frame
mainFrame = Frame(master, width=1100, height=600, bg='gray')
mainFrame.grid(row=2, column=0, padx=20, pady=20)

# define master GUI options
master.width = 1126
master.height = 718
master.minsize(master.width, master.height)
master.maxsize(master.width, master.height)
master.grid_propagate(False)

# launch MyGui
app = GUI(master)
master.mainloop()
