# ------------------------------------------------------------
# *** full_pre_processing ***
# Author: Pierre Chapuis
# Date: Dec. 2018
# ------------------------------------------------------------

# import libraries
from PIL import Image
import numpy as np
import glob
import os


"""
Description:
Load, rotate, mirror, shift and save images.
Author:
Pierre Chapuis

Manual:
This function needs 4 arguments when called:
    * path (string): location of raw images
    * angle (int: 90, 180, 270): specifying one or multiple angle(s) of rotation.
    * mirror (string: H, V): specifying mirror type (Horizontal OR Vertical).
    * shift (int): specifying shift value (in pixel).
      Applied from left to right (+ inverse) and from top to bottom (+ inverse), with boundary conditions.
"""


class FullPreProcessing(object):
    def __init__(self, path, angle, mirror, shift):
        self.path = path
        self.degree = angle
        self.direction = mirror
        self.value = shift

    def rotating(self):
        # import images
        filelist = glob.glob(os.path.join(self.path, '*.jpg'))

        # loop over each image
        for i in range(0, len(filelist)):
            # open current image
            image_list = Image.open(filelist[i])

            # loop over each rotation step
            for j in self.degree:
                if j != 0:
                    # rotate current image
                    image_list.rotate(j).save(os.path.join(self.path, 'rot_%d_%d.jpg' % (i, j)))

        return

    def mirroring(self):
        # import image stack
        filelist = glob.glob(os.path.join(self.path, '*.jpg'))

        # loop over each image
        for i in range(0, len(filelist)):
            # open current image
            image = Image.open(filelist[i])

            # mirror and save current image
            if self.direction == 1:
                image.transpose(Image.FLIP_TOP_BOTTOM).save(os.path.join(self.path, 'mirror_H_%d.jpg' % i))
            elif self.direction == 2:
                image.transpose(Image.FLIP_LEFT_RIGHT).save(os.path.join(self.path, 'mirror_V_%d.jpg' % i))

        return

    def shifting(self):
        # import images
        filelist = glob.glob(os.path.join(self.path, '*.jpg'))

        for i in range(0, len(filelist)):
            # open current image
            image = Image.open(filelist[i])

            arr = np.array(image)

            arr_up = np.roll(arr, -self.value, axis=0)
            arr_down = np.roll(arr, self.value, axis=0)
            arr_right = np.roll(arr, self.value, axis=1)
            arr_left = np.roll(arr, -self.value, axis=1)

            img_shift_up = Image.fromarray(arr_up, 'RGB')
            img_shift_up.save(os.path.join(self.path, 'shift_up_%d.jpg' % i))
            img_shift_down = Image.fromarray(arr_down, 'RGB')
            img_shift_down.save(os.path.join(self.path, 'shift_down_%d.jpg' % i))
            img_shift_left = Image.fromarray(arr_left, 'RGB')
            img_shift_left.save(os.path.join(self.path, 'shift_left_%d.jpg' % i))
            img_shift_right = Image.fromarray(arr_right, 'RGB')
            img_shift_right.save(os.path.join(self.path, 'shift_right_%d.jpg' % i))

        return


if __name__ == "__main__":
    # define file path
    file_path = '/Users/pierrechapuis/Documents/PyCharm/UBC/image_analysis/' \
                'filmQuality/Samples/graphite_films/1_new/'

    # set parameters
    # define rotation angles
    degree = [90, 180, 270]
    # set mirror type ('H' for 'Horizontal', or 'V' for 'Vertical')
    mirror_type = 1
    # set shift value
    shift_value = 4

    # enable processes individually (rotations, mirror, shifts: 'True' or 'False')
    processes = [True, False, False]

    # start full pre-processing
    my_class = FullPreProcessing(file_path, degree, mirror_type, shift_value)
    if processes[0]:
        my_class.rotating()
    if processes[1]:
        my_class.mirroring()
    if processes[2]:
        my_class.shifting()
