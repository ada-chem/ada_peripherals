# ------------------------------------------------------------
# *** setup ***
# Author: Pierre Chapuis
# Date: Dec. 2018
# ------------------------------------------------------------

# IMPORTANT: to compile the app from the Terminal -> $ python3 setup.py py2app

# import libraries
from setuptools import setup

# main program
APP = ['Gui_main.py']
# data files used in the program
DATA_FILES = ['Ada_Logo.jpg', 'Ada_Black.jpg']
# set options
OPTIONS = {'argv_emulation': True}

# setup for compiling standalone app.
setup(
    app=APP,
    py_modules=['findWorkspace', 'autoCropImg', 'full_pre_processing', 'img2Pickle', 'trainModel'],
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app'],
)
