from PIL import Image
import glob

# import images
filelist = glob.glob('/Users/pierrechapuis/Documents/PyCharm/UBC'
                     '/image_analysis/filmQuality/crackHomogen_48x48/crack_homogen/*.jpg')


# define rotation angles
degree = [90, 180, 270]
# degree = [180]

# loop over each image
for i in range(0, len(filelist)):
    # open current image with crack(s)
    image_crack = Image.open(filelist[i])

    # loop over each rotation step
    for j in degree:
        # rotate current image
        image_crack.rotate(j).save('/Users/pierrechapuis/Documents/PyCharm/UBC/image_analysis'
                                   '/filmQuality/crackHomogen_48x48/crack_homogen/'
                                   'rot_%d_%d.jpg' % (i, j))

    # open current image without crack
    # image_nocrack = Image.open(filelist_nocrack[i])

    # loop over each rotation step
    # for j in degree:
    # rotate current image
    #    image_nocrack.rotate(j).save('/Users/pierrechapuis/Documents/PyCharm/UBC/'
    #                                 'image_analysis/filmQuality/crack_48–48_0/rot_%d_%d.jpg' % (i, j))
