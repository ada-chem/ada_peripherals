# import packages
from matplotlib.patches import Rectangle
from PIL import Image
import matplotlib
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import os

# set CPU calculation for TensorFlow
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class AnalyseWithModel(object):
    def __init__(self, x_grid, y_grid, x_img, y_img, path):
        self.path = path
        self.grid_x = x_grid
        self.grid_y = y_grid
        self.img_x = x_img
        self.img_y = y_img
        self.x_range = 0
        self.y_range = 0
        self.image_list = []
        self.img = []
        self.feature = []

        self.load_image()
        self.cut_image()
        self.start_model()

    # load image
    def load_image(self):
        # define number of blocks in one row and one column
        self.x_range = round(self.img_x / self.grid_x)
        self.y_range = round(self.img_y / self.grid_y)

        self.img = Image.open(self.path)
        self.image_list = [self.img]

        return self.x_range, self.y_range, self.image_list, self.img

    # cut image into sub-blocks
    def cut_image(self):
        # initialize real_image list
        real_image = []

        # loop over sub-blocks
        for x_coord in range(self.x_range):
            for y_coord in range(self.y_range):
                # define sub-block coordinates
                bbox = (x_coord * self.grid_x, y_coord * self.grid_y,
                        x_coord * self.grid_x + self.grid_x, y_coord * self.grid_y + self.grid_y)
                # crop image into sub-block, resize into square and convert into array
                img_crop = self.image_list[0].crop(bbox)
                img_resized = img_crop.resize(size, Image.ANTIALIAS)
                image_in_array = np.array(img_resized)
                # append image to real_image list
                real_image.append(image_in_array)

        self.feature = np.asarray(real_image)
        return self.feature

    # perform single inference to predict sample quality
    def start_model(self):
        tf.reset_default_graph()
        with tf.Session() as sess:
            # Load meta graph and restore weights and biases
            saver = tf.train.import_meta_graph('/Users/pierrechapuis/Documents/PyCharm/UBC/'
                                               'image_analysis/filmQuality/crackHomogen_model'
                                               '/model.ckpt.meta')
            saver.restore(sess, tf.train.latest_checkpoint('/Users/pierrechapuis/Documents/PyCharm/'
                                                           'UBC/image_analysis/filmQuality/'
                                                           'crackHomogen_model/'))
            # get graph of the session
            graph = sess.graph

            # recover placeholders
            x_ph = graph.get_tensor_by_name("x_ph:0")
            keep_prob = graph.get_tensor_by_name("keep_prob:0")
            if_training = graph.get_tensor_by_name("if_training:0")
            logits = graph.get_tensor_by_name("output:0")

            # define inputs to feed the program
            feed_dict = {x_ph: self.feature, keep_prob: 1.0, if_training: 'false'}

            # perform prediction
            prediction = sess.run([logits], feed_dict=feed_dict)[0]
            # print prediction
            print(prediction)

            # set plot font size
            matplotlib.rcParams.update({'font.size': 16})
            # convert raw image (not converted into square) to array
            img_in_array = np.array(self.img)
            # initialize figure canvas
            plt.figure(figsize=(10, 7))
            # get current axis
            current_axis = plt.gca()
            # plot image
            plt.imshow(img_in_array)
            plt.xlabel('Row # (pixel)')
            plt.ylabel('Column # (pixel)')

            # loop over the sub-blocks and incrementing the prediction vector to display tagged areas
            k = 0
            # for x in range(0, range_x):
            for x in range(self.x_range):
                # for y in range(0, range_y):
                for y in range(self.y_range):
                    # Coordinates of top left triangles
                    top_left_triangle = [[x * self.grid_x, y * self.grid_y],
                                         [x * self.grid_x + self.grid_x, y * self.grid_y],
                                         [x * self.grid_x, y * self.grid_y + self.grid_y]]
                    # Coordinates of top left triangles
                    bottom_right_triangle = [[x * self.grid_x + self.grid_x, y * self.grid_y],
                                             [x * self.grid_x + self.grid_x,
                                              y * self.grid_y + self.grid_y],
                                             [x * self.grid_x, y * self.grid_y + self.grid_y]]

                    # Check predictions about crack identification
                    if prediction[k][1] >= 0.95:
                        current_axis.add_patch(plt.Polygon(top_left_triangle,
                                                           alpha=0.25, facecolor='red',
                                                           edgecolor=None, linewidth=0.0))
                    elif prediction[k][1] <= 0.05:
                        current_axis.add_patch(plt.Polygon(top_left_triangle,
                                                           alpha=0.25, facecolor='blue',
                                                           edgecolor=None, linewidth=0.0))
                    else:
                        current_axis.add_patch(plt.Polygon(top_left_triangle,
                                                           alpha=0.25, facecolor='gray',
                                                           edgecolor=None, linewidth=0.0))

                    # Check predictions about homogeneity evaluation
                    if prediction[k][3] >= 0.95:
                        current_axis.add_patch(plt.Polygon(bottom_right_triangle,
                                                           alpha=0.25, facecolor='blue',
                                                           edgecolor=None, linewidth=0.0))
                        k = k + 1
                    elif prediction[k][3] <= 0.05:
                        current_axis.add_patch(plt.Polygon(bottom_right_triangle,
                                                           alpha=0.25, facecolor='red',
                                                           edgecolor=None, linewidth=0.0))
                        k = k + 1
                    else:
                        current_axis.add_patch(plt.Polygon(bottom_right_triangle,
                                                           alpha=0.25, facecolor='gray',
                                                           edgecolor=None, linewidth=0.0))
                        k = k + 1

                    current_axis.add_patch(Rectangle((x * self.grid_x, y * self.grid_y),
                                                     self.grid_x, self.grid_y,
                                                     linewidth=1.5, alpha=1,
                                                     facecolor='none', edgecolor='black'))
            # display figure
            plt.show()

            # close current session
            sess.close()


if __name__ == "__main__":
    # define image size
    img_x = 1024
    img_y = 768

    # define block size
    grid_x = 64
    grid_y = 48

    # new block size (compressed in square shape)
    size = 48, 48

    # image path
    path_img = '/Users/pierrechapuis/Documents/PyCharm/UBC/image_analysis/filmQuality/' \
               'imageStacks/YC20180709_cleanedGlass_B-5/CHTP_autosave__489_ch1.jpg'

    # start model
    AnalyseWithModel(grid_x, grid_y, img_x, img_y, path_img)
