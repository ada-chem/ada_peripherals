import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button


fig, ax = plt.subplots()
ax.minorticks_on()
fig.canvas.set_window_title('Interactive graph')
plt.subplots_adjust(bottom=0.26)

E = np.arange(0.1, 50.0, 0.05)
S_0 = 55.0
Ue_0 = 0.0
Eg = 986.0
Ue = Ue_0
S = S_0

sigma = (S / (np.sqrt(E * (E + Ue)))) * np.exp(-np.sqrt(Eg / (E + Ue)))
sigma_0 = (S_0 / (np.sqrt(E * E))) * np.exp(-np.sqrt(Eg / E))
l, = plt.semilogy(E, sigma, lw=2, color=(0.875, 0.423, 0.148))
l_0, = plt.semilogy(E, sigma_0, lw=2, dashes=[3, 1], color=(0.270, 0.453, 0.633))
plt.title(r'$\sigma(E,U_{e})=\dfrac{1}{\sqrt{E(E+U_{e})}}.'
          r'\exp{\left(-\sqrt{\frac{E_{g}}{(E+U_{e})}}\right)}.S(E)$'
          '\n')

plt.xlabel('E (keV)')
plt.ylabel('$\sigma$ (barns)')
plt.xlim((0.0, 15.0))
plt.ylim((1e-8, 1e-2))
plt.legend([l, l_0], ['interactive curve', 'S = 55 keV barns, Ue = 0 keV'], loc=4)

ax_color = 'lightgoldenrodyellow'
ax_Ue = plt.axes([0.20, 0.05, 0.60, 0.03], facecolor=ax_color)
ax_S = plt.axes([0.20, 0.10, 0.60, 0.03], facecolor=ax_color)

Ue_slider = Slider(ax_Ue, 'Ue (keV)', 0.0, 4.0, valinit=Ue_0)
S_slider = Slider(ax_S, 'S (keV barns)', 35.0, 75.0, valinit=S_0)


def update(val):
    S = S_slider.val
    Ue = Ue_slider.val
    l.set_ydata((S / (np.sqrt(E * (E + Ue)))) * np.exp(-np.sqrt(Eg / (E + Ue))))
    fig.canvas.draw_idle()


Ue_slider.on_changed(update)
S_slider.on_changed(update)

reset_ax = plt.axes([0.9, 0.035, 0.08, 0.04])
button = Button(reset_ax, 'Reset', color=ax_color, hovercolor='0.975')


def reset(event):
    Ue_slider.reset()
    S_slider.reset()


button.on_clicked(reset)

plt.show()
