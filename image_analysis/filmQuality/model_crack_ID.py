#!/usr/bin/env python
from random import shuffle
from random import randrange

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import seaborn as sns
# import edward as ed
import pickle
import sys


def generator(arrays, batch_size):
    starts = [0] * len(arrays)
    while True:
        batches = []
        for i, array in enumerate(arrays):
            start = starts[i]
            stop = start + batch_size
            diff = stop - array.shape[0]
            if diff <= 0:
                batch = array[start: stop]
                starts[i] += batch_size
            else:
                batch = np.concatenate([array[start:], array[:diff]])
                starts[i] = diff
            batches.append(batch)
        yield batches


# set color palettes
sns.set_context('paper', font_scale=1.5, rc={'lines.linewidth': 2})
sns.set_style('ticks')
colors = sns.color_palette('RdYlBu', 256)
set_colors = sns.color_palette('deep', 7)
train_set_color = set_colors[0]
valid_set_color = set_colors[1]

# ===============================================================================

# set hyper parameters
TRAIN_ITERS = 10 ** 8
BATCH_SIZE = 540
LEARNING_RATE = 1e-3
MLP_SIZE = 30

# ===============================================================================

# load dataset
dataset_name = '/Users/pierrechapuis/Documents/PyCharm/UBC/' \
               'image_analysis/filmQuality/crackDataSet.pkl'

# open images
fileObject = open(dataset_name, 'rb')
train_set, valid_set, test_set = pickle.load(fileObject)

# fill train/valid/test sets
train_features = train_set[0]
train_targets = train_set[1]
valid_features = valid_set[0]
valid_targets = valid_set[1]
test_features = test_set[0]
test_targets = test_set[1]

# zip and shuffle list (then unzip)
zip_list = list(zip(train_targets, train_features))
shuffle(zip_list)
train_targets, train_features = zip(*zip_list)

# convert into numpy arrays
train_features = np.asarray(train_features)
train_targets = np.asarray(train_targets)
valid_features = np.asarray(valid_features)
valid_targets = np.asarray(valid_targets)
test_features = np.asarray(test_features)
test_targets = np.asarray(test_targets)

# mean_features, std_features = np.mean(train_features, axis=0), np.std(train_features, axis=0)
# std_features = np.where(std_features == 0, 1., std_features)
# train_features = (train_features - mean_features) / std_features
# valid_features = (valid_features - mean_features) / std_features
# test_features = (test_features - mean_features) / std_features

# ===============================================================================

# set CNN parameters
FILTER_SIZE = 3
CONV_HIDDEN = 8
STRIDE = 2


# ===============================================================================


def conv_activation(x_):
    x_ = tf.nn.leaky_relu(x_, 0.2)
    return x_


def mlp_activation(x_):
    x_ = tf.nn.leaky_relu(x_, 0.2)
    return x_


# ===============================================================================

# build the model

# define placeholders
x_ph = tf.placeholder(tf.float32,
                      [None, train_features.shape[1],
                       train_features.shape[2], train_features.shape[3]], name='x_ph')
y_ph = tf.placeholder(tf.float32, [None, train_targets.shape[1]], name='y_ph')

if_training = tf.placeholder(tf.bool, name='if_training')

keep_prob = tf.placeholder(tf.float32, name='keep_prob')
SIGMA = 0.01

# initialize weights and biases (for CNN)
weights_conv_0 = tf.get_variable('weights_conv_0', [FILTER_SIZE, FILTER_SIZE, train_features.shape[-1],
                                                    CONV_HIDDEN * train_features.shape[-1]],
                                 initializer=tf.random_normal_initializer(0., SIGMA))
bias_conv_0 = tf.get_variable('bias_conv_0', [CONV_HIDDEN * train_features.shape[-1]],
                              initializer=tf.random_normal_initializer(0., SIGMA))

weights_conv_1 = tf.get_variable('weights_conv_1', [FILTER_SIZE, FILTER_SIZE,
                                                    CONV_HIDDEN * train_features.shape[-1],
                                                    2 * CONV_HIDDEN * train_features.shape[-1]],
                                 initializer=tf.random_normal_initializer(0., SIGMA))
bias_conv_1 = tf.get_variable('bias_conv_1', [2 * CONV_HIDDEN * train_features.shape[-1]],
                              initializer=tf.random_normal_initializer(0., SIGMA))

# build CNN
conv_0 = tf.nn.conv2d(x_ph, weights_conv_0, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
proc_0 = conv_activation(conv_0 + bias_conv_0)
pool_0 = tf.nn.max_pool(proc_0, [1, FILTER_SIZE, FILTER_SIZE, 1],
                        [1, FILTER_SIZE, FILTER_SIZE, 1], padding='VALID')

conv_1 = tf.nn.conv2d(pool_0, weights_conv_1, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
conv_1 = tf.nn.l2_normalize(conv_1, epsilon=1e-8)
proc_1 = conv_activation(conv_1 + bias_conv_1)
pool_1 = tf.nn.max_pool(proc_1, [1, FILTER_SIZE, FILTER_SIZE, 1],
                        [1, FILTER_SIZE, FILTER_SIZE, 1], padding='VALID')

# convert 2D images into 1D vector
cnn_output = tf.contrib.layers.flatten(pool_1)

# initialize weights and biases (for MLP)
weights_0 = tf.get_variable('weights_0', [cnn_output.get_shape()[1], MLP_SIZE],
                            initializer=tf.random_normal_initializer(0., SIGMA))
bias_0 = tf.get_variable('bias_0', [MLP_SIZE], initializer=tf.random_normal_initializer(0., SIGMA))

weights_1 = tf.get_variable('weights_1', [MLP_SIZE, MLP_SIZE],
                            initializer=tf.random_normal_initializer(0., SIGMA))
bias_1 = tf.get_variable('bias_1', [MLP_SIZE],
                         initializer=tf.random_normal_initializer(0., SIGMA))

weights_2 = tf.get_variable('weights_2', [MLP_SIZE, MLP_SIZE],
                            initializer=tf.random_normal_initializer(0., SIGMA))
bias_2 = tf.get_variable('bias_2', [MLP_SIZE],
                         initializer=tf.random_normal_initializer(0., SIGMA))

weights_3 = tf.get_variable('weights_3', [MLP_SIZE, train_targets.shape[1]],
                            initializer=tf.random_normal_initializer(0., SIGMA))
bias_3 = tf.get_variable('bias_3', [train_targets.shape[1]],
                         initializer=tf.random_normal_initializer(0., SIGMA))

# build MLP
inter_0 = tf.matmul(cnn_output, weights_0) + bias_0
inter_0 = tf.layers.batch_normalization(inter_0, training=if_training)
inter_0 = mlp_activation(inter_0)

inter_1 = tf.matmul(inter_0, weights_1) + bias_1
inter_1 = tf.layers.batch_normalization(inter_1, training=if_training)
inter_1 = mlp_activation(inter_1)

inter_2 = tf.matmul(inter_1, weights_2) + bias_2
inter_2 = tf.layers.batch_normalization(inter_2, training=if_training)
inter_2 = mlp_activation(inter_2)

logits = tf.matmul(inter_2, weights_3) + bias_3
logits = mlp_activation(logits)
# add dropout
logits = tf.nn.dropout(logits, keep_prob)

# softmax/argmax
y_probs = tf.nn.softmax(logits, name='output')
y = tf.argmax(logits, axis=1)

# define a quadratic loss
quadratic_loss = tf.reduce_mean(tf.square(y_probs - y_ph))
train_op_quadratic = tf.train.AdamOptimizer(LEARNING_RATE).minimize(quadratic_loss)
# or use RMSPropOptimizer (?)

# define a cross entropy loss
entropy_loss = tf.reduce_mean(- tf.reduce_sum(y_ph * tf.log(y_probs), reduction_indices=[0]))
train_op_entropy = tf.train.AdamOptimizer(LEARNING_RATE).minimize(entropy_loss)

# initialize computational graph
sess = tf.Session()
with sess.as_default():
    tf.global_variables_initializer().run()

# ===============================================================================

# initialize figures
fig = plt.figure(figsize=(10, 7))
ax0 = plt.subplot2grid((3, 4), (0, 0))
ax1 = plt.subplot2grid((3, 4), (0, 1))
ax2 = plt.subplot2grid((3, 4), (1, 0))
ax3 = plt.subplot2grid((3, 4), (1, 1))
ax4 = plt.subplot2grid((3, 4), (0, 2))
ax5 = plt.subplot2grid((3, 4), (0, 3))
ax6 = plt.subplot2grid((3, 4), (1, 2))
ax7 = plt.subplot2grid((3, 4), (1, 3))
ax8 = plt.subplot2grid((3, 4), (2, 0), colspan=2)
ax9 = plt.subplot2grid((3, 4), (2, 2), colspan=2)

test_images_raw = test_features[:BATCH_SIZE]
test_images = np.reshape(test_images_raw, (len(test_images_raw), 48, 48, 3))

random_image_0 = random_image_1 = random_image_2 = random_image_3 = 0
while not (random_image_0 != random_image_1 != random_image_2 != random_image_3):
    random_image_0 = randrange(0, BATCH_SIZE, 1)
    random_image_1 = randrange(0, BATCH_SIZE, 1)
    random_image_2 = randrange(0, BATCH_SIZE, 1)
    random_image_3 = randrange(0, BATCH_SIZE, 1)
ax0.imshow(test_images[random_image_0])
ax2.imshow(test_images[random_image_1])
ax4.imshow(test_images[random_image_2])
ax6.imshow(test_images[random_image_3])
for ax in [ax0, ax2, ax4, ax6]:
    ax.set_xticks([])
    ax.set_yticks([])
plt.ion()

train_accuracies, valid_accuracies = [], []
batch_train_data = generator([train_features, train_targets], BATCH_SIZE)

train_target_numbers = np.argmax(train_targets, axis=1)
valid_target_numbers = np.argmax(valid_targets, axis=1)
train_target_occurrences = []
valid_target_occurrences = []
for number in range(2):
    train_target_occurrences.append(len(np.where(train_target_numbers == number)[0]))
    valid_target_occurrences.append(len(np.where(valid_target_numbers == number)[0]))

lower_loss = 1e6
# loop over each iteration
for epoch in range(TRAIN_ITERS):

    train_x, train_y = next(batch_train_data)
    loss, _ = sess.run([entropy_loss, train_op_entropy], {x_ph: train_x, y_ph: train_y,
                                                          keep_prob: 1.0, if_training: 'true'})
    if loss < lower_loss:
        lower_loss = loss

    # update figure every 10 iterations
    if epoch % 500 == 0:

        print('running epoch: ', epoch)
        print('lower loss: ', lower_loss)

        # get predictions for test images
        pred_probs = sess.run([y_probs], {x_ph: test_images_raw, keep_prob: 0.9,
                                          if_training: 'false'})[0]

        for ax in [ax1, ax3, ax5, ax7]:
            ax.cla()
            ax.set_ylim(-0.1, 1.1)
            ax.plot([-1, 10], [0., 0.], color='k', ls='--', lw=3, alpha=0.5)
            ax.plot([-1, 10], [1., 1.], color='k', ls='--', lw=3, alpha=0.5)
        sns.barplot(x=np.arange(2), y=pred_probs[random_image_0], ax=ax1, color=set_colors[2])
        sns.barplot(x=np.arange(2), y=pred_probs[random_image_1], ax=ax3, color=set_colors[2])
        sns.barplot(x=np.arange(2), y=pred_probs[random_image_2], ax=ax5, color=set_colors[2])
        sns.barplot(x=np.arange(2), y=pred_probs[random_image_3], ax=ax7, color=set_colors[2])
        labels = [item.get_text() for item in ax1.get_xticklabels()]
        labels[0] = 'No crack'
        labels[1] = 'Crack(s)'
        ax1.set_xticklabels(labels)
        ax3.set_xticklabels(labels)
        ax5.set_xticklabels(labels)
        ax7.set_xticklabels(labels)

        # get training predictions
        train_preds = []
        for batch_index in range(len(train_features) // BATCH_SIZE):
            train_feature = train_features[batch_index * BATCH_SIZE: (batch_index + 1) * BATCH_SIZE]
            train_probs = sess.run([y_probs], {x_ph: train_feature, keep_prob: 0.9,
                                               if_training: 'false'})[0]
            train_preds.extend(train_probs)
        train_preds = np.array(train_preds)
        train_cats = np.around(train_preds)

        # get validation predictions
        valid_preds = []
        for batch_index in range(len(valid_features) // BATCH_SIZE):
            valid_feature = valid_features[batch_index * BATCH_SIZE: (batch_index + 1) * BATCH_SIZE]
            valid_probs = sess.run([y_probs], {x_ph: valid_feature, keep_prob: 0.9,
                                               if_training: 'false'})[0]
            valid_preds.extend(valid_probs)
        valid_preds = np.array(valid_preds)
        valid_cats = np.around(valid_preds)

        # get overall prediction accuracies
        ax8.cla()

        train_comparison = np.sum(train_cats - train_targets, axis=1)
        train_sum_is_zero = len(np.where(train_comparison == 0)[0])
        train_accuracy = train_sum_is_zero / float(len(train_comparison))
        train_accuracies.append(train_accuracy)

        valid_comparison = np.sum(valid_cats - valid_targets, axis=1)
        valid_sum_is_zero = len(np.where(valid_comparison == 0)[0])
        valid_accuracy = valid_sum_is_zero / float(len(valid_comparison))
        valid_accuracies.append(valid_accuracy)

        SIZE = 20
        ax8.set_title('Prediction accuracies')
        ax8.plot(np.arange(len(train_accuracies[-SIZE:])) + len(train_accuracies[:-SIZE]),
                 np.zeros(len(train_accuracies[-SIZE:])) + np.amax(train_accuracies),
                 color=train_set_color, lw=3,
                 ls='--', alpha=0.5)
        ax8.plot(np.arange(len(valid_accuracies[-SIZE:])) + len(valid_accuracies[:-SIZE]),
                 np.zeros(len(valid_accuracies[-SIZE:])) + np.amax(valid_accuracies),
                 color=valid_set_color, lw=3,
                 ls='--', alpha=0.5)
        ax8.plot(np.arange(len(train_accuracies[-SIZE:])) + len(train_accuracies[:-SIZE]),
                 train_accuracies[-SIZE:],
                 lw=3, color=train_set_color, label='train')
        ax8.plot(np.arange(len(valid_accuracies[-SIZE:])) + len(valid_accuracies[:-SIZE]),
                 valid_accuracies[-SIZE:],
                 lw=3, color=valid_set_color, label='valid')
        ax8.legend()

        # get individual accuracies
        train_pred_numbers = np.argmax(train_preds, axis=1)
        train_diff = train_pred_numbers - train_target_numbers
        train_correct_by_digit = []
        for digit in range(2):
            indices = np.where(train_target_numbers == digit)[0]
            correct = len(np.where(train_diff[indices] == 0)[0])
            train_correct_by_digit.append(correct / float(train_target_occurrences[digit]))
        train_correct_by_digit = np.array(train_correct_by_digit)

        valid_pred_numbers = np.argmax(valid_preds, axis=1)
        valid_diff = valid_pred_numbers - valid_target_numbers
        valid_correct_by_digit = []
        for digit in range(2):
            indices = np.where(valid_target_numbers == digit)[0]
            correct = len(np.where(valid_diff[indices] == 0)[0])
            valid_correct_by_digit.append(correct / float(valid_target_occurrences[digit]))
        valid_correct_by_digit = np.array(valid_correct_by_digit)

        ax9.cla()
        ax9.set_xticks(np.arange(2))
        ax0.set_xticklabels(np.arange(2))
        ax9.set_ylim(0.9 * np.minimum(np.amin(train_correct_by_digit),
                                      np.amin(valid_correct_by_digit)), 1.02)
        ax9.bar(np.arange(2) - 0.21, train_correct_by_digit, width=0.4, color=train_set_color)
        ax9.bar(np.arange(2) + 0.21, valid_correct_by_digit, width=0.4, color=valid_set_color)
        labels = [item.get_text() for item in ax9.get_xticklabels()]
        labels[0] = 'No crack'
        labels[1] = 'Crack(s)'
        ax9.set_xticklabels(labels)

        # plot figures
        plt.tight_layout()
        plt.pause(0.05)

    if loss <= 0.05:
        saver = tf.train.Saver(write_version=tf.train.SaverDef.V2)
        save_path = saver.save(sess, '/Users/pierrechapuis/Documents/PyCharm/UBC/'
                                     'image_analysis/filmQuality/crack_model/model.ckpt')
        print('Model saved in file: % s' % save_path)
        print('Loss: %s' % loss)
        print('At epoch: %s' % epoch)

        sys.exit()
