from matplotlib.patches import Rectangle
from PIL import Image
import matplotlib
import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

# define block size
grid_x = 64
grid_y = 48
size = 48, 48

# define number of blocks in one row and one column
range_x = round(1024 / grid_x)
range_y = round(768 / grid_y)

# open raw image
img = Image.open('/Users/pierrechapuis/Documents/PyCharm/UBC/image_analysis/filmQuality/'
                 'imageStacks/YC20180709_cleanedGlass_B-5/CHTP_autosave__486_ch1.jpg')
# add image to list
image_list = [img]

# initialize real_image list
real_image = []
# loop over sub-blocks
for x in range(range_x):
    for y in range(range_y):
        # define sub-block coordinates
        bbox = (x * grid_x, y * grid_y, x * grid_x + grid_x, y * grid_y + grid_y)
        # crop image into sub-block, resize into square and convert into array
        img_crop = image_list[0].crop(bbox)
        img_resized = img_crop.resize(size, Image.ANTIALIAS)
        image_in_array = np.array(img_resized)
        # append image to real_image list
        real_image.append(image_in_array)

image_feature = np.asarray(real_image)

# image_in_array = np.array(image)
# image_feature = [image_in_array]

tf.reset_default_graph()
with tf.Session() as sess:
    # Load meta graph and restore weights and biases
    saver = tf.train.import_meta_graph('/Users/pierrechapuis/Documents/PyCharm/UBC/'
                                       'image_analysis/filmQuality/crack_model/model.ckpt.meta')

    saver.restore(sess, tf.train.latest_checkpoint('/Users/pierrechapuis/Documents/PyCharm/UBC/'
                                                   'image_analysis/filmQuality/crack_model/'))
    # get graph of the session
    graph = sess.graph

    # recover placeholders
    x_ph = graph.get_tensor_by_name("x_ph:0")
    keep_prob = graph.get_tensor_by_name("keep_prob:0")
    if_training = graph.get_tensor_by_name("if_training:0")
    logits = graph.get_tensor_by_name("output:0")

    # define inputs to feed the program
    feed_dict = {x_ph: image_feature, keep_prob: 1.0, if_training: 'false'}

    # perform prediction
    prediction = sess.run([logits], feed_dict=feed_dict)[0]
    # print prediction
    print(prediction)

    # print([node.name for node in graph.as_graph_def().node])

    # set plot font size
    matplotlib.rcParams.update({'font.size': 16})
    # convert raw image (not converted into square) to array
    img_in_array = np.array(img)
    # initialize figure canvas
    plt.figure(figsize=(10, 7))
    # get current axis
    currentAxis = plt.gca()
    # plot image
    plot_image = plt.imshow(img_in_array)
    plt.xlabel('Row # (pixel)')
    plt.ylabel('Column # (pixel)')

    # loop over the sub-blocks and incrementing the prediction vector to display tagged areas
    k = 0
    for x in range(range_x):
        for y in range(range_y):
            # for y in range(0, range_y):
            if prediction[k][1] >= 0.95:
                currentAxis.add_patch(Rectangle((x * 64, y * 48), 64, 48,
                                                alpha=0.25, facecolor='red', edgecolor='black'))
                k = k + 1
            elif prediction[k][1] <= 0.05:
                currentAxis.add_patch(Rectangle((x * 64, y * 48), 64, 48,
                                                alpha=0.25, facecolor='blue', edgecolor='black'))
                k = k + 1
            else:
                currentAxis.add_patch(Rectangle((x * 64, y * 48), 64, 48,
                                                alpha=0.25, facecolor='gray', edgecolor='black'))
                k = k + 1

    # display figure
    plt.show()

    # close current session
    sess.close()
