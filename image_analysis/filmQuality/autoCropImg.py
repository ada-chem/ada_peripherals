from PIL import Image
import glob


"""
Description:
Load, crop and save images.
Author:
Pierre Chapuis

Manual:
When loading this function, the different arguments are:
    * x_grid/y_grid (int): horizontal/vertical size of one sub-block (in pixel).
    * x_img/y_img (int): size of raw images (in pixel).
    * size_img (int): size of the output images (in pixel). Reshaping in square to allow image rotation.
    * path_file (string): disk location where raw images are stored.
    * path_save (string): disk location where new cut images will be saved.
"""


class AutoCropImg(object):
    def __init__(self, x_grid, y_grid, x_img, y_img, size_img, path_file, path_save):

        self.grid_x = x_grid
        self.grid_y = y_grid
        self.img_x = x_img
        self.img_y = y_img
        self.size = size_img
        self.file_path = path_file
        self.save_path = path_save

    def crop(self):
        # define number of blocks in one row and one column
        range_x = round(self.img_x / self.grid_x)
        range_y = round(self.img_y / self.grid_y)

        # initialize list of images being treated
        image_list = []
        # loop over each file containing '.jpg' extension
        for filename in glob.glob(self.file_path + '*.jpg'):
            img = Image.open(filename)
            image_list.append(img)

        # loop over images
        for i in range(0, len(image_list)):
            # loop over sub-blocks
            for x in range(range_x):
                for y in range(range_y):
                    # define sub-block coordinates
                    bbox = (x * grid_x, y * grid_y, x * grid_x + grid_x, y * grid_y + grid_y)
                    # crop image into sub-block
                    img_crop = image_list[i].crop(bbox)
                    img_resized = img_crop.resize(self.size, Image.LANCZOS)
                    # save image in .jpg file
                    img_resized.save(self.save_path + str(i) + '_' + str(x)
                                     + '_' + str(y) + '.jpg', optimize=True)


if __name__ == "__main__":
    # set image and block parameters
    grid_x = 20
    grid_y = 18
    img_x = 700
    img_y = 180
    size = 48, 48

    # set file path
    file_path = '/Users/pierrechapuis/Documents/PyCharm/UBC/image_analysis/' \
                'filmQuality/Samples/graphite_films/bright_data/'
    save_path = '/Users/pierrechapuis/Documents/PyCharm/UBC/image_analysis/' \
                'filmQuality/imageCropped/bright_'

    # call class
    my_class = AutoCropImg(grid_x, grid_y, img_x, img_y, size, file_path, save_path)
    # start cropping process
    my_class.crop()
