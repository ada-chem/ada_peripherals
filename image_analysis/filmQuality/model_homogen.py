#!/usr/bin/env python
from random import shuffle
import numpy as np
import tensorflow as tf
import pickle
import os
import sys


"""
Description:
Model for single and multi-label classification.
Author:
Pierre Chapuis

Manual:
The core of the model. When called, required arguments are:
    * ds_path (string): dataset location.
    * sm_path (string): location of the upcoming saved model. 
    * threshold (float): stopping condition of the model.
    * train_iters (int): setting number of maximum iterations.
    * batch_size (int): setting the batch size.
    * learning_rate (float): setting the learning rate.
    * mlp_size (int): setting the multi-layer perceptron size (how many layers).
    * filter_size (int): setting the filter size (how many layers).
    * conv_hidden (int): setting the convolutional neural network (CNN) size (how many layers).
    * stride (int): setting jump step size for CNN procedure.
"""


# !!! IF GPU IS NOT TENSORFLOW COMPATIBLE !!!
# Preventing TensorFlow to display a warning message if GPU is not compatible.
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class TrainWithModel(object):
    def __init__(self, ds_path, sm_path, threshold,
                 train_iters, batch_size, learning_rate, mlp_size,
                 filter_size, conv_hidden, stride):

        self.path = ds_path
        self.model_path = sm_path
        self.loss_threshold = threshold

        self.TRAIN_ITERS = train_iters
        self.BATCH_SIZE = batch_size
        self.LEARNING_RATE = learning_rate
        self.MLP_SIZE = mlp_size

        self.FILTER_SIZE = filter_size
        self.CONV_HIDDEN = conv_hidden
        self.STRIDE = stride

    # generate batch
    @staticmethod
    def generator(arrays, size_batch):
        starts = [0] * len(arrays)
        while True:
            batches = []
            for i, array in enumerate(arrays):
                start = starts[i]
                stop = start + size_batch
                diff = stop - array.shape[0]
                if diff <= 0:
                    batch = array[start: stop]
                    starts[i] += size_batch
                else:
                    batch = np.concatenate([array[start:], array[:diff]])
                    starts[i] = diff
                batches.append(batch)
            yield batches

    # load dataset
    def load_dataset(self):
        # open images
        file_object = open(self.path, 'rb')
        train_set, valid_set, test_set = pickle.load(file_object)

        # fill train/valid/test sets
        train_features = train_set[0]
        train_targets = train_set[1]
        valid_features = valid_set[0]
        valid_targets = valid_set[1]
        test_features = test_set[0]
        test_targets = test_set[1]

        # zip and shuffle list (then unzip)
        zip_list = list(zip(train_targets, train_features))
        shuffle(zip_list)
        train_targets, train_features = zip(*zip_list)

        # convert into numpy arrays
        train_features = np.asarray(train_features)
        train_targets = np.asarray(train_targets)
        valid_features = np.asarray(valid_features)
        valid_targets = np.asarray(valid_targets)
        test_features = np.asarray(test_features)
        test_targets = np.asarray(test_targets)

        return (train_features, train_targets, valid_features,
                valid_targets, test_features, test_targets)

    # convolutional activation layer
    @staticmethod
    def conv_activation(x_):
        x_ = tf.nn.leaky_relu(x_, 0.2)
        return x_

    # multi layer perceptron activation layer
    @staticmethod
    def mlp_activation(x_):
        x_ = tf.nn.leaky_relu(x_, 0.2)
        return x_

    # build and run the model
    def start_model(self, train_features, train_targets):

        # define placeholders
        x_ph = tf.placeholder(tf.float32,
                              [None, train_features.shape[1], train_features.shape[2],
                               train_features.shape[3]], name='x_ph')
        y_ph = tf.placeholder(tf.float32, [None, train_targets.shape[1]], name='y_ph')

        if_training = tf.placeholder(tf.bool, name='if_training')

        keep_prob = tf.placeholder(tf.float32, name='keep_prob')
        sigma = 0.01

        # initialize weights and biases (for CNN)
        weights_conv_0 = tf.get_variable('weights_conv_0', [FILTER_SIZE, FILTER_SIZE,
                                                            train_features.shape[-1],
                                                            CONV_HIDDEN * train_features.shape[-1]],
                                         initializer=tf.random_normal_initializer(0., sigma))
        bias_conv_0 = tf.get_variable('bias_conv_0', [CONV_HIDDEN * train_features.shape[-1]],
                                      initializer=tf.random_normal_initializer(0., sigma))

        weights_conv_1 = tf.get_variable('weights_conv_1', [FILTER_SIZE, FILTER_SIZE,
                                                            CONV_HIDDEN * train_features.shape[-1],
                                                            2 * CONV_HIDDEN * train_features.shape[-1]],
                                         initializer=tf.random_normal_initializer(0., sigma))
        bias_conv_1 = tf.get_variable('bias_conv_1', [2 * CONV_HIDDEN * train_features.shape[-1]],
                                      initializer=tf.random_normal_initializer(0., sigma))

        # build CNN
        # layer #0
        conv_0 = tf.nn.conv2d(x_ph, weights_conv_0, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        proc_0 = self.conv_activation(conv_0 + bias_conv_0)
        pool_0 = tf.nn.max_pool(proc_0, [1, FILTER_SIZE, FILTER_SIZE, 1],
                                [1, FILTER_SIZE, FILTER_SIZE, 1], padding='VALID')

        # layer #1
        conv_1 = tf.nn.conv2d(pool_0, weights_conv_1, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        conv_1 = tf.nn.l2_normalize(conv_1, epsilon=1e-8)
        proc_1 = self.conv_activation(conv_1 + bias_conv_1)
        pool_1 = tf.nn.max_pool(proc_1, [1, FILTER_SIZE, FILTER_SIZE, 1],
                                [1, FILTER_SIZE, FILTER_SIZE, 1], padding='VALID')

        # convert 2D images into 1D vector
        cnn_output = tf.contrib.layers.flatten(pool_1)

        # initialize weights and biases (for MLP)
        weights_0 = tf.get_variable('weights_0', [cnn_output.get_shape()[1], MLP_SIZE],
                                    initializer=tf.random_normal_initializer(0., sigma))
        bias_0 = tf.get_variable('bias_0', [MLP_SIZE],
                                 initializer=tf.random_normal_initializer(0., sigma))

        weights_1 = tf.get_variable('weights_1', [MLP_SIZE, MLP_SIZE],
                                    initializer=tf.random_normal_initializer(0., sigma))
        bias_1 = tf.get_variable('bias_1', [MLP_SIZE],
                                 initializer=tf.random_normal_initializer(0., sigma))

        weights_2 = tf.get_variable('weights_2', [MLP_SIZE, MLP_SIZE],
                                    initializer=tf.random_normal_initializer(0., sigma))
        bias_2 = tf.get_variable('bias_2', [MLP_SIZE],
                                 initializer=tf.random_normal_initializer(0., sigma))

        weights_3 = tf.get_variable('weights_3', [MLP_SIZE, train_targets.shape[1]],
                                    initializer=tf.random_normal_initializer(0., sigma))
        bias_3 = tf.get_variable('bias_3', [train_targets.shape[1]],
                                 initializer=tf.random_normal_initializer(0., sigma))

        # build MLP
        inter_0 = tf.matmul(cnn_output, weights_0) + bias_0
        inter_0 = tf.layers.batch_normalization(inter_0, training=if_training)
        inter_0 = self.mlp_activation(inter_0)

        inter_1 = tf.matmul(inter_0, weights_1) + bias_1
        inter_1 = tf.layers.batch_normalization(inter_1, training=if_training)
        inter_1 = self.mlp_activation(inter_1)

        inter_2 = tf.matmul(inter_1, weights_2) + bias_2
        inter_2 = tf.layers.batch_normalization(inter_2, training=if_training)
        inter_2 = self.mlp_activation(inter_2)

        logits = tf.matmul(inter_2, weights_3) + bias_3
        logits = self.mlp_activation(logits)
        # add dropout
        logits = tf.nn.dropout(logits, keep_prob)

        # softmax/argmax
        y_probs = tf.nn.sigmoid(logits, name='output')
        # y = tf.argmax(logits, axis=1)

        # define a cross entropy loss
        entropy_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=logits,
                                                                              labels=y_ph))
        train_op_entropy = tf.train.AdamOptimizer(LEARNING_RATE).minimize(entropy_loss)

        # initialize computational graph
        sess = tf.Session()

        # if model already exists:
        if any(file_name.endswith('.meta') for file_name in os.listdir(self.model_path)):
            # initialize Saver
            saver = tf.train.Saver()
            # load model
            saver.restore(sess, tf.train.latest_checkpoint(self.model_path))
            # get graph
            tf.get_default_graph()
        else:
            # otherwise initialize global variables
            with sess.as_default():
                tf.global_variables_initializer().run()

        # select a number of train data (depends on BATCH_SIZE) from train_features/train_targets
        batch_train_data = self.generator([train_features, train_targets], self.BATCH_SIZE)

        lower_loss = 1e6
        # loop over each iteration
        for epoch in range(TRAIN_ITERS):

            train_x, train_y = next(batch_train_data)
            loss, _ = sess.run([entropy_loss, train_op_entropy], {x_ph: train_x, y_ph: train_y,
                                                                  keep_prob: 1.0, if_training: 'true'})
            if loss < lower_loss:
                lower_loss = loss

            # update results every 500 iterations
            if epoch % 500 == 0:
                print('epoch #: ', epoch)
                print('lower loss: ', lower_loss)
                print('current loss: ', loss)

            # compare current loss to lower one, and save model if the 1st one is the lowest
            if loss <= self.loss_threshold:
                saver = tf.train.Saver(write_version=tf.train.SaverDef.V2, save_relative_paths=True)
                save_path = saver.save(sess, self.model_path + 'model.ckpt')
                print('Model saved in file: % s' % save_path)
                print('Loss: %s' % loss)
                print('At epoch # %s' % epoch)

                if loss <= 0.0000001:
                    completed = True
                    return completed, y_probs
                else:
                    self.loss_threshold = loss


if __name__ == "__main__":
    # set hyper parameters
    TRAIN_ITERS = 10 ** 8
    BATCH_SIZE = 900
    LEARNING_RATE = 1e-3
    MLP_SIZE = 30
    # set CNN parameters
    FILTER_SIZE = 3
    CONV_HIDDEN = 8
    STRIDE = 2
    # set stop condition
    loss_threshold = 1e-2

    # set paths
    dataset_path = ('/Users/pierrechapuis/Documents/PyCharm/UBC/'
                    'image_analysis/filmQuality/test.pkl')
    savedmodel_path = ('/Users/pierrechapuis/Documents/PyCharm/UBC/'
                       'image_analysis/filmQuality/saved_models/test/')

    my_class = TrainWithModel(dataset_path, savedmodel_path, loss_threshold,
                              TRAIN_ITERS, BATCH_SIZE, LEARNING_RATE, MLP_SIZE,
                              FILTER_SIZE, CONV_HIDDEN, CONV_HIDDEN)

    tr_f, tr_t, va_f, va_t, te_f, te_t = my_class.load_dataset()
    completed_program = my_class.start_model(tr_f, tr_t)

    if completed_program:
        sys.exit()
