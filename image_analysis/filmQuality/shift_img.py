from PIL import Image
import glob
import numpy as np

# import images
filelist = glob.glob('/Users/pierrechapuis/Documents/PyCharm/UBC'
                     '/image_analysis/filmQuality/crackHomogen_48x48/no-crack_homogen/*.jpg')

for i in range(0, len(filelist)):
    # open current image
    image = Image.open(filelist[i])

    arr = np.array(image)

    arr_up = np.roll(arr, -4, axis=0)
    arr_down = np.roll(arr, 4, axis=0)
    arr_right = np.roll(arr, 4, axis=1)
    arr_left = np.roll(arr, -4, axis=1)

    img_shift_up = Image.fromarray(arr_up, 'RGB')
    img_shift_up.save('/Users/pierrechapuis/Documents/PyCharm/UBC'
                      '/image_analysis/filmQuality/crackHomogen_48x48/no-crack_homogen'
                      '/shift_up_%d.jpg' % i)
    img_shift_down = Image.fromarray(arr_down, 'RGB')
    img_shift_down.save('/Users/pierrechapuis/Documents/PyCharm/UBC'
                        '/image_analysis/filmQuality/crackHomogen_48x48/no-crack_homogen'
                        '/shift_down_%d.jpg' % i)
    img_shift_left = Image.fromarray(arr_left, 'RGB')
    img_shift_left.save('/Users/pierrechapuis/Documents/PyCharm/UBC'
                        '/image_analysis/filmQuality/crackHomogen_48x48/no-crack_homogen'
                        '/shift_left_%d.jpg' % i)
    img_shift_right = Image.fromarray(arr_right, 'RGB')
    img_shift_right.save('/Users/pierrechapuis/Documents/PyCharm/UBC'
                         '/image_analysis/filmQuality/crackHomogen_48x48/no-crack_homogen'
                         '/shift_right_%d.jpg' % i)
