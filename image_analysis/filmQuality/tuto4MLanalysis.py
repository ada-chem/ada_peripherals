from findWorkspace import FindRect
from analyse_homogen import AnalyseWithModel

if __name__ == "__main__":
    # FIND WORKSPACE
    # set rectangle location
    loc_rect = (380, 240, 1080, 420)

    # set file path
    file_path = '/Users/pierrechapuis/Documents/PyCharm/UBC/' \
                'image_analysis/filmQuality/Samples/graphite_films/'
    save_path = file_path

    # call class
    my_class = FindRect(loc_rect, file_path, save_path)
    # start cropping process
    img_path = my_class.select()

    # IMAGE ANALYSIS
    # define image size
    img_x = 700
    img_y = 180
    # define block size
    grid_x = 20
    grid_y = 18
    # new block size (compressed in square shape)
    size = 48, 48

    # image path
    path_img = img_path
    path_check = '/Users/pierrechapuis/Documents/PyCharm/UBC/' \
                 'image_analysis/filmQuality/new_coverage_model/'
    path_graph = path_check + 'model.ckpt.meta'

    # start analyse
    my_class = AnalyseWithModel(grid_x, grid_y, img_x, img_y, size, path_img, path_graph, path_check)
    my_class.load_image()
    my_class.cut_image()
    my_class.start_model()
