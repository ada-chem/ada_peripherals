#!/usr/bin/env python
from PIL import Image
from random import shuffle
import glob
import pickle
import numpy as np


# import image stack
filelist_cover = glob.glob('/Users/pierrechapuis/Documents/PyCharm/UBC/'
                           'image_analysis/filmQuality/Samples/graphite_films/1_new/*.jpg')
filelist_noCover = glob.glob('/Users/pierrechapuis/Documents/PyCharm/UBC/'
                             'image_analysis/filmQuality/Samples/graphite_films/0_new/*.jpg')

# initialize targets
targets_cover = []
targets_noCover = []

for i in range(0, 18800):
    targets_cover.append((0, 1))
# fill target vector for crack and non-homogeneous images
for i in range(0, 15200):
    targets_noCover.append((1, 0))

# shuffle file lists
shuffle(filelist_cover)
shuffle(filelist_noCover)

# initialize features
features_cover = []
features_noCover = []

# fill feature matrices for crack and homogeneous images
for i in range(0, 18800):
    image = Image.open(filelist_cover[i])
    image_in_array = np.array(image)
    features_cover.append(image_in_array)

# fill feature matrices for non-crack and homogeneous images
for i in range(0, 15200):
    image = Image.open(filelist_noCover[i])
    image_in_array = np.array(image)
    features_noCover.append(image_in_array)

# lock list of crack and homogeneous images
zip_list_cover = list(zip(targets_cover, features_cover))
# shuffle list
shuffle(zip_list_cover)

# lock list of non-cracked and homogeneous images
zip_list_noCover = list(zip(targets_noCover, features_noCover))
# shuffle list
shuffle(zip_list_noCover)

# initialize train/valid/test sets
train_set = []
valid_set = []
test_set = []

# fill  train/valid/test sets by respecting 60%-20%-20% ratios
for i in range(0, 11280):
    train_set.append(zip_list_cover[i])
for i in range(0, 9120):
    train_set.append(zip_list_noCover[i])

for i in range(11280, 15040):
    valid_set.append(zip_list_cover[i])
for i in range(9120, 12160):
    valid_set.append(zip_list_noCover[i])

for i in range(15040, 18800):
    test_set.append(zip_list_cover[i])
for i in range(12160, 15200):
    test_set.append(zip_list_noCover[i])

# shuffle sets
shuffle(train_set)
shuffle(valid_set)
shuffle(test_set)

# unzip lists
train_targets, train_features = zip(*train_set)
valid_targets, valid_features = zip(*valid_set)
test_targets, test_features = zip(*test_set)

# pack train feature/target together
train_set = [train_features, train_targets]
# pack valid feature/target together
valid_set = [valid_features, valid_targets]
# pack test feature/target together
test_set = [test_features, test_targets]

# pack train/valid/test sets together
all_set = [train_set, valid_set, test_set]

# define name for pickle file
file_Name = "brightCoverageSet.pkl"
# open the file for writing
fileObject = open(file_Name, 'wb')
# append data to pickle file
pickle.dump(all_set, fileObject)
# close file
fileObject.close()
