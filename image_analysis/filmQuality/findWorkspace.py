from PIL import Image
import glob


"""
Description:
Load, select rectangle area (workspace) and save every image in a given folder.
Author:
Pierre Chapuis

Manual:
When calling this function, one need to specify 3 parameters:
    * coord_rect (int): location of the area of interest (top-left and bottom-right corners, in pixel).
    * path_file (string): disk location where raw images are stored.
    * path_save (string): disk location where new cut images will be saved.
"""


class FindRect(object):
    def __init__(self, coord_rect, path_file, path_save):

        self.loc_rect = coord_rect
        self.file_path = path_file
        self.save_path = path_save

    def select(self):
        # define rectangle
        bbox = self.loc_rect
        # initialize count
        count = 0
        # loop over each file containing '.jpg' extension
        for filename in glob.glob(self.file_path + '*.jpg'):
            count = count + 1
            img = Image.open(filename)
            # crop image into sub-block
            select_rect = img.crop(bbox)
            # save image in .jpg file
            select_rect.save(self.save_path + str(count) + '.jpg', optimize=True)

        return self.save_path + str(count) + '.jpg'


if __name__ == "__main__":
    # set rectangle location
    loc_rect = (380, 240, 1080, 420)

    # set file path (opening raw images)
    file_path = '/Users/pierrechapuis/Documents/PyCharm/UBC/' \
                'image_analysis/filmQuality/Samples/graphite_films/bright_data/'
    # set folder path (saving procedure)
    save_path = '/Users/pierrechapuis/Documents/PyCharm/UBC/' \
                'image_analysis/filmQuality/Samples/graphite_films/bright_data/'

    # call class
    my_class = FindRect(loc_rect, file_path, save_path)
    # start cropping process
    my_class.select()
