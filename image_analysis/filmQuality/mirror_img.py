from PIL import Image
import glob

# import image stack
filelist = glob.glob('/Users/pierrechapuis/Documents/PyCharm/UBC/image_analysis/'
                     'filmQuality/crackHomogen_48x48/crack_homogen/*.jpg')

# loop over each image
for i in range(0, len(filelist)):
    # open current image
    image = Image.open(filelist[i])

    # mirror and save current image
    image.transpose(Image.FLIP_TOP_BOTTOM).save('/Users/pierrechapuis/Documents/PyCharm/UBC/'
                                                'image_analysis/filmQuality/crackHomogen_48x48/'
                                                'crack_homogen/mirror_H_%d.jpg' % i)
