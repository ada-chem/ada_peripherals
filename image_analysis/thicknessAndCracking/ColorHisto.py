from PIL import Image
import matplotlib.pyplot as plt


def get_red(red_val):
    return '#%02x%02x%02x' % (red_val, 0, 0)


def get_green(green_val):
    return '#%02x%02x%02x' % (0, green_val, 0)


def get_blue(blue_val):
    return '#%02x%02x%02x' % (0, 0, blue_val)


# Create an Image with specific RGB value
image = Image.open("microImageFilm.png")
# Modify the color of two pixels
image.putpixel((0, 1), (1, 1, 5))
image.putpixel((0, 2), (2, 1, 5))
# Display the image
# image.show()
# Get the color histogram of the image
histogram = image.histogram()
# Take only the Red counts
l1 = histogram[0:256]
# Take only the Blue counts
l2 = histogram[256:512]
# Take only the Green counts
l3 = histogram[512:768]
plt.figure()
# R histogram
for i in range(0, 256):
    plt.bar(i, l1[i], color=get_red(i), edgecolor=get_red(i), alpha=0.3)
# G histogram
for i in range(0, 256):
    plt.bar(i, l2[i], color=get_green(i), edgecolor=get_green(i), alpha=0.3)
# B histogram
for i in range(0, 256):
    plt.bar(i, l3[i], color=get_blue(i), edgecolor=get_blue(i), alpha=0.3)

plt.show()
