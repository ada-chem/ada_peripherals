from skimage.feature import local_binary_pattern
from skimage.color import label2rgb
from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

METHOD = 'uniform'
plt.rcParams['font.size'] = 10

# settings for LBP
radius = 3
n_points = 8 * radius


def overlay_labels(img, loc_bin_pat, label):
    mask = np.logical_or.reduce([loc_bin_pat == each for each in label])
    return label2rgb(mask, image=img, bg_label=0, alpha=0.5)


def highlight_bars(bar, indexes):
    for i in indexes:
        bar[i].set_facecolor('r')


image_to_load = ("/Users/pierrechapuis/Documents/PyCharm/UBC/"
                 "image_analysis/thicknessAndCracking/film_01.png")
image_gray = Image.open(image_to_load).convert("L")
image = np.asarray(image_gray)
lbp = local_binary_pattern(image, n_points, radius, METHOD)


def hist(axe, loc_bin_pat):
    n_bins = int(loc_bin_pat.max() + 1)
    return axe.hist(loc_bin_pat.ravel(), normed=True, bins=n_bins, range=(0, n_bins),
                    facecolor='0.5', align='mid')


# plot histograms of LBP of textures
fig, (ax_img, ax_hist) = plt.subplots(nrows=2, ncols=3, figsize=(9.5, 6))
plt.gray()

titles = ('edge', 'flat', 'corner')
w = width = radius - 1
edge_labels = range(n_points // 2 - w, n_points // 2 + w + 1)
flat_labels = list(range(0, w + 1)) + list(range(n_points - w, n_points + 2))
i_14 = n_points // 4  # 1/4th of the histogram
i_34 = 3 * (n_points // 4)  # 3/4th of the histogram
corner_labels = (list(range(i_14 - w, i_14 + w + 1)) +
                 list(range(i_34 - w, i_34 + w + 1)))

label_sets = (edge_labels, flat_labels, corner_labels)

for ax, labels in zip(ax_img, label_sets):
    ax.imshow(overlay_labels(image, lbp, labels))

for ax, labels, name in zip(ax_hist, label_sets, titles):
    counts, _, bars = hist(ax, lbp)
    highlight_bars(bars, labels)
    ax.set_ylim(ymax=np.max(counts[:-1]))
    ax.set_xlim(xmax=n_points + 2)
    ax.set_title(name)

ax_hist[0].set_xlabel('Pattern #')
ax_hist[1].set_xlabel('Pattern #')
ax_hist[2].set_xlabel('Pattern #')
ax_hist[0].set_ylabel('Percentage')
for ax in ax_img:
    ax.axis('off')

plt.tight_layout()
plt.show()
