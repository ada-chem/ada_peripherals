# import libraries
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import matplotlib as mpl

mpl.rcParams['grid.linestyle'] = "--"
plt.rcParams['axes.axisbelow'] = True

# open image then convert in gray scale
imageGray = Image.open("microImageFilm.png").convert("L")
arr = np.asarray(imageGray)
# open image
imageColor = mpimg.imread("microImageFilm.png")

# initialise figure canvas
fig = plt.figure(figsize=(11, 5))

# define subplots
(ax1, ax2) = fig.subplots(1, 2)
# display gray image on the left
grayImagePlot = ax1.imshow(arr, interpolation="nearest", cmap="gray", vmin=0, vmax=255)
# define colorbar
cbar = plt.colorbar(grayImagePlot, ax=ax1, fraction=0.046, pad=0.04)
# set tick label size
cbar.ax.tick_params(labelsize=10)
# set colorbar and x-y labels
cbar.set_label("Channel #", fontsize=12)
ax1.set_xlabel("Row # (pixel)", fontsize=12)
ax1.set_ylabel("Column # (pixel)", fontsize=12)
# minor ticks enabled
ax1.minorticks_on()
# set title
ax1.set_title("Gray image", fontsize=12)

# display histogram on the right
# grid enabled
ax2.grid()
# minor ticks enabled
ax2.minorticks_on()
# display histogram
ax2.hist(imageColor.ravel()*255, bins=256, range=(0, 255), color="darkslategray")
# set tick format to scientific notation
ax2.ticklabel_format(style="sci", axis="y", scilimits=(0, 0), labelsize=12)
# determine aspect ratio
asp = np.diff(ax2.get_xlim())[0] / np.diff(ax2.get_ylim())[0]
# set aspect ratio
ax2.set_aspect(asp)
# set x-y labels
ax2.set_xlabel("Channel #", fontsize=12)
ax2.set_ylabel("Pixel count", fontsize=12)
# set title
ax2.set_title("Channel distribution", fontsize=12)

# tight layout
fig.tight_layout()
# adjust space between subplots
fig.subplots_adjust(wspace=0.3)
# save output figure
plt.savefig("grayChannels.png", format="png", transparent="True", dpi=200)
# plot output figure
# plt.show()
