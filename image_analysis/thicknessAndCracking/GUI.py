# import libraries
from tkinter import *
from tkinter import filedialog
from tkinter import StringVar
from PIL import Image
from PIL import ImageTk
from matplotlib.figure import Figure
import matplotlib.image as mpimg
import math
from skimage import feature
import os
import tkinter as tk
import numpy as np
import matplotlib

# need to set backend prior to load PyPlot
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.backends.backend_tkagg import NavigationToolbar2Tk


# libraries not used:
# from matplotlib import cm
# from skimage import feature
# import matplotlib.image as mpimg


# main class
class MyGui(Frame):

    # declare variables in __init__
    def __init__(self, root):
        Frame.__init__(self, root)

        # declare self variables
        self.file_var = StringVar(master, name='filevar')
        self.filename = None
        self.filepath = None
        self.close_button = None
        self.canvas = None
        self.canvas2 = None
        self.canvas3 = None
        self.canvas4 = None
        self.canvas5 = None
        self.canvas6 = None
        self.widget3 = None
        self.toolbar = None
        self.entry = None
        self.arr = None
        self.image_from_arr = None
        self.fig = None
        self.ax = None
        self.img = None
        self.mean_max = None
        self.sigma = None
        self.total_pixel = None
        self.x_size = None
        self.y_size = None
        self.score = None
        self.id_text1 = None
        self.id_text2 = None
        self.id_text3 = None
        self.id_text4 = None
        self.id_text5 = None
        self.id_text6 = None
        self.id_text7 = None
        self.id_text8 = None
        self.init_value = None

        # define frames on master
        self.frame_input_left = Frame(frame_left, padx=4, pady=4, bg="gray")
        self.frame_input_left.pack(side=TOP)
        self.frame_input_right = Frame(frame_right, padx=4, pady=4, bg="gray")
        self.frame_input_right.pack(side=TOP)
        self.frame_image = Frame(frame_left, padx=0, pady=0)
        self.frame_image.pack(side=TOP)
        self.frame_histo = Frame(frame_left, padx=2, pady=2, bg="black")
        self.frame_histo.pack(side=TOP)
        self.frame_cracks = Frame(frame_right, padx=0, pady=0)
        self.frame_cracks.pack(side=TOP, anchor=NW)
        self.frame_text = Frame(frame_right, padx=0, pady=0)
        self.frame_text.pack(side=TOP)

        # define buttons on frame_input
        button_open = Button(self.frame_input_left, text="1_ Choose sample to evaluate",
                             command=self.open_file_handler,
                             height=2, width=25, foreground="blue")
        button_open.pack(side=LEFT)

        button_convert = Button(self.frame_input_left, text="2_ Convert into grayscale",
                                command=self.convert_into_grayscale,
                                height=2, width=25, foreground="blue")
        button_convert.pack(side=LEFT)

        button_histo = Button(self.frame_input_left, text="3_ Analyse color channels",
                              command=self.generate_histogram,
                              height=2, width=25, foreground="blue")
        button_histo.pack(side=LEFT)

        button_crack = Button(self.frame_input_right, text="4_ Identify cracks/shapes",
                              command=self.crack_id,
                              height=2, width=25, foreground="blue")
        button_crack.pack(side=LEFT)

        # set menubar
        self.menubar = Menu(master)
        # set File menu in menubar
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Open sample...", command=self.open_file_handler,
                                  accelerator="Control+o")
        self.filemenu.add_command(label="Save histogram...", command=self.save_histo_as,
                                  accelerator="Control+1")
        self.filemenu.add_command(label="Save crack_id...", command=self.save_crack_as,
                                  accelerator="Control+2")
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Exit", command=self.quit_app,
                                  accelerator="Control+q")
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        # set Help menu in menubar
        self.helpmenu = Menu(self.menubar, tearoff=0)
        self.helpmenu.add_command(label="Help...", command=self.popup_help,
                                  accelerator="Control+h")
        self.helpmenu.add_command(label="About...", command=self.popup_about,
                                  accelerator="Control+a")
        self.menubar.add_cascade(label="Help", menu=self.helpmenu)

        # enable shortcut from everywhere on the gui.
        master.bind_all('<Control-Key-o>', self.open_file_handler)
        master.bind_all('<Control-Key-1>', self.save_histo_as)
        master.bind_all('<Control-Key-2>', self.save_crack_as)
        master.bind_all('<Control-Key-a>', self.popup_about)
        master.bind_all('<Control-Key-h>', self.popup_help)
        master.bind_all('<Control-Key-q>', self.quit_app)

        # enable menubar
        master.config(menu=self.menubar)

    # open raw image
    def open_file_handler(self, event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

        # open dialog
        self.filename = filedialog.askopenfilename(initialdir=os.getcwd(),
                                                   title="Select file",
                                                   filetypes=(("png files", "*.png"),
                                                              ("all files", "*.*")))
        # check filename not None
        if self.filename:
            # set file name
            self.file_var.set(self.filename)
            # get file path
            self.filepath = self.file_var.get()
            # load image
            self.load_image()

    # draw rectangles to define image locations on frame_image
    def draw_rectangles(self):
        # create canvas for raw image
        self.canvas = Canvas(self.frame_image, height=200, width=200)
        self.canvas.pack(side=LEFT, anchor=NW)
        # create canvas for gray image
        self.canvas2 = Canvas(self.frame_image, height=200, width=200)
        self.canvas2.pack(side=LEFT, anchor=NW)
        # create canvas for crack_id image
        self.canvas4 = Canvas(self.frame_cracks, height=230, width=230)
        self.canvas4.pack(side=LEFT, anchor=NW)
        # create canvas for entry text (threshold)
        self.canvas5 = Canvas(self.frame_text, height=20, width=150, highlightthickness=0)
        self.canvas5.pack(side=TOP, anchor=N)
        # create canvas for output text
        self.canvas6 = Canvas(self.frame_text, height=360, width=230)
        self.canvas6.pack(side=LEFT, anchor=NW)

    # load raw image, resize and display it
    def load_image(self):
        # prevent canvas re-draw if they already exist
        if not self.canvas:
            MyGui.draw_rectangles(self)

        # open image
        image = Image.open(self.filepath)
        # resize image
        image_resized = image.resize((200, 200))
        image_resized = ImageTk.PhotoImage(image=image_resized)
        # add resized image to canvas
        self.canvas.image = image_resized
        self.canvas.create_image(0, 0, anchor='nw', image=image_resized)

    # convert raw image into gray scale (from RGB)
    def convert_into_grayscale(self):
        # open image
        image_gray = Image.open(self.filepath).convert("L")
        # store image into array
        self.arr = np.asarray(image_gray)
        # tkinter image from array
        image_from_arr = Image.fromarray(self.arr)
        # resize image
        image_resized = image_from_arr.resize((200, 200))
        image2 = ImageTk.PhotoImage(image=image_resized)
        # add resized image to canvas
        self.canvas2.image = image2
        self.canvas2.create_image(0, 0, anchor='nw', image=image2)

    # create histogram of color channels
    def generate_histogram(self):
        # remove old figure if it exists, with attached toolbar
        if self.toolbar:
            self.widget3.destroy()
            self.toolbar.destroy()

        # create figure
        self.fig = Figure(figsize=(5.5, 4.2), dpi=100)
        self.ax = self.fig.add_subplot(111)
        # get size of self.arr
        self.x_size, self.y_size = self.arr.shape

        # create histogram
        count, bins, _ = self.ax.hist(self.arr.ravel(),
                                      bins=256,
                                      range=(0, 256),
                                      color="darkolivegreen",
                                      align="left",
                                      edgecolor="black",
                                      linewidth=0.25)

        # set initial variables to calculation mean value based on used-only channels
        non_zero_list = np.nonzero(count)
        coord_bin_min = non_zero_list[0][0]
        coord_bin_max = non_zero_list[0][-1]
        sum_max = 0
        # loop over bins
        for countBin in range(coord_bin_min, coord_bin_max + 1):
            # count > 0
            if count[countBin] > 0:
                # increase sum_max by count value
                sum_max = sum_max + count[countBin]

        # calculate number of occupied bins of the current image
        number_bins_occupied = coord_bin_max - coord_bin_min + 1

        # calculate mean value of max bar values
        self.mean_max = sum_max / number_bins_occupied

        # initialise sum_x
        sum_x = 0
        # loop over bins
        for countBin in range(coord_bin_min, coord_bin_max + 1):
            # sum over all probabilities
            sum_x = sum_x + (countBin * count[countBin])

        # estimate bin mean value
        bin_mean = sum_x / sum_max
        # initialise sum_sqrt
        sum_sqrt = 0
        # loop over bins
        for countBin in range(coord_bin_min, coord_bin_max + 1):
            # estimate square root
            sum_sqrt = sum_sqrt + count[countBin] * ((countBin - bin_mean) ** 2)

        # calculate variance
        sigma_sqrt = sum_sqrt / sum_max
        # calculate standard deviation
        self.sigma = math.sqrt(sigma_sqrt)

        # calculate total number of pixels
        self.total_pixel = self.x_size * self.y_size

        # draw line of mean_max value on histogram
        self.ax.axhline(self.mean_max, color="darkorange", linestyle="dashed", linewidth=2)

        # set tick format to scientific notation
        self.ax.ticklabel_format(style="sci", axis="y", scilimits=(0, 0), labelsize=12)
        # set aspect ratio
        self.ax.set_aspect("auto")
        # set x-y labels
        self.ax.set_xlabel("Channel #", fontsize=12)
        self.ax.set_ylabel("Pixel count", fontsize=12)
        # set title
        self.ax.set_title("Channel distribution", fontsize=12)

        # create canvas for histogram
        canvas3 = FigureCanvasTkAgg(self.fig, master=self.frame_histo)
        canvas3.draw()
        # add widget3 to canvas3
        self.widget3 = canvas3.get_tk_widget()

        # add navigation toolbar
        self.toolbar = NavigationToolbar2Tk(canvas3, self.frame_histo)
        self.toolbar.update()

        self.widget3.pack()

        # display output text of image/histogram properties
        self.display_text_histo()

    # analyse crack/shape
    def crack_id(self):
        # define sigma (threshold for contour detection)
        sigma_value = [float(self.entry.get())]
        # read raw image
        self.img = mpimg.imread(self.filepath)
        # convert to gray scale
        img_gray = self.rgb2gray()
        # find contours
        edges = feature.canny(img_gray, sigma=sigma_value[0])
        # convert edges in 'binary' integers ('0' or '1')
        img_edges_for_count = edges.astype(int)
        # convert edges in 'binary' gray scale ('0' or '255')
        img_edges = (edges * 255).astype(np.uint8)
        # convert image to array
        self.image_from_arr = Image.fromarray(img_edges, mode='L')
        # resize image
        image_resized = self.image_from_arr.resize((250, 250))
        image_crack = ImageTk.PhotoImage(image=image_resized)
        # add image to canvas4
        self.canvas4.image = image_crack  # <--- keep reference of your image
        self.canvas4.create_image(0, 0, anchor='nw', image=image_crack)

        # initialise total number of 'crack' pixels
        total_crack_pixel = 0
        # find numbers of pixels in row and column
        row, column = img_edges.shape
        # calculate total number of pixels
        total_pixel = row * column

        # loop over row and columns
        for countRow in range(row):
            for countColumn in range(column):
                # if pixel intensity is '1'
                if img_edges_for_count[countRow, countColumn] == 1:
                    # determine number of 'crack' pixels
                    total_crack_pixel = total_crack_pixel + 1

        # calculate score (not damaged percentage)
        self.score = (total_crack_pixel * 100 / total_pixel)

        # display text
        self.display_text_cracks()

    # add output text related to image and histogram
    def display_text_histo(self):
        # remove old text if it exists
        if self.id_text1:
            self.canvas6.delete(self.id_text1)
            self.canvas6.delete(self.id_text2)
            self.canvas6.delete(self.id_text3)
            self.canvas6.delete(self.id_text4)
            self.canvas6.delete(self.id_text5)
            self.canvas6.delete(self.id_text6)

        # get size of image
        size = str('%d x %d' % (self.x_size, self.y_size))
        # define total pixel of image in string format
        total_pixel_text = str("{:,}".format(self.total_pixel))
        # define mean value in string format
        mean_max_text = str(round(self.mean_max, 1))
        # define sigma threshold in string format
        sigma_text = str(round(self.sigma, 1))

        # set tagged (self.id_text<#>) text outputs
        self.id_text1 = self.canvas6.create_text(30, 40, font="Times 14 italic bold",
                                                 text="Raw image:",
                                                 anchor="nw")
        self.id_text2 = self.canvas6.create_text(30, 60,
                                                 text=f"Size image = %s px\N{SUPERSCRIPT TWO}" % size,
                                                 anchor="nw")
        self.id_text3 = self.canvas6.create_text(30, 80,
                                                 text="Total pixels = %s px" % total_pixel_text,
                                                 anchor="nw")
        self.id_text4 = self.canvas6.create_text(30, 120, font="Times 14 italic bold",
                                                 text="Histogram:",
                                                 anchor="nw")
        self.id_text5 = self.canvas6.create_text(30, 140,
                                                 text="Mean value = %s px" % mean_max_text,
                                                 fill="darkorange", anchor="nw")
        self.id_text6 = self.canvas6.create_text(30, 160,
                                                 text="Variance σ = %s" % sigma_text,
                                                 fill="darkolivegreen", anchor="nw")

        # add text + entry field
        if not self.entry:
            label_text = StringVar()
            label_text.set("Threshold:")
            self.init_value = 0.1
            Label(self.canvas5, width=8, textvariable=label_text).pack(side=LEFT, expand=YES, fill=X)
            self.entry = Entry(self.canvas5, width=4)
            self.entry.config(highlightbackground="red")
            self.entry.insert(0, str(round(self.init_value, 1)))
            self.entry.pack(side=RIGHT, expand=YES, fill=X)

            # enable shortcuts
            master.bind('<Control-Key-minus>', self.minus_button)
            master.bind('<Control-Key-plus>', self.plus_button)

            # add '+' and '- buttons'
            plus_button = tk.Button(self.canvas5, text="+", width=3, height=1,
                                    font=('times', 14, 'bold'), command=self.plus_button)
            plus_button.pack(side=TOP, anchor=E)
            minus_button = tk.Button(self.canvas5, text="-", width=3, height=1,
                                     font=('times', 14, 'bold'), command=self.minus_button)
            minus_button.pack(side=TOP, anchor=E)

    # display output text for crack recognition properties
    def display_text_cracks(self):
        # remove old text if it exists
        if self.id_text7:
            self.canvas6.delete(self.id_text7)
            self.canvas6.delete(self.id_text8)

        # define score in string format
        score_text = str(round(self.score, 2))

        # set tagged (self.id_text<#>) text outputs
        self.id_text7 = self.canvas6.create_text(30, 200, font="Times 14 italic bold",
                                                 text="Identification:",
                                                 anchor="nw")
        self.id_text8 = self.canvas6.create_text(30, 220,
                                                 text="Score = %s" % score_text,
                                                 fill="red", anchor="nw")

    # save histo figure
    def save_histo_as(self, event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

        self.fig.savefig('histogram.png', format="png", transparent="True", dpi=200)

    # save crack figure
    def save_crack_as(self, event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

        self.image_from_arr.save("crack_id.png")

    # convert rgb to gray scale
    def rgb2gray(self):
        image_gray = np.dot(self.img[..., :3], [0.299, 0.587, 0.144])

        return image_gray

    # define '+' button
    def plus_button(self, event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

        # increase threshold value
        self.init_value = round(self.init_value + 0.1, 1)
        # condition 'value <= 2' true
        if self.init_value <= 2:
            # deletes the current value
            self.entry.delete(0, END)
            # inserts new value assigned by 2nd parameter
            self.entry.insert(0, str(self.init_value))
        else:
            # else reset previous value
            self.init_value = round(self.init_value - 0.1, 1)

    # define '+' button
    def minus_button(self, event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

        # decrease threshold value
        self.init_value = round(self.init_value - 0.1, 1)
        # condition 'value >= 0' true
        if self.init_value >= 0:
            # deletes the current value
            self.entry.delete(0, END)
            # inserts new value assigned by 2nd parameter
            self.entry.insert(0, str(self.init_value))
        else:
            # else reset previous value
            self.init_value = round(self.init_value + 0.1, 1)

    # display popup window (About...)
    def popup_about(self, event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

        # define window for 'About...'
        popup = tk.Tk()
        # set 'About...' window properties (size and location: centered)
        popup_width = 220
        popup_height = 170
        popup_x = (master.winfo_screenwidth() / 2) - (popup_width / 2)
        popup_y = (master.winfo_screenheight() / 2) - (popup_height / 2)
        popup.geometry('%dx%d+%d+%d' % (popup_width, popup_height, popup_x, popup_y))
        # set title and font
        popup.wm_title("About...")
        label_font = ('times', 14, 'bold')

        # add text to window
        text_about = tk.Label(popup,
                              text="Sample Quality Evaluation\n"
                                   "(ver_1.1)\n\n"
                                   "Author: Pierre Chapuis\n"
                                   "Date: 2018/06/25")
        text_about.pack(side="top", fill="both", expand='yes')
        text_about.config(bg='black', fg='white')
        text_about.config(font=label_font)
        text_about.config(height=3, width=30)

        # add button 'OK'
        self.close_button = tk.Button(popup, text="Close", font=('times', 16, 'bold'),
                                      command=popup.destroy)
        self.close_button.pack(side="top", fill="x", expand='yes')

        popup.mainloop()

    def popup_help(self, event=None):
        # prevent 'event not used' warning
        if event is None:
            pass

        # define window for 'Help...'
        popup = tk.Tk()
        # set 'About...' window properties (size and location: centered)
        popup_width = 350
        popup_height = 180
        popup_x = (master.winfo_screenwidth() / 2) - (popup_width / 2)
        popup_y = (master.winfo_screenheight() / 2) - (popup_height / 2)
        popup.geometry('%dx%d+%d+%d' % (popup_width, popup_height, popup_x, popup_y))
        # set title and font
        popup.wm_title("Help...")
        label_title = ('times', 16, 'bold')
        label_font = ('times', 12)

        # add navigation toolbar tip
        title_navig = tk.Label(popup,
                               text="Navigation toolbar tip:")
        title_navig.pack(side="top", fill="both", expand='yes', anchor=tk.NW)
        title_navig.config(bg='white', fg='black')
        title_navig.config(font=label_title)
        title_navig.config(height=1, width=30)

        text_navig = tk.Label(popup,
                              text="If buttons not working, click on the graph before selecting "
                                   "a button.")
        text_navig.pack(side="top", fill="both", expand='yes', anchor=tk.NW)
        text_navig.config(bg='white', fg='black')
        text_navig.config(font=label_font)
        text_navig.config(height=1, width=30)

        # add threshold definition
        title_threshold = tk.Label(popup,
                                   text="Threshold (red frame):")
        title_threshold.pack(side="top", fill="both", expand='yes', anchor=tk.NW)
        title_threshold.config(bg='white', fg='black')
        title_threshold.config(font=label_title)
        title_threshold.config(height=1, width=30)

        text_threshold = tk.Label(popup,
                                  text="Set a value for contour searching, between ]0, 2].\n"
                                       "Shortcuts: decrease \u2192 CTRL-, increase \u2192 CTRL+.")
        text_threshold.pack(side="top", fill="both", expand='yes', anchor=tk.NW)
        text_threshold.config(bg='white', fg='black')
        text_threshold.config(font=label_font)
        text_threshold.config(height=2, width=30)

        # add save function
        title_save = tk.Label(popup,
                              text="Save output files:")
        title_save.pack(side="top", fill="both", expand='yes', anchor=tk.NW)
        title_save.config(bg='white', fg='black')
        title_save.config(font=label_title)
        title_save.config(height=1, width=30)

        text_save = tk.Label(popup,
                             text="Save easily histogram and crack_id image from File menu.")
        text_save.pack(side="top", fill="both", expand='yes', anchor=tk.NW)
        text_save.config(bg='white', fg='black')
        text_save.config(font=label_font)
        text_save.config(height=1, width=30)

        # add button 'OK'
        self.close_button = tk.Button(popup, text="Close", font=('times', 16, 'bold'),
                                      command=popup.destroy)
        self.close_button.pack(side="top", fill="x", expand='yes')

        popup.mainloop()

    def quit_app(self, event=None):
        # prevent 'event not used' warning
        if event is None:
            pass
        self.file_var = StringVar(master, name='quit app')
        sys.exit(0)


# create tkinter Gui
master = Tk()
# set titles
master.title('Sample Quality Evaluation')
title = tk.Label(master, text="Please, follow steps below:", fg="white", bg="black", height=2)
title.pack(side=TOP, anchor=N, fill=X)

# add Ada icon
ada_image = ImageTk.PhotoImage(Image.open("Ada_mini.png"))
ada_proj = ImageTk.PhotoImage(Image.open("Ada_pxPerfect.png"))
ada_panel_l = tk.Label(master, image=ada_image, height=44, width=44)
ada_panel_r = tk.Label(master, image=ada_proj, height=44, width=44)
ada_panel_l.pack(side=LEFT, anchor=NW)
ada_panel_r.pack(side=RIGHT, anchor=NE)

# define frames
frame_left = Frame(master, padx=4, pady=4)
frame_left.pack(side=LEFT, anchor=NW)
frame_right = Frame(master, padx=4, pady=4)
frame_right.pack(side=RIGHT, anchor=NE)

# set master size and location (centered)
master.width = 1040
master.height = 740
master.minsize(1040, 740)
master.maxsize(1040, 740)
width_screen = master.winfo_screenwidth()
height_screen = master.winfo_screenheight()
master_x = (width_screen / 2) - (master.width / 2)
master_y = (height_screen / 2) - (master.height / 2)
master.geometry('%dx%d+%d+%d' % (master.width, master.height, master_x, master_y))

# launch MyGui
app = MyGui(master)
master.mainloop()
