# import libraries
# import math
import numpy as np
from PIL import Image
from skimage.feature import greycomatrix, greycoprops
# from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import glob


def sample_eval():
    sample_count = 0
    bin_labels = []
    green_histo_corr_diss = []
    blue_histo_corr_diss = []
    red_histo_corr_diss = []
    green_histo_ene_homog = []
    blue_histo_ene_homog = []
    red_histo_ene_homog = []

    file_list = glob.glob(os.path.join("/Users/pierrechapuis/Documents/PyCharm/UBC/"
                                       "image_analysis/thicknessAndCracking/samples", '*.png'))
    for file in sorted(file_list):
        if file.endswith(".png"):
            filepath = file
            file_label = file[92:-4]
            bin_labels.append(file_label)

            sample_count = sample_count + 1
            print("Sample #%d" % sample_count)
            print("----------")

            mpl.rcParams['grid.linestyle'] = "--"
            plt.rcParams['axes.axisbelow'] = True

            # open image then convert in gray scale
            image_gray = Image.open(filepath).convert("L")
            arr = np.asarray(image_gray)
            # find size of array (image)
            x_size, y_size = arr.shape

            # set current pixel range (lower/upper values found in raw gray image)
            old_min = arr.min()
            old_max = arr.max()
            # estimate current range
            old_range = old_max - old_min

            # set new pixel range, to take into account the whole gray scale (0 -> 255)
            new_min = 0
            new_max = 255
            # estimate new range
            new_range = new_max - new_min
            # initialise new array
            new_arr = np.zeros((y_size, x_size))

            # loop over each pixel
            for countRow in range(x_size):
                for countColumn in range(y_size):
                    # estimate ratio of current image
                    scale = (arr[countRow, countColumn] - old_min) / old_range
                    # estimate new array, ranging from 0 to 255
                    new_arr[countRow, countColumn] = (new_range * scale) + new_min

            # test GLCM
            # patch size can be: 85, 68, 34, 17, (10, 5 -> not relevant)
            patch_size = 17
            # number of data points
            number_of_points = round(680 / patch_size) ** 2
            image = arr

            # initialize areas list
            areas = []
            # populate areas list based on both patch and image sizes
            for i in range(0, 680 - patch_size + 1, patch_size):
                for j in range(0, 680 - patch_size + 1, patch_size):
                    areas.append((i, j))

            # initialize patch list
            patches = []
            for loc in areas:
                patches.append(image[loc[0]:loc[0] + patch_size, loc[1]:loc[1] + patch_size])

            # compute some GLCM properties each patch
            x1 = []
            y1 = []
            x2 = []
            y2 = []

            for patch in patches:
                glcm_45m = greycomatrix(patch, [1], [-45], 256, symmetric=True, normed=True)
                glcm_45p = greycomatrix(patch, [1], [45], 256, symmetric=True, normed=True)
                glcm_90 = greycomatrix(patch, [1], [90], 256, symmetric=True, normed=True)
                glcm_0 = greycomatrix(patch, [1], [0], 256, symmetric=True, normed=True)

                glcm_mean = (glcm_45p + glcm_45m + glcm_0 + glcm_90) / 4
                x1.append(greycoprops(glcm_mean, 'dissimilarity')[0, 0])
                y1.append(greycoprops(glcm_mean, 'correlation')[0, 0])
                x2.append(greycoprops(glcm_mean, 'homogeneity')[0, 0])
                y2.append(greycoprops(glcm_mean, 'energy')[0, 0])

            # Initialise count and threshold
            green_point = 0
            blue_point = 0
            red_point = 0
            threshold_x1 = 0.6
            threshold_y1 = 0.8

            for i in range(0, len(x1)):
                if (x1[i] <= threshold_x1) and (y1[i] >= threshold_y1):
                    green_point = green_point + 1
                elif (x1[i] <= threshold_x1) and (y1[i] < threshold_y1):
                    blue_point = blue_point + 1
                elif x1[i] > threshold_x1:
                    red_point = red_point + 1

            green_histo_corr_diss.append(green_point * 100 / number_of_points)
            blue_histo_corr_diss.append(blue_point * 100 / number_of_points)
            red_histo_corr_diss.append(red_point * 100 / number_of_points)

            print("Correlation vs Dissimilarity:")
            print("green: %.1f" % green_point)
            print("blue: %.1f" % blue_point)
            print("red: %.1f" % red_point)

            # Initialise count and threshold
            green_point = 0
            blue_point = 0
            red_point = 0
            threshold_x2 = 0.7
            threshold_y2 = 0.3

            for i in range(0, len(x2)):
                if (x2[i] >= threshold_x2) and (y2[i] >= threshold_y2):
                    green_point = green_point + 1
                elif (x2[i] >= threshold_x2) and (y2[i] < threshold_y2):
                    blue_point = blue_point + 1
                elif x2[i] < threshold_x2:
                    red_point = red_point + 1

            green_histo_ene_homog.append(green_point * 100 / number_of_points)
            blue_histo_ene_homog.append(blue_point * 100 / number_of_points)
            red_histo_ene_homog.append(red_point * 100 / number_of_points)
            print("Energy vs Homogeneity:")
            print("green: %.1f" % green_point)
            print("blue: %.1f" % blue_point)
            print("red: %.1f\n" % red_point)

    # create figure
    plt.Figure(figsize=(10, 5), dpi=100)
    bins = np.arange(len(green_histo_corr_diss))
    w = 0.2

    # create bar plot
    ax_1 = plt.subplot(211)
    ax_1.bar(bins - w, green_histo_corr_diss, width=w, color='g', align='center')
    ax_1.bar(bins, blue_histo_corr_diss, width=w, color='b', align='center')
    ax_1.bar(bins + w, red_histo_corr_diss, width=w, color='r', align='center')
    ax_1.set_xlabel("Sample #")
    ax_1.set_ylabel("Scores (%)")
    ax_1.set_title("Correlation VS Dissimilarity")
    plt.xticks(bins, bin_labels, rotation="vertical")
    ax_2 = plt.subplot(212)
    ax_2.bar(bins - w, green_histo_ene_homog, width=w, color='g', align='center')
    ax_2.bar(bins, blue_histo_ene_homog, width=w, color='b', align='center')
    ax_2.bar(bins + w, red_histo_ene_homog, width=w, color='r', align='center')
    ax_2.set_xlabel("Sample #")
    ax_2.set_ylabel("Scores (%)")
    ax_2.set_title("Energy VS Homogeneity")
    plt.xticks(bins, bin_labels, rotation="vertical")

    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    sample_eval()
