# import libraries
# import math
import numpy as np
from PIL import Image
from skimage.feature import greycomatrix, greycoprops
import matplotlib.pyplot as plt
import matplotlib as mpl


def sample_eval(filepath):
    mpl.rcParams['grid.linestyle'] = "--"
    plt.rcParams['axes.axisbelow'] = True

    # open image then convert in gray scale
    image_gray = Image.open(filepath).convert("L")
    arr = np.asarray(image_gray)
    # find size of array (image)
    x_size, y_size = arr.shape

    # set current pixel range (lower/upper values found in raw gray image)
    old_min = arr.min()
    old_max = arr.max()
    # estimate current range
    old_range = old_max - old_min

    # set new pixel range, to take into account the whole gray scale (0 -> 255)
    new_min = 0
    new_max = 255
    # estimate new range
    new_range = new_max - new_min
    # initialise new array
    new_arr = np.zeros((y_size, x_size))

    # loop over each pixel
    for countRow in range(x_size):
        for countColumn in range(y_size):
            # estimate ratio of current image
            scale = (arr[countRow, countColumn] - old_min) / old_range
            # estimate new array, ranging from 0 to 255
            new_arr[countRow, countColumn] = (new_range * scale) + new_min

    # initialise figure canvas
    fig = plt.figure(figsize=(9, 7))

    # define subplots
    ((ax1, ax3), (ax2, ax4)) = fig.subplots(2, 2)
    # display gray image on the left
    gray_image_plot = ax1.imshow(arr, interpolation="nearest", cmap="gray", vmin=0, vmax=255)
    # define colorbar
    cbar = plt.colorbar(gray_image_plot, ax=ax1, fraction=0.046, pad=0.04)
    # set tick label size
    cbar.ax.tick_params(labelsize=10)
    # set colorbar and x-y labels
    cbar.set_label("Channel #", fontsize=10)
    ax1.set_xlabel("Row # (pixel)", fontsize=10)
    ax1.set_ylabel("Column # (pixel)", fontsize=10)
    # minor ticks enabled
    ax1.minorticks_on()
    # set title
    ax1.set_title("Gray image", fontsize=10)

    # display gray image on the right
    gray_image_plot = ax2.imshow(arr, interpolation="nearest", cmap="gray", vmin=0, vmax=255)
    # define colorbar
    cbar = plt.colorbar(gray_image_plot, ax=ax2, fraction=0.046, pad=0.04)
    # set tick label size
    cbar.ax.tick_params(labelsize=10)
    # set colorbar and x-y labels
    cbar.set_label("Channel #", fontsize=10)
    ax2.set_xlabel("Row # (pixel)", fontsize=10)
    ax2.set_ylabel("Column # (pixel)", fontsize=10)
    # minor ticks enabled
    ax2.minorticks_on()

    # test GLCM
    # patch size can be: 85, 68, 34, 17, (10, 5 -> not relevant)
    patch_size = 17
    # number of data points
    number_of_points = round(680/patch_size)**2
    image = arr

    # initialize areas list
    areas = []
    # populate areas list based on both patch and image sizes
    for i in range(0, 680-patch_size+1, patch_size):
        for j in range(0, 680-patch_size+1, patch_size):
            areas.append((i, j))

    # initialize patch list
    patches = []
    for loc in areas:
        patches.append(image[loc[0]:loc[0] + patch_size, loc[1]:loc[1] + patch_size])

    # compute some GLCM properties each patch
    x1 = []
    y1 = []
    x2 = []
    y2 = []

    for patch in patches:
        glcm_45m = greycomatrix(patch, [1], [-45], 256, symmetric=True, normed=True)
        glcm_45p = greycomatrix(patch, [1], [45], 256, symmetric=True, normed=True)
        glcm_90 = greycomatrix(patch, [1], [90], 256, symmetric=True, normed=True)
        glcm_0 = greycomatrix(patch, [1], [0], 256, symmetric=True, normed=True)

        glcm_mean = (glcm_45p + glcm_45m + glcm_0 + glcm_90)/4
        x1.append(greycoprops(glcm_mean, 'dissimilarity')[0, 0])
        y1.append(greycoprops(glcm_mean, 'correlation')[0, 0])
        x2.append(greycoprops(glcm_mean, 'homogeneity')[0, 0])
        y2.append(greycoprops(glcm_mean, 'energy')[0, 0])

    # Initialise count and threshold
    count = 0
    green_point = 0
    blue_point = 0
    red_point = 0
    threshold_x1 = 0.6
    threshold_y1 = 0.8
    # display original image with areas of patches
    for (y, x) in areas:
        if (x1[count] <= threshold_x1) and (y1[count] >= threshold_y1):
            ax1.plot(x + patch_size / 2, y + patch_size / 2, 's', color='green', markersize=2)
            count = count + 1
            green_point = green_point + 1
        elif (x1[count] <= threshold_x1) and (y1[count] < threshold_y1):
            ax1.plot(x + patch_size / 2, y + patch_size / 2, 's', color='blue', markersize=2)
            count = count + 1
            blue_point = blue_point + 1
        elif x1[count] > threshold_x1:
            ax1.plot(x + patch_size / 2, y + patch_size / 2, 's', color='red', markersize=2)
            count = count + 1
            red_point = red_point + 1

    green_point = green_point * 100 / number_of_points
    blue_point = blue_point * 100 / number_of_points
    red_point = red_point * 100 / number_of_points
    print("Correlation vs Dissimilarity:")
    print("green: %.1f %%" % green_point)
    print("blue: %.1f %%" % blue_point)
    print("red: %.1f %%\n" % red_point)

    # Initialise count and threshold
    count = 0
    green_point = 0
    blue_point = 0
    red_point = 0
    threshold_x2 = 0.7
    threshold_y2 = 0.3
    for (y, x) in areas:
        if (x2[count] >= threshold_x2) and (y2[count] >= threshold_y2):
            ax2.plot(x + patch_size / 2, y + patch_size / 2, 's', color='green', markersize=2)
            count = count + 1
            green_point = green_point + 1
        elif (x2[count] >= threshold_x2) and (y2[count] < threshold_y2):
            ax2.plot(x + patch_size / 2, y + patch_size / 2, 's', color='blue', markersize=2)
            count = count + 1
            blue_point = blue_point + 1
        elif x2[count] < threshold_x2:
            ax2.plot(x + patch_size / 2, y + patch_size / 2, 's', color='red', markersize=2)
            count = count + 1
            red_point = red_point + 1

    green_point = green_point * 100 / number_of_points
    blue_point = blue_point * 100 / number_of_points
    red_point = red_point * 100 / number_of_points
    print("Energy vs Homogeneity:")
    print("green: %.1f %%" % green_point)
    print("blue: %.1f %%" % blue_point)
    print("red: %.1f %%\n" % red_point)

    for count in range(0, number_of_points):
        if (x1[count] <= threshold_x1) and (y1[count] >= threshold_y1):
            ax3.plot(x1[count], y1[count], 'o', color='green', markersize=2)
        elif (x1[count] <= threshold_x1) and (y1[count] < threshold_y1):
            ax3.plot(x1[count], y1[count], 'o', color='blue', markersize=2)
        elif x1[count] > threshold_x1:
            ax3.plot(x1[count], y1[count], 'o', color='red', markersize=2)

    ax3.set_xlabel('Dissimilarity')
    ax3.set_ylabel('Correlation')
    ax3.set_title("GLCM", fontsize=10)
    ax3.set_aspect('auto')

    for count in range(0, number_of_points):
        if (x2[count] >= threshold_x2) and (y2[count] >= threshold_y2):
            ax4.plot(x2[count], y2[count], 'o', color='green', markersize=2)
        elif (x2[count] >= threshold_x2) and (y2[count] < threshold_y2):
            ax4.plot(x2[count], y2[count], 'o', color='blue', markersize=2)
        elif x2[count] < threshold_x2:
            ax4.plot(x2[count], y2[count], 'o', color='red', markersize=2)

    ax4.set_xlabel('Homogeneity')
    ax4.set_ylabel('Energy')
    ax4.set_aspect('auto')

    fig.tight_layout()
    plt.show()


if __name__ == "__main__":
    sample_eval("samples/film_04a.png")
