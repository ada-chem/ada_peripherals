# import libraries
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from skimage import feature
from matplotlib import cm


# definition: RGB to Gray
def rgb2gray(rgb):
    return np.dot(rgb[..., :3], [0.299, 0.587, 0.144])


# read raw image (color)
img = mpimg.imread('film_03.png')
# convert RGB to Gray
gray = rgb2gray(img)

# initialise figure
fig = plt.figure(figsize=(10.5, 7))
# define subplot for raw image
ax = fig.add_subplot(2, 3, 1)
# show raw image
ax.imshow(img)
# remove axis
ax.axis('off')
# set title
ax.set_title('Raw image', fontsize=14)

# define subplot for gray image
ax = fig.add_subplot(2, 3, 2)
# show gray image
ax.imshow(gray, cmap=cm.gray)
# remove axis
ax.axis('off')
# set title
ax.set_title('Gray image', fontsize=14)

# define sigma value (parameter for crack recognition)
sigma_value = [0.10, 0.40, 0.70, 1.0]

# loop over the sigma values
for countSigma in range(4):
    # find edges (BOOL)
    edges = feature.canny(gray, sigma=sigma_value[countSigma])
    # convert BOOL to BIN
    imgEdges = edges.astype(int)

    # define subplot for each sigma values
    ax = fig.add_subplot(2, 3, countSigma+3)
    # show gray image for each sigma values
    ax.imshow(imgEdges, cmap=cm.gray)
    # remove axis
    ax.axis('off')

    # initialise total number of 'crack' pixels
    totalCrackPixel = 0
    # find numbers of pixels in row and column
    row, column = imgEdges.shape
    # calculate total number of pixels
    totalPixel = row * column

    # loop over row and columns
    for countRow in range(row):
        for countColumn in range(column):
            # if pixel intensity is '1'
            if imgEdges[countRow, countColumn] == 1:
                # determine number of 'crack' pixels
                totalCrackPixel = totalCrackPixel + 1

    # calculate score (not damaged percentage)
    score = (totalCrackPixel * 100 / totalPixel)
    # print score
    print("Score = %0.1f %%" % score)
    # set title for each sigma values
    ax.set_title('$\sigma=$ %0.1f (%0.1f %%)' % (sigma_value[countSigma], score), fontsize=14)

fig.tight_layout()
# save figure
# plt.savefig('crackId.png', format="png", transparent="True", dpi=200)
plt.show()
