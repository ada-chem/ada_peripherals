# import libraries
import math
import numpy as np
from PIL import Image
from skimage.feature import greycomatrix, greycoprops
import matplotlib.pyplot as plt
import matplotlib as mpl


def sample_eval(filepath):
    mpl.rcParams['grid.linestyle'] = "--"
    plt.rcParams['axes.axisbelow'] = True

    # open image then convert in gray scale
    image_gray = Image.open(filepath).convert("L")
    arr = np.asarray(image_gray)
    # find size of array (image)
    x_size, y_size = arr.shape

    # set current pixel range (lower/upper values found in raw gray image)
    old_min = arr.min()
    old_max = arr.max()
    # estimate current range
    old_range = old_max - old_min

    # set new pixel range, to take into account the whole gray scale (0 -> 255)
    new_min = 0
    new_max = 255
    # estimate new range
    new_range = new_max - new_min
    # initialise new array
    new_arr = np.zeros((y_size, x_size))

    # loop over each pixel
    for countRow in range(x_size):
        for countColumn in range(y_size):
            # estimate ratio of current image
            scale = (arr[countRow, countColumn] - old_min) / old_range
            # estimate new array, ranging from 0 to 255
            new_arr[countRow, countColumn] = (new_range * scale) + new_min

    # initialise figure canvas
    fig = plt.figure(figsize=(11, 3.3))

    # define subplots
    (ax1, ax2, ax3) = fig.subplots(1, 3)
    # display gray image on the left
    gray_image_plot = ax1.imshow(arr, interpolation="nearest", cmap="gray", vmin=0, vmax=255)
    # define colorbar
    cbar = plt.colorbar(gray_image_plot, ax=ax1, fraction=0.046, pad=0.04)
    # set tick label size
    cbar.ax.tick_params(labelsize=10)
    # set colorbar and x-y labels
    cbar.set_label("Channel #", fontsize=10)
    ax1.set_xlabel("Row # (pixel)", fontsize=10)
    ax1.set_ylabel("Column # (pixel)", fontsize=10)
    # minor ticks enabled
    ax1.minorticks_on()
    # set title
    ax1.set_title("Gray image", fontsize=10)

    # grid enabled
    ax2.grid()
    # minor ticks enabled
    ax2.minorticks_on()
    # display histogram on the right and save variables
    count, bins, _ = ax2.hist(arr.ravel(),
                              bins=256,
                              range=(0, 256),
                              color="darkolivegreen",
                              align="left",
                              edgecolor="black",
                              linewidth=0.25)

    non_zero_list = np.nonzero(count)
    coord_bin_min = non_zero_list[0][0]
    coord_bin_max = non_zero_list[0][-1]
    print('coord_bin_min = %d' % coord_bin_min)
    print('coord_bin_max = %d' % coord_bin_max)

    # initialise variables
    sum_max = 0
    # loop over bins
    for countBin in range(coord_bin_min, coord_bin_max + 1):
        # count > 0
        if count[countBin] > 0:
            # increase sum_max by count value
            sum_max = sum_max + count[countBin]

    number_bins_occupied = coord_bin_max - coord_bin_min + 1

    # calculate mean value of max bar values
    mean_max = sum_max / number_bins_occupied
    # draw line of mean_max value on histogram
    ax2.axhline(mean_max, color='darkorange', linestyle='dashed', linewidth=2)

    # number of data points
    n = sum_max
    # initialise sum_x
    sum_x = 0
    # loop over bins
    for countBin in range(coord_bin_min, coord_bin_max + 1):
        # sum over all probabilities
        sum_x = sum_x + (countBin * count[countBin])

    # estimate bin mean value
    bin_mean = sum_x / n
    # initialise sum_sqrt
    sum_sqrt = 0
    # loop over bins
    for countBin in range(coord_bin_min, coord_bin_max + 1):
        # estimate square root
        sum_sqrt = sum_sqrt + count[countBin] * ((countBin - bin_mean) ** 2)

    # calculate variance
    sigma_sqrt = sum_sqrt / n
    # calculate standard deviation
    sigma = math.sqrt(sigma_sqrt)

    # calculate total number of pixels
    total_pixel = x_size * y_size

    # print results
    print('size = %d x %d' % (x_size, y_size))
    print('mean max = %d' % mean_max)
    print('sum_max = %d' % sum_max)
    print('total pixel # = %d' % total_pixel)
    print('sigma = %.2f' % sigma)

    # set tick format to scientific notation
    ax2.ticklabel_format(style="sci", axis="y", scilimits=(0, 0), labelsize=10)
    # determine aspect ratio
    asp = np.diff(ax2.get_xlim())[0] / np.diff(ax2.get_ylim())[0]
    # set aspect ratio
    ax2.set_aspect(asp)
    # set x-y labels
    ax2.set_xlabel("Channel #", fontsize=10)
    ax2.set_ylabel("Pixel count", fontsize=10)
    # set title
    ax2.set_title("Channel distribution", fontsize=10)

    # tight layout
    fig.tight_layout()
    # adjust space between subplots
    fig.subplots_adjust(wspace=0.5)
    # save output figure
    # plt.savefig("grayChannels.png", format="png", transparent="True", dpi=200)
    # plt.savefig("grayChannels.eps", format="eps", transparent="True", dpi=150)
    # plot output figure

    # test GLCM
    patch_size = 85
    image = arr

    areas = [(0, 0), (85, 0), (170, 0), (255, 0), (340, 0), (425, 0), (510, 0), (595, 0),
             (0, 85), (85, 85), (170, 85), (255, 85), (340, 85), (425, 85), (510, 85), (595, 85),
             (0, 170), (85, 170), (170, 170), (255, 170), (340, 170), (425, 170), (510, 170), (595, 170),
             (0, 255), (85, 255), (170, 255), (255, 255), (340, 255), (425, 255), (510, 255), (595, 255),
             (0, 340), (85, 340), (170, 340), (255, 340), (340, 340), (425, 340), (510, 340), (595, 340),
             (0, 425), (85, 425), (170, 425), (255, 425), (340, 425), (425, 425), (510, 425), (595, 425),
             (0, 510), (85, 510), (170, 510), (255, 510), (340, 510), (425, 510), (510, 510), (595, 510),
             (0, 595), (85, 595), (170, 595), (255, 595), (340, 595), (425, 595), (510, 595), (595, 595)]

    patches = []
    for loc in areas:
        patches.append(image[loc[0]:loc[0] + patch_size, loc[1]:loc[1] + patch_size])

    # compute some GLCM properties each patch
    xs = []
    ys = []
    for patch in patches:
        glcm = greycomatrix(patch, [1], [0], 256, symmetric=True, normed=True)
        xs.append(greycoprops(glcm, 'dissimilarity')[0, 0])
        ys.append(greycoprops(glcm, 'correlation')[0, 0])

    # Initialise count and threshold
    count = 0
    threshold_xs = 1.0
    threshold_ys = 0.9
    # display original image with areas of patches
    for (y, x) in areas:
        if (xs[count] <= threshold_xs) and (ys[count] >= threshold_ys):
            ax1.plot(x + patch_size / 2, y + patch_size / 2, 'gs', color='green')
            count = count + 1
        elif (xs[count] <= threshold_xs) and (ys[count] < threshold_ys):
            ax1.plot(x + patch_size / 2, y + patch_size / 2, 'gs', color='blue')
            count = count + 1
        elif xs[count] > threshold_xs:
            ax1.plot(x + patch_size / 2, y + patch_size / 2, 'gs', color='red')
            count = count + 1

    for count in range(0, 64):
        if (xs[count] <= threshold_xs) and (ys[count] >= threshold_ys):
            ax3.plot(xs[count], ys[count], 'go', color='green', label='areas')
        elif (xs[count] <= threshold_xs) and (ys[count] < threshold_ys):
            ax3.plot(xs[count], ys[count], 'go', color='blue', label='areas')
        elif xs[count] > threshold_xs:
            ax3.plot(xs[count], ys[count], 'go', color='red', label='areas')

    ax3.set_xlabel('Dissimilarity')
    ax3.set_ylabel('Correlation')
    ax3.set_title("GLCM", fontsize=10)
    # ax3.legend()
    ax3.set_aspect('auto')

    plt.show()


if __name__ == "__main__":
    sample_eval("film_02.png")
