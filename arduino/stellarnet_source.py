"""
The following code is to control a 20180928 Files from StellarNet DT2000 Light Source through an Arduino Uno Rev3.
The code to be written to the arduino is commented at the bottom of this script.
"""

import serial
import time

class DT200_arduino(object):

    # Instantiate the connection to the Arduino
    def __init__(self, port='COM7', baud=9600):

        # Store connection to Arduino
        self.conn = serial.Serial(port, baud)

        # Set timeout to be 5 seconds.
        self.conn.timeout = 5  # Seconds

    def shutter_on(self):

        # Turn on shutter
        self._write('P6')

    def uv_on(self):

        self._write('P2')

    def vis_on(self):

        self._write('P3')

    # Check the status of a pin
    def _status(self, pin):

        # Read status on arduino
        self._write(f'R{pin}')

        # Read result from serial
        response = self._read(size=3)

        print(response)


    # Send a string to the instrument
    def _write(self, command):

        # Send command
        self.conn.write(command.encode())

    # Read data from arduino
    def _read(self, size=3):

        # Read
        return self.conn.read(size)


if __name__ == "__main__":

    # Make connection
    ref = DT200_arduino()
    while True:
        ref.shutter_on()
        time.sleep(2)
        ref.vis_on()
        time.sleep(2)

# Below is the C code to be written to the arduino
# int pin; // 2 - 8
# char set; // P for pulse, F for float, G for ground, R for read
# int data; // 1 or 0 back from sensor
#
# void setup() {
#   // put your setup code here, to run once:
#   Serial.begin(9600); // Initiate serial communication
#   Serial.setTimeout(100); // Set serial timeout limit
#   pinMode(6, INPUT); // Initiate pin 2 for input
# }
#
# void loop() {
#   // put your main code here, to run repeatedly:
#   if (Serial.available() > 0) { // If a command has been sent, begin reading serial character by character
#     set = Serial.read(); // P for pulse, F for float, G for ground
#     delay(5); // This delay may be needed to have work properly.
#     pin = Serial.parseInt(); // 2 - 8
#     delay(5); // If not delayed, next character is not correctly read.
#
#     // write.
#     switch (set) {
#       case 'F': // Float pin.
#         pinMode(pin, INPUT); // Float pin.
#
#       case 'G': // Ground pin
#         pinMode(pin, OUTPUT); // Set pin to output mode.
#         digitalWrite(pin, LOW); // Ground pin.
#
#       case 'P': // Pulse
#         pinMode(pin, OUTPUT); // Set pin to output mode.
#         digitalWrite(pin, LOW); // Ground pin.
#         delay(50); // Wait 50 ms.
#         pinMode(pin, INPUT); // Float pin.
#
#       case 'R': // Read status of pin.
#         pinMode(pin, INPUT); // Set pin to input mode.
#         delay(10); // Wait 10 ms for stability.
#         int data = digitalRead(pin); // Get status of pin as 1 or 0.
#         Serial.println(data); // Send data back over serial.
#         delay(10); // Wait 10 ms for stability.
#       };
#   };
# };