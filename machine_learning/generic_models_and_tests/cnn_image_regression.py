import tensorflow as tf
import numpy as np
import edward as ed
import cv2
from scipy.stats import mode
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import os
import ada_peripherals
import imutils
from sklearn.metrics import r2_score
import ada_core
from ada_peripherals.machine_learning.generic_models_and_tests.cnn_image_categorical import load_data_set, generator,\
    load_images_from_folder_no_label

sns.set_context('paper', font_scale = 1.5, rc = {'lines.linewidth': 2})
sns.set_style('ticks')

uncertainty_colors = sns.color_palette('Greys', 256)
colors = sns.color_palette('RdYlBu', 256)

set_colors = sns.color_palette('PRGn', 7)
train_set_color = set_colors[0]
valid_set_color = set_colors[-1]

# create a regression bayesian model

def load_data_set(pickle_data_path,
                  img_height,
                  img_width,
                  number_of_channels,
                  train_ratio=0.7,
                  valid_ratio=0.9,
                  ):
    # dataset path is the path to the pickle file
    # train_ratio = the percentage as a float, so that betweeb 0 and train_ratio, is the range of the dataset to use
    # for training
    # valid_ratio = the percentage as a float, so that between train_ratio and valid_ratio, is the range of the
    # dataset to use for validation
    # number of channels; 1 if grayscale, 3 if rgb
    data = pickle.load(open(pickle_data_path, 'rb'))
    unshuffled_features = data['features']
    unshuffled_targets = data['targets']

    # shuffle features and targets the same way
    permutation = np.random.permutation(unshuffled_features.shape[0])
    features = unshuffled_features[permutation]
    targets = unshuffled_targets[permutation]

    train_features = []
    valid_features = []
    test_features = []
    train_targets = []
    valid_targets = []
    test_targets = []

    for i in range(0, int(len(features) * train_ratio)):
        train_features.append(features[i])
        train_targets.append(targets[i])
    for i in range(int(len(features) * train_ratio), int(len(features) * valid_ratio)):
        valid_features.append(features[i])
        valid_targets.append(targets[i])
    for i in range(int(len(features) * valid_ratio), len(features)):
        test_features.append(features[i])
        test_targets.append(targets[i])

    train_targets = np.asarray(train_targets, dtype=np.float32)
    valid_targets = np.asarray(valid_targets, dtype=np.float32)
    test_targets = np.asarray(test_targets, dtype=np.float32)

    train_targets = np.reshape(train_targets, (len(train_targets), 1))
    valid_targets = np.reshape(valid_targets, (len(valid_targets), 1))
    test_targets = np.reshape(test_targets, (len(test_targets), 1))

    train_features = np.asarray(train_features, dtype=np.float32)
    valid_features = np.asarray(valid_features, dtype=np.float32)
    test_features = np.asarray(test_features, dtype=np.float32)

    train_features = np.reshape(train_features, (len(train_features), img_height, img_width, number_of_channels))
    # check height and width is correct order
    valid_features = np.reshape(valid_features, (len(valid_features), img_height, img_width, number_of_channels))
    test_features = np.reshape(test_features, (len(test_features), img_height, img_width, number_of_channels))

    return train_features, train_targets, valid_features, valid_targets, test_features, test_targets


class ImageCNNModelRegression():  # todo edit this
    def __init__(self,
                 batch_size,
                 features_shape,
                 targets_shape,
                 model_folder_path,  # path to folder for model
                 mlp_size=24,
                 learning_rate=1e-3,
                 loss_threshold=0.001,
                 ):
        self.batch_size = batch_size
        self.features_shape = features_shape  # should be in the style of shape so is something like [number of,
        # rows of pixels, columns of pixels, channels]
        self.targets_shape = targets_shape  # should be something like [number of, number of labels]
        self.model_folder_path = model_folder_path
        self.mlp_size = mlp_size
        self.learning_rate = learning_rate
        self.train_features = None
        self.train_targets = None
        self.valid_features = None
        self.valid_targets = None
        self.test_features = None
        self.test_targets = None
        self.loss_threshold = loss_threshold

    def set_data(self,
                 train_features,
                 train_targets,
                 valid_features,
                 valid_targets,
                 test_features,
                 test_targets):
        self.train_features = train_features
        self.train_targets = train_targets
        self.valid_features = valid_features
        self.valid_targets = valid_targets
        self.test_features = test_features
        self.test_targets = test_targets

    def build_model(self,
                    filter_size=3,
                    conv_hidden=3,
                    stride=1,
                    train_iters=10**8,
                    sigma=0.01,
                    ):
        # put hyper parameters here

        FILTER_SIZE = filter_size
        CONV_HIDDEN = conv_hidden
        STRIDE = stride
        LEARNING_RATE = self.learning_rate
        TRAIN_ITERS = train_iters
        SIGMA = sigma
        MLP_SIZE = self.mlp_size

        x_ph = tf.placeholder(tf.float32,
                              [None, self.features_shape[1], self.features_shape[2],
                               self.features_shape[3]],
                              name='x_ph')
        y_ph = tf.placeholder(tf.float32,
                              [None, self.targets_shape[1]],  # todo change this so the name is instead
                              # todo train_targets_shape, and do the same for features above
                              name='y_ph')
        keep_prob = tf.placeholder(tf.float32, name='keep_prob')

        # is_training = tf.placeholder(tf.bool,
        #                              name='is_training')

        conv_activation = lambda x_: tf.nn.leaky_relu(x_, 0.2)
        mlp_activation = lambda x_: tf.nn.leaky_relu(x_, 0.2)

        weights_conv_0 = tf.get_variable('weights_conv_0', [FILTER_SIZE, FILTER_SIZE, self.features_shape[-1],
                                                            CONV_HIDDEN * self.features_shape[-1]],
                                         initializer=tf.random_normal_initializer(0., SIGMA))
        bias_conv_0 = tf.get_variable('bias_conv_0', [CONV_HIDDEN * self.features_shape[-1]],
                                      initializer=tf.random_normal_initializer(0., SIGMA))

        weights_conv_1 = tf.get_variable('weights_conv_1',
                                         [FILTER_SIZE, FILTER_SIZE, CONV_HIDDEN * self.features_shape[-1],
                                          2 * CONV_HIDDEN * self.features_shape[-1]],
                                         initializer=tf.random_normal_initializer(0., SIGMA))
        bias_conv_1 = tf.get_variable('bias_conv_1', [2 * CONV_HIDDEN * self.features_shape[-1]],
                                      initializer=tf.random_normal_initializer(0., SIGMA))

        weights_conv_2 = tf.get_variable('weights_conv_2',
                                         [FILTER_SIZE, FILTER_SIZE, 2 * CONV_HIDDEN * self.features_shape[-1],
                                          2 * CONV_HIDDEN * self.features_shape[-1]],
                                         initializer=tf.random_normal_initializer(0., SIGMA))
        bias_conv_2 = tf.get_variable('bias_conv_2', [2 * CONV_HIDDEN * self.features_shape[-1]],
                                      initializer=tf.random_normal_initializer(0., SIGMA))


        conv_0 = tf.nn.conv2d(x_ph, weights_conv_0, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        proc_0 = conv_activation(conv_0 + bias_conv_0)
        # l2_0 = tf.nn.l2_normalize(proc_0)
        pool_0 = tf.nn.max_pool(proc_0, [1, FILTER_SIZE, FILTER_SIZE, 1], [1, FILTER_SIZE, FILTER_SIZE, 1],
                                padding='VALID')

        conv_1 = tf.nn.conv2d(pool_0, weights_conv_1, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        proc_1 = conv_activation(conv_1 + bias_conv_1)
        pool_1 = tf.nn.max_pool(proc_1, [1, FILTER_SIZE, FILTER_SIZE, 1], [1, FILTER_SIZE, FILTER_SIZE, 1],
                                padding='VALID')

        conv_2 = tf.nn.conv2d(pool_1, weights_conv_2, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        proc_2 = conv_activation(conv_2 + bias_conv_2)
        drop_2 = tf.nn.dropout(proc_2, keep_prob)
        pool_2 = tf.nn.max_pool(drop_2, [1, FILTER_SIZE, FILTER_SIZE, 1], [1, FILTER_SIZE, FILTER_SIZE, 1],
                                padding='VALID')

        cnn_output = tf.contrib.layers.flatten(pool_2)

        # make the MLP

        weights_0 = tf.get_variable('weights_0', [cnn_output.get_shape()[1], MLP_SIZE],
                                    initializer=tf.random_normal_initializer(0., SIGMA))
        bias_0 = tf.get_variable('bias_0', [MLP_SIZE], initializer=tf.random_normal_initializer(0., SIGMA))
        weights_1 = tf.get_variable('weights_1', [MLP_SIZE, self.targets_shape[1]],
                                    initializer=tf.random_normal_initializer(0., SIGMA))
        bias_1 = tf.get_variable('bias_1', [self.targets_shape[1]],
                                 initializer=tf.random_normal_initializer(0., SIGMA))

        inter_0 = tf.matmul(cnn_output, weights_0) + bias_0
        inter_0 = mlp_activation(inter_0)
        inter_0 = tf.nn.dropout(inter_0, keep_prob)

        logits = tf.matmul(inter_0, weights_1) + bias_1
        logits = mlp_activation(logits)
        # logits = tf.nn.dropout(logits, keep_prob)

        y = logits
        print(y)
        # todo, should put this back in
        y = tf.identity(y, name='y')  # need to # todo
        # track name?

        # # defining my own accuracy
        # correct_prediction = tf.equal(tf.argmax(y), tf.argmax(y_ph))
        # with tf.name_scope('accuracy'):
        #     accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        # accuracy_summary = tf.summary.scalar('accuracy', accuracy)
        #
        # # define a cross entropy loss
        # with tf.name_scope('entropy_loss'):
        #     entropy_loss = tf.reduce_mean(- tf.reduce_sum(y_ph * tf.log(y_probs), reduction_indices=[0]),
        #                                   name='entropy_loss')
        # entropy_loss_summary = tf.summary.scalar('entropy_loss', entropy_loss)
        #
        # with tf.name_scope('train'):
        #     train_op_entropy = tf.train.AdamOptimizer(LEARNING_RATE).minimize(entropy_loss)
        #

        # define a quadratic loss
        quadratic_loss     = tf.reduce_mean(tf.square(y - y_ph))
        train_op_quadratic = tf.train.AdamOptimizer(LEARNING_RATE).minimize(quadratic_loss)

        # initialize computational graph
        sess = tf.Session()

        merged = tf.summary.merge_all()
        train_writer = tf.summary.FileWriter(f'{self.model_folder_path}/model_logs/train', sess.graph)
        test_writer = tf.summary.FileWriter(logdir=f'{self.model_folder_path}/model/logs/test')

        # if model already exists:
        if any(file_name.endswith('.meta') for file_name in os.listdir(self.model_folder_path)):
            print('load model')
            tf.get_default_graph()
            tf.global_variables_initializer()
            saver = tf.train.Saver()
            saver.restore(sess, self.model_folder_path + '/model.ckpt')
        else:
            print('no model to load')
            # otherwise initialize global variables
            with sess.as_default():
                tf.global_variables_initializer().run()

        train_accuracies, valid_accuracies = [], []
        train_errors, valid_errors = [], []
        batch_train_data = generator([self.train_features, self.train_targets], self.batch_size)

        lowest_loss_so_far = 1e6
        print(f'train until loss is < {self.loss_threshold}')

        for epoch in range(TRAIN_ITERS):

            train_x, train_y = next(batch_train_data)
            # loss, _, loss_summary, acc_summary = sess.run([quadratic_loss, train_op_quadratic, entropy_loss_summary,
            #                                                     accuracy,
            #                                                     accuracy_summary],
            #                                                     {x_ph: train_x, y_ph: train_y,
            #                                                     keep_prob: 0.85,
            #                                                     })
            # train_writer.add_summary(acc_summary, epoch)
            #  train_writer.add_summary(loss_summary, epoch)

            loss, _ = sess.run([quadratic_loss, train_op_quadratic], feed_dict={x_ph: train_x,
                                                                                y_ph: train_y,
                                                                                keep_prob: 0.85,
                                                                                })

            if epoch % 5 == 0:
                # get training set predictions
                train_preds = []
                for batch_index in range(len(self.train_features) // self.batch_size):
                    train_feature = self.train_features[batch_index * self.batch_size: (batch_index + 1) * self.batch_size]
                    train_pred = sess.run([y], {x_ph: train_feature, keep_prob: 0.85})[0]
                    train_preds.extend(train_pred)
                train_preds = np.array(train_preds)

                train_error = r2_score(self.train_targets, train_preds)
                train_errors.append(train_error)

                # get validation set predictions
                valid_preds = []
                for batch_index in range(len(self.valid_features) // self.batch_size):
                    valid_feature = self.valid_features[batch_index * self.batch_size: (batch_index + 1) * self.batch_size]
                    valid_pred = sess.run([y], {x_ph: valid_feature, keep_prob: 1.0})[0]
                    valid_preds.extend(valid_pred)
                valid_preds = np.array(valid_preds)

                valid_error = r2_score(self.valid_targets, valid_preds)
                valid_errors.append(valid_error)

                print(f'running epoch: {epoch} - loss: {loss}')
                print(f'train error: {train_error} - valid error: {valid_error}')

                if loss < lowest_loss_so_far:
                    lowest_loss_so_far = loss
                    saver = tf.train.Saver()
                    save_path = saver.save(sess, self.model_folder_path + '/model.ckpt')
                    print(f'Model saved in file: {save_path} with loss {loss}')

                if lowest_loss_so_far < self.loss_threshold:
                    print(f'Model has loss: {loss} < loss threshold: {self.loss_threshold} ')
                    print(f'training completed')
                    return


def predict_class(model_folder_path,
                  data_to_test,  # must be a list
                  show_prediction=False,  # whether you want to display the image and the predicted label or not
                  ):
    tf.reset_default_graph()
    with tf.Session() as sess:
        # Load meta graph and restore weights and biases
        print(model_folder_path)
        saver = tf.train.import_meta_graph(f'{model_folder_path}/model.ckpt.meta')  # todo
        saver.restore(sess, tf.train.latest_checkpoint(model_folder_path))
        # get graph of the session
        graph = sess.graph

        # recover placeholders
        x_ph = graph.get_tensor_by_name('x_ph:0')
        # is_training = graph.get_tensor_by_name('is_training:0')
        keep_prob = graph.get_tensor_by_name("keep_prob:0")
        y = graph.get_tensor_by_name('y:0')

        # get predictions for image
        # pred_probs = sess.run([y_probs], feed_dict={x_ph: data_to_test,
        #                                               keep_prob: 1.0,
        #                                               # is_training: False
        #                                               })[0]

        predicted_y = sess.run([y], {x_ph: data_to_test,  # list of predictions, the prediction but it is the hot key
                                    # prediction
                                 keep_prob: 1.0,
                                 # is_training: False
                                 })[0]

        print(f'predicted_y: {predicted_y}')

        def show_img(img, label=None):
            if label is not None:
                cv2.putText(img, label, (10, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)

            cv2.imshow('image', img)
            cv2.waitKey(0)

        if show_prediction is True:
            for idx, img in enumerate(data_to_test):
                show_img(data_to_test[idx], predicted_y[idx])  # label image with the prediction

        return predicted_y  # should be list of the predictions

######################################################################################################
 # todo edit this for now just comment this all out
# set up specific for the model - this one is a dummy one using images for daisies and roses

# root_dir = os.path.dirname(os.path.abspath(ada_peripherals.__file__))  # root directory of zippleback; use until get
# connection to NAS
# root dir is the path to the NAS directory
# root_dir = os.getenv('imagefolder')  # path to NAS
#
# machine_learning_dir = os.path.join(root_dir, 'machine_learning')
#
# directoy_for_this_model_in_nas_name = 'test_cnn_image_categorical'
# directory_for_this_model_in_nas = os.path.join(machine_learning_dir, directoy_for_this_model_in_nas_name)
#
# # to use resize = imutils.resize(frame, 320) for all the training images first to resize them before attatching a
# # label to them for training
#
# model_folder_name = 'cnn_model'
# model_folder_path = os.path.join(directory_for_this_model_in_nas, model_folder_name)
#
# data_images_folder_name = 'cnn_data'
# data_images_folder_path = os.path.join(directory_for_this_model_in_nas, data_images_folder_name)
#
# pickle_data_file_name = 'cnn_data_set.pkl'
# pickle_data_path = os.path.join(directory_for_this_model_in_nas, pickle_data_file_name)
#
# images_to_test_path = os.path.join(directory_for_this_model_in_nas, 'test_images')
#
# image_height = 200
# image_width = 300
# number_of_channels = 3
# number_of_classes = 2
#
# ######################################################################################################
#
# # todo edit this; for now just comment this all out
# def train():
#     label_data(data_set_path=data_images_folder_path,
#                pickle_file_path=pickle_data_path,
#                img_width=image_width,
#                img_height=image_height,)
#
#
#
#     train_features, train_targets,\
#         valid_features, valid_targets, \
#         test_features, test_targets = load_data_set(pickle_data_path=pickle_data_path,
#                                                     encoding_dictionary=encoding_dictionary,
#                                                     img_height=image_height,
#                                                     img_width=image_width,
#                                                     number_of_channels=number_of_channels)
#
#     cnn_image_model = ImageCNNModel(batch_size=20,
#                                     features_shape=train_features.shape,
#                                     targets_shape=train_targets.shape,
#                                     number_of_classes=2,
#                                     model_folder_path=model_folder_path,
#                                     loss_threshold=0.02,
#                                     )
#
#     cnn_image_model.set_data(train_features,
#                              train_targets,
#                              valid_features,
#                              valid_targets,
#                              test_features,
#                              test_targets,
#                              )
#
#     cnn_image_model.build_model()
#
# # ########
# # # test prediction
#
#
# def predict():
#     loaded_images = load_images_from_folder_no_label(path_to_folder=images_to_test_path,
#                                                      img_width=image_width,
#                                                      img_height=image_height,
#                                                      )
#     predict_class(model_folder_path=model_folder_path,
#                   data_to_test=loaded_images,
#                   encoding_dictionary=encoding_dictionary)
#
# # view_dataset()
# # train()
# # predict()
#
#  # tensorboard --logdir=/full_path_to_your_logs
# #     # tensorboard --logdir=logs/{model_name}
# #     # To use tensor board, need to use the terminal. In the pycharm terminal, you need to choose the model name
# #     # correctly in order to access the correct tensorboard logs; a folder within the logs folder is for the
# #     # logs for that specific model
# #
#
# # path to logs
# # tensorboard --logdir=Z:\machine_learning\test_cnn_image_categorical_solubility\cnn_model
