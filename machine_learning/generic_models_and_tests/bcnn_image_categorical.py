import tensorflow as tf
import numpy as np
import edward as ed
import cv2
from scipy.stats import mode
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import os
import ada_peripherals
import imutils
import ada_core

sns.set_context('paper', font_scale = 1.5, rc = {'lines.linewidth': 2})
sns.set_style('ticks')

uncertainty_colors = sns.color_palette('Greys', 256)
colors = sns.color_palette('RdYlBu', 256)

set_colors = sns.color_palette('PRGn', 7)
train_set_color = set_colors[0]
valid_set_color = set_colors[-1]

# create a categorical bayesian model


def generator(arrays, size_batch):
    starts = [0] * len(arrays)
    while True:
        batches = []
        for i, array in enumerate(arrays):
            start = starts[i]
            stop = start + size_batch
            diff = stop - array.shape[0]
            if diff <= 0:
                batch = array[start: stop]
                starts[i] += size_batch
            else:
                batch = np.concatenate([array[start:], array[:diff]])
                starts[i] = diff
            batches.append(batch)
        yield batches


def label_data(data_set_path,
               pickle_file_path,
               img_width,
               img_height,
               ):
    # data_set_path is a folder, the name of the folder with subfolders inside that contain all the images. The
    # subfolder names are the labels for the image
    # pickle_file_path is the path to the pickle file where the information will be written to
    # if new images are taken, then the old images can be moved to an 'old' folder so the data won't be copied over
    # twice, so the old pickle file will also need to be moved, and time stamped for when it wsa made

    features = []
    targets = []

    def load_images_from_folder(folder,
                                img_width,
                                img_height,
                                features,
                                targets,
                                ):  # doesnt turn to greyscale
        """
        Takes a folder in the and loads images and uses the folder name as the label, and appends them into separate
        arrays that are passed through (the features nad labels arrays), that then will be returned
        :param folder: the folder in the directory or the path to a folder from the directory to load images from
        :param int, img_height:
        :param int, img_width:
        :return: features and targets are arrays; features is an array of the images for a folder, and labels is an
            array with the labels for the corresponding images
        """

        for filename in os.listdir(folder):
            img = cv2.imread(os.path.join(folder, filename))

            split_up_folder = folder.split('\\')
            folder_name = split_up_folder[-1]

            img = cv2.resize(img, (img_width, img_height))  # should use the imutils version later
            # img = imutils.resize(image=img, width=img_width, height=img_height)
            img = np.asarray(img, dtype=np.float32)

            if img is not None:
                features.append(img)
                targets.append(f'{folder_name}')

        return features, targets

    for a_folder_of_images in os.listdir(data_set_path):  # iterate through all the folders in the data set path and
        #  append to the features and labels lists all the images from all the folders
        a_folder_of_images_path = os.path.join(data_set_path, a_folder_of_images)

        features, targets = load_images_from_folder(a_folder_of_images_path,
                                                   img_width,
                                                   img_height,
                                                   features,
                                                   targets,)

    features = np.asarray(features)
    targets = np.asarray(targets)

    # # shuffle everything
    # permutation = np.random.permutation(features.shape[0])
    # features = features[permutation]
    # targets = targets[permutation]

    # write the features and labels into a pickle file
    dataset = {'features': features, 'targets': targets}
    pickle.dump(dataset, open(pickle_file_path, 'wb'))


def load_data_set(pickle_data_path,
                  encoding_dictionary,  # dictionary to do encoding.
                  img_height,
                  img_width,
                  number_of_channels,
                  train_ratio=0.7,
                  valid_ratio=0.9,
                  ):
    # dataset path is the path to the pickle file
    # train_ratio = the percentage as a float, so that betweeb 0 and train_ratio, is the range of the dataset to use
    # for training
    # valid_ratio = the percentage as a float, so that between train_ratio and valid_ratio, is the range of the
    # dataset to use for validation
    # number of channels; 1 if grayscale, 3 if rgb
    data = pickle.load(open(pickle_data_path, 'rb'))
    unshuffled_features = data['features']
    unshuffled_targets = data['targets']

    # shuffle features and targets the same way
    permutation = np.random.permutation(unshuffled_features.shape[0])
    features = unshuffled_features[permutation]
    not_encoded_targets = unshuffled_targets[permutation]
    targets = []  # targets that will be encoded

    # one hot encode
    for value in not_encoded_targets:  # value is the string label
        target = np.zeros(len(encoding_dictionary.values()))  # make a [0., 0., 0.,...] list with the number of zeros
        #  equal to the number of classes
        value = encoding_dictionary[value]  # now the value is not the string of the class, but a number,
        # that will be related to the index in the target
        target[value] += 1.  # increase the value in the target at a specific index; this is the label for the class
        targets.append(target)

    train_features = []
    valid_features = []
    test_features = []
    train_targets = []
    valid_targets = []
    test_targets = []

    for i in range(0, int(len(features) * train_ratio)):
        train_features.append(features[i])
        train_targets.append(targets[i])
    for i in range(int(len(features) * train_ratio), int(len(features) * valid_ratio)):
        valid_features.append(features[i])
        valid_targets.append(targets[i])
    for i in range(int(len(features) * valid_ratio), len(features)):
        test_features.append(features[i])
        test_targets.append(targets[i])

    train_targets = np.asarray(train_targets, dtype=np.float32)
    valid_targets = np.asarray(valid_targets, dtype=np.float32)
    test_targets = np.asarray(test_targets, dtype=np.float32)

    train_targets = np.argmax(train_targets, axis=1)
    valid_targets = np.argmax(valid_targets, axis=1)
    test_targets = np.argmax(test_targets, axis=1)

    # train_targets = np.reshape(train_targets, (len(train_targets),))
    # valid_targets = np.reshape(valid_targets, (len(valid_targets),))
    # test_targets = np.reshape(test_targets, (len(test_targets),))

    train_features = np.asarray(train_features, dtype=np.float32)
    valid_features = np.asarray(valid_features, dtype=np.float32)
    test_features = np.asarray(test_features, dtype=np.float32)

    train_features = np.reshape(train_features, (len(train_features), img_height, img_width, number_of_channels))
    # check height and width is correct order
    valid_features = np.reshape(valid_features, (len(valid_features), img_height, img_width, number_of_channels))
    test_features = np.reshape(test_features, (len(test_features), img_height, img_width, number_of_channels))

    return train_features, train_targets, valid_features, valid_targets, test_features, test_targets


class BCNNModel():
    def __init__(self,
                 batch_size,
                 features_shape,
                 targets_shape,
                 number_of_classes,
                 model_folder_path,  # path to folder for model
                 mlp_size=24,
                 number_of_samples=10,
                 learning_rate=1e-3,
                 reg=1.0,
                 ):
        self.batch_size = batch_size
        self.features_shape = features_shape  # should be in the style of shape so is something like [number of,
        # rows of pixels, columns of pixels, channels]
        self.targets_shape = targets_shape  # should be something like [number of, label]
        self.number_of_classes = number_of_classes
        self.model_folder_path = model_folder_path
        self.mlp_size = mlp_size
        self.number_of_samples = number_of_samples
        self.learning_rate = learning_rate
        self.reg = reg
        self.train_features = None
        self.train_targets = None
        self.valid_features = None
        self.valid_targets = None
        self.test_features = None
        self.test_targets = None

    def set_data(self,
                 train_features,
                 train_targets,
                 valid_features,
                 valid_targets,
                 test_features,
                 test_targets):
        self.train_features = train_features
        self.train_targets = train_targets
        self.valid_features = valid_features
        self.valid_targets = valid_targets
        self.test_features = test_features
        self.test_targets = test_targets

    def build_model(self,
                    filter_size=3,
                    conv_hidden=3,
                    stride=1,
                    prefactor=-5.0,
                    train_iters=10**8,
                    ):
        # put hyper parameters here

        FILTER_SIZE = filter_size
        REG = self.reg
        CONV_HIDDEN = conv_hidden
        STRIDE = stride
        PREFACTOR = prefactor
        LEARNING_RATE = self.learning_rate
        TRAIN_ITERS = train_iters
        NUM_SAMPLES = self.number_of_samples

        x_ph = tf.placeholder(tf.float32,
                              [None, self.features_shape[1], self.features_shape[2], self.features_shape[3]],
                              name='x_ph')
        y_ph = tf.placeholder(tf.int32,
                              [None, ],
                              name='y_ph')
        # is_training = tf.placeholder(tf.bool,
        #                              name='is_training')

        conv_activation = lambda x_: tf.nn.leaky_relu(x_, 0.2)
        mlp_activation = lambda x_: tf.nn.leaky_relu(x_, 0.2)

        # the bcnn:[filter, filter, input, output]
        weight_shape = [FILTER_SIZE, FILTER_SIZE, self.features_shape[-1], CONV_HIDDEN * self.features_shape[-1]]
        weights_conv_0 = ed.models.Normal(loc=tf.zeros(weight_shape), scale=REG * tf.ones(weight_shape))
        bias_conv_0 = ed.models.Normal(loc=tf.zeros(weight_shape[-1]), scale=REG * tf.ones(weight_shape[-1]))

        weight_shape = [FILTER_SIZE, FILTER_SIZE, CONV_HIDDEN * self.features_shape[-1],
                        2 * CONV_HIDDEN * self.features_shape[-1]]
        weights_conv_1 = ed.models.Normal(loc=tf.zeros(weight_shape), scale=REG * tf.ones(weight_shape))
        bias_conv_1 = ed.models.Normal(loc=tf.zeros(weight_shape[-1]), scale=REG * tf.ones(weight_shape[-1]))

        conv_0 = tf.nn.conv2d(x_ph, weights_conv_0, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        # batch_0 = tf.layers.batch_normalization(conv_0,
        #                                         training=is_training
        #                                         )
        #
        proc_0 = conv_activation(conv_0 + bias_conv_0)
        pool_0 = tf.nn.max_pool(proc_0, [1, FILTER_SIZE, FILTER_SIZE, 1], [1, FILTER_SIZE, FILTER_SIZE, 1],
                                padding='VALID')

        conv_1 = tf.nn.conv2d(pool_0, weights_conv_1, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        # batch_1 = tf.layers.batch_normalization(conv_1,
        #                                         training=is_training
        #                                         )
        # proc_1 = conv_activation(batch_1 + bias_conv_1)
        proc_1 = conv_activation(conv_1 + bias_conv_1)
        pool_1 = tf.nn.max_pool(proc_1, [1, FILTER_SIZE, FILTER_SIZE, 1], [1, FILTER_SIZE, FILTER_SIZE, 1],
                                padding='VALID')

        cnn_output = tf.contrib.layers.flatten(pool_1)

        # for MLP: [input, output]
        MLP_SIZE = self.mlp_size
        NUM_CLASSES = self.number_of_classes

        weight_shape = [cnn_output.get_shape()[1], MLP_SIZE]
        weights_0 = ed.models.Normal(loc=tf.zeros(weight_shape), scale=REG * tf.ones(weight_shape))
        bias_0 = ed.models.Normal(loc=tf.zeros(weight_shape[-1]), scale=REG * tf.ones(weight_shape[-1]))

        weight_shape = [MLP_SIZE, NUM_CLASSES]
        weights_1 = ed.models.Normal(loc=tf.zeros(weight_shape), scale=REG * tf.ones(weight_shape))
        bias_1 = ed.models.Normal(loc=tf.zeros(weight_shape[-1]), scale=REG * tf.ones(weight_shape[-1]))

        inter_0 = tf.matmul(cnn_output, weights_0) + bias_0
        # batch_0 = tf.layers.batch_normalization(inter_0,
        # 									   training=is_training
        # 									   )
        inter_0 = mlp_activation(inter_0)

        logits = tf.matmul(inter_0, weights_1) + bias_1
        # logits = tf.layers.batch_normalization(logits,
        # 									   training=is_training
        # 									   )
        logits = mlp_activation(logits)

        y_probs = tf.nn.softmax(logits, name='y_probs')

        y = ed.models.Categorical(y_probs)


        # define the posterior

        weight_shape = [FILTER_SIZE, FILTER_SIZE, self.features_shape[-1], CONV_HIDDEN * self.features_shape[-1]]
        q_weights_conv_0 = ed.models.Normal(loc=tf.Variable(tf.zeros(weight_shape)),
                                            scale=tf.nn.softplus(tf.Variable(PREFACTOR * tf.ones(weight_shape))))
        q_bias_conv_0 = ed.models.Normal(loc=tf.Variable(tf.zeros(weight_shape[-1])),
                                         scale=tf.nn.softplus(tf.Variable(PREFACTOR * tf.ones(weight_shape[-1]))))

        weight_shape = [FILTER_SIZE, FILTER_SIZE, CONV_HIDDEN * self.features_shape[-1],
                        2 * CONV_HIDDEN * self.features_shape[-1]]
        q_weights_conv_1 = ed.models.Normal(loc=tf.Variable(tf.zeros(weight_shape)),
                                            scale=tf.nn.softplus(tf.Variable(PREFACTOR * tf.ones(weight_shape))))
        q_bias_conv_1 = ed.models.Normal(loc=tf.Variable(tf.zeros(weight_shape[-1])),
                                         scale=tf.nn.softplus(tf.Variable(PREFACTOR * tf.ones(weight_shape[-1]))))

        weight_shape = [cnn_output.get_shape()[1], MLP_SIZE]
        q_weights_0 = ed.models.Normal(loc=tf.Variable(tf.zeros(weight_shape)),
                                       scale=tf.nn.softplus(tf.Variable(PREFACTOR * tf.ones(weight_shape))))
        q_bias_0 = ed.models.Normal(loc=tf.Variable(tf.zeros(weight_shape[-1])),
                                    scale=tf.nn.softplus(tf.Variable(PREFACTOR * tf.ones(weight_shape[-1]))))

        weight_shape = [MLP_SIZE, NUM_CLASSES]
        q_weights_1 = ed.models.Normal(loc=tf.Variable(tf.zeros(weight_shape)),
                                       scale=tf.nn.softplus(tf.Variable(PREFACTOR * tf.ones(weight_shape))))
        q_bias_1 = ed.models.Normal(loc=tf.Variable(tf.zeros(weight_shape[-1])),
                                    scale=tf.nn.softplus(tf.Variable(PREFACTOR * tf.ones(weight_shape[-1]))))

        y_post = ed.copy(y, {weights_0: q_weights_0, bias_0: q_bias_0,
                             weights_1: q_weights_1, bias_1: q_bias_1,
                             weights_conv_0: q_weights_conv_0, bias_conv_0: q_bias_conv_0,
                             weights_conv_1: q_weights_conv_1, bias_conv_1: q_bias_conv_1},)

        # build inference
        inference_dict = {weights_0: q_weights_0, bias_0: q_bias_0,
                          weights_1: q_weights_1, bias_1: q_bias_1,
                          weights_conv_0: q_weights_conv_0, bias_conv_0: q_bias_conv_0,
                          weights_conv_1: q_weights_conv_1, bias_conv_1: q_bias_conv_1}

        optimizer = tf.train.AdamOptimizer(LEARNING_RATE)
        inference = ed.KLqp(inference_dict, data={y: y_ph})
        inference.initialize(optimizer=optimizer, n_iter=TRAIN_ITERS, var_list=tf.trainable_variables())

        # initialize computational graph
        sess = tf.Session()
        with sess.as_default():

            # if model already exists:
            if any(file_name.endswith('.meta') for file_name in os.listdir(self.model_folder_path)):
                print('load model')
                # initialize Saver
                saver = tf.train.Saver()
                # load model
                saver.restore(sess, self.model_folder_path)
                # get graph
                tf.get_default_graph()
            else:
                print('no model to load')
                # otherwise initialize global variables
                tf.global_variables_initializer().run()

            train_accuracies, valid_accuracies = [], []
            # batch_train_data = generator([self.train_features, self.train_targets], self.batch_size)

            for epoch in range(TRAIN_ITERS):

                inference.update(feed_dict={
                    x_ph: self.train_features,
                    y_ph: self.train_targets,
                    # is_training: True,
                })

                if epoch % 1 == 0:
                    print('running epoch: ', epoch)

                    train_pred_samples = y_post.sample(NUM_SAMPLES).eval(feed_dict={x_ph: self.train_features,
                                                                                    # is_training: False,
                                                                                    })
                    valid_pred_samples = y_post.sample(NUM_SAMPLES).eval(feed_dict={x_ph: self.valid_features,
                                                                                    # is_training: False
                                                                                    })

                    train_cats = np.squeeze(mode(train_pred_samples, axis=0)[0])
                    valid_cats = np.squeeze(mode(valid_pred_samples, axis=0)[0])

                    train_diff = train_cats - self.train_targets
                    valid_diff = valid_cats - self.valid_targets

                    train_accuracy = len(np.where(train_diff == 0)[0]) / float(len(train_cats))
                    train_accuracies.append(train_accuracy)
                    valid_accuracy = len(np.where(valid_diff == 0)[0]) / float(len(valid_cats))
                    valid_accuracies.append(valid_accuracy)

                    print(f'train accuracy: {train_accuracy}, valid accuracy: {valid_accuracy}')

                    if epoch % 3 == 0:
                        saver = tf.train.Saver()
                        save_path = saver.save(sess, self.model_folder_path + '/model.ckpt')
                        print(f'Model saved in file: {save_path}')


def load_images_from_folder_no_label(path_to_folder,
                                     img_width,
                                     img_height):
    images = []
    for filename in os.listdir(path_to_folder):
        img = cv2.imread(os.path.join(path_to_folder, filename))
        img = cv2.resize(img, (img_width, img_height))  # should use the imutils version later
        # img = imutils.resize(image=img, width=img_width, height=img_height)
        img = np.asarray(img, dtype=np.float32)
        if img is not None:
            images.append(img)

    images = np.asarray(images)

    return images


def predict_class(model_folder_path,
                  data_to_test,  # must be a dictionary
                  ):
    tf.reset_default_graph()
    with tf.Session() as sess:
        # Load meta graph and restore weights and biases
        print(model_folder_path)
        saver = tf.train.import_meta_graph(f'{model_folder_name}/model.ckpt.meta')  # todo
        saver.restore(sess, tf.train.latest_checkpoint(model_folder_path))
        # get graph of the session
        graph = sess.graph

        # recover placeholders
        x_ph = graph.get_tensor_by_name('x_ph:0')
        # is_training = graph.get_tensor_by_name('is_training:0')
        y_probs = graph.get_tensor_by_name('y_probs:0')

        feed_dict = {
            x_ph: data_to_test,
            # is_training: False,
        }

        prediction = sess.run(y_probs, feed_dict=feed_dict)

            # y_post.sample(NUM_SAMPLES).eval(feed_dict={x_ph: train_features,
            #                                                             is_training: False
            #                                                             })

        print(f'predictions: {prediction}')

        def show_img(img, label=None):
            if label is not None:
                cv2.putText(img, label, (10, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)

            cv2.imshow('image', img)
            cv2.waitKey(0)
            # cv2.destroyAllWindows()

        for idx, image in enumerate(data_to_test):
            show_img(image, prediction[idx])



######################################################################################################

# set up specific for the model

# root_dir = os.path.dirname(os.path.abspath(ada_peripherals.__file__))  # root directory of zippleback; use until get
# connection to NAS
# root dir is the path to the NAS directory
root_dir = os.getenv('imagefolder')  # path to NAS

this_script_name = 'bcnn_image_categorical_solubility'
image_directory_for_script_in_nas = os.path.join(root_dir, this_script_name)

# to use resize = imutils.resize(frame, 320) for all the training images first to resize them before attatching a
# label to them for training

model_folder_name = 'bcnn_model'
model_folder_path = os.path.join(image_directory_for_script_in_nas, model_folder_name)

data_images_folder_name = 'bcnn_data'
data_images_folder_path = os.path.join(image_directory_for_script_in_nas, data_images_folder_name)

pickle_data_file_name = 'bcnn_data_set.pkl'
pickle_data_path = os.path.join(image_directory_for_script_in_nas, pickle_data_file_name)

image_height = 200
image_width = 300
number_of_channels = 3
number_of_classes = 2

encoding_dictionary = {'lion': 0, 'tiger': 1}  # dictionary to do one hot encoding

######################################################################################################

# label_data(data_set_path=data_images_folder_path,
#            pickle_file_path=pickle_data_path,
#            img_width=image_width,
#            img_height=image_height,)
#
# dataset = pickle.load(open(pickle_data_path, 'rb'))
#
# train_features, train_targets,\
#     valid_features, valid_targets, \
#     test_features, test_targets = load_data_set(encoding_dictionary=encoding_dictionary,
#                                                 pickle_data_path=pickle_data_path,
#                                                 img_height=image_height,
#                                                 img_width=image_width,
#                                                 number_of_channels=number_of_channels)
#
# bcnn_image_model = BCNNModel(batch_size=10,
#                              features_shape=train_features.shape,
#                              targets_shape=train_targets.shape,
#                              number_of_classes=2,
#                              model_folder_path=model_folder_path,
#                              )
#
# bcnn_image_model.set_data(train_features,
#                           train_targets,
#                           valid_features,
#                           valid_targets,
#                           test_features,
#                           test_targets,
#                           )
#
# bcnn_image_model.build_model()
#
# ########
# # test prediction
#
loaded_images = load_images_from_folder_no_label(path_to_folder=os.path.join(image_directory_for_script_in_nas,
                                                                             'test_images'),
                                                 img_width=image_width,
                                                 img_height=image_height,
                                                 )

predict_class(model_folder_path=model_folder_path,
              data_to_test=loaded_images)