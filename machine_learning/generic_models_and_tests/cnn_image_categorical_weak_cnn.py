# SAME AS THE OTHER GENERIC MODEL, EXCEPT NOW INSTEAD THE CNN IS NOT AS STRONG
import tensorflow as tf
import numpy as np
import edward as ed
import cv2
from scipy.stats import mode
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
import os
import ada_peripherals
import imutils
import ada_core

sns.set_context('paper', font_scale = 1.5, rc = {'lines.linewidth': 2})
sns.set_style('ticks')

uncertainty_colors = sns.color_palette('Greys', 256)
colors = sns.color_palette('RdYlBu', 256)

set_colors = sns.color_palette('PRGn', 7)
train_set_color = set_colors[0]
valid_set_color = set_colors[-1]

# create a categorical bayesian model


def generator(arrays, size_batch):
    starts = [0] * len(arrays)
    while True:
        batches = []
        for i, array in enumerate(arrays):
            start = starts[i]
            stop = start + size_batch
            diff = stop - array.shape[0]
            if diff <= 0:
                batch = array[start: stop]
                starts[i] += size_batch
            else:
                batch = np.concatenate([array[start:], array[:diff]])
                starts[i] = diff
            batches.append(batch)
        yield batches


def label_data(data_set_path,
               pickle_file_path,
               img_width,
               img_height,
               ):
    # data_set_path is a folder, the name of the folder with subfolders inside that contain all the images. The
    # subfolder names are the labels for the image
    # pickle_file_path is the path to the pickle file where the information will be written to
    # if new images are taken, then the old images can be moved to an 'old' folder so the data won't be copied over
    # twice, so the old pickle file will also need to be moved, and time stamped for when it wsa made

    features = []
    targets = []

    def load_images_from_folder(folder,
                                img_width,
                                img_height,
                                features,
                                targets,
                                ):  # doesnt turn to greyscale
        """
        Takes a folder in the and loads images and uses the folder name as the label, and appends them into separate
        arrays that are passed through (the features nad labels arrays), that then will be returned
        :param folder: the folder in the directory or the path to a folder from the directory to load images from
        :param int, img_height:
        :param int, img_width:
        :return: features and targets are arrays; features is an array of the images for a folder, and labels is an
            array with the labels for the corresponding images
        """

        for filename in os.listdir(folder):
            img = cv2.imread(os.path.join(folder, filename))

            split_up_folder = folder.split('\\')
            folder_name = split_up_folder[-1]

            img = cv2.resize(img, (img_width, img_height))  # should use the imutils version later
            # img = imutils.resize(image=img, width=img_width, height=img_height)
            img = np.asarray(img)

            if img is not None:
                features.append(img)
                targets.append(f'{folder_name}')

        return features, targets

    for a_folder_of_images in os.listdir(data_set_path):  # iterate through all the folders in the data set path and
        #  append to the features and labels lists all the images from all the folders
        a_folder_of_images_path = os.path.join(data_set_path, a_folder_of_images)

        features, targets = load_images_from_folder(a_folder_of_images_path,
                                                   img_width,
                                                   img_height,
                                                   features,
                                                   targets,)

    features = np.asarray(features)
    targets = np.asarray(targets)

    # # shuffle everything
    # permutation = np.random.permutation(features.shape[0])
    # features = features[permutation]
    # targets = targets[permutation]

    # write the features and labels into a pickle file
    dataset = {'features': features, 'targets': targets}
    pickle.dump(dataset, open(pickle_file_path, 'wb'))


def load_data_set(pickle_data_path,
                  encoding_dictionary,  # dictionary to do encoding.
                  img_height,
                  img_width,
                  number_of_channels,
                  train_ratio=0.7,
                  valid_ratio=0.9,
                  ):
    # dataset path is the path to the pickle file
    # train_ratio = the percentage as a float, so that betweeb 0 and train_ratio, is the range of the dataset to use
    # for training
    # valid_ratio = the percentage as a float, so that between train_ratio and valid_ratio, is the range of the
    # dataset to use for validation
    # number of channels; 1 if grayscale, 3 if rgb
    data = pickle.load(open(pickle_data_path, 'rb'))
    unshuffled_features = data['features']
    unshuffled_targets = data['targets']

    # shuffle features and targets the same way
    permutation = np.random.permutation(unshuffled_features.shape[0])
    features = unshuffled_features[permutation]
    not_encoded_targets = unshuffled_targets[permutation]
    targets = []  # targets that will be encoded

    # one hot encode
    for value in not_encoded_targets:  # value is the string label
        target = np.zeros(len(encoding_dictionary.values()))  # make a [0., 0., 0.,...] list with the number of zeros
        #  equal to the number of classes
        value = encoding_dictionary[value]  # now the value is not the string of the class, but a number,
        # that will be related to the index in the target
        target[value] += 1.  # increase the value in the target at a specific index; this is the label for the class
        targets.append(target)

    train_features = []
    valid_features = []
    test_features = []
    train_targets = []
    valid_targets = []
    test_targets = []

    for i in range(0, int(len(features) * train_ratio)):
        train_features.append(features[i])
        train_targets.append(targets[i])
    for i in range(int(len(features) * train_ratio), int(len(features) * valid_ratio)):
        valid_features.append(features[i])
        valid_targets.append(targets[i])
    for i in range(int(len(features) * valid_ratio), len(features)):
        test_features.append(features[i])
        test_targets.append(targets[i])

    train_targets = np.asarray(train_targets, dtype=np.float32)
    valid_targets = np.asarray(valid_targets, dtype=np.float32)
    test_targets = np.asarray(test_targets, dtype=np.float32)

    # train_targets = np.argmax(train_targets, axis=1)
    # valid_targets = np.argmax(valid_targets, axis=1)
    # test_targets = np.argmax(test_targets, axis=1)

    # train_targets = np.reshape(train_targets, (len(train_targets),))
    # valid_targets = np.reshape(valid_targets, (len(valid_targets),))
    # test_targets = np.reshape(test_targets, (len(test_targets),))

    train_features = np.asarray(train_features, dtype=np.float32)
    valid_features = np.asarray(valid_features, dtype=np.float32)
    test_features = np.asarray(test_features, dtype=np.float32)

    train_features = np.reshape(train_features, (len(train_features), img_height, img_width, number_of_channels))
    # check height and width is correct order
    valid_features = np.reshape(valid_features, (len(valid_features), img_height, img_width, number_of_channels))
    test_features = np.reshape(test_features, (len(test_features), img_height, img_width, number_of_channels))

    return train_features, train_targets, valid_features, valid_targets, test_features, test_targets


class ImageCNNModel():
    def __init__(self,
                 batch_size,
                 features_shape,
                 targets_shape,
                 number_of_classes,
                 model_folder_path,  # path to folder for model
                 mlp_size=24,
                 learning_rate=1e-3,
                 loss_threshold=0.02,
                 ):
        self.batch_size = batch_size
        self.features_shape = features_shape  # should be in the style of shape so is something like [number of,
        # rows of pixels, columns of pixels, channels]
        self.targets_shape = targets_shape  # should be something like [number of, number of labels]
        self.number_of_classes = number_of_classes
        self.model_folder_path = model_folder_path
        self.mlp_size = mlp_size
        self.learning_rate = learning_rate
        self.train_features = None
        self.train_targets = None
        self.valid_features = None
        self.valid_targets = None
        self.test_features = None
        self.test_targets = None
        self.loss_threshold = loss_threshold

    def set_data(self,
                 train_features,
                 train_targets,
                 valid_features,
                 valid_targets,
                 test_features,
                 test_targets):
        self.train_features = train_features
        self.train_targets = train_targets
        self.valid_features = valid_features
        self.valid_targets = valid_targets
        self.test_features = test_features
        self.test_targets = test_targets

    def build_model(self,
                    filter_size=3,
                    conv_hidden=3,
                    stride=1,
                    train_iters=10**8,
                    sigma=0.01,
                    ):
        # put hyper parameters here

        FILTER_SIZE = filter_size
        CONV_HIDDEN = conv_hidden
        STRIDE = stride
        LEARNING_RATE = self.learning_rate
        TRAIN_ITERS = train_iters
        SIGMA = sigma
        MLP_SIZE = self.mlp_size

        x_ph = tf.placeholder(tf.float32,
                              [None, self.features_shape[1], self.features_shape[2],
                               self.features_shape[3]],
                              name='x_ph')
        y_ph = tf.placeholder(tf.float32,
                              [None, self.targets_shape[1]],  # todo change this so the name is instead
                              # todo train_targets_shape, and do the same for features above
                              name='y_ph')
        keep_prob = tf.placeholder(tf.float32, name='keep_prob')

        # is_training = tf.placeholder(tf.bool,
        #                              name='is_training')

        conv_activation = lambda x_: tf.nn.leaky_relu(x_, 0.2)
        mlp_activation = lambda x_: tf.nn.leaky_relu(x_, 0.2)

        weights_conv_0 = tf.get_variable('weights_conv_0', [FILTER_SIZE, FILTER_SIZE, self.features_shape[-1],
                                                            CONV_HIDDEN * self.features_shape[-1]],
                                         initializer=tf.random_normal_initializer(0., SIGMA))
        bias_conv_0 = tf.get_variable('bias_conv_0', [CONV_HIDDEN * self.features_shape[-1]],
                                      initializer=tf.random_normal_initializer(0., SIGMA))

        weights_conv_1 = tf.get_variable('weights_conv_1',
                                         [FILTER_SIZE, FILTER_SIZE, CONV_HIDDEN * self.features_shape[-1],
                                          2 * CONV_HIDDEN * self.features_shape[-1]],
                                         initializer=tf.random_normal_initializer(0., SIGMA))
        bias_conv_1 = tf.get_variable('bias_conv_1', [2 * CONV_HIDDEN * self.features_shape[-1]],
                                      initializer=tf.random_normal_initializer(0., SIGMA))

        weights_conv_2 = tf.get_variable('weights_conv_2',
                                         [FILTER_SIZE, FILTER_SIZE, 2 * CONV_HIDDEN * self.features_shape[-1],
                                          2 * CONV_HIDDEN * self.features_shape[-1]],
                                         initializer=tf.random_normal_initializer(0., SIGMA))
        bias_conv_2 = tf.get_variable('bias_conv_2', [2 * CONV_HIDDEN * self.features_shape[-1]],
                                      initializer=tf.random_normal_initializer(0., SIGMA))


        conv_0 = tf.nn.conv2d(x_ph, weights_conv_0, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        proc_0 = conv_activation(conv_0 + bias_conv_0)
        # l2_0 = tf.nn.l2_normalize(proc_0)
        pool_0 = tf.nn.max_pool(proc_0, [1, FILTER_SIZE, FILTER_SIZE, 1], [1, FILTER_SIZE, FILTER_SIZE, 1],
                                padding='VALID')

        # conv_1 = tf.nn.conv2d(pool_0, weights_conv_1, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        # proc_1 = conv_activation(conv_1 + bias_conv_1)
        # pool_1 = tf.nn.max_pool(proc_1, [1, FILTER_SIZE, FILTER_SIZE, 1], [1, FILTER_SIZE, FILTER_SIZE, 1],
        #                         padding='VALID')

        # conv_2 = tf.nn.conv2d(pool_1, weights_conv_2, strides=[1, STRIDE, STRIDE, 1], padding='VALID')
        # proc_2 = conv_activation(conv_2 + bias_conv_2)
        # drop_2 = tf.nn.dropout(proc_2, keep_prob)
        # pool_2 = tf.nn.max_pool(drop_2, [1, FILTER_SIZE, FILTER_SIZE, 1], [1, FILTER_SIZE, FILTER_SIZE, 1],
        #                         padding='VALID')

        cnn_output = tf.contrib.layers.flatten(pool_0)

        # make the MLP

        weights_0 = tf.get_variable('weights_0', [cnn_output.get_shape()[1], MLP_SIZE],
                                    initializer=tf.random_normal_initializer(0., SIGMA))
        bias_0 = tf.get_variable('bias_0', [MLP_SIZE], initializer=tf.random_normal_initializer(0., SIGMA))
        weights_1 = tf.get_variable('weights_1', [MLP_SIZE, self.targets_shape[1]],
                                    initializer=tf.random_normal_initializer(0., SIGMA))
        bias_1 = tf.get_variable('bias_1', [self.targets_shape[1]],
                                 initializer=tf.random_normal_initializer(0., SIGMA))

        inter_0 = tf.matmul(cnn_output, weights_0) + bias_0
        inter_0 = mlp_activation(inter_0)
        inter_0 = tf.nn.dropout(inter_0, keep_prob)

        logits = tf.matmul(inter_0, weights_1) + bias_1
        logits = mlp_activation(logits)
        # logits = tf.nn.dropout(logits, keep_prob)

        y_probs = tf.nn.softmax(logits, name='y_probs')
        y = tf.argmax(logits, axis=1, name='y')

        # defining my own accuracy
        correct_prediction = tf.equal(tf.argmax(y), tf.argmax(y_ph))
        with tf.name_scope('accuracy'):
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        accuracy_summary = tf.summary.scalar('accuracy', accuracy)

        # define a cross entropy loss
        with tf.name_scope('entropy_loss'):
            entropy_loss = tf.reduce_mean(- tf.reduce_sum(y_ph * tf.log(y_probs), reduction_indices=[0]),
                                          name='entropy_loss')
        entropy_loss_summary = tf.summary.scalar('entropy_loss', entropy_loss)

        with tf.name_scope('train'):
            train_op_entropy = tf.train.AdamOptimizer(LEARNING_RATE).minimize(entropy_loss)


        # initialize computational graph
        sess = tf.Session()

        merged = tf.summary.merge_all()
        train_writer = tf.summary.FileWriter(f'{self.model_folder_path}/model_logs/train', sess.graph)
        test_writer = tf.summary.FileWriter(logdir=f'{self.model_folder_path}/model/logs/test')

        # if model already exists:
        if any(file_name.endswith('.meta') for file_name in os.listdir(self.model_folder_path)):
            print('load model')
            tf.get_default_graph()
            tf.global_variables_initializer()
            saver = tf.train.Saver()
            saver.restore(sess, self.model_folder_path + '/model.ckpt')
        else:
            print('no model to load')
            # otherwise initialize global variables
            with sess.as_default():
                tf.global_variables_initializer().run()

        train_accuracies, valid_accuracies = [], []
        batch_train_data = generator([self.train_features, self.train_targets], self.batch_size)

        lowest_loss_so_far = 1e6

        for epoch in range(TRAIN_ITERS):

            train_x, train_y = next(batch_train_data)
            loss, _, loss_summary, acc, acc_summary = sess.run([entropy_loss, train_op_entropy, entropy_loss_summary,
                                                                accuracy,
                                                                accuracy_summary],
                                                                {x_ph: train_x, y_ph: train_y,
                                                                keep_prob: 0.85,
                                                                })
            train_writer.add_summary(acc_summary, epoch)
            train_writer.add_summary(loss_summary, epoch)

            if epoch % 50 == 0:
                print(f'running epoch: {epoch} - acc: {acc} - loss: {loss}')

                # get predictions for test images
                pred_probs = sess.run([y_probs], {x_ph: self.test_features,
                                                  keep_prob: 1.0,
                                                  # is_training: False
                                                  })[0]

                # # # Record summaries and test-set accuracy
                # # summary, acc = sess.run([merged, accuracy], {x_ph: self.test_features,
                # #                                   keep_prob: 1.0,
                # #                                   # is_training: False
                # #                                   })[0]
                # # test_writer.add_summary(summary, epoch)
                # # print('Accuracy at step %s: %s' % (epoch, acc))
                # #
                #
                # # get training predictions
                # train_preds = []
                # for batch_index in range(len(self.train_features) // self.batch_size):
                #     train_feature = self.train_features[batch_index * self.batch_size: (batch_index + 1) * self.batch_size]
                #     train_probs = sess.run([y_probs], {x_ph: train_feature,
                #                                        keep_prob: 1.0,
                #                                        # is_training: False
                #                                        })[0]
                #     train_preds.extend(train_probs)
                # train_preds = np.array(train_preds)
                # train_cats = np.around(train_preds)
                #
                # # # trying my own way to get accuracy
                # # for batch_index in range(len(self.train_features) // self.batch_size):
                # #     train_feature = self.train_features[batch_index * self.batch_size: (batch_index + 1) * self.batch_size]
                # #     train_acc = sess.run([y_probs], {x_ph: train_feature,
                # #                                        keep_prob: 1.0,
                # #                                        # is_training: False
                # #                                        })[0]
                # #
                # # print(f'train acc: {train_acc}')
                #
                # # get validation predictions
                # valid_preds = []
                # for batch_index in range(len(self.valid_features) // self.batch_size):
                #     valid_feature = self.valid_features[batch_index * self.batch_size: (batch_index + 1) * self.batch_size]
                #     valid_probs = sess.run([y_probs], {x_ph: valid_feature,
                #                                        keep_prob: 1.0,
                #                                        # is_training: False
                #                                        })[0]
                #     valid_preds.extend(valid_probs)
                # valid_preds = np.array(valid_preds)
                # valid_cats = np.around(valid_preds)
                # #
                # # # trying my own way to get accuracy
                # # for batch_index in range(len(self.valid_features) // self.batch_size):
                # #     valid_feature = self.valid_features[batch_index * self.batch_size: (batch_index + 1) * self.batch_size]
                # #     valid_acc = sess.run([accuracy], {x_ph: valid_feature,
                # #                                        keep_prob: 1.0,
                # #                                        # is_training: False
                # #                                        })[0]
                # # print(f'valid acc: {valid_acc}')
                # # get overall prediction accuracies
                #
                # train_comparison = np.sum(train_cats - self.train_targets, axis=1)
                # train_sum_is_zero = len(np.where(train_comparison == 0)[0])
                # train_accuracy = train_sum_is_zero / float(len(train_comparison))
                # train_accuracies.append(train_accuracy)
                #
                # valid_comparison = np.sum(valid_cats - self.valid_targets, axis=1)
                # valid_sum_is_zero = len(np.where(valid_comparison == 0)[0])
                # valid_accuracy = valid_sum_is_zero / float(len(valid_comparison))
                # valid_accuracies.append(valid_accuracy)

                # print(f'loss: {loss}, train accuracy: {train_accuracy}, valid accuracy: {valid_accuracy}')

                print(f'loss: {loss}')

                if loss < lowest_loss_so_far:
                    lowest_loss_so_far = loss
                    saver = tf.train.Saver()
                    save_path = saver.save(sess, self.model_folder_path + '/model.ckpt')
                    print(f'Model saved in file: {save_path} with loss {loss}')

                if lowest_loss_so_far < self.loss_threshold:
                    print(f'Model has loss: {loss} < loss threshold: {self.loss_threshold} ')
                    print(f'training completed')
                    return


def load_images_from_folder_no_label(path_to_folder,
                                     img_width,
                                     img_height):
    images = []
    for filename in os.listdir(path_to_folder):
        img = cv2.imread(os.path.join(path_to_folder, filename))
        img = cv2.resize(img, (img_width, img_height))  # should use the imutils version later
        # img = imutils.resize(image=img, width=img_width, height=img_height)
        img = np.asarray(img)
        if img is not None:
            images.append(img)


    images = np.asarray(images)

    return images


def predict_class(model_folder_path,
                  data_to_test,  # must be a list
                  encoding_dictionary,
                  show_prediction=False,  # Whether you want to show the image with prediction on it or not
                  ):
    tf.reset_default_graph()
    with tf.Session() as sess:
        # Load meta graph and restore weights and biases
        # print(model_folder_path)
        saver = tf.train.import_meta_graph(f'{model_folder_path}/model.ckpt.meta')  # todo
        saver.restore(sess, tf.train.latest_checkpoint(model_folder_path))
        # get graph of the session
        graph = sess.graph

        # recover placeholders
        x_ph = graph.get_tensor_by_name('x_ph:0')
        # is_training = graph.get_tensor_by_name('is_training:0')
        keep_prob = graph.get_tensor_by_name("keep_prob:0")
        y_probs = graph.get_tensor_by_name('y_probs:0')
        y = graph.get_tensor_by_name('y:0')


        # get predictions for image
        pred_probs = sess.run([y_probs], feed_dict={x_ph: data_to_test,
                                                      keep_prob: 1.0,
                                                      # is_training: False
                                                      })[0]

        pred_label = sess.run([y], {x_ph: data_to_test,  # list of predictions, the prediction but it is the hot key
                                    # prediction
                                 keep_prob: 1.0,
                                 # is_training: False
                                 })[0]

        # print(f'predictions: {pred_label}')  # prints a list of all the predictions for each of the images

        def show_img(img, label=None):
            if label is not None:
                cv2.putText(img, label, (10, 25), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)

            cv2.imshow('image', img)
            cv2.waitKey(0)

        if show_prediction is True:  # if want to show_prediction is True, then display the image with the prediction
            for idx, img in enumerate(data_to_test):
                the_pred_label_for_this_image = pred_label[idx]
                for label in encoding_dictionary:  # iterate through all the string labels in the encoding dictionary
                    if the_pred_label_for_this_image == encoding_dictionary[label]:  # if the predicted hot key label is
                        # equal to the (hot key label gotten after searching through the encoding dictionary using the
                        # string label), then the string associated with the hot key value has been found
                        show_img(data_to_test[idx], label)  # label is the actual string of what the prediction is. Now
                        # show the image with the label predicted on it
        # pred__probs is # should be a list of the probabilities of the predictions, but not saying what the
        #                 # prediction actually is
        # example of what pred_probs looks like:
        # pred_probs: [[1.0000000e+00 4.1800200e-13]
        #                 [1.0000000e+00 1.3581524e-12]]
        # example of pred_label:
        # pred_label: [0 0]
        return pred_probs, pred_label  # should be a list of predictions



######################################################################################################
if __name__ is '__main__':

    # set up specific for the model - this one is a dummy one using images for daisies and roses

    # root_dir = os.path.dirname(os.path.abspath(ada_peripherals.__file__))  # root directory of zippleback; use until get
    # connection to NAS
    # root dir is the path to the NAS directory
    root_dir = os.getenv('imagefolder')  # path to NAS

    machine_learning_dir = os.path.join(root_dir, 'machine_learning')

    directoy_for_this_model_in_nas_name = 'test_cnn_image_categorical'
    directory_for_this_model_in_nas = os.path.join(machine_learning_dir, directoy_for_this_model_in_nas_name)

    # to use resize = imutils.resize(frame, 320) for all the training images first to resize them before attatching a
    # label to them for training

    model_folder_name = 'cnn_model'
    model_folder_path = os.path.join(directory_for_this_model_in_nas, model_folder_name)

    data_images_folder_name = 'cnn_data'
    data_images_folder_path = os.path.join(directory_for_this_model_in_nas, data_images_folder_name)

    pickle_data_file_name = 'cnn_data_set.pkl'
    pickle_data_path = os.path.join(directory_for_this_model_in_nas, pickle_data_file_name)

    images_to_test_path = os.path.join(directory_for_this_model_in_nas, 'test_images')

    image_height = 200
    image_width = 300
    number_of_channels = 3
    number_of_classes = 2

    encoding_dictionary = {'daisy': 0, 'rose': 1}  # dictionary to do one hot encoding

    ######################################################################################################

    def view_dataset(pickle_data_path):
        dataset = pickle.load(open(pickle_data_path, 'rb'))
        for img in dataset['features']:
            cv2.imshow('i', img)
            cv2.waitKey(0)


    def train():
        label_data(data_set_path=data_images_folder_path,
                   pickle_file_path=pickle_data_path,
                   img_width=image_width,
                   img_height=image_height,)



        train_features, train_targets,\
            valid_features, valid_targets, \
            test_features, test_targets = load_data_set(pickle_data_path=pickle_data_path,
                                                        encoding_dictionary=encoding_dictionary,
                                                        img_height=image_height,
                                                        img_width=image_width,
                                                        number_of_channels=number_of_channels)

        cnn_image_model = ImageCNNModel(batch_size=20,
                                        features_shape=train_features.shape,
                                        targets_shape=train_targets.shape,
                                        number_of_classes=2,
                                        model_folder_path=model_folder_path,
                                        loss_threshold=0.02,
                                        )

        cnn_image_model.set_data(train_features,
                                 train_targets,
                                 valid_features,
                                 valid_targets,
                                 test_features,
                                 test_targets,
                                 )

        cnn_image_model.build_model()

    # ########
    # # test prediction


    def predict():
        loaded_images = load_images_from_folder_no_label(path_to_folder=images_to_test_path,
                                                         img_width=image_width,
                                                         img_height=image_height,
                                                         )
        predict_class(model_folder_path=model_folder_path,
                      data_to_test=loaded_images,
                      encoding_dictionary=encoding_dictionary)

    # view_dataset()
    # train()
    predict()

     # tensorboard --logdir=/full_path_to_your_logs
    #     # tensorboard --logdir=logs/{model_name}
    #     # To use tensor board, need to use the terminal. In the pycharm terminal, you need to choose the model name
    #     # correctly in order to access the correct tensorboard logs; a folder within the logs folder is for the
    #     # logs for that specific model
    #

    # path to logs
    # tensorboard --logdir=Z:\machine_learning\test_cnn_image_categorical_solubility\cnn_model
