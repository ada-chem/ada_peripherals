"""
Collection of functions to help with the creation and usage of the machine learning version of the liquid
level/meniscus finder - this is the regression version - do not use this
"""
import cv2
import pickle
import numpy as np
from ada_peripherals.vision.liquid_level import LiquidLevel, NoMeniscusFound
import ada_peripherals
import tensorflow as tf
import edward as ed
from scipy.stats import mode
import matplotlib.pyplot as plt
import seaborn as sns
import os
import ada_peripherals
import imutils
import ada_core
from ada_peripherals.machine_learning.generic_models_and_tests.cnn_image_regression import ImageCNNModelRegression,  \
    predict_class, load_data_set
from ada_peripherals.machine_learning.generic_models_and_tests.cnn_image_categorical import  \
    load_images_from_folder_no_label

# this function will iterate through the images in a folder, path of folder needs to be specified, then the user
# needs to specify where to look for the meniscus (only the view of the vial), then the function will run the
# first generation of the liquid level finding algorithm on it all the images in the folder (but really cropped images
#  based on the region of interest the user selected), then it will save,
# to a separate folder, all the images, but with lines drawn on them for where the found meniscus was.


def run_liquid_level_algorithm_on_folder_of_images(image_folder_path,  # path to folder of images to run liquid level on
                                                   pickle_file_path,  # path to pickle file to dump everything into
                                                   save_drawn_image_folder_path,  # path to folder to save images
                                                   # with the drawn version with where the meniscus is at to so user
                                                   # can make sure the naive algorthm (liquid level version 1)
                                                   # labeled the images correctly
                                                   ):
    # load the images
    images, image_names = load_images_from_folder(folder=image_folder_path)

    print(f'number of images loaded: {len(image_names)}')

    features = images  # create a list of features, with the features being the images
    targets = []

    # display first image to user so user can select where the vial is aka the region of interest
    first_image = images[0]
    image_height, image_width, _ = first_image.shape  # find width and height of all the images

    # create an instance of the liquid level object to use, use the width of all the images (taken as the width of
    # the first image) to create the liquid level object
    liquid_level = LiquidLevel(camera=None, width=image_width)

    # load the first image, and use that as the image to select a region of interest from
    liquid_level.set_image_as_reference(img=first_image)
    first_image_as_edge_image = liquid_level.find_contour(fill=first_image)  # find the edge image of the first image
    liquid_level.ref_closed = first_image_as_edge_image  # nede to set the liquid level object to have a reference
    # 'closed' (should really be called edge) image, in order to properly view the image to select the region of
    # interest

    # allow used to select the region of interest to look for a meniscus
    liquid_level.select_frame()

    # iterate through all images, and pass the region of rest area to find the liquid level; essentially use
    # what there already is in the liquid_level script, and add the result of where the meniscus is at in relation to
    #  the overall height of the image, as the target for the corresponding feature (image)
    for idx, an_image in enumerate(images):  # iterate through all the loaded images
        # # self.load_img(an_image) # shouldn't need this
        edge_image = liquid_level.find_contour(fill=an_image)  # finds the edges for the image

        # find the frame you want to look for menisci in the edge image, and then find the appropriate number of
        # menisci in the image
        # left, right, top, and bottom are all int that represent the rows and columns that define the region of
        # interest (the frame) to look for the meniscus in; uses liquid_level.crop_left (and crop_right, crop_bottom,
        #  and crop_top; these are the float (fraction values essentially) values on how far to crop an image to
        # inwards from the top, left, right, and bottom to get the region of interest; but we don't need to access
        # them here, because they are set and kept in the liquid level object to be used to find left, right, top,
        # and bottom)
        left, right, top, bottom = liquid_level.find_frame(closed=edge_image, show=False)
        # find the row number (from the top of the image down) that corresponds to where the meniscus is
        row = liquid_level.find_meniscus(close=edge_image)

        # relative to the entire image, find the fraction up from the bottom of the image, where the meniscus is found
        meniscus_height_as_fraction_of_image = (image_height - row)/image_height

        # set the target for the image, as this fraction up from the bottom of the image height
        targets.append(meniscus_height_as_fraction_of_image)

        # save all images with the names in a separate folder that has drawn as a part of the folder name; save the
        # drawn images so the user knows which ones to delete if the meniscus wasn't found correctly
        drawn_image = liquid_level.draw_level_line(img=an_image)  # draw the found meniscus level on the image,
        # and return the image as drawn_image

        # save the drawn image to the folder to save drawn images to
        an_image_name = image_names[idx]  # the name of the image that is being iterated on
        cv2.imwrite(f'{save_drawn_image_folder_path}\\drawn_{an_image_name}', drawn_image)

        print(f'found and drawn meniscus for image {idx+1}')

    print(f'found and recorded liquid level for all images using naive liquid level algorithm')

    # save pickle file with features as image and targets as % up from roi

    features = np.asarray(features)
    targets = np.asarray(targets)

    # write the features and labels into a pickle file
    dataset = {'features': features, 'targets': targets}
    pickle.dump(dataset, open(pickle_file_path, 'wb'))

    print(f'wrote pickle file to {pickle_file_path}')


def load_images_from_folder(folder):
    """
    Takes a folder in the and loads images and uses the folder and puts it into an
    array, but also creates another list with the names of the images
    :param folder: the folder in the directory or the path to a folder from the directory to load images from
    :return: images is an array of the images for a folder, image_names is a list of the names of the images that
        were loaded
    """
    images = []
    image_names = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))

        split_up_filename = filename.split('\\')
        image_name = split_up_filename[-1]  # the name of the image
        #
        # # only let .jpg files be read and added, if a file is not .jpg type, then skip it (to avoid errors trying to
        # # load .txt files as images)
        # split_by_full_stop = filename.split('.')  # split up the file name according to '.'', then if statement
        # # isolates the last part of the string after the last '.' in the file name
        # if split_by_full_stop[-1] is not 'jpg':  # if the ending of the file name is not .jpg, continue
        #     continue

        img = np.asarray(img)

        if img is not None:
            images.append(img)
            image_names.append(f'{image_name}')

    # images = np.asarray(images)  # need this?

    return images, image_names

#######################################################################

# # root_dir = os.getenv('imagefolder')  # this is to connect to the NAS, comment out for now because testing with a
# folder of images that isn't in the NAS
root_dir = os.path.dirname(os.path.abspath(ada_peripherals.__file__))  # root directory of ada_peripherals

vision_folder_name = 'vision'
vision_folder_path = os.path.join(root_dir, vision_folder_name)

test_meniscus_folder_name = 'test_meniscus_images'
test_meniscus_folder_path = os.path.join(vision_folder_path, test_meniscus_folder_name)

test_image_folder_name = 'test_image_data'
test_image_folder_path = os.path.join(test_meniscus_folder_path, test_image_folder_name)

pickle_data_file_name = 'test_meniscus_finding_ml_data_set.pkl'
pickle_data_file_path = os.path.join(vision_folder_path, pickle_data_file_name)

test_drawn_image_folder_name = 'drawn_test_image_data'
test_drawn_image_folder_path = os.path.join(test_meniscus_folder_path, test_drawn_image_folder_name)

model_folder_name = 'test_liquid_level_ml_model'
model_folder_path = os.path.join(vision_folder_path, model_folder_name)

images_to_test_path = os.path.join(test_meniscus_folder_path, 'test_images')

image_width = 1280
image_height = 720
number_of_channels = 3

loss_threshold = 0.001

########################################################################


def view_dataset(pickle_data_path):
    dataset = pickle.load(open(pickle_data_path, 'rb'))
    for img in dataset['features']:
        cv2.imshow('i', img)
        cv2.waitKey(0)


def train():
    train_features, train_targets, \
    valid_features, valid_targets, \
    test_features, test_targets = load_data_set(pickle_data_path=pickle_data_file_path,
                                                img_height=image_height,
                                                img_width=image_width,
                                                number_of_channels=number_of_channels)

    cnn_image_model = ImageCNNModelRegression(batch_size=10,
                                              features_shape=train_features.shape,
                                              targets_shape=train_targets.shape,
                                              model_folder_path=model_folder_path,
                                              loss_threshold=loss_threshold)

    cnn_image_model.set_data(train_features,
                             train_targets,
                             valid_features,
                             valid_targets,
                             test_features,
                             test_targets,
                             )

    cnn_image_model.build_model()


def predict_liquid_level(model_folder_path, data_to_test, image_height, image_width):
    predictions = predict_class(model_folder_path=model_folder_path,
                                data_to_test=data_to_test,
                                )
    # draw line on image
    for idx, image in enumerate(data_to_test):
        row = int(image_height * (1 - predictions[idx]))  # find the row in the image where the meniscus was found

        def draw_level_line(img, row, img_width):
            check_top_left = (0, row)  # the 'top left' value for the line, really just the left part of the line.
            # check means that it is the determined meniscus water level line
            check_lower_right = (img_width, row)
            # draw green line for level you just checked
            img = cv2.line(img, check_top_left, check_lower_right, (255, 255, 255))
            cv2.putText(img, 'liquid level', (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
            return img

        drawn_image = draw_level_line(img=image, row=row, img_width=image_width)
        cv2.imshow('predicted_image', drawn_image)
        cv2.waitKey(0)

    return predictions


def predict():
    loaded_images = load_images_from_folder_no_label(path_to_folder=images_to_test_path,
                                                     img_width=image_width,
                                                     img_height=image_height,
                                                     )

    predict_liquid_level(model_folder_path=model_folder_path, data_to_test=loaded_images, image_height=image_height,
                         image_width=image_width)


# this will get all the images from a folder and apply the naive liquid level algorithm on it, and then save a pickle
#  file. user needs to tell it where to look for a meniscus, and check through the drawn images folder to makes sure

#  todo
# run_liquid_level_algorithm_on_folder_of_images(image_folder_path=test_image_folder_path,
#                                                pickle_file_path=pickle_data_file_path,
#                                                save_drawn_image_folder_path=test_drawn_image_folder_path)

# or run this to see all of the drawn images also
# view_dataset(pickle_data_path=pickle_data_file_path)

# this will create a model and train it if no model in the specificed folder exists, or continue to train a model if
# a model already exists # todo
# train()

# this will predict using a model; it will display drawn images of what it has identified as the liquid level
# # todo
# predict()



###################################################3

# another way of testing the machine learning version of liquid level, but instead of inputting the complete images,
# input the edge images

pickle_data_edge_file_name = 'test_meniscus_finding_ml_data_set_edge.pkl'
pickle_data_edge_file_path = os.path.join(vision_folder_path, pickle_data_edge_file_name)

test_drawn_image_edge_folder_name = 'drawn_test_image_data_edge'
test_drawn_image_edge_folder_path = os.path.join(test_meniscus_folder_path, test_drawn_image_edge_folder_name)

model_edge_folder_name = 'test_liquid_level_ml_model_edge'
model_edge_folder_path = os.path.join(vision_folder_path, model_edge_folder_name)

number_of_channels_edge = 1

########################################################################


# edge version; so instead of having the images as RGB, use the edge images
def run_liquid_level_algorithm_on_folder_of_images_edge(image_folder_path,  # path to folder of images to run liquid
                                                    # level on
                                                   pickle_file_path,  # path to pickle file to dump everything into
                                                   save_drawn_image_folder_path,  # path to folder to save images
                                                   # with the drawn version with where the meniscus is at to so user
                                                   # can make sure the naive algorthm (liquid level version 1)
                                                   # labeled the images correctly
                                                   ):
    # load the images
    images, image_names = load_images_from_folder(folder=image_folder_path)

    print(f'number of images loaded: {len(image_names)}')

    features_as_edge_images = []  # create a list of features, with the features being the images (edge versions)
    targets = []

        # display first image to user so user can select where the vial is aka the region of interest
    first_image = images[0]
    image_height, image_width, _ = first_image.shape  # find width and height of all the images

    # create an instance of the liquid level object to use, use the width of all the images (taken as the width of
    # the first image) to create the liquid level object
    liquid_level = LiquidLevel(camera=None, width=image_width)

    # load the first image, and use that as the image to select a region of interest from
    liquid_level.set_image_as_reference(img=first_image)
    first_image_as_edge_image = liquid_level.find_contour(fill=first_image)  # find the edge image of the first image
    liquid_level.ref_closed = first_image_as_edge_image  # nede to set the liquid level object to have a reference
    # 'closed' (should really be called edge) image, in order to properly view the image to select the region of
    # interest

    # allow used to select the region of interest to look for a meniscus
    liquid_level.select_frame()

    # iterate through all images, and pass the region of rest area to find the liquid level; essentially use
    # what there already is in the liquid_level script, and add the result of where the meniscus is at in relation to
    #  the overall height of the image, as the target for the corresponding feature (image)
    for idx, an_image in enumerate(images):  # iterate through all the loaded images
        # # self.load_img(an_image) # shouldn't need this

        edge_image = liquid_level.find_contour(fill=an_image)  # finds the edges for the image

        # find the frame you want to look for menisci in the edge image, and then find the appropriate number of
        # menisci in the image
        # left, right, top, and bottom are all int that represent the rows and columns that define the region of
        # interest (the frame) to look for the meniscus in; uses liquid_level.crop_left (and crop_right, crop_bottom,
        #  and crop_top; these are the float (fraction values essentially) values on how far to crop an image to
        # inwards from the top, left, right, and bottom to get the region of interest; but we don't need to access
        # them here, because they are set and kept in the liquid level object to be used to find left, right, top,
        # and bottoZZm)
        left, right, top, bottom = liquid_level.find_frame(closed=edge_image, show=False)
        # find the row number (from the top of the image down) that corresponds to where the meniscus is
        row = liquid_level.find_meniscus(close=edge_image)

        # relative to the entire image, find the fraction up from the bottom of the image, where the meniscus is found
        meniscus_height_as_fraction_of_image = (image_height - row)/image_height

        # set the target for the image, as this fraction up from the bottom of the image height
        targets.append(meniscus_height_as_fraction_of_image)

        # set the features as the edge images
        features_as_edge_images.append(edge_image)

        # save all images with the names in a separate folder that has drawn as a part of the folder name; save the
        # drawn images so the user knows which ones to delete if the meniscus wasn't found correctly
        drawn_image = liquid_level.draw_level_line(img=an_image)  # draw the found meniscus level on the image,
        # and return the image as drawn_image

        # save the drawn image to the folder to save drawn images to
        an_image_name = image_names[idx]  # the name of the image that is being iterated on
        cv2.imwrite(f'{save_drawn_image_folder_path}\\drawn_{an_image_name}', drawn_image)

        print(f'found and drawn meniscus for image {idx+1}')

    print(f'found and recorded liquid level for all images using naive liquid level algorithm')

    # save pickle file with features as image and targets as % up from roi

    features_as_edge_images = np.asarray(features_as_edge_images)
    targets = np.asarray(targets)

    # write the features and labels into a pickle file
    dataset = {'features': features_as_edge_images, 'targets': targets}
    pickle.dump(dataset, open(pickle_file_path, 'wb'))

    print(f'wrote pickle file to {pickle_file_path}')


def train_edge():
    train_features, train_targets, \
    valid_features, valid_targets, \
    test_features, test_targets = load_data_set(pickle_data_path=pickle_data_edge_file_path,
                                                img_height=image_height,
                                                img_width=image_width,
                                                number_of_channels=number_of_channels_edge)

    cnn_image_model = ImageCNNModelRegression(batch_size=10,
                                              features_shape=train_features.shape,
                                              targets_shape=train_targets.shape,
                                              model_folder_path=model_edge_folder_path,
                                              loss_threshold=loss_threshold)

    cnn_image_model.set_data(train_features,
                             train_targets,
                             valid_features,
                             valid_targets,
                             test_features,
                             test_targets,
                             )

    cnn_image_model.build_model()


def predict_edge():
    liquid_level = LiquidLevel(camera=None, width=image_width)

    loaded_images = load_images_from_folder_no_label(path_to_folder=images_to_test_path,
                                                     img_width=image_width,
                                                     img_height=image_height,
                                                     )

    loaded_images_as_edge_images = []

    first_image = loaded_images[0]

    liquid_level.set_image_as_reference(img=first_image)

    for an_image in loaded_images:
        edge_image = liquid_level.find_contour(fill=an_image)
        edge_image = np.reshape(edge_image, (image_height, image_width, number_of_channels_edge))
        loaded_images_as_edge_images.append(edge_image)

    predictions = predict_liquid_level(model_folder_path=model_edge_folder_path,
                                     data_to_test=loaded_images_as_edge_images,
                         image_height=image_height,
                         image_width=image_width)

    def draw_level_line(img, row, img_width):
        check_top_left = (0, row)  # the 'top left' value for the line, really just the left part of the line.
        # check means that it is the determined meniscus water level line
        check_lower_right = (img_width, row)
        # draw green line for level you just checked
        img = cv2.line(img, check_top_left, check_lower_right, (0, 255, 0))
        cv2.putText(img, 'liquid level', (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
        return img

    for idx, an_image in enumerate(loaded_images):
        row = int(image_height * (1 - predictions[idx]))  # find the row in the image where the meniscus was found
        drawn_image = draw_level_line(img=an_image, row=row, img_width=image_width)
        cv2.imshow('predicted_image', drawn_image)
        cv2.waitKey(0)

# # # run the liquid level algorithm, but this one saves the features as the edge image
# #  todo
# run_liquid_level_algorithm_on_folder_of_images_edge(image_folder_path=test_image_folder_path,
#                                                        pickle_file_path=pickle_data_edge_file_path,
#                                                        save_drawn_image_folder_path=test_drawn_image_edge_folder_path)
# # todo
# train()
#
# # todo
#
# predict()

