"""
Collection of functions to help with the creation and usage of the machine learning version of the liquid
level/meniscus finder
"""
import cv2
import pickle
import numpy as np
from ada_peripherals.vision.liquid_level import LiquidLevel
import os
import ada_peripherals
# todo switch from weak to normal now that there is lots of data to augment and train everthing on
from ada_peripherals.machine_learning.generic_models_and_tests.cnn_image_categorical_weak_cnn import ImageCNNModel,  \
    predict_class, load_data_set, load_images_from_folder_no_label
from ada_peripherals.machine_learning.generic_models_and_tests.cnn_image_categorical import augment_image_data

# this function will iterate through the images in a folder, path of folder needs to be specified, then the user
# needs to specify where to look for the meniscus (only the view of the vial), then the function will run the
# first generation of the liquid level finding algorithm on it all the images in the folder (but really cropped images
#  based on the region of interest the user selected), then it will save,
# to a separate folder, all the images, but with lines drawn on them for where the found meniscus was.

# for targets, if found a meniscus, that is 1, if not, then is 0

# the alorithm isn't messing up when it identifies menisci
# but the issue with this right now is there are so many more non-menisci images so the training set is heavily
# skewed, so the model doesn't work well

def sequentially_augment_image_data(image,
                                    augment_image_data_arguments,
                                    ):
    # apply a lot of augmentations to a single image in sequential order to get a collection of agumented images
    # augment_image_data_arguments is a dictionary where the keys are 'list_of_angles_to_rotate', 'horizontal_flip',
    # 'vertical_flip', 'list_of_horizontal_shift', 'list_of_vertical_shift', 'list_of_crop_and_resize_arguments'.
    # return a list of augmented images
    # get the lists of parameters for data augmentation. give default arguments if they are not supplied
    if 'list_of_angles_to_rotate' in augment_image_data_arguments.keys():
        list_of_angles_to_rotate = augment_image_data_arguments['list_of_angles_to_rotate']
    else:
        list_of_angles_to_rotate = [0.,]
    if 'horizontal_flip' in augment_image_data_arguments.keys():
        if augment_image_data_arguments['horizontal_flip'] is True:
            # this is done as a list, so that later when augmenting the data both the unflipped and flipped version
            # of the image will be used
            list_of_horizontal_flip = [False, True]
    else:
        list_of_horizontal_flip = [False]
    if 'vertical_flip' in augment_image_data_arguments.keys():
        if augment_image_data_arguments['vertical_flip'] is True:
            list_of_vertical_flip = [False, True]
    else:
        list_of_vertical_flip = [False]
    if 'list_of_horizontal_shift' in augment_image_data_arguments.keys():
        list_of_horizontal_shift = augment_image_data_arguments['list_of_horizontal_shift']
    else:
        list_of_horizontal_shift = [0.,]
    if 'list_of_vertical_shift' in augment_image_data_arguments.keys():
        list_of_vertical_shift = augment_image_data_arguments['list_of_vertical_shift']
    else:
        list_of_vertical_shift = [0.,]
    if 'list_of_crop_and_resize_arguments' in augment_image_data_arguments.keys():
        list_of_crop_and_resize_arguments = augment_image_data_arguments['list_of_crop_and_resize_arguments']
    else:
        list_of_crop_and_resize_arguments = [{'crop_left': 0.,
                                              'crop_right': 0.,
                                              'crop_top': 0.,
                                              'crop_bottom': 0.,
                                              }]

    list_of_augmented_images = []

    for angle in list_of_angles_to_rotate:
        for horizontal_flip in list_of_horizontal_flip:
            for vertical_flip in list_of_vertical_flip:
                for horizontal_shift in list_of_horizontal_shift:
                    for vertical_shift in list_of_vertical_shift:
                        for crop_and_resize_arguments in list_of_crop_and_resize_arguments:
                            image_copy = image.copy()
                            augmented_image = augment_image_data(image=image_copy,
                                                                 angle_to_rotate=angle,
                                                                 horizontal_flip=horizontal_flip,
                                                                 vertical_flip=vertical_flip,
                                                                 horizontal_shift=horizontal_shift,
                                                                 vertical_shift=vertical_shift,
                                                                 crop_and_resize_arguments=crop_and_resize_arguments,
                                                                 )
                            list_of_augmented_images.append(augmented_image)

    return list_of_augmented_images

# todo should ALSO HAVE A FLAG FOR THIS FUNCTION TO ENABLE DATA AUGMENTATION OF THE IMAGES, DO NOT NEED TO SAVE THE
# IMAGES, BUT SHOULD SAVE THE DATA TO THE PICKLE FILES, ALTHOUGH ARGUABLY FOR EVERY IMAGE ONLY NEED TO AUGMENT THE
# IMAGE WHERE THE MENISCUS WAS ACTUALLY FOUND AND CREATE MORE ENTRIES IN THE DATA SET WITH THOSE AUGMENTED IMAGES,
# SINCE THERE IS ALREADY OVERSATURATION OF THE NON-MENISCUS IMAGES
def run_liquid_level_algorithm_on_folder_of_images(image_folder_path,  # path to folder of images to run liquid level on
                                                   pickle_file_path,  # path to pickle file to dump everything into
                                                   save_drawn_image_folder_path,  # path to folder to save images
                                                   # with the drawn version with where the meniscus is at to so user
                                                   # can make sure the naive algorthm (liquid level version 1)
                                                   # labeled the images correctly
                                                   rows_to_count,  # number of rows to count in an image to be
                                                   # considered when finding the meniscus
                                                   augment_image_data_arguments=None,
                                                   ):
    # augment_image_data_arguments is a dictionary where the keys are 'list_of_angles_to_rotate', 'horizontal_flip',
    # 'vertical_flip', 'list_of_horizontal_shift', 'list_of_vertical_shift', 'list_of_crop_and_resize_arguments'.
    # get the lists of parameters for data augmentation

    print(f'In run_liquid_level_algorithm_on_folder_of_images')

    # load the images
    images, image_names = load_images_from_folder(folder=image_folder_path)  # images is the list of images loaded,
    # and image_names is the list of the image names

    number_of_items_before_augmentation = len(images)

    # augment the image data
    if augment_image_data_arguments is not None:
        print(f'Augment image data')
        # append augmented images to the images list
        for idx, image in enumerate(images):
            # this if statement to prevent an infinite loop by only augmenting image data if it is doing it for one
            # of the originally loaded images, and not one of the images that was already augmented
            if (idx+1) <= number_of_items_before_augmentation:
                if idx % 10 is 0 or idx is len(images):
                    print(f'Augment image {idx+1} out of {number_of_items_before_augmentation}')
                image_copy = image.copy()
                list_of_augmented_images = sequentially_augment_image_data(
                    image=image_copy,
                    augment_image_data_arguments=augment_image_data_arguments,
                )
                for augmented_image in list_of_augmented_images:
                    images.append(augmented_image)

    print(f'Number of images after augmentation: {len(images)}')

    features = []  # create a list of features, with the features being the slices of the images
    targets = []

    # display first image to user so user can select where the vial is aka the region of interest
    first_image = images[0]
    image_height, image_width, _ = first_image.shape  # find width and height of all the images

    # create an instance of the liquid level object to use, use the width of all the images (taken as the width of
    # the first image) to create the liquid level object
    liquid_level = LiquidLevel(camera=None, width=image_width, rows_to_count=rows_to_count)

    # load the first image, and use that as the image to select a region of interest from
    liquid_level.set_image_as_reference(img=first_image)
    first_image_as_edge_image = liquid_level.find_contour(fill=first_image)  # find the edge image of the first image
    liquid_level.ref_closed = first_image_as_edge_image  # nede to set the liquid level object to have a reference
    # 'closed' (should really be called edge) image, in order to properly view the image to select the region of
    # interest

    # allow used to select the region of interest to look for a meniscus
    liquid_level.select_frame()

    # iterate through all images, and pass the region of rest area to find the liquid level; for each image,
    # save where the found meniscus is, and save horizontal strips as the features from the image, and for target,
    # say if the meniscus was found in there or not
    for idx, an_image in enumerate(images):  # iterate through all the loaded images
        features, targets, liquid_level = find_where_meniscus_is_in_an_image(image=an_image,
                                                                             features=features,
                                                                             targets=targets,
                                                                             liquid_level=liquid_level,)
        if idx % 50 is 0 or idx is len(images):
            print(f'found meniscus for image {idx+1} out of {len(images)}')

    print(f'found and recorded liquid level for all images using naive liquid level algorithm')

    # save pickle file with features as image and targets meniscus or no_meniscus for that horizontal slice
    features = np.asarray(features)
    targets = np.asarray(targets)

    # write the features and labels into a pickle file
    dataset = {'features': features, 'targets': targets}
    pickle.dump(dataset, open(pickle_file_path, 'wb'), protocol=4)

    print(f'wrote pickle file to {pickle_file_path}')

    for idx, an_image in enumerate(images):  # iterate through all the loaded images
        # save all images with the names in a separate folder that has drawn as a part of the folder name; save the
        # drawn images so the user knows which ones to delete if the meniscus wasn't found correctly
        number_of_loaded_images = len(image_names)
        if idx == number_of_loaded_images:
            # this breaks the the loop if the length of the image_names list has been reached, since the list of
            # images was increased by augmented the image data, but the list of image names was not increased,
            # so this break needs to be put here; so only the non-augmented image data gets saved
            break
        drawn_image = liquid_level.draw_level_line(img=an_image)  # draw the found meniscus level on the image,
        # and return the image as drawn_image

        # save the drawn image to the folder to save drawn images to
        an_image_name = image_names[idx]  # the name of the image that is being iterated on
        cv2.imwrite(f'{save_drawn_image_folder_path}\\drawn_{an_image_name}', drawn_image)

    print(f'drew meniscus for all images')

def split_and_label_image(image, liquid_level, found_row, features, targets):
    # take an image and split it up into many horizontal slices of the image
    # features is the list of the split up horizontal slices of the images/is the features for the cnn,
    # so we want to save the horizontal slices to features
    # found_row is the row where the meniscus was actually found in; although actually found_row is row+(
    # rows_to_count//2), so first should change that
    # first need to find frame in order to match up looking for the rows with the correct corresponding label with
    # the rows that were used to find the meniscus in the first place
    edge_image = liquid_level.find_contour(fill=image)  # finds the edges for the image; this is needed because
    # find_frame right now only works on binary black/white images
    left, right, top, bottom = liquid_level.find_frame(edge_image=edge_image)
    rows_to_count = liquid_level.rows_to_count  # number of rows to count to find the best meniscus is
    found_row = (found_row - (rows_to_count//2))  # backtrack to know which row it was that was actually  identified
    # as the row with the meniscus in it; because what gets identified as the fouund row is the row that was looked
    # in + (rows to count // 2), because when drawing the meniscus line that might be better
    rows = range(top, bottom, rows_to_count)  # list of the row that starts every horizontal strip
    # you  want to split the image into  # todo but this will miss the very last/last few rows if the number of
    # rows in the image is not perfectly divisible by liquid_level.rows_to_count

    for row in rows:  # iterate through the "rows" of the image; this is really the number of the first row for a
        #  small horizontal split in the image
        horizontal_slice = image[row:(row+rows_to_count)]
        features.append(horizontal_slice)  # append the horizontal slice to the features list
        if row == found_row:  # if the row is the row that was identified to be where the meniscus is,
            # then append 'meniscus' to targets
            targets.append('meniscus')
        else:
            targets.append('no_meniscus')  # if row isnt where the meniscus was found, then append 'no_meniscus' to targets

    return features, targets

def find_where_meniscus_is_in_an_image(image, features, targets, liquid_level):
    # in this function, want to take an image and slice it to get horizontal strips and get an array of the row,
    # and the associated strip. then find where the meniscus is in the original image, and save that image to another
    #  folder. Also with all the horizontal strips, add them all to the features list, and add the target,
    # either no_meniscus or meniscus to the targets lits. only the horizontal strip of where the meniscus was  found
    # has meniscus as the target

    image_height, image_width, _ = image.shape  # find width and height of the image

    # # self.load_img(an_image) # shouldn't need this
    edge_image = liquid_level.find_contour(fill=image)  # finds the edges for the image

    # find the frame you want to look for menisci in the edge image, and then find the appropriate number of
    # menisci in the image
    # left, right, top, and bottom are all int that represent the rows and columns that define the region of
    # interest (the frame) to look for the meniscus in; uses liquid_level.crop_left (and crop_right, crop_bottom,
    #  and crop_top; these are the float (fraction values essentially) values on how far to crop an image to
    # inwards from the top, left, right, and bottom to get the region of interest; but we don't need to access
    # them here, because they are set and kept in the liquid level object to be used to find left, right, top,
    # and bottom)
    left, right, top, bottom = liquid_level.find_frame(closed=edge_image)

    # find the row number (from the top of the image down) that corresponds to where the meniscus is
    found_row = liquid_level.find_liquid_level(close=edge_image)

    # cut up the image into rows; the cutting up of the images will depend on the number of rows the liquid level
    # instance used to find the best meniscus in. the cut up images get put into features, and the targets,
    # either 'no_meniscus' or 'meniscus', gets appended to the targets list
    features, targets = split_and_label_image(image, liquid_level, found_row, features, targets)

    return features, targets, liquid_level


def load_images_from_folder(folder):
    """
    Takes a folder in the and loads images and uses the folder and puts it into an
    array, but also creates another list with the names of the images
    :param folder: the folder in the directory or the path to a folder from the directory to load images from
    :return: images is an array of the images for a folder, image_names is a list of the names of the images that
        were loaded
    """
    images = []
    image_names = []
    print(f'Load images from {folder}')
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))

        split_up_filename = filename.split('\\')
        image_name = split_up_filename[-1]  # the name of the image
        #
        # # only let .jpg files be read and added, if a file is not .jpg type, then skip it (to avoid errors trying to
        # # load .txt files as images)
        # split_by_full_stop = filename.split('.')  # split up the file name according to '.'', then if statement
        # # isolates the last part of the string after the last '.' in the file name
        # if split_by_full_stop[-1] is not 'jpg':  # if the ending of the file name is not .jpg, continue
        #     continue

        img = np.asarray(img)

        if img is not None:
            images.append(img)
            image_names.append(f'{image_name}')

    # images = np.asarray(images)  # need this?
    print(f'number of images loaded: {len(images)}')
    return images, image_names


def extract_classes_into_separate_pickle_files(pickle_file_path,
                                               encoding_dictionary,
                                               folder_path_to_put_resulting_pickle_files_in):
    # from a pickle file, extract out the different targets into their own separate pickle file.
    #  the folder_to_put_resulting_pickle_files_in is
    # the  path to a folder to put the pickle files separated by target that get returned by this function.
    #  pickle_file_path is to a pickle file
    # encoding dictionary is the encoding dictionary for the pickle file
    # that is a dictionary, with features and targets as the keys.
    # str representation of the targets
    print('in extract type from pickle file path function')
    target_1 = None
    target_2 = None
    for idx, key in enumerate(encoding_dictionary.keys()):
        # find what the targets are
        if idx is 0:
            target_1 = key
        elif idx is 1:
            target_2 = key
    print(f'the two types to extract from parent pickle file {pickle_file_path} are: {target_1} and {target_2}')
    # todo right now this would only work for separating pickle file with 2 targets
    features_for_target_one = []  # create a list of features, with the features being the slices of the images
    targets_for_target_one = []
    features_for_target_two = []  # create a list of features, with the features being the slices of the images
    targets_for_target_two = []

    pickle_data = pickle.load(open(pickle_file_path, 'rb'))  # load parent pickle file to separate
    for idx, current_target in enumerate(pickle_data['targets']):
        # loop through all the targets in the pickle file
        if current_target == target_1:
            # print(f'for idx {idx+1} target is {target_1}')
            # if the current_target is target_1, then append the current target feature and target to the
            # corresponding list to collect features and targets for the to be separated by targets pickle file
            features_for_target_one.append(pickle_data['features'][idx])
            targets_for_target_one.append(pickle_data['targets'][idx])
        elif current_target == target_2:
            # print(f'for idx {idx+1} target is {target_2}')
            features_for_target_two.append(pickle_data['features'][idx])
            targets_for_target_two.append(pickle_data['targets'][idx])

    features_for_target_one = np.asarray(features_for_target_one)
    targets_for_target_one = np.asarray(targets_for_target_one)
    features_for_target_two = np.asarray(features_for_target_two)
    targets_for_target_two = np.asarray(targets_for_target_two)

    # create the two new pickle files in appropriate location with appropriate file name
    dataset_one = {'features': features_for_target_one, 'targets': targets_for_target_one}
    pickle_file_path_one = os.path.join(folder_path_to_put_resulting_pickle_files_in, f'{target_1}.pkl')
    pickle.dump(dataset_one, open(pickle_file_path_one, 'wb'), protocol=4)

    dataset_two = {'features': features_for_target_two, 'targets': targets_for_target_two}
    pickle_file_path_two = os.path.join(folder_path_to_put_resulting_pickle_files_in, f'{target_2}.pkl')
    pickle.dump(dataset_two, open(pickle_file_path_two, 'wb'), protocol=4)

    print('made the two new pickle files')
    print(f'for target {target_1}, pickle file saved to {pickle_file_path_one}')
    print(f'for target {target_2}, pickle file saved to {pickle_file_path_two}')


def get_subset_of_data_in_pickle_file(number_of_items_to_get,
                                      pickle_file_path,
                                      new_pickle_file_path):
    # from a pickle file path, take only a subset of the data in the pickle file (the dictionary of features and
    # targets), and then save that to the new_pickle_file_path
    # todo ... or maybe don't need to do this if now I know that separation of types actually works... should try to
    # run the normal catagorical classification now to create a new model on the horizontal slices, and then predict
    # on a non split up image
    print('get subset of data in a pickle file')
    print(f'get {number_of_items_to_get} items from {pickle_file_path} and make new pickle file '
          f'{new_pickle_file_path}')
    new_pickle_file_features = []
    new_pickle_file_targets = []
    pickle_data = pickle.load(open(pickle_file_path, 'rb'))  # load parent pickle file to separate
    for idx, current_target in enumerate(pickle_data['targets']):  # just need to loop through the pickle data,
        # print(f'idx {idx} has been added to the new pickle file')
        # current_target doesnt really matter
        # loop through all the targets in the pickle file. if the index of the loop is less than the number of items
        # to get, then can add that to the list of features and targets in order
        if idx < number_of_items_to_get:
            new_pickle_file_features.append(pickle_data['features'][idx])
            new_pickle_file_targets.append(pickle_data['targets'][idx])
    print(f'create new pickle file at {new_pickle_file_path} with {number_of_items_to_get} items')
    new_pickle_file_features = np.asarray(new_pickle_file_features)
    new_pickle_file_targets = np.asarray(new_pickle_file_targets)
    dataset = {'features': new_pickle_file_features, 'targets': new_pickle_file_targets}
    pickle.dump(dataset, open(new_pickle_file_path, 'wb'), protocol=4)


def merge_pickle_files_into_one_file(folder_path_with_multiple_pickle_files,
                                     path_to_put_resulting_pickle_file_in):
    # for this, take multiple pickle files in a folder, and create one big pickle file with all the data innside of
    # the other pickle files, then save that file into a specific location
    print(f'merge pickle files in folder {folder_path_with_multiple_pickle_files} into one pickle file '
          f'{path_to_put_resulting_pickle_file_in}')
    resulting_pickle_file_features = []
    resulting_pickle_file_targets = []

    for pickle_file in os.listdir(folder_path_with_multiple_pickle_files):
        print(f'add data from {pickle_file} into the resulting pickle file')
        path_to_pickle_file = os.path.join(folder_path_with_multiple_pickle_files, pickle_file)
        pickle_data = pickle.load(open(path_to_pickle_file, 'rb'))  # load pickle file in folder
        for idx, current_target in enumerate(pickle_data['targets']):  # just need to loop through the pickle data,
            # print(f'added idx {idx} from {pickle_file} into resulting pickle file')
            # current_target doesnt really matter
            # extract all features and targets into the giant resulting pickle file
            resulting_pickle_file_features.append(pickle_data['features'][idx])
            resulting_pickle_file_targets.append(pickle_data['targets'][idx])
    print(f'create new pickle file at {path_to_put_resulting_pickle_file_in} with '
          f'{len(resulting_pickle_file_features)}  items')
    resulting_pickle_file_features = np.asarray(resulting_pickle_file_features)
    resulting_pickle_file_targets = np.asarray(resulting_pickle_file_targets)
    resulting_dataset = {'features': resulting_pickle_file_features, 'targets': resulting_pickle_file_targets}
    pickle.dump(resulting_dataset, open(path_to_put_resulting_pickle_file_in, 'wb'), protocol=4)

#######################################################################

# # root_dir = os.getenv('imagefolder')  # this is to connect to the NAS, comment out for now because testing with a
# folder of images that isn't in the NAS
# root_dir = os.path.dirname(os.path.abspath(ada_peripherals.__file__))  # root directory of ada_peripherals
#
# vision_folder_name = 'vision'
# vision_folder_path = os.path.join(root_dir, vision_folder_name)
#
# test_meniscus_folder_name = 'test_meniscus_images'
# test_meniscus_folder_path = os.path.join(vision_folder_path, test_meniscus_folder_name)
#
# test_image_folder_name = 'test_image_data'
# test_image_folder_path = os.path.join(test_meniscus_folder_path, test_image_folder_name)
#
# pickle_data_file_name = 'test_meniscus_finding_ml_data_set.pkl'
# pickle_data_file_path = os.path.join(vision_folder_path, pickle_data_file_name)
#
# folder_path_to_put_resulting_pickle_files_in_name = 'meniscus_finding_pickle_files'
# folder_path_to_put_resulting_pickle_files_in_path = os.path.join(vision_folder_path, folder_path_to_put_resulting_pickle_files_in_name)
#
# merged_pickle_files_into_one_file_name = 'merged_data_pickle_file.pkl'
# merged_pickle_files_into_one_file_path = os.path.join(vision_folder_path, merged_pickle_files_into_one_file_name)
#
# path_to_pickle_file_no_meniscus_specific = os.path.join(folder_path_to_put_resulting_pickle_files_in_path,
#                                                         'no_meniscus.pkl')
# path_to_pickle_file_meniscus_specific = os.path.join(folder_path_to_put_resulting_pickle_files_in_path, 'meniscus.pkl')
#
# test_drawn_image_folder_name = 'drawn_test_image_data'
# test_drawn_image_folder_path = os.path.join(test_meniscus_folder_path, test_drawn_image_folder_name)
#
# model_folder_name = 'test_liquid_level_ml_model'
# model_folder_path = os.path.join(vision_folder_path, model_folder_name)
#
# images_to_test_path = os.path.join(test_meniscus_folder_path, 'test_images')
#
# augment_image_dictionary = {'horizontal_flip': True,
#                             # 'list_of_crop_and_resize_arguments': [{'crop_left': 0.0,
#                             #                                        'crop_right': 0.0,
#                             #                                        'crop_top': 0.0,
#                             #                                        'crop_bottom': 0.0},
#                             #                                       {'crop_left': 0.05,
#                             #                                        'crop_right': 0.05,
#                             #                                        'crop_top': 0.05,
#                             #                                        'crop_bottom': 0.05},
#                             #                                       {'crop_left': 0.1,
#                             #                                        'crop_right': 0.1,
#                             #                                        'crop_top': 0.1,
#                             #                                        'crop_bottom': 0.1},
#                             #                                       ],
#                             }
#
#
# image_width = 1280
# image_height = 720
# image_slice_height = 10  # the height of a horizontal slice of an image
# rows_to_count = image_slice_height  # number of rows to count to find the meniscus
# number_of_channels = 3
# number_of_classes = 2
# encoding_dictionary = {'meniscus': 1, 'no_meniscus': 0}
#
# learning_rate = 1e-4
# loss_threshold = 0.0001

########################################################################


def view_dataset(pickle_data_path):
    dataset = pickle.load(open(pickle_data_path, 'rb'))
    for img in dataset['features']:
        cv2.imshow('features', img)
        cv2.waitKey(0)


def count_items_in_feature_list_in_pickle_file(pickle_data_path, key=None):
    # key is the key whoose value you want to count in the dictionary; the value must be a list, or if None,
    # count the number of items in the first key of the dictionary, the value of which should be a list
    dataset = pickle.load(open(pickle_data_path, 'rb'))
    if key is None:
        key = list(dataset.keys())[0]
    else:
        key = key
    total_number_of_items = len(dataset[key])
    return total_number_of_items


def train(pickle_data_path,
          encoding_dictionary,
          model_folder_path,
          image_slice_height,  # height of the slice, not of the entire image
          image_width,
          number_of_channels,
          number_of_classes,
          loss_threshold,
          learning_rate,
          batch_size=10,
          ):
    train_features, train_targets, \
    valid_features, valid_targets, \
    test_features, test_targets = load_data_set(pickle_data_path=pickle_data_path,
                                                encoding_dictionary=encoding_dictionary,
                                                img_height=image_slice_height,
                                                img_width=image_width,
                                                number_of_channels=number_of_channels)

    cnn_image_model = ImageCNNModel(batch_size=batch_size,
                                    features_shape=train_features.shape,
                                    targets_shape=train_targets.shape,
                                    number_of_classes=number_of_classes,
                                    model_folder_path=model_folder_path,
                                    loss_threshold=loss_threshold,
                                    learning_rate=learning_rate,
                                    )

    cnn_image_model.set_data(train_features,
                             train_targets,
                             valid_features,
                             valid_targets,
                             test_features,
                             test_targets,
                             )

    cnn_image_model.build_model()


def predict_liquid_level(model_folder_path,
                         data_to_test,
                         image_height,
                         image_width,
                         encoding_dictionary,
                         show_prediction,
                         rows_to_count):
    # draw line on image where the meniscus is for each image
    for idx, image in enumerate(data_to_test):
        # todo instead of doing this, need to cut the image up into blocks and then find the one with highest
        rows_to_count = rows_to_count  # number of rows to count to find the best meniscus is
        rows = range(0, image_height, rows_to_count)  # list of the row that starts every horizontal strip
        # predicted score for having a meniscus, then need to draw on that section

        # best_score_so_far = 0  # the best prediction of having a meniscus so far
        # row_with_best_score = 0  # the index of the row that had the best score so far

        full_image_from_slices = []  # the full image made up of slices that you want to individually want to find
        # the meniscus in

        for row in rows:  # iterate through the "rows" of the image; this is really the number of the first row for a
            #  small horizontal split in the image
            horizontal_slice = image[row:(row + rows_to_count)]
            full_image_from_slices.append(horizontal_slice)

        full_image_from_slices = np.asarray(full_image_from_slices)

        # get the predictions for each slice in the full image
        prediction_probabilities, predictions = predict_class(model_folder_path=model_folder_path,
                                                              data_to_test=full_image_from_slices,
                                                              encoding_dictionary=encoding_dictionary,
                                                              show_prediction=show_prediction,
                                                              )

        meniscus_row = 0  # the row the meniscus was found in
        best_probability_of_meniscus = 0  # the score of what was found to be the best meniscus so far

        for idx, probability_array in enumerate(prediction_probabilities):  # loop through all the predictions
            # proabilities for a single large image; loop through the horizontal slices. probability_array is a list
            # of float, where idx 0 is probability of being not a meniscus, and idx 1 is probaility of being a meniscus
            if probability_array[1] > best_probability_of_meniscus:  # if probability of being a mniscus is greater
                # than the best so far, then make the new meniscus row the new idx that was found to be the best,
                # and update what the best probability so far is
                meniscus_row = rows[idx]  # set the row that you found the meniscus in; need to go back to the list
                # of rows because that has the actual number, and can look up the correct row using the idx
                best_probability_of_meniscus = probability_array[1]

        # then draw line on image at where the row where the meniscus was found at was

        def draw_level_line(img, row, img_width):
            check_top_left = (0, row)  # the 'top left' value for the line, really just the left part of the line.
            # check means that it is the determined meniscus water level line
            check_lower_right = (img_width, row)
            # draw green line for level you just checked
            img = cv2.line(img, check_top_left, check_lower_right, (0, 255, 0))
            cv2.putText(img, 'liquid level', (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
            return img

        drawn_image = draw_level_line(img=image, row=meniscus_row, img_width=image_width)
        cv2.imshow('predicted_image', drawn_image)
        cv2.waitKey(0)


def predict(path_to_folder_of_images_to_test,
            image_width,
            image_height,
            model_folder_path,
            encoding_dictionary,
            rows_to_count,  # number of rows to count to predict the liquid level
            show_prediction=False,
            ):
    loaded_images = load_images_from_folder_no_label(path_to_folder=path_to_folder_of_images_to_test,
                                                     img_width=image_width,
                                                     img_height=image_height,
                                                     )

    predict_liquid_level(model_folder_path=model_folder_path,
                         data_to_test=loaded_images,
                         image_height=image_height,
                         image_width=image_width,
                         encoding_dictionary=encoding_dictionary,
                         show_prediction=show_prediction,
                         rows_to_count=rows_to_count,
                         )


# this will get all the images from a folder and apply the naive liquid level algorithm on it, and then save a pickle
#  file. user needs to tell it where to look for a meniscus, and check through the drawn images folder to makes sure
# the alorithm isn't messing up when it identifies menisci
#  todo run next line to do the basic run of first gen liquid level on folder of images to label images
# STEP 1
# run_liquid_level_algorithm_on_folder_of_images(image_folder_path=test_image_folder_path,
#                                                pickle_file_path=pickle_data_file_path,
#                                                save_drawn_image_folder_path=test_drawn_image_folder_path,
#                                                rows_to_count=rows_to_count,
#                                                # augment_image_data_arguments=augment_image_dictionary,
#                                                )

# todo run this to see all of the ~drawn~ images
# view_dataset(pickle_data_path=pickle_data_file_path)

# todo try new thing where i will first separate the pickle file into 2 smaller pickle files according to the target,
#  and then use those 2 pickle files in order to get a better ratio of meniscus to no_meniscus features and targets (
# by basically just selecting only a subset of the images with no_meniscus in them) in order to not bias the model
# towards no_meniscus when it will be trained. but then for this, i would just use a normal classification for the
# training, and then in the prediction, would need to first split the image to predict on, and then use the model,
# since the model will be trained on small horizontally sliced images, but the image to predict on will not be sliced
#  beforehand
# todo split parent pickle file into 2 sub pickle files based on target - the sub pickle files will automatically be
# named based on what the class is called
# STEP 2
# extract_classes_into_separate_pickle_files(pickle_file_path=pickle_data_file_path,
#                                            encoding_dictionary=encoding_dictionary,
#                                            folder_path_to_put_resulting_pickle_files_in=folder_path_to_put_resulting_pickle_files_in_path)

# view_dataset(pickle_data_path=path_to_pickle_file_meniscus_specific)
# view_dataset(pickle_data_path=path_to_pickle_file_no_meniscus_specific)

# number_of_items_in_meniscus_pickle_file = count_items_in_feature_list_in_pickle_file(pickle_data_path=path_to_pickle_file_meniscus_specific)
# print(f'number of items in meniscus pickle file is {number_of_items_in_meniscus_pickle_file} at  '
#       f'{path_to_pickle_file_meniscus_specific}')
#
# number_of_items_in_no_meniscus_pickle_file = count_items_in_feature_list_in_pickle_file(
#     pickle_data_path=path_to_pickle_file_no_meniscus_specific)
# print(f'number of items in meniscus pickle file before getting subset of no_meniscus images from pickle file is '
#       f'{number_of_items_in_no_meniscus_pickle_file} at  '
#       f'{path_to_pickle_file_no_meniscus_specific}')

# todo... that didnt work, so now onto the plan of getting only a subset of data and then merging that new subset of
# no_meniscus with meniscus
# for this case, new picle file path is same as old, so new pickle file will replace the old one. This is to reduce
# the number of images in the no_meniscus pickle file path.
# todo find a better way to extract the number of items in the meniscus pickle file; especially once data
# augmentation will be used
# STEP 3
# get_subset_of_data_in_pickle_file(number_of_items_to_get=number_of_items_in_meniscus_pickle_file,  # which is just the number of items in the meniscus
#                                   #  pickle file
#                                   pickle_file_path=path_to_pickle_file_no_meniscus_specific,
#                                   new_pickle_file_path=path_to_pickle_file_no_meniscus_specific,
#                                   )

# number_of_items_in_no_meniscus_pickle_file_post_subset = count_items_in_feature_list_in_pickle_file(
#     pickle_data_path=path_to_pickle_file_no_meniscus_specific)
# print(f'number of items in meniscus pickle file after getting subset of no_meniscus images from pickle file is '
#       f'{number_of_items_in_no_meniscus_pickle_file_post_subset} at  '
#       f'{path_to_pickle_file_no_meniscus_specific}')
# STEP 4
# merge_pickle_files_into_one_file(folder_path_with_multiple_pickle_files=folder_path_to_put_resulting_pickle_files_in_path,
#                                  path_to_put_resulting_pickle_file_in=merged_pickle_files_into_one_file_path)

# view_dataset(pickle_data_path=merged_pickle_files_into_one_file_path)

# todo then now can use this resulting merged pickle file to train
# STEP 5
# train(pickle_data_path=merged_pickle_files_into_one_file_path,
#       encoding_dictionary=encoding_dictionary,
#       model_folder_path=model_folder_path,
#       image_slice_height=image_slice_height,  # height of the slice, not of the entire image
#       image_width=image_width,
#       number_of_channels=number_of_channels,
#       number_of_classes=number_of_classes,
#       loss_threshold=loss_threshold,
#       learning_rate=learning_rate,
#       batch_size=10,
#       )
# STEP 6
# predict(path_to_folder_of_images_to_test=images_to_test_path,
#         image_width=image_width,
#         image_height=image_height,
#         model_folder_path=model_folder_path,
#         encoding_dictionary=encoding_dictionary,
#         show_prediction=False,
#         )
# TODO THIS WORKS! NEXT STEP IS TO CLEAN THIS MESSY SCRIPT UP SO IT TAKES IN VARIABLE NUMER OF INPUTS AND I DONT NEED
#  TO HARD CODE IN ANY NUMBERS, NOT IMAGE WIDTH OR HEIGHT OR ANYTHING, AND NOT THE NUMBER OF IMAGES THAT THERE ARE (
# AKA THE NUMBER OF IMAGES THAT SHOULD BE IN THE SUBSET NO MENISCUS PICKLE FILE)
