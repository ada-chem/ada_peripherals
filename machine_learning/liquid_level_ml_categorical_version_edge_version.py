"""
Collection of functions to help with the creation and usage of the machine learning version of the liquid
level/meniscus finder
"""
import cv2
import pickle
import numpy as np
from ada_peripherals.vision.liquid_level import LiquidLevel, NoMeniscusFound
import ada_peripherals
import tensorflow as tf
import edward as ed
from scipy.stats import mode
import matplotlib.pyplot as plt
import seaborn as sns
import os
import ada_peripherals
import imutils
import ada_core
from ada_peripherals.machine_learning.generic_models_and_tests.cnn_image_categorical_weak_cnn import ImageCNNModel,  \
    predict_class, load_data_set, load_images_from_folder_no_label

# this function will iterate through the images in a folder, path of folder needs to be specified, then the user
# needs to specify where to look for the meniscus (only the view of the vial), then the function will run the
# first generation of the liquid level finding algorithm on it all the images in the folder (but really cropped images
#  based on the region of interest the user selected), then it will save,
# to a separate folder, all the images, but with lines drawn on them for where the found meniscus was.

# for targets, if found a meniscus, that is 1, if not, then is 0

def run_liquid_level_algorithm_on_folder_of_images(image_folder_path,  # path to folder of images to run liquid level on
                                                   pickle_file_path,  # path to pickle file to dump everything into
                                                   save_drawn_image_folder_path,  # path to folder to save images
                                                   # with the drawn version with where the meniscus is at to so user
                                                   # can make sure the naive algorthm (liquid level version 1)
                                                   # labeled the images correctly
                                                   rows_to_count,  # number of rows to count in an image to be
                                                   # considered when finding the meniscus
                                                   ):
    # load the images
    images, image_names = load_images_from_folder(folder=image_folder_path)  # images is the list of images loaded,
    # and image_names is the list of the image names

    print(f'number of images loaded: {len(image_names)}')

    features = []  # create a list of features, with the features being the slices of the images
    targets = []

    # display first image to user so user can select where the vial is aka the region of interest
    first_image = images[0]
    image_height, image_width, _ = first_image.shape  # find width and height of all the images

    # create an instance of the liquid level object to use, use the width of all the images (taken as the width of
    # the first image) to create the liquid level object
    liquid_level = LiquidLevel(camera=None, width=image_width, rows_to_count=rows_to_count)

    # load the first image, and use that as the image to select a region of interest from
    liquid_level.set_image_as_reference(img=first_image)
    first_image_as_edge_image = liquid_level.find_contour(fill=first_image)  # find the edge image of the first image
    liquid_level.ref_closed = first_image_as_edge_image  # nede to set the liquid level object to have a reference
    # 'closed' (should really be called edge) image, in order to properly view the image to select the region of
    # interest

    # allow used to select the region of interest to look for a meniscus
    liquid_level.select_frame()

    # iterate through all images, and pass the region of rest area to find the liquid level; for each image,
    # save where the found meniscus is, and save horizontal strips as the features from the image, and for target,
    # say if the meniscus was found in there or not
    for idx, an_image in enumerate(images):  # iterate through all the loaded images
        features, targets, liquid_level = find_where_meniscus_is_in_an_image(image=an_image,
                                                                             features=features,
                                                                             targets=targets,
                                                                             liquid_level=liquid_level,)
        print(f'found meniscus for image {idx+1}')

    print(f'found and recorded liquid level for all images using naive liquid level algorithm')

    # save pickle file with features as image and targets as % up from roi

    features = np.asarray(features)
    targets = np.asarray(targets)

    # write the features and labels into a pickle file
    dataset = {'features': features, 'targets': targets}
    pickle.dump(dataset, open(pickle_file_path, 'wb'))

    print(f'wrote pickle file to {pickle_file_path}')

    for idx, an_image in enumerate(images):  # iterate through all the loaded images
        # save all images with the names in a separate folder that has drawn as a part of the folder name; save the
        # drawn images so the user knows which ones to delete if the meniscus wasn't found correctly
        drawn_image = liquid_level.draw_level_line(img=an_image)  # draw the found meniscus level on the image,
        # and return the image as drawn_image

        # save the drawn image to the folder to save drawn images to
        an_image_name = image_names[idx]  # the name of the image that is being iterated on
        cv2.imwrite(f'{save_drawn_image_folder_path}\\drawn_{an_image_name}', drawn_image)

    print(f'drew meniscus for all images')

def split_and_label_image(image, liquid_level, found_row, features, targets):
    # take an image and split it up into many horizontal slices of the image
    # features is the list of the split up horizontal slices of the images/is the features for the cnn,
    # so we want to save the horizontal slices to features
    # found_row is the row where the meniscus was actually found in; although actually found_row is row+(
    # rows_to_count//2), so first should change that
    rows_to_count = liquid_level.rows_to_count  # number of rows to count to find the best meniscus is
    found_row = (found_row - (rows_to_count//2))
    rows = range(0, image_height, rows_to_count)  # list of the row that starts every horizontal strip
    # you  want to split the image into  # todo but this will miss the very last/last few rows if the number of
    # rows in the image is not perfectly divisible by liquid_level.rows_to_count

    for row in rows:  # iterate through the "rows" of the image; this is really the number of the first row for a
        #  small horizontal split in the image
        horizontal_slice = image[row:(row+rows_to_count)]
        features.append(horizontal_slice)  # append the horizontal slice to the features list
        if row == found_row:  # if the row is the row that was identified to be where the meniscus is,
            # then append 'meniscus' to targets
            targets.append('meniscus')
        else:
            targets.append('no_meniscus')  # if row isnt where the meniscus was found, then append 'no_meniscus' to targets

    return features, targets

def find_where_meniscus_is_in_an_image(image, features, targets, liquid_level):
    # in this function, want to take an image and slice it to get horizontal strips and get an array of the row,
    # and the associated strip. then find where the meniscus is in the original image, and save that image to another
    #  folder. Also with all the horizontal strips, add them all to the features list, and add the target,
    # either no_meniscus or meniscus to the targets lits. only the horizontal strip of where the meniscus was  found
    # has meniscus as the target

    image_height, image_width, _ = image.shape  # find width and height of the image

    # # self.load_img(an_image) # shouldn't need this
    edge_image = liquid_level.find_contour(fill=image)  # finds the edges for the image

    # find the frame you want to look for menisci in the edge image, and then find the appropriate number of
    # menisci in the image
    # left, right, top, and bottom are all int that represent the rows and columns that define the region of
    # interest (the frame) to look for the meniscus in; uses liquid_level.crop_left (and crop_right, crop_bottom,
    #  and crop_top; these are the float (fraction values essentially) values on how far to crop an image to
    # inwards from the top, left, right, and bottom to get the region of interest; but we don't need to access
    # them here, because they are set and kept in the liquid level object to be used to find left, right, top,
    # and bottom)
    left, right, top, bottom = liquid_level.find_frame(closed=edge_image)

    # find the row number (from the top of the image down) that corresponds to where the meniscus is
    found_row = liquid_level.find_liquid_level(close=edge_image)

    # cut up the image into rows; the cutting up of the images will depend on the number of rows the liquid level
    # instance used to find the best meniscus in. the cut up images get put into features, and the targets,
    # either 'no_meniscus' or 'meniscus', gets appended to the targets list
    features, targets = split_and_label_image(edge_image, liquid_level, found_row, features, targets)

    return features, targets, liquid_level


def load_images_from_folder(folder):
    """
    Takes a folder in the and loads images and uses the folder and puts it into an
    array, but also creates another list with the names of the images
    :param folder: the folder in the directory or the path to a folder from the directory to load images from
    :return: images is an array of the images for a folder, image_names is a list of the names of the images that
        were loaded
    """
    images = []
    image_names = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder, filename))

        split_up_filename = filename.split('\\')
        image_name = split_up_filename[-1]  # the name of the image
        #
        # # only let .jpg files be read and added, if a file is not .jpg type, then skip it (to avoid errors trying to
        # # load .txt files as images)
        # split_by_full_stop = filename.split('.')  # split up the file name according to '.'', then if statement
        # # isolates the last part of the string after the last '.' in the file name
        # if split_by_full_stop[-1] is not 'jpg':  # if the ending of the file name is not .jpg, continue
        #     continue

        img = np.asarray(img)

        if img is not None:
            images.append(img)
            image_names.append(f'{image_name}')

    # images = np.asarray(images)  # need this?

    return images, image_names


#######################################################################

# # root_dir = os.getenv('imagefolder')  # this is to connect to the NAS, comment out for now because testing with a
# folder of images that isn't in the NAS
root_dir = os.path.dirname(os.path.abspath(ada_peripherals.__file__))  # root directory of ada_peripherals

vision_folder_name = 'vision'
vision_folder_path = os.path.join(root_dir, vision_folder_name)

test_meniscus_folder_name = 'test_meniscus_images'
test_meniscus_folder_path = os.path.join(vision_folder_path, test_meniscus_folder_name)

test_image_folder_name = 'test_image_data'
test_image_folder_path = os.path.join(test_meniscus_folder_path, test_image_folder_name)

pickle_data_file_name = 'test_meniscus_finding_ml_data_set_edge.pkl'
pickle_data_file_path = os.path.join(vision_folder_path, pickle_data_file_name)

test_drawn_image_folder_name = 'drawn_test_image_data_edge'
test_drawn_image_folder_path = os.path.join(test_meniscus_folder_path, test_drawn_image_folder_name)

model_folder_name = 'test_liquid_level_ml_model_edge'
model_folder_path = os.path.join(vision_folder_path, model_folder_name)

images_to_test_path = os.path.join(test_meniscus_folder_path, 'test_images')

image_width = 1280
image_height = 720
image_slice_height = 5  # the height of a horizontal slice of an image
rows_to_count = image_slice_height  # number of rows to count to find the meniscus
number_of_channels = 1
number_of_classes = 2
encoding_dictionary = {'meniscus': 1, 'no_meniscus': 0}

loss_threshold = 0.0005

########################################################################


def view_dataset(pickle_data_path):
    dataset = pickle.load(open(pickle_data_path, 'rb'))
    for img in dataset['features']:
        cv2.imshow('features', img)
        cv2.waitKey(0)


def train():
    train_features, train_targets, \
    valid_features, valid_targets, \
    test_features, test_targets = load_data_set(pickle_data_path=pickle_data_file_path,
                                                encoding_dictionary=encoding_dictionary,
                                                img_height=image_slice_height,
                                                img_width=image_width,
                                                number_of_channels=number_of_channels)

    cnn_image_model = ImageCNNModel(batch_size=10,
                                    features_shape=train_features.shape,
                                    targets_shape=train_targets.shape,
                                    number_of_classes=number_of_classes,
                                    model_folder_path=model_folder_path,
                                    loss_threshold=loss_threshold)

    cnn_image_model.set_data(train_features,
                             train_targets,
                             valid_features,
                             valid_targets,
                             test_features,
                             test_targets,
                             )

    cnn_image_model.build_model()


def predict_liquid_level(model_folder_path, data_to_test, image_height, image_width, encoding_dictionary,
                         show_prediction, rows_to_count):
    # draw line on image where the meniscus is for each image
    for idx, image in enumerate(data_to_test):
        # todo instead of doing this, need to cute the image up into blocks and then find the one with highest
        rows_to_count = rows_to_count  # number of rows to count to find the best meniscus is
        rows = range(0, image_height, rows_to_count)  # list of the row that starts every horizontal strip
        # predicted score for having a meniscus, then need to draw on that section

        # best_score_so_far = 0  # the best prediction of having a meniscus so far
        # row_with_best_score = 0  # the index of the row that had the best score so far

        full_image_from_slices = []  # the full image made up of slices that you want to individually want to find
        # the meniscus in

        for row in rows:  # iterate through the "rows" of the image; this is really the number of the first row for a
            #  small horizontal split in the image
            horizontal_slice = image[row:(row + rows_to_count)]
            full_image_from_slices.append(horizontal_slice)

        full_image_from_slices = np.asarray(full_image_from_slices)

        # get the predictions for each slice in the full image
        prediction_probabilities, predictions = predict_class(model_folder_path=model_folder_path,
                                    data_to_test=full_image_from_slices,
                                    encoding_dictionary=encoding_dictionary,
                                    show_prediction=show_prediction,
                                    )
        # print(prediction_probabilities)
        # print(predictions)
        meniscus_row = 0  # the row the meniscus was found in
        best_probability_of_meniscus = -1  # the score of what was found to be the best meniscus so far, initialize
        # to be -1,

        for idx, probability_array in enumerate(prediction_probabilities):  # loop through all the predictions
            # proabilities for a single large image; loop through the horizontal slices. probability_array is a list
            # of float, where idx 0 is probability of being not a meniscus, and idx 1 is probaility of being a meniscus
            if probability_array[1] > best_probability_of_meniscus:  # if probability of being a mniscus is greater
                # than the best so far, then make the new meniscus row the new idx that was found to be the best,
                # and update what the best probability so far is
                meniscus_row = rows[idx]  # set the row that you found the meniscus in; need to go back to the list
                # of rows because that has the actual number, and can look up the correct row using the idx
                best_probability_of_meniscus = probability_array[1]

        # then draw line on image at where the row where the meniscus was found at was

        def draw_level_line(img, row, img_width):
            check_top_left = (0, row)  # the 'top left' value for the line, really just the left part of the line.
            # check means that it is the determined meniscus water level line
            check_lower_right = (img_width, row)
            # draw green line for level you just checked
            print(f'row: {row}')
            img = cv2.line(img, check_top_left, check_lower_right, (255, 255, 255))
            cv2.putText(img, 'liquid level', (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255))
            return img

        drawn_image = draw_level_line(img=image, row=meniscus_row, img_width=image_width)
        cv2.imshow('predicted_image', drawn_image)
        cv2.waitKey(0)




def predict():
    liquid_level = LiquidLevel(camera=None, width=image_width, rows_to_count=rows_to_count)

    loaded_images = load_images_from_folder_no_label(path_to_folder=images_to_test_path,
                                                     img_width=image_width,
                                                     img_height=image_height,
                                                     )

    loaded_images_as_edge_images = []

    first_image = loaded_images[0]

    liquid_level.set_image_as_reference(img=first_image)

    for an_image in loaded_images:
        edge_image = liquid_level.find_contour(fill=an_image)
        edge_image = np.reshape(edge_image, (image_height, image_width, number_of_channels))
        loaded_images_as_edge_images.append(edge_image)

    predict_liquid_level(model_folder_path=model_folder_path, data_to_test=loaded_images_as_edge_images, image_height=image_height,
                         image_width=image_width, encoding_dictionary=encoding_dictionary, show_prediction=False,
                         rows_to_count=rows_to_count)


# this will get all the images from a folder and apply the naive liquid level algorithm on it, and then save a pickle
#  file. user needs to tell it where to look for a meniscus, and check through the drawn images folder to makes sure
# the alorithm isn't messing up when it identifies menisci
#  todo run next line to do the basic run of first gen liquid level on folder of images to label images
run_liquid_level_algorithm_on_folder_of_images(image_folder_path=test_image_folder_path,
                                               pickle_file_path=pickle_data_file_path,
                                               save_drawn_image_folder_path=test_drawn_image_folder_path,
                                               rows_to_count=rows_to_count)

# todo run this to see all of the drawn images also
# view_dataset(pickle_data_path=pickle_data_file_path)

# todo this will create a model and train it if no model in the specificed folder exists, or continue to train a
# model if a model already exists
train()

# todo this will predict using a model; it will display drawn images of what it has identified as the liquid level
predict()



###################################################3

# another way of testing the machine learning version of liquid level, but instead of inputting the complete images,
# input the edge images

pickle_data_edge_file_name = 'test_meniscus_finding_ml_data_set_edge.pkl'
pickle_data_edge_file_path = os.path.join(vision_folder_path, pickle_data_edge_file_name)

test_drawn_image_edge_folder_name = 'drawn_test_image_data_edge'
test_drawn_image_edge_folder_path = os.path.join(test_meniscus_folder_path, test_drawn_image_edge_folder_name)

model_edge_folder_name = 'test_liquid_level_ml_model_edge'
model_edge_folder_path = os.path.join(vision_folder_path, model_edge_folder_name)

number_of_channels = 1
# number_of_classes = 2
# encoding_dictionary = {'meniscus': 1, 'no_meniscus': 0}

########################################################################

# todo redo this
# # edge version; so instead of having the images as RGB, use the edge images
# def run_liquid_level_algorithm_on_folder_of_images_edge(image_folder_path,  # path to folder of images to run liquid
#                                                     # level on
#                                                    pickle_file_path,  # path to pickle file to dump everything into
#                                                    save_drawn_image_folder_path,  # path to folder to save images
#                                                    # with the drawn version with where the meniscus is at to so user
#                                                    # can make sure the naive algorthm (liquid level version 1)
#                                                    # labeled the images correctly
#                                                    ):
#     # load the images
#     images, image_names = load_images_from_folder(folder=image_folder_path)
#
#     print(f'number of images loaded: {len(image_names)}')
#
#     features_as_edge_images = []  # create a list of features, with the features being the images (edge versions)
#     targets = []
#
#     first_image = images[0]
#     image_height, image_width, _ = first_image.shape  # find width and height of all the images
#
#     # create an instance of the liquid level object to use, use the width of all the images (taken as the width of
#     # the first image) to create the liquid level object
#     liquid_level = LiquidLevel(camera=None, width=image_width)
#
#     # load the first image, and use that as the image to select a region of interest from
#     liquid_level.set_image_as_reference(img=first_image)
#     first_image_as_edge_image = liquid_level.find_contour(fill=first_image)  # find the edge image of the first image
#     liquid_level.ref_closed = first_image_as_edge_image  # nede to set the liquid level object to have a reference
#     # 'closed' (should really be called edge) image, in order to properly view the image to select the region of
#     # interest
#
#     # allow used to select the region of interest to look for a meniscus
#     liquid_level.select_region_of_interest()
#
#     # iterate through all images, and pass the region of rest area to find the liquid level; essentially use
#     # what there already is in the liquid_level script, and add the result of where the meniscus is at in relation to
#     #  the overall height of the image, as the target for the corresponding feature (image)
#     for idx, an_image in enumerate(images):  # iterate through all the loaded images
#         # # self.load_img(an_image) # shouldn't need this
#
#         edge_image = liquid_level.find_contour(fill=an_image)  # finds the edges for the image
#
#         # find the frame you want to look for menisci in the edge image, and then find the appropriate number of
#         # menisci in the image
#         # left, right, top, and bottom are all int that represent the rows and columns that define the region of
#         # interest (the frame) to look for the meniscus in; uses liquid_level.crop_left (and crop_right, crop_bottom,
#         #  and crop_top; these are the float (fraction values essentially) values on how far to crop an image to
#         # inwards from the top, left, right, and bottom to get the region of interest; but we don't need to access
#         # them here, because they are set and kept in the liquid level object to be used to find left, right, top,
#         # and bottom)
#         left, right, top, bottom = liquid_level.find_frame(closed=edge_image, show=False)
#         # find the row number (from the top of the image down) that corresponds to where the meniscus is
#         row = liquid_level.find_liquid_level(close=edge_image, left=left, right=right, top=top, bottom=bottom)
#
#         # relative to the entire image, find the fraction up from the bottom of the image, where the meniscus is found
#         meniscus_height_as_fraction_of_image = (image_height - row)/image_height
#
#         # set the target for the image, as this fraction up from the bottom of the image height
#         targets.append(meniscus_height_as_fraction_of_image)
#
#         # set the features as the edge images
#         features_as_edge_images.append(edge_image)
#
#         # save all images with the names in a separate folder that has drawn as a part of the folder name; save the
#         # drawn images so the user knows which ones to delete if the meniscus wasn't found correctly
#         drawn_image = liquid_level.draw_level_line(img=an_image)  # draw the found meniscus level on the image,
#         # and return the image as drawn_image
#
#         # save the drawn image to the folder to save drawn images to
#         an_image_name = image_names[idx]  # the name of the image that is being iterated on
#         cv2.imwrite(f'{save_drawn_image_folder_path}\\drawn_{an_image_name}', drawn_image)
#
#         print(f'found and drawn meniscus for image {idx+1}')
#
#     print(f'found and recorded liquid level for all images using naive liquid level algorithm')
#
#     # save pickle file with features as image and targets as % up from roi
#
#     features_as_edge_images = np.asarray(features_as_edge_images)
#     targets = np.asarray(targets)
#
#     # write the features and labels into a pickle file
#     dataset = {'features': features_as_edge_images, 'targets': targets}
#     pickle.dump(dataset, open(pickle_file_path, 'wb'))
#
#     print(f'wrote pickle file to {pickle_file_path}')
#
#
# def train_edge():
#     train_features, train_targets, \
#     valid_features, valid_targets, \
#     test_features, test_targets = load_data_set(pickle_data_path=pickle_data_edge_file_path,
#                                                 encoding_dictionary=encoding_dictionary,
#                                                 img_height=image_height,
#                                                 img_width=image_width,
#                                                 number_of_channels=number_of_channels_edge)
#
#     cnn_image_model = ImageCNNModelRegression(batch_size=10,
#                                               features_shape=train_features.shape,
#                                               targets_shape=train_targets.shape,
#                                               model_folder_path=model_edge_folder_path,
#                                               loss_threshold=loss_threshold)
#
#     cnn_image_model.set_data(train_features,
#                              train_targets,
#                              valid_features,
#                              valid_targets,
#                              test_features,
#                              test_targets,
#                              )
#
#     cnn_image_model.build_model()
#
#
# def predict_edge():
#     liquid_level = LiquidLevel(camera=None, width=image_width)
#
#     loaded_images = load_images_from_folder_no_label(path_to_folder=images_to_test_path,
#                                                      img_width=image_width,
#                                                      img_height=image_height,
#                                                      )
#
#     loaded_images_as_edge_images = []
#
#     # display first image to user so user can select where the vial is aka the region of interest
#     first_image = loaded_images[0]
#
#     liquid_level.set_image_as_reference(img=first_image)
#
#     for an_image in loaded_images:
#         edge_image = liquid_level.find_contour(fill=an_image)
#         edge_image = np.reshape(edge_image, (image_height, image_width, number_of_channels_edge))
#         loaded_images_as_edge_images.append(edge_image)
#
#     predictions = predict_liquid_level(model_folder_path=model_edge_folder_path,
#                                      data_to_test=loaded_images_as_edge_images,
#                          image_height=image_height,
#                          image_width=image_width)
#
#     def draw_level_line(img, row, img_width):
#         check_top_left = (0, row)  # the 'top left' value for the line, really just the left part of the line.
#         # check means that it is the determined meniscus water level line
#         check_lower_right = (img_width, row)
#         # draw green line for level you just checked
#         img = cv2.line(img, check_top_left, check_lower_right, (0, 255, 0))
#         cv2.putText(img, 'liquid level', (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
#         return img
#
#     for idx, an_image in enumerate(loaded_images):
#         row = int(image_height * (1 - predictions[idx]))  # find the row in the image where the meniscus was found
#         drawn_image = draw_level_line(img=an_image, row=row, img_width=image_width)
#         cv2.imshow('predicted_image', drawn_image)
#         cv2.waitKey(0)
#
# # # # run the liquid level algorithm, but this one saves the features as the edge image # todo
# # run_liquid_level_algorithm_on_folder_of_images_edge(image_folder_path=test_image_folder_path,
# #                                                        pickle_file_path=pickle_data_edge_file_path,
# #                                                        save_drawn_image_folder_path=test_drawn_image_edge_folder_path)
