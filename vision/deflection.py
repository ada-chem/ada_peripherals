import numpy as np
import cv2
import imutils
from scipy.spatial import distance as dist
import openpyxl
import logging
import os
import logging.config
from ada_core.dependencies.coordinates import Location, rotate_offset
from ada_core.dependencies.interface import controller
from ada_peripherals.vision.webcam import Camera
from ada_core.dependencies.arm import robotarm as n9

n9.change_velocity(10000)
n9.armbias = 'elbow right'

# todo the state the script is in now, it will resize the images to find the optimum, but when trying to view the
# todo drawn contour fo the scale bar there is a shift in the drawing of the contour onto the image

class NeedleDeflection:
    def __init__(self,
                 deflection_x,  # instance of deflection class for the x offset
                 deflection_y,  # instance of deflection class for the y offset
                 ):
        self.deflection_x = deflection_x
        self.deflection_y = deflection_y

    def pre_run(self):
        # set up, have user select the frames for search for the needle
        self.deflection_x.pre_run()  # first time, select by cropping ( draw a box from top left to bottom right of
        # where to look for everything so include the reference and scale box in there), second time, select where to
        #  look for the tip of the needle tip, do not include
        self.deflection_y.pre_run()  # same as above

    def run(self):
        # find the offset for a needle; assumes needle already on proble, so it will bring the arm to the camera
        # station and take the picture and calculate the offset, and then know how to change the offset for the
        # needle, then go to safeheight
        n9.safeheight()

        # dist_x and dist_y are the offsets for the needle tip to those places
        dist_x = self.deflection_x.arm_and_run()
        dist_y = self.deflection_y.arm_and_run()

        print(f'x_offset: {dist_x}, y_offset: {dist_y}')

        n9.safeheight()

        self.apply_link_offsets(x_displacement=dist_x, y_displacement=dist_y)

        print(f'applied link offsets')

    def apply_link_offsets(self, x_displacement, y_displacement, module_name='probe'):
        """
        Calculates the link offset of the item installed on the module and applies them to the item.

        :param x_displacement: The difference between the actual vs. ideal ProbeItem location in the x-axis
        :param y_displacement: The difference between the actual vs. ideal ProbeItem location in the y-axis
        :param module_name: name of the module on which the item is installed
        :return:
        """
        # the delta angle when the module is at the camera station
        link_displacement = rotate_offset(
            offset=Location(
                x=x_displacement,
                y=y_displacement
            ),
            theta=n9._shoulder.degrees + n9._elbow.degrees
        )
        item = n9.modules[module_name].item
        # if isinstance(item, ProbeItem) is False:
        #     raise ValueError('Displacements may not be automatically applied to the installed item')

        # set the offsets
        item.lower_offset = link_displacement.x
        item.hoffset = link_displacement.y


class Deflection():
    """
    Class to identify the degree of deflection of a needle; the distance between the needle tip and a point of reference
    """
    def __init__(self,
                 pb,
                 ):
        """

        :param pb: PhotoBooth, the photobooth instance, the one to bring the needle to
        """
        self.scaled_size = None  # size to resize image to optimize finding contours and the pixel per UnitFloat value
        self.bottom_row = None  # the bottom most row that first encounters a white pixel in the search for the
        # needle algorithm
        self.bottom_column = None  # the column of the first white pixel encountered in the find needle algorithm
        self.img = None  # the original image
        self.img_resized = None  # resized version of the original image
        self.ratio = None
        self.img_width = None  # height of the original image
        self.img_height = None  # width of the original image
        self.ref_x = None  # where the reference mark is in terms of pixels
        self.scale = None  # is pixel per UnitValue conversion
        self.optimum_scaled_size_scale = None
        self.optimum_scaled_size_ref = None
        self.scale_contour = None  # image of the contour of the scale bar
        self.ref_contour = None  # image of the contour of the reference
        self.needle_close = None  # image of closed image of needle against background
        self.needle_edge = None  # image of edges of needle against background
        self.pb = pb  # the photobooth instance
        # length of ref box and scale bar
        self.scale_size, self.ref_size = self.pb.bg  # the actual size (mm) of the scale bar and reference point

        self.global_crop_left = None  # float, how much to crop (relative) every image that goes through from the
        # left; mainly this is to remove any visual interference from the actual set up of the photobooth example:
        # you can see the other scale and reference bars from the other background
        self.global_crop_right = None
        self.global_crop_top = None
        self.global_crop_bottom = None

        self.item_crop_left = None  # float, how much to crop from the left to get to the region of
        # interest for needle; this is a fraction relative to the size of the image
        self.item_crop_right = None
        self.item_crop_top = None
        self.item_crop_bottom = None

        self.item_crop_left_pixels = None  # int, number of pixels tp crop from the left to get to the region of
        # interest for needle, not relative to anything
        self.item_crop_right_pixels = None
        self.item_crop_top_pixels = None
        self.item_crop_bottom_pixels = None

        if os.path.isfile('calibrate_for_needle.xlsx'):
            self.book = openpyxl.load_workbook('calibrate_for_needle.xlsx')
        else:
            # create book file
            self.make_new_excel_file()

        controller.verbose = True
        self.logger = logging.getLogger('find_needle')

        # TODO: need to know how to be able to choose which camera, and then may need that to be a parameter
        # in the functions

    def set_frames(self, image, crop_type):
        """
        Set and the region of interest to
        look for the item (needle or vial or ...)

        :param image:
        :param str, type: 'item' if you want to select region of interest for the item to crop, 'global' if you
            want to select frame for the global cropping
        :return:
        """
        print(f'type in set_frames {type(image)}')
        if crop_type is 'item':
            # select frame for item
            self.item_crop_left, self.item_crop_right, \
            self.item_crop_top, self.item_crop_bottom = self.pb.select_frame(image)
        if crop_type is 'global':
            # select frame for global cropping
            self.global_crop_left, self.global_crop_right, \
            self.global_crop_top, self.global_crop_bottom = self.pb.select_frame(image)

    def pre_run(self):
        """
        Take a picture and then user selects the region of interest for globally cropping every image, and then
        user selects the frame for finding the item. Must be done when there is no item in the photobooth.
        This is essentially a calibration step that needs to be done once before running everything

        :return:
        """

        # take picture
        img = self.pb.take_picture(False)
        print(f'type in prerun {type(img)}')

        # have user select where to crop every image that comes through; global cropping to remove visual
        # interference for all images
        print('Set global frame')
        self.set_frames(img, 'global')

        # load image, also crops it
        print('load image')
        self.load_image(img)

        # run find_optimum_scaled_size
        print('find optimum scale size')
        self.find_optimum_scaled_size()

        # set the scaled sized, and find the scale and ref
        self.set_scaled_size(self.optimum_scaled_size_scale)
        self.find_scale()
        self.set_scaled_size(self.optimum_scaled_size_ref)
        self.find_ref()
        self.set_scaled_size(self.optimum_scaled_size_ref)

        # make user select the region of interest for one image
        print('Set image frame')
        self.set_frames(self.img, 'item')

    def arm_and_run(self):
        """
        Moves the arm to the photobooth location and finds the needle tip distance from the reference box

        :return: float, dist: the distance in mm between the needle tip and the point of reference
        """

        self.pb.arm_to_photo_booth()
        dist = self.take_picture_find_needle()

        return dist

    def take_picture_find_needle(self):
        """
        Takes a picture, loads it, crops it, finds the needle in a pre-defined region

        :return: float, dist: the distance in mm between the needle tip and the point of reference
        """

        # take a picture
        print('take pic')
        img = self.pb.take_picture(self.pb.cam)
        print(f'pic type: {type(img)}')
        # load image, also crops it
        print('load img')
        self.load_image(img)

        # find needle
        dist = self.find_needle()
        print(dist)

        return dist

    def check_needle(self, img=None):
        """
        Loads the image passed, img, or if nothing is passed then uses the image last loaded, and finds where the
        bottom left white pixel in the closed version of the image is. Essentially, need to have already optimized the
        parameters by calling find_optimum_scaled_size, and then this function will call run_one to use those
        optimal values to find the distance from the point of reference to the tip of the needle
        :param img: None or str or numpy.ndarray, the image with needle in the top portion to find the tip of
        :return: the distance in mm between the needle tip and the point of reference
        """
        if img is not None:
            self.load_image(img)
        dist = self.run_one()
        return dist

    def make_new_excel_file(self):
        """
        creates a new excel workbook for the object, and creates the headings, but doesnt save it
        :return:
        """
        self.book = openpyxl.Workbook()
        self.book.active.title = 'best scaled sizes'
        self.book.create_sheet('optimize scaled size')
        sheet = self.book.active
        sheet.cell(row=1, column=1, value='scaled size: scale box')
        sheet.cell(row=1, column=2, value='scaled size: ref box')
        sheet.cell(row=1, column=3, value='ref box size (mm)')
        sheet.cell(row=1, column=4, value='absolute difference between calculated and actual ref box size (mm)')
        self.book.active = 1
        sheet = self.book.active

        sheet.cell(row=1, column=1, value='ref box size')
        sheet.cell(row=1, column=2, value='scaled size: scale box')
        sheet.cell(row=1, column=3, value='scaled size: ref box')
        sheet.cell(row=1, column=4, value='calculated ref box size (mm)')
        sheet.cell(row=1, column=5, value='absolute difference between calculated and actual ref box size (mm)')

    def optimize_and_run(self):
        """
        A single function for one image with needle. Will find the optimum parameters to measure
        needle deflection, and then find the distance between the point of reference and the needle tip
        :return: dist, the distance in mm between the needle tip and the point of reference
        """
        # self.make_new_excel_file()
        self.find_optimum_scaled_size()
        self.save_excel_file()
        dist = self.run_one()

        return dist

    def run_one(self):
        """
        Should have run find_optimum_scaled_size() and save_excel() before to already know the optimal
         scaled_sizes. This function will find the needle deflection distance in mm.
        :return: dist, the distance in mm between the needle tip and the point of reference
        """
        self.set_scaled_size(self.optimum_scaled_size_scale)
        self.find_scale()
        self.set_scaled_size(self.optimum_scaled_size_ref)
        self.find_ref()
        self.set_scaled_size(self.optimum_scaled_size_ref)
        dist = self.find_needle()

        return dist

    def load_image(self, img):
        """
        Loads an image and sets it as the image for the object. Also sets the values for the height, width, and
        channels based on this image. Also crops the image either on the left side or the right side depending
        on what camera/background the parameters have been set to, to remove some interference in the image from the
        other background being in the camera shot

        :param img: str or numpy.ndarray, path to image or already loaded image
        :return:
        """
        if type(img) is str:
            img = cv2.imread(img)

        img = self.pb.crop_horizontal(img, self.global_crop_left, self.global_crop_right)
        img = self.pb.crop_vertical(img, self.global_crop_top, self.global_crop_bottom)
        self.img = img

        self.img_height, self.img_width, channels = self.img.shape

    def set_scaled_size(self, scaled_size):
        """
        simple setter function. Sets the self.scaled_size
        :param scaled_size: int, the size to scale to
        :return:
        """
        self.scaled_size = scaled_size
        self.logger.debug(f'self.scaled_size: {self.scaled_size}')
        return

    def save_excel_file(self):
        """
        Saves the excel workbook; saves it so that it saved in the directory, overwritting the previous excel file
        :return:
        """
        self.book.save('calibrate_for_needle.xlsx')

    def find_optimum_scaled_size(self):
        """
        From self.image, the image that is loaded, find the optimal scaled size vales for
        both the reference and scale boxes and sets self.optimum_scaled_size_ref and
        self.optimum_scaled_size_scale. Writes the results of trying all combinations using values 650-1000
        in steps of 50 into the excel workbook.
        :return:
        """
        sizes = [x for x in range(650, 1050, 50)]

        self.book.active = 1
        sheet = self.book.active
        min = 999999
        min_scaled_size_ref = 0
        min_scaled_size_scale = 0
        absolute_difference = 999999

        for scale_size in sizes:
            self.set_scaled_size(scale_size)
            self.logger.debug(f'outer loop scaled size is: {self.scaled_size}')
            self.find_scale()
            for ref_size in sizes:
                next_row = sheet.max_row + 1
                self.logger.debug(f'the scaled_size-scale to try is: {scale_size}')
                self.logger.debug(f'the scaled_size-ref to try is: {ref_size}')
                self.set_scaled_size(ref_size)
                self.logger.debug(f'scaled size: {self.scaled_size}')
                calculated_ref_box_size = self.find_ref()
                self.logger.debug(f'self.ref size: {self.ref_size}')
                self.logger.debug(f'calculate ref box size: {calculated_ref_box_size}')

                absolute_diff = abs(calculated_ref_box_size-self.ref_size)
                sheet.cell(row=next_row, column=1, value=self.ref_size)
                sheet.cell(row=next_row, column=2, value=scale_size)
                sheet.cell(row=next_row, column=3, value=ref_size)
                sheet.cell(row=next_row, column=4, value=calculated_ref_box_size)
                sheet.cell(row=next_row, column=5, value=absolute_diff)
                if absolute_diff < min:
                    min = abs(calculated_ref_box_size-self.ref_size)
                    min_scaled_size_ref = ref_size
                    min_scaled_size_scale = scale_size
                    absolute_difference = absolute_diff
                self.logger.debug(f'go to next loop in size in sizes')

        # put best one in the first sheet
        self.book.active = 0
        sheet = self.book.active
        next_row = sheet.max_row + 1
        sheet.cell(row=next_row, column=1, value=min_scaled_size_scale)
        sheet.cell(row=next_row, column=2, value=min_scaled_size_ref)
        sheet.cell(row=next_row, column=3, value=self.ref_size)
        sheet.cell(row=next_row, column=4, value=absolute_difference)

        self.optimum_scaled_size_ref = min_scaled_size_ref
        self.optimum_scaled_size_scale = min_scaled_size_scale

        self.set_scaled_size(None)

        return

    def find_needle(self):
        """
        Call to find the needle in an image. Only looks in the needle region of interest to find the needle
        if first transforms the image and then using the closed image, it finds the first white pixel, start looking
        from the bottom left to the top right

        :return: dist, the distance in mm between the needle tip and the point of reference
        """
        # crop the image accoring to the region of interest
        img = self.img
        cnts, edge, close = self.find_contours_needle(img, big=True)
        self.needle_close = close  # this close image with the needle; has the reference and scale bars in them too
        self.needle_edge = edge  # image of edges of the image with the needle

        # self.bottom_row, self.bottom_column = self.find_bottom_pixel(close)
        # # print(f'bottomr row and col with the closed: {self.bottom_row}, {self.bottom_column}')

        self.bottom_row, self.bottom_column = self.find_bottom_pixel(edge)
        # print(f'bottomr row and col with the edge: {self.bottom_row}, {self.bottom_column}')

        dist = self.find_distance()

        self.logger.debug(f'bottom col: {self.bottom_column}, ref_x: {self.ref_x}, '
                          f'pix: {dist*self.scale}, dist: {dist}')
        print(f'bottom col: {self.bottom_column}, ref_x: {self.ref_x}, '
                          f'pix: {dist*self.scale}, dist: {dist}')

        return dist

    def find_scale(self):
        """
        Finds the scale bar and calculates what the pixel per mm value is and sets self.scale.
        :return:
        """
        cnt, img, edge, close = self.transform(self.img, big=True)
        self.scale_contour = cnt

        points = self.order_points(cnt)
        dist = points[1][0] - points[0][0]
        self.scale = dist/self.scale_size
        return

    def find_ref(self):
        """
        Finds the reference box and calculates its length. Finds where the reference x value is and
        sets it in self.ref_x.

        :return: ref_box_size: the calculated size of the reference box based on the pixel per mm value gotten from the
            scale bar; important mainly in find_optimum_scaled_size()
        """
        cnt, img, edge, close = self.transform(self.img, big=False)
        self.ref_contour = cnt

        points = self.order_points(cnt)
        dist = points[1][0] - points[0][0]
        ref_box_size = dist/self.scale
        self.logger.debug(f'length of ref calclated is :{ref_box_size}')
        self.ref_x = points[0][0]+(dist/2)
        return ref_box_size

    def find_distance(self):
        """
        Finds and returns the distance between the need tip (self.bottom_column_ and the reference x value.
        :return: dist, the distance in mm between the needle tip and the point of reference
        """
        # calculate distance between needle and ref point. for now just worry about the x values. check later to
        # see if need to also consider the y difference
        dist = self.bottom_column - self.ref_x  # can be a negative number, that means needle to left of ref point
        dist = dist/self.scale
        return dist  # is distance in mm

    def find_contours_needle(self, image, big):
        """
        Finds the contours of the image. Depending on what big is: True sorts largest to smallest, to find
        the largest contour in the image. Returns the contours, and the edges, and the closed for the image. But doesnt
        select and return the largest or smallest contour. After resizing the image, it also find the
        region of interest in terms of pixels, that will be used later in the find needle function.

        :param image: numpy.ndarray, image
        :param big: bool, true if you want to sort to find the largest rectangle
        :return: cnts is a list of the rectangle contours as numpy.ndarrays. edges is a list of the rectangle edges as
            numpy.ndarrays. closed is a list of the rectangle closed images as numpy.ndarrays.
        """
        # sets the ratio between the old and new image when scale original image to the self.scaled_size
        resized = imutils.resize(image, width=self.scaled_size)  # try to see if this works
        self.ratio = image.shape[0]/float(resized.shape[0])  # remove for now

        image = resized

        print('in find_contours_needle')
        print(f'type image: {type(image)}')
        # Find the boundaries for the region of interest for the image
        self.item_crop_left_pixels, self.item_crop_right_pixels, self.item_crop_top_pixels,\
        self.item_crop_bottom_pixels = self.pb.find_frame(image, False, self.item_crop_left, self.item_crop_right,
                                                          self.item_crop_top, self.item_crop_bottom)

        # convert the image to grayscale, blur it, and find edges in the image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (3, 3), 0)
        # cv2.imshow('Blur', gray)
        #
        # gray_copy = gray.copy()  # todo find good binary threshold image?
        # _, thresh = cv2.threshold(gray_copy, 0, 255, cv2.THRESH_BINARY)

        # TODO OR COULD JUST USE THE EDGES INSTEAD AND JUST ERODE AND DIALATE TO GET RID OF NOISE, AND THEN
        # TODO USE THAT EDGE INSTEAD OF THE CLOSED IMAGE, MIGHT BE BETTER!!!!!!!!!!
        edges = cv2.Canny(gray, 30, 100)

        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
        closed = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)  # gets the closed image. fills insides of edges
                                                # is essentially a dilation follwed by erosion
        # todo later see if putting these two lines above the line to make closed makes it better, bc this is esentially
        # doing cv2.morphologyEx(img, cv2.MORPH_OPEN, kernal) which removes noise
        closed = cv2.erode(closed, None, iterations=1)  # can change to 4
        closed = cv2.dilate(closed, None, iterations=1)  # can change to 4
        print(f'show closed image within find_contours_needle')
        # cv2.imshow('closed in find contours needle', closed)
        # cv2.waitKey(0)
        # _, cnts, _ = cv2.findContours(image=edges, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
        _, cnts, _ = cv2.findContours(image=edges, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)

        if big is True:
            cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:10]  # sorts to find largest rectangle
        else:
            cnts = sorted(cnts, key=cv2.contourArea, reverse=False)[:10]  # sorts to find smallest

        return cnts, edges, closed

    def transform(self, image, big):
        """
        Finds the contours of the image. Finds either the largest or the smallest box in the image.
        Depending on what big is: True sorts largest to smallest, to find
        the largest box in the image. Returns the contours, and the edges, and the closed for either the largest or
        smallest box in the image, depending on the value of big

        :param image: numpy.ndarray, image
        :param big: bool, true if you want to sort to find the largest rectangle
        :return: main_cnt is a rectangle contour as a numpy.ndarray; either largest or smallest. image is a
            numpy.ndarray of the original image. edges is an edge image
            as a numpy.ndarray for the largest or smallest box. closed is a closed images as numpy.ndarray of the largest
            or smallest box.
        """
        # sets the ratio between the old and new image when scale original image to the self.scaled_size

        image_copy = image.copy()
        resized = imutils.resize(image, width=self.scaled_size)  # try to see if this works
        # self.ratio = image.shape[0] / float(resized.shape[0])  # remove for now

        image = resized

        img_height, img_width, _ = image_copy.shape

        self.img_resized = image

        img_height, img_width, _ = image_copy.shape

        # convert the image to grayscale, blur it, and find edges
        # in the image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # gray = cv2.bilateralFilter(gray, 11, 17, 17) # originally 11, 17, 17
        gray = cv2.GaussianBlur(gray, (3, 3), 0)
        edges = cv2.Canny(gray, 30, 100)
        # edges = cv2.morphologyEx(edges, cv2.MORPH_OPEN, kernel)  # todo added this to see if it made edge images less noisy

        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
        closed = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel)
        closed = cv2.erode(closed, None, iterations=1) # can change to 4
        closed = cv2.dilate(closed, None, iterations=1) # can change to 4

        # _, cnts, _ = cv2.findContours(image=edges, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
        _, cnts, _ = cv2.findContours(image=edges, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)

        if big is True:
            cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:10] # sorts to find largest rectangle
        else:
            cnts = sorted(cnts, key=cv2.contourArea, reverse=False)[:10] # sorts to find smallest

        # initialize to be the entire image
        main_cnt = np.array((((0, 0)), ((0, img_height)), ((img_width, img_height)), ((img_width, 0))))

        for c in cnts:
            peri = cv2.arcLength(c, True)
            approx = cv2.approxPolyDP(c, 0.07 * peri, True)
            if len(approx) == 4:
                main_cnt = approx
                break

        # main_cnt is the one contour (box), that is either biggest (True) or smallest (False) depending on
        # the bool value big (parameter). image is the original image passed. edges is the list of edges for
        # the image, closed is the list for the black and white closed p1ture of the image
        return main_cnt, image, edges, closed

    def crop_contour(self, img, arr):
        """
        Crop out the contour, passed by arr that has already been ordered by order_points, from img.
        :param img: numpy.ndarray, image
        :param arr: list of int, list of the top left, top right, bottom right, and bottom left points of the contour
        :return: cropped, original image cropped to only include the contour as a numpy.ndarray
        """
        tl, tr, br, bl = arr[0], arr[1], arr[2], arr[3]

        minWidth = min(int(tl[0]), int(bl[0]))
        maxWidth = max(int(tr[0]), int(br[0]))
        minHeight = min(int(tr[1]), int(tl[1]))
        maxHeight = max(int(bl[1]), int(bl[1]))

        cropped = img[minHeight:maxHeight, minWidth:maxWidth]

        return cropped

    def order_points(self, pts):
        """
         takes a list of points and returns a list ordered top-left, top-right, bottom-right, bottom-left
        of the points in the list (rectangle).
        :param pts: list of int: list of points to order
        :return: ordered list: a 2D array [[x1,y1], [x2,y2], [x3,y3], [x4,y4]]
        """
        # reshape 3D array into 2D array and sort as TL, TR, BR, BL
        pts = pts.reshape((4, 2))

        # sort points by x coordinate
        xSorted = pts[np.argsort(pts[:, 0]), :]

        leftMost = xSorted[:2, :]
        rightMost = xSorted[2:, :]

        # sort according to y coordinates
        leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
        (tl, bl) = leftMost

        D = dist.cdist(tl[np.newaxis], rightMost, 'euclidean')[0]
        (br, tr) = rightMost[np.argsort(D)[::-1], :]

        return np.array([tl, tr, br, bl], dtype='float32')

    def find_bottom_pixel(self, img):
        """
        Finds the first white pixel starting from the bottom left to the top right of the close image looking
        only in the region of interest specified by the user (from previous functions).

        :param img: numpy.ndarray, the closed/edge image of the needle against the background, also has the
            ref and scale bars in the image but they should not be used in the finding needle algorithm if the pre-run
            step was run
        :return: row, is an int, is the row number of the first white pixel. col, is an int, is the column number
        of the first white pixel
        """
        rows = reversed(range(self.item_crop_top_pixels, self.item_crop_bottom_pixels))
        print(f'rows: {rows}')
        cols = range(self.item_crop_left_pixels, self.item_crop_right_pixels)
        print(f'cols: {cols}')

        self.logger.debug(f' num rows and cols in close for needle: {rows}, {cols}')

        for row in rows:
            for col in cols:
                if img[row][col] > 0:  # if pixel is white
                    self.bottom_column = col
                    self.bottom_row = row
                    return row, col
                else:
                    pass
                    # todo
                    # raise an error because there should be something found (needle show be shown in image
                    # and should be able to find the contour]

    def draw_contour(self, img, cnt):
        """
        Edits the image to draw the contour (cnt) on the img in green
        :param img: numpy.ndarray, image
        :param cnt: numpy.ndarray: is a contour of a rectangle
        :return: the original image but with a green box drawn where the contour is
        """
        img_copy = img.copy()
        return cv2.drawContours(img_copy, [cnt], -1, (0, 255, 0), 3)

    def show_all(self, save=False):
        """
        Shows all contours and close images, or saves them to the directory
        :param save: bool, True if you don't want to view the contour, but just save it in the directory instead. False
        if you want to view the contour but not save it to the directory.
        :return:
        """
        self.show_contours(save)
        self.show_close(save)
        self.show_edge(save)
        return

    def show_contours(self, save=False):
        """
        Show both contours, or save both contours overlaid on the original image
        :param save: bool, True if you don't want to view the contour, but just save it in the directory instead. False
        if you want to view the contour but not save it to the directory.
        :return:
        """
        self.show_contour('ref', save)
        self.show_contour('scale', save)
        return

    def show_contour(self, part, save=False):
        """
        Show the contour on the image or save an image with the contour on it; the contour is for either the reference
        box or the scale bar found on the image
        :param part: str: 'ref' or 'scale'. choose the contour you want to see, either the reference box or the scale
        bar.
        :param save: bool, True if you don't want to view the contour, but just save it in the directory instead. False
        if you want to view the contour but not save it to the directory.
        :return: either view contour drawn on image, or save image to directory
        """
        # TODO: right now not working, afer showing one contour, the contour gets places on all
        # TODO: proceeding images
        img_resized_copy = self.img_resized.copy()

        if part is 'ref':
            if save is True:
                cv2.imwrite('find_ref.jpg', self.draw_contour(img_resized_copy, self.ref_contour))
            else:
                cv2.imshow('find_ref',  self.draw_contour(img_resized_copy, self.ref_contour))
                cv2.waitKey(0)

        img_resized_copy = self.img_resized.copy()

        if part is 'scale':
            if save is True:
                cv2.imwrite('find_scale.jpg', self.draw_contour(img_resized_copy, self.scale_contour))
            else:
                cv2.imshow('find_scale',  self.draw_contour(img_resized_copy, self.scale_contour))
                cv2.waitKey(0)
        else:
            # TODO: add error because dont have any other images to see
            pass

    def show_close(self, save=False):
        """
        Show the closed image or save the closed image; specific for the top part of the original image, so should
        only have the needle present in the image
        :param save: bool, True if want to save the closed image to directory, False if want to just view it
        :return: view closed image or save it to the directory
        """
        if save is True:
            cv2.imwrite('needle_close.jpg', self.needle_close)
        else:
            cv2.imshow('needle_close', self.needle_close)
            cv2.waitKey(0)

    def show_edge(self, save=False):
        if save is True:
            cv2.imwrite('needle_edge.jpg', self.needle_edge)
        else:
            cv2.imshow('needle_edge', self.needle_edge)
            cv2.waitKey(0)

    def calibrate_many(self, image_paths, scale_box_sizes, ref_box_sizes):
        """
        From the passed list of images (image_paths), list of scale box lengths (scale_box_sizes), and
        list of reference box lengths (ref_box_sizes), it creates and saves an excel document that records
        all trys to optimize each set of reference and scale boxes. No needles, just the background.
        :param image_paths: list of str, list of the images
        :param scale_box_sizes: list of int, list of scale bar sizes
        :param ref_box_sizes:  list of int, list of reference box sizes
        :return: save an excel file in the directory with extracted information of the optimum scaled sizes to
        use for each background of reference and scale bar
        """
        # todo this function really isnt use much anymore
        for i in range(len(image_paths)):
            if i is 0:
                self.make_new_excel_file()
            self.scale_size = scale_box_sizes[i]
            self.ref_size = ref_box_sizes[i]
            self.load_image(image_paths[i])
            self.find_optimum_scaled_size()

        self.save_excel_file()
        self.save_excel_file()

bg = [19.41, 1.14]

def video_run(cam=0):  # camera to use
    camera = Camera(bg=bg, cam=cam)
    d = Deflection(camera)

    d.pre_run()
    d.save_excel_file()

    video_capture = cv2.VideoCapture(cam)
    while True:
        # capture frame-by-frame
        ret, image = video_capture.read()

        if not ret:  # doesn't really matter here, it matters more for reading from an image, but for video doesnt occur
            break

        # do the liquid level stuff - the actual run and identification of the mesnicus

        cv2.imshow('Live video', image)

        d.load_image(image)

        user_selected_frame_from_image = d.img

        img_resized = imutils.resize(user_selected_frame_from_image, width=d.scaled_size)

        _, _, edges, closed = d.transform(image, True)

        cv2.imshow('Live frame', img_resized)
        cv2.imshow('Edges from transform', edges)
        cv2.imshow('Closed from transform', closed)

        largest_contour, _, _, _ = d.transform(img_resized, True)
        smallest_contour, _, _, _ = d.transform(img_resized, False)

        cv2.imshow('Smallest box', d.draw_contour(img_resized, smallest_contour))
        cv2.imshow('Largest box', d.draw_contour(img_resized, largest_contour))

        # if press the q button exit
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    video_capture.release()
    cv2.destroyAllWindows()

temp_cam_take_pic_loc = Location({'x': 337, 'y': -12.8012, 'z': 268.8})

# pb = PhotoBooth(loc=temp_cam_take_pic_loc, bg=bg, cam=0, item='needle')
# d = Deflection(pb)
# d.pre_run()
# d.show_all()
# d.take_picture_find_needle()