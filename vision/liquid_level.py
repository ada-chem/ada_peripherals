import os
import tkinter
from tkinter import messagebox
from datetime import datetime
import cv2
import numpy as np
import imutils
import threading
import time
import pandas as pd
from PIL import ImageTk, Image
from ada_peripherals.vision.webcam import Camera
from ada_core.dependencies.slackbot import SlackBot
import logging


logger = logging.getLogger(__name__)


class NoMeniscusFound(Exception):
    def __init__(self, image, closed_image):
        #print(f'No meniscus was found in the frame for the most recent image taken')
        self.error_image = image
        self.error_closed_image = closed_image


# TODO LOTS OF NEW FUNCTIONALITY HAS BEEN ADDED TO THE GRONCKLE GUI, SOME OF THOSE FEATURES SHOULD BE ADDED TO HERE.
# THIS INCLUDES RESET BUTTONS FOR LIQUID LEVEL, SHOW/NOT SHOWING LIQUID LEVEL LINES IN THE LIVE STREAM,
# MAKING IT EASIER TO KNOW WHEN THE CAMERA IS ON OR NOT, SETTING THE REFERENCE ROW/IMAGE, BETTER MESSAGEBOX USAGE FOR
# WHEN THERE ARE ERRORS, GET AND SET LIQUID LEVEL PARAMETERS FROM THE GUI ITSELF - check out the commit from march 7

class LiquidLevel():
    def __init__(self, camera, tolerance=1, menisci_to_check=1, rows_to_count=2, width=None,
                 find_meniscus_minimum=0, no_error=False):
        """
        Class to find a/many liquid levels in

        :param Camera, camera: camera object from ada_peripherals
        :param float, tolerance: float between 0 and 1, defines how much variation in terms of % of the image you can
            tolerate for a change in liquid level. This is used for applications to track the liquid level,
            and is automatically set to 1 if tracking the liquid level is not a concern
        :param int, menisci_to_check: int, number of menisci to keep track of in a single image. if it is 0,
            then it will be changed in here to be 999. this allows for LiquidLevel to find all the menisci so the
            user doesn't have to specify a single number; should be used in conjunction with using a non-zero value for
            find_meniscus_minimum
        :param None, int, width: int: width to resize all images to, if None, then don't resize the images
        :param float, find_meniscus_minimum: float between 0 and 1, is the minimum fraction of pixels that must be
            white in a contour image within a single slice of looking for the meniscus. A slice of looking for a
            meniscus is the width of the region to look for the meniscus * the number of rows of pixels to count to
            look for the meniscus.
        :param bool, no_error: False if you want an error to be thrown when a meniscus can't be found, True if you
            want the error when a meniscus wasn't found to not be thrown
        """
        self.logger = logger.getChild(self.__class__.__name__)
        self.logger.info("Created an instance of LiquidLevel")
        self.camera = camera
        self.width = width  # width to resize an image to when trying to find the contours in the image
        self.tolerance = tolerance  # decimal b/w 0 and 1; percentage of
        self.ref_img = None
        self.ref_closed = None
        self.ref_img_height = 0
        self.ref_img_width = 0
        self.img = None  # the image that was most recently loaded
        self.ref_row = 0  # the row and where first white pixel is/reference of meniscus
        self.row = 0  # keeping track of current (not ref) meniscus level/height
        self.list_of_frame_points_frame_points_list = []  # just for use in select frame frame points instead of lists
        self.list_of_frame_points_frame_points_tuple = []  # same as frame points except input inside are tuples for
        self.mask_to_search_inside = None  # mask, inside of which to look for the liquid level - the region of
        # interest to search inside for the liquid level
        self.current_frame_point = None
        self.tolerance_points = []  # just for use in select tolerance
        self.menisci_to_check = menisci_to_check  # if menisci to check is 0, then later when finding the number of
        # menisci it will be converted to 999 to look for all the menisci in the image
        self.menisci_array = []
        self.rows_to_count = rows_to_count  # int, number of rows to use to rank horizontal lines; assume that the higher
                                           # ranked a contour is, the more likely it is to be a menisci
        self.all_images_with_lines = []  # list of list of timestamp, img: [[timestamp, img], [timestamp, img]...]
                                        # image has lines
        self.all_images_no_lines = []  # list of list of timestamp, img: [[timestamp, img], [timestamp, img]...]
                                        # images dont have lines
        self.all_images_edge = []  # # list of list of timestamp, img: [[timestamp, img], [timestamp, img]...]
                                        # images are the edge images

        self.pump_to_pixel_ratio = {}  # this will only be used if the liquid level object is used for a pump,
        # and you want to do some self-corrective things based on images that are taken of the liquid level. This is
        # a dictionary, so this is applicable whether using a peristaltic pump or a syringe pump. Regardless of the
        # kind of pump being used, pixels must be a key, and is the number of pixels difference after a specific pump.
        # Then if using a peristaltic pump, need rpm and time as keys, corresponding to the rpm and time used to move
        #  the meniscus that was detected. if using a syringe pump, then volume must be a key, that corresponds to
        # the volume that was pumped to move the meniscus that was detected.

        self.pixel_to_mm_ratio = None  # ratio of how many pixels is in a mm (px/mm)
        self.find_meniscus_minimum = find_meniscus_minimum  # float, minimum area of the slide in which to look for a
        #  meniscus, that needs to be white pixels in the contour image in order to validate that a meniscus was found
        self.no_error = no_error  # bool, if true, then no error will be thrown if a meniscus was not found in the
        # entire region of interest; what is displayed on the image instead is a line at the top of the image in the
        # colour of what the current liquid level line should be

        # set initial values for parameters to find the contours of an image in find_contour()
        # (int, int), gaussian_kernel_size: Gaussian kernel size as(width, height) for Gaussian blur of image
        self.gaussian_kernel_size = (3, 3)
        # int, gaussian_x_std_dev: for Gaussian blur, the Gaussian kernel standard deviation in X direction.
        self.gaussian_x_std_dev = 0
        # int, canny_threshold_1, the first threshold for the hysteresis procedure.
        self.canny_threshold_1 = 30
        # int, canny_threshold_1, the second threshold for the hysteresis procedure.
        self.canny_threshold_2 = 100
        # (int, int), morph_dilate_kernel_size: kernel size for the morphological operation, dilate as (width, height)
        self.morph_dilate_kernel_size = (7, 7)
        # (int, int), morph_rect_kernel_size: kernel size to create a structuring element as (width, height)
        self.morph_rect_kernel_size = (6, 1)

        # values for find_contour, true or false to apply certain operations
        self.apply_gray_scale = True
        self.apply_gaussian_blur = False
        self.apply_histogram_equalization = True
        self.apply_canny_edge_detection = True
        self.apply_morphological_dilate = True
        self.apply_find_contour_mask = True
        self.apply_morphological_open = True

        # initial parameters to be used by reset()
        self.initial_arguments = {
            'camera': self.camera,
            'width': self.width,
            'tolerance': self.tolerance,
            'menisci_to_check': self.menisci_to_check,
            'rows_to_count': self.rows_to_count,
            'find_meniscus_minimum': self.find_meniscus_minimum,
            'no_error': self.no_error,
        }

    def reset(self):
        # if this is called, reset all the initial attributes
        # function to reset everything so it is like starting with a new version of liquid level; so no reference
        # line, tolerance goes back to the initial value, there are no images saved in memory or anything. also call
        # reset() for the camera so if there were any images were saved to memory they will be deleted
        self.camera.reset()
        self.width = self.initial_arguments['width']
        self.tolerance = self.initial_arguments['tolerance']
        self.ref_img = None
        self.ref_img_height = 0
        self.ref_img_width = 0
        self.img = None
        self.ref_row = 0
        self.row = 0
        self.list_of_frame_points_frame_points_list = []
        self.list_of_frame_points_frame_points_tuple = []
        self.mask_to_search_inside = None
        self.current_frame_point = None
        self.tolerance_points = []
        self.menisci_to_check = self.initial_arguments['menisci_to_check']
        self.menisci_array = []
        self.rows_to_count = self.initial_arguments['rows_to_count']
        self.all_images_with_lines = []
        self.all_images_no_lines = []
        self.all_images_edge = []
        self.ref_closed = None

        self.pump_to_pixel_ratio = {}
        self.pixel_to_mm_ratio = None
        self.find_meniscus_minimum = self.initial_arguments['find_meniscus_minimum']
        self.no_error = self.initial_arguments['no_error']

        self.gaussian_kernel_size = (3, 3)
        self.gaussian_x_std_dev = 0
        self.canny_threshold_1 = 30
        self.canny_threshold_2 = 100
        self.morph_dilate_kernel_size = (7, 7)
        self.morph_rect_kernel_size = (6, 1)

        self.apply_gray_scale = True
        self.apply_gaussian_blur = True
        self.apply_canny_edge_detection = True
        self.apply_morphological_dilate = True
        self.apply_find_contour_mask = True
        self.apply_morphological_open = True

    def set_menisci_to_check(self, int):
        self.menisci_to_check = int
        #print(f'menisci_to_check set to {int}')
        return

    def set_tolerance(self, int):
        self.tolerance = int
        #print(f'tolerance set to {int}')
        return

    def set_width(self, int):
        self.width = int
        #print(f'width to resize images to set to {int}')
        return

    def set_rows_to_count(self, int):
        self.rows_to_count = int
        #print(f'rows_to_count set to {int}')
        return

    def load_image_and_select_and_set_parameters(self,
                                                 img,
                                                 select_region_of_interest=True,
                                                 set_reference=True,
                                                 select_tolerance=True,
                                                 ):
        """
        load an image and set the reference variables: the row with the most white pixels in the frame of the closed
        image after finding a frame (frame made by cropping), and an array of rows with the most white pixels in them;
        the number is dependent on the self.menisci_to_check value

        :param img: 'str' or numpy.ndarray: image to load
        :param bool, select_region_of_interest: If True, then allow user to select the area to look for the meniscus
        :param bool, set_reference: If True, then find where the most prominent liquid level is and set the reference
            level
        :param bool, select_tolerance: If True, then allow user to select the area that will be the tolerance -
            really only applies for when you have applications where you want to keep the meniscus within a range
            using computer vision
        :return:
        """
        #print(f'load and set reference image and reference meniscus line')
        image = self.set_image_as_reference(img)
        edge = self.find_contour(image)
        self.ref_closed = edge

        if select_region_of_interest:
            self.select_frame()
        # next line can throw NoMeniscusFound exception
        if set_reference:
            row = self.set_ref_pixel(edge)
        if select_tolerance:
            self.select_tolerance()
        return edge

    def load_and_find_contour(self, img):
        fill = self.load_img(img)
        closed = self.find_contour(fill)
        return closed

    def load_and_find_level(self, img, show=False):
        """
        loads an image, finds its contour, gets a frame (frame made by cropping) of the closed image, and then
        finds the row with the most white pixels in the frame, and an array of rows with the most white pixels in them;
        the number is dependent on the self.menisci_to_check value
        :param img: 'str' or numpy.ndarray: image to load
        :return:
        """
        #print(f'load and find meniscus for the loaded image')
        fill = self.load_img(img)
        contour_image = self.find_contour(fill)
        row = self.find_level(contour_image, show)
        return contour_image

    def set_image_as_reference(self, img):
        """
        Loads an image and extracts the closed, width and height of the image; use this image as the reference image.
        :param img: img: 'str' or numpy.ndarray: image to load
        :return: fill: a closed version of the original image (contour)
        """
        #print(f'set image')
        image = self.load_img(img)
        self.ref_img_height, self.ref_img_width, _ = image.shape
        self.ref_img = image
        return image

    def load_img(self, img):
        """
        Load and resize an image to a particular width; height will automaticall adjust
        :param str, img, img: 'str' or numpy.ndarray: image to load
        :return: img: resized image as numpy.ndarray
        """
        #print(f'loade image')
        # loads and resizes image
        if type(img) is str:
            img = cv2.imread(img)

        if self.width is not None:
            img = imutils.resize(img, width=self.width)

        self.img = img

        return img

    def find_contour(self,
                     fill,
                     auto_canny=True,
                     ):
        """
        Given an image find a closed image that tries to eliminate non-horizontal lines

        :param fill: numpy.ndarray: image to find the contour of
        :param boolean, auto_canny: whether to compute the parameters for canny edge detection automaticlaly or not
        :return: closed: black and white image, where detected contours are in white
        """
        # get these values from
        gaussian_kernel_size = self.gaussian_kernel_size
        gaussian_x_std_dev = self.gaussian_x_std_dev
        canny_threshold_1 = self.canny_threshold_1
        canny_threshold_2 = self.canny_threshold_2
        morph_dilate_kernel_size = self.morph_dilate_kernel_size
        morph_rect_kernel_size = self.morph_rect_kernel_size

        return_image = fill

        if self.apply_gray_scale is True:
            return_image = cv2.cvtColor(return_image, cv2.COLOR_BGR2GRAY)
        if self.apply_gaussian_blur is True:
            return_image = cv2.GaussianBlur(return_image, gaussian_kernel_size, gaussian_x_std_dev)
        # apply histogram equalization
        if self.apply_histogram_equalization is True:
            return_image = cv2.equalizeHist(return_image)
        if auto_canny is True:
            canny_threshold_1, canny_threshold_2 = self.find_parameteres_for_canny_edge(return_image)
        if self.apply_canny_edge_detection is True:
            return_image = cv2.Canny(return_image, canny_threshold_1, canny_threshold_2)
        if self.apply_morphological_dilate is True:
            return_image = cv2.morphologyEx(return_image, cv2.MORPH_DILATE, morph_dilate_kernel_size)

        _, cnts, _ = cv2.findContours(image=return_image, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
        mask = np.ones(fill.shape[:2], dtype='uint8') * 255

        if self.apply_find_contour_mask is True:
            for c in cnts:
                # to remove bad contours need to add the bad countours to a mask
                if self.bad_contours(c):
                    cv2.drawContours(mask, [c], -1, 0, -1)

            # apply mask to edges
                    return_image = cv2.bitwise_and(return_image, return_image, mask=mask)

        if self.apply_morphological_open is True:
            # create a horizontal structural element;
            horizontal_structure = cv2.getStructuringElement(cv2.MORPH_RECT, morph_rect_kernel_size)
            # to the edges, apply morphological opening operation to remove vertical lines from the contour image
            return_image = cv2.morphologyEx(return_image, cv2.MORPH_OPEN, horizontal_structure)

        return return_image

    def find_parameteres_for_canny_edge(self, image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        median = np.median(image)
        # find bounds for Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * median))
        upper = int(min(255, (1.0 + sigma) * median))
        return lower, upper

    def bad_contours(self, c):
        """
        Return True if a contour is bad; in this case, if the contour has less than 4 edges, i.e. a circle or a triangle
        :param c: contour
        :return: bool
        """
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)  # 0.02 larger
        # return True aka a bad contour if it has less than 4 edges; so things like circles and triangles should be
        # removed
        return len(approx) < 4  # or len(approx) > 15

    def set_gaussian_kernel_size(self, kernel_width, kernel_height):
        # (int, int), gaussian_kernel_size: Gaussian kernel size as(width, height) for Gaussian blur of image
        self.gaussian_kernel_size = (kernel_width, kernel_height)

    def set_gaussian_x_std_dev(self, x_std_dev):
        # int, gaussian_x_std_dev: for Gaussian blur, the Gaussian kernel standard deviation in X direction.
        self.gaussian_x_std_dev = x_std_dev

    def set_canny_threshold_1(self, canny_threshold):
        # int, canny_threshold_1, the first threshold for the hysteresis procedure.
        self.canny_threshold_1 = canny_threshold

    def set_canny_threshold_2(self, canny_threshold):
        # int, canny_threshold_1, the second threshold for the hysteresis procedure.
        self.canny_threshold_2 = canny_threshold

    def set_morph_dilate_kernel_size(self, kernel_width, kernel_height):
        # (int, int), morph_dilate_kernel_size: kernel size for the morphological operation, dilate as (width, height)
        self.morph_dilate_kernel_size = (kernel_width, kernel_height)

    def set_morph_rect_kernel_size(self, kernel_width, kernel_height):
        # (int, int), morph_rect_kernel_size: kernel size to create a structuring element as (width, height)
        self.morph_rect_kernel_size = (kernel_width, kernel_height)

    def set_apply_gray_scale(self, boolean):
        self.apply_gray_scale = boolean

    def set_gaussian_blur(self, boolean):
        self.apply_gaussian_blur = boolean

    def set_histogram_equakization(self, boolean):
        self.apply_histogram_equalization = boolean

    def set_canny_edge_detection_bool(self, boolean):
        self.apply_canny_edge_detection = boolean

    def set_morphological_dilate_bool(self, boolean):
        self.apply_morphological_dilate = boolean

    def set_mask_bool(self, boolean):
        self.apply_find_contour_mask = boolean

    def set_morphological_open(self, boolean):
        self.apply_morphological_open = boolean

    def set_ref_pixel(self, closed, show=False):
        """
        From a closed image, determine the frame and find the menisus/menisci, and set self.ref_row, the reference
        row/height for liquid in a container

        :param closed: closed image (contour image)
        :return: row: int, the row in the closed image where, with the frame, there were the most white pixels; finds
        the strongest horizontal line within the frame (should be meniscus)
        """
        #print(f'set reference pixel row')

        # next line can throw NoMeniscusFound exception
        row = self.find_level(closed, show)
        self.ref_row = row
        return row

    def find_level(self, closed, show=False):
        """
        Find the frame you want to look for menisci in the closed image, and then find the appropriate number
        of menisci (self.menisci_to_check)

        :param closed: closed image (contour image)
        :return: row: int, the row in the closed image where, with the frame, there were the most white pixels; finds
        the strongest horizontal line within the frame (should be meniscus)
        """
        #print(f'find level')
        # left, right, top, bottom = self.find_frame(closed, show)  # finds where region of interest is encompassed by
        # find_meniscus can raise a NoMeniscusFound error
        row = self.find_meniscus(closed, self.mask_to_search_inside)
        return row

    def find_frame(self, closed, show=False):
        """
        Finds a frame to look for menisci within a closed image. This will be based off the mask that was created
        when the user specified the area to look for the meniscus, and get the left, right, top, and bottom row or
        column number that represents a rectangle that encompasses the non zero values in the mask (the mask of
        where to look for a meniscus)

        :param closed: closed image (contour image)
        :param bool, show: True to view the drawn frame on the closed image
        :return: left, right, top, bottom, are all int, that represent the row or column that together define the frame
        """
        #print(f'find frame')
        img_height, img_width = closed.shape
        left, right, top, bottom = self.find_maximum_edges_of_mask()

        if show is True:
            img = self.img.copy()
            img = cv2.line(img, (left, top), (right, top), (0, 255, 0))
            img = cv2.line(img, (left, top), (left, bottom), (0, 255, 0))
            img = cv2.line(img, (left, bottom), (right, bottom), (0, 255, 0))
            img = cv2.line(img, (right, bottom), (right, top), (0, 255, 0))

            line = np.copy(closed)
            line = cv2.line(line, (left, top), (right, top), (255, 0, 0))
            line = cv2.line(line, (left, top), (left, bottom), (255, 0, 0))
            line = cv2.line(line, (left, bottom), (right, bottom), (255, 0, 0))
            line = cv2.line(line, (right, bottom), (right, top), (255, 0, 0))

            cv2.imshow('image', img)
            cv2.imshow('line', line)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return left, right, top, bottom

    def select_frame(self):
        """
        Allows user to see an image of both the original reference image and the closed version, and to draw
        a box on the image and use that as the selected frame of where to look for a meniscus. After drawing a box
        you can press 'r' to clear it to reselect a different box or press 'c' to choose the box that will be
        the frame.

        :return: float, the fraction relative to the image size for each side that you would have to crop from to get the
            region of interest.
        """
        # https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/
        # have it so that it opens a window so the user can click and drag to define the frame for the image, and then
        # that is the frame that should be set, so it also needs to  set crop left, right, top, bottom

        # todo make a change so  that will resize the image to the size of the screen, which means need to track the
        # old and new image width and height in order to convert between the two since getting the x and y points in
        # an image of the resized image will not be the same as for the original image size
        # when making points to connect, the last point will be drawn as connected to the first point in the mask
        # when trying to draw it
        # reset any selections made before
        self.mask_to_search_inside = None
        self.list_of_frame_points_frame_points_tuple = []
        self.list_of_frame_points_frame_points_list = []
        # also need to add stuff to make a mask

        def make_selection(event, x, y, flags, param):
            # current_mouse_position = None
            # if left mouse button clicked, record the starting(x, y) coordinates
            if event is cv2.EVENT_LBUTTONDOWN:
                self.list_of_frame_points_frame_points_tuple.append((x, y))
                self.list_of_frame_points_frame_points_list.append([x, y])
            # check if left mouse button was released
            # elif event is cv2.EVENT_LBUTTONUP:
            #     self.list_of_frame_points_frame_points_tuple.append((x, y))

            # if event is cv2.EVENT_MOUSEMOVE:
            #     current_mouse_position = (x, y)

            if event is cv2.EVENT_LBUTTONUP:
                if len(self.list_of_frame_points_frame_points_tuple) >= 2:
                    cv2.line(image, self.list_of_frame_points_frame_points_tuple[-2],
                             self.list_of_frame_points_frame_points_tuple[-1], (0, 255, 0), 2)
                    cv2.line(closed_image, self.list_of_frame_points_frame_points_tuple[-2],
                             self.list_of_frame_points_frame_points_tuple[-1], (255, 0, 0), 2)

            if len(self.list_of_frame_points_frame_points_tuple) > 0:
                # draw rectangle around selected region
                cv2.line(image, self.list_of_frame_points_frame_points_tuple[-1],
                         self.list_of_frame_points_frame_points_tuple[-1],
                         (0, 255, 0), 2)
                cv2.line(closed_image, self.list_of_frame_points_frame_points_tuple[-1],
                         self.list_of_frame_points_frame_points_tuple[-1], (255, 0, 0), 2)
                cv2.imshow('Select frame', image)
                cv2.imshow('Select frame - closed', closed_image)
                # # box_width = self.list_of_frame_points_frame_points_tuple[1][0] -
                # # self.list_of_frame_points_frame_points_tuple[0][0]
                # # box_height = self.list_of_frame_points_frame_points_tuple[1][1] -
                # # self.list_of_frame_points_frame_points_tuple[0][1]
                # draw where current mouse position is as a line to the last point that was made
                    # print(f'box width: {box_width}')
                    # print(f'box height: {box_height}')

        # clone image and set up cv2 window
        image = self.ref_img.copy()
        clone = self.ref_img.copy()
        closed_image = self.ref_closed.copy()
        closed_clone = self.ref_closed.copy()

        cv2.namedWindow('Select frame')
        cv2.namedWindow('Select frame - closed')
        cv2.setMouseCallback('Select frame', make_selection)
        cv2.setMouseCallback('Select frame - closed', make_selection)

        # keep looping until 'q' is pressed
        while True:

            # print(f"frame points tuple: {self.list_of_frame_points_frame_points_tuple}")
            # print(f"frame points list: {self.list_of_frame_points_frame_points_list}")
            # display image, wait for a keypress
            cv2.imshow('Select frame', image)
            cv2.imshow('Select frame - closed', closed_image)
            key = cv2.waitKey(1) & 0xFF

            # if 'r' key is pressed, reset the cropping region
            if key == ord('r'):
                image = clone.copy()
                closed_image = closed_clone.copy()
                self.list_of_frame_points_frame_points_tuple = []
                self.list_of_frame_points_frame_points_list = []

            # if 'c' key pressed break from while True loop
            elif key == ord('c'):
                # make into np array because this is the format it is needed to make a mask
                self.list_of_frame_points_frame_points_list = np.array(self.list_of_frame_points_frame_points_list)
                break

        # create mask inside of which the liquid level will be searched for
        # use this for help:
        # https://stackoverflow.com/questions/48301186/cropping-concave-polygon-from-image-using-opencv-python
        # create initial blank (all black) mask
        self.mask_to_search_inside = np.zeros(shape=clone.shape[0:2], dtype=np.uint8)
        cv2.imshow("mask_to_search_inside_initial", self.mask_to_search_inside)
        # make the mask; connect the points that the user selected to create the mask area, and make that area white
        # pixels. the mask automatically connects the first and last points that were made, to create an enclosed
        # area for the mask
        cv2.drawContours(self.mask_to_search_inside, [self.list_of_frame_points_frame_points_list], -1,
                         (255, 255, 255), -1, cv2.LINE_AA)

        # do bit wise operation, this gives image with only selected region showing, and everything else is black
        dst = cv2.bitwise_and(clone, clone, mask=self.mask_to_search_inside)
        cv2.imshow("image+mask", dst)

        cv2.waitKey(0)
        cv2.destroyAllWindows()

    def select_tolerance(self):
        """
        User draws a line where they want the top of the tolerance to be at, and this will automatically
        adjust the tolerance of meniscus height. The distance between the reference and the selected upper tolerance
        limit will be identical to the code calculated lower tolerance level.

        :return:
        """
        self.tolerance_points = []
        def make_selection(event, x, y, flags, param):

            # if left mouse button clicked, record the starting(x, y) coordinates
            if event is cv2.EVENT_LBUTTONDOWN:
                self.tolerance_points.append((x, y))

            if len(self.tolerance_points) is 1:
                # draw line for tolerance
                tolerance_left = (0, self.tolerance_points[0][1])
                tolerance_right = (self.ref_img_width, self.tolerance_points[0][1])
                cv2.line(image, tolerance_left, tolerance_right, (0, 255, 0), 2)
                cv2.imshow('Select tolerance', image)

        # clone image and set up cv2 window
        image = self.ref_img.copy()
        clone = self.ref_img.copy()

        cv2.namedWindow('Select tolerance')
        cv2.setMouseCallback('Select tolerance', make_selection)

        # keep looping until 'q' is pressed
        while True:
            # display image, wait for a keypress
            cv2.imshow('Select tolerance', image)
            key = cv2.waitKey(1) & 0xFF

            # if 'r' key is pressed, reset the cropping region
            if key == ord('r'):
                self.tolerance_points = []
                image = clone.copy()

            # if 'c' key pressed break from while True loop
            elif key == ord('c'):
                break

        # if there are a reference point then select the region of interest from the image to display
        if len(self.tolerance_points) == 1:
            cv2.namedWindow('Selected tolerance')
            cv2.imshow('Selected tolerance', image)
            cv2.waitKey(0)

            # adjust tolerance level based on user input on image
            tol = self.ref_row - self.tolerance_points[0][1]
            self.tolerance = tol/self.ref_img_height
            #print(f'new tolerance: {self.tolerance}')

        cv2.destroyAllWindows()

    def find_maximum_edges_of_mask(self):
        # from the mask to search for a liquid level, find the row and column values that together would make a
        # rectangle surrounding the area that encompasses all non zero values of the mask. top, would be the top row,
        # right would be the right column, and so on
        left = 1000*1000
        right = 0
        top = 1000*1000
        bottom = 0
        for idx, pixel_point_value in enumerate(self.list_of_frame_points_frame_points_list):
            # loop through all the points that the user selected to create the outline of the mask inner list is
            # a list of the x and y coordinates of that point
            if pixel_point_value[0] < left:
                left = pixel_point_value[0]
            if pixel_point_value[0] > right:
                right = pixel_point_value[0]
            if pixel_point_value[1] < top:
                top = pixel_point_value[1]
            if pixel_point_value[1] > bottom:
                bottom = pixel_point_value[1]
        return left, right, top, bottom

    def find_meniscus(self, edge, mask_to_search_inside=None):
        """
        Finds a/multiple menisci within a frame of a edge image (contour image). Updates self.row and
        self.menisci_array, the single
        row where the only/strongest horizontal line/meniscus is, and an array of the rows with lines/menisci
        ordered by rank (higher means stronger horizontal line)
        :param edge: contour image
        :param np.array, mask_to_search_inside: a mask. the area of which is where you should search to find the
            meniscus in the edge image. areas (pixels) not to search should have a value of 0.
        :return:
        """
        menisci_data_frame = pd.DataFrame(columns=('row', 'fraction_of_pixels'))  # create a pandas dataframe,
        # with 2 rows
        img_height, img_width = edge.shape  # size of the edge image
        left_row, right_row, top_row, bottom_row = self.find_maximum_edges_of_mask()
        rows = range(top_row, bottom_row, self.rows_to_count)  # the rows to consider for iteration; but for this
        # list only iterate over rows, separated self.rows_to_count
        cols = range(left_row, right_row)  # columns to consider

        for row in rows:  # iterate through every section, by iterating through rows separated by self.rows_to_count
            list_of_fractions_of_white_pixels_in_a_section = []
            total_number_of_pixels_in_section = 0
            for i in range(0, self.rows_to_count):  # iterate through a row in a section
                number_of_pixels_in_this_row = 0
                number_of_white_pixels_in_this_row = 0
                for col in cols:  # iterate through the columns in a row
                    if row+i <= img_height-1:  # if for the original row plus the 'offset' to consider the next few
                        # rows for finding the meniscus doesn't go out of bounds of the image height
                        if self.mask_to_search_inside[row + i][col] > 0:  # if in the mask the value is greater than
                            # 0 at that pixel, which means it was part of the selection area to search in
                            number_of_pixels_in_this_row = number_of_pixels_in_this_row + 1
                            if edge[row+i][col] > 0:  # if the pixel at that row and column location in the edge
                                # image is greater than zero aka a white pixel
                                number_of_white_pixels_in_this_row = number_of_white_pixels_in_this_row + 1
                total_number_of_pixels_in_section = total_number_of_pixels_in_section + number_of_pixels_in_this_row
                if number_of_pixels_in_this_row is 0:  # sometimes division by zero error can occur
                    number_of_pixels_in_this_row = 1
                fraction_of_white_pixels_in_this_row = number_of_white_pixels_in_this_row / number_of_pixels_in_this_row
                list_of_fractions_of_white_pixels_in_a_section.append(fraction_of_white_pixels_in_this_row)
            if len(list_of_fractions_of_white_pixels_in_a_section) is 0:
                average_fraction_of_white_pixels_in_a_section = 0
            else:
                average_fraction_of_white_pixels_in_a_section = \
                    sum(list_of_fractions_of_white_pixels_in_a_section)/len(list_of_fractions_of_white_pixels_in_a_section)
            if average_fraction_of_white_pixels_in_a_section >= self.find_meniscus_minimum:
                # if there is more than the minimum white pixel count required in a section identify the section as
                # having a liquid level
                menisci_data_frame = menisci_data_frame.append(
                    {'row': int(row+(self.rows_to_count//2)), 'fraction_of_pixels':
                        average_fraction_of_white_pixels_in_a_section},
                    ignore_index=True
                    )  # append a row to the panda dataframe, where the value for row is the middle  row in the rows
                #  that were used to find the meniscus, and the pixel count is number of white pixels  counted

        menisci_sorted = menisci_data_frame.sort_values(by=['fraction_of_pixels'], ascending=False, kind='mergesort')
        # after finding all the rows with white pixels in them, then sort the dataframe by pixel count, so that the
        # row with the most white pixels is sorted at the top (or first) of the dataframe
        menisci_array = menisci_sorted.values  # make an array out of the sorted values

        row_array = []  # array sorted of the rows with most pixel count

        # next block basically to extract the order of rows with the highest pixel count into its own array called
        # row_array
        for row in menisci_array:
            row_array.append(int(row[0]))

        try:
            self.row = row_array[0]  # try to find the row with the most white pixels in it
        except IndexError:  # if couldn't find a meniscus in the array/couldn't find anything in the array,
            # because there were no white pixels to have been found in the region of interest in the edge image
            if self.no_error is False:
                raise NoMeniscusFound(self.img, edge)  # raise the NoMeniscusFound error
            else:
                # print('meniscus was not found setting meniscus to be the top of the image')
                # so becuase you dont want this to error out, then set the meniscus to be the top of the image at row 0
                row_array.append(0)
                self.row = row_array[0]

        if self.menisci_to_check is 0:
            number_of_menisci_to_check = 999
        else:
            number_of_menisci_to_check = self.menisci_to_check

        self.menisci_array = row_array[0:int(number_of_menisci_to_check)]  # store the values for the rows for the number
        # of menisci that the user wanted to check aka if user wanted to look for 2 menisci, then save the row values
        #  for the top 2 'found menisci'. if the self.menisci_to_check was set to 0 because user doesn't want to
        # set a specific number to check, this will still work because 999 will actually be passed to the row_array
        # call to find 999 menisci; it will go to the maximum number it can go to

        return row_array[0]

    def number_of_levels_last_found(self):
        # return number of liquid levels last found when find_meniscus was run
        return len(self.menisci_array)

    def in_tolerance(self):
        """
        Checks if a just measured liquid level is within the tolerance of the reference liquid level
        :return: bool: whether you are in the tolerance (True) or not (False). float, percent_diff: the fraction away
            from the tolerance level the current meniscus is. The float value is a percentage relative to the entire
            height of the image
        """
        #print(f'check if meniscus is in tolerance')
        upper_limit = self.ref_row + int(self.ref_img_height * self.tolerance)
        lower_limit = self.ref_row - int(self.ref_img_height * self.tolerance)

        # if the liquid level is above the reference, then ref minus measured > 0

        diff = self.ref_row - self.row
        percent_diff = diff / self.ref_img_height

        if self.row in range(lower_limit, upper_limit):
            #print('liquid level is within tolerance')
            #print(f'liquid level is within tolerance')

            if percent_diff >= 0:
                print(f'liquid level measured is above reference: {percent_diff} percent')
                print(f'liquid level tolerance is {self.tolerance} percent')

            else:
                print(f'liquid level measured is below reference: {percent_diff} percent')
                print(f'liquid level tolerance is {self.tolerance} percent')

            return True, percent_diff

        else:
            #print('liquid level is not within tolerance')
            #print(f'liquid level deviates from the reference by {percent_diff} percent')

            #print('liquid level is not within tolerance')
            #print(f'liquid level deviates from the reference by {percent_diff} percent')
            return False, percent_diff

    def draw_menisci(self, img):
        """
        Draws all menisci lines on the original image at where the menisci were calculated
        :param img: image to draw line on
        :return: line: the image with all the menisci drawn on it
        """
        for row in self.menisci_array:
            self.row = row
            img = self.draw_level_line(img)

        return img

    def draw_lines(self, save=False, show=False, img=None):
        """
        Draws all the lines (reference, actual level, and tolerance) on the original image at where the calculated
        menisus is and either show or save the image
        :param img, the image you want to draw on
        :param bool, save: True if you want to save the image to directory
        :param bool, show: True if you just want to view the image
        :return: line: image with lines drawn on it
        """
        if img is None:
            line = self.img
        else:
            line = img
        line = self.draw_menisci(line)
        line = self.draw_ref_line(line)
        line = self.draw_tolerance_lines(line)

        if save is True:
            pass
            # cv2.imwrite('draw_lines.jpg', line)
        if show is True:
            cv2.imshow('draw_lines', line)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return line

    def draw_level(self, save):
        """
        Draw the current meniscus on the original image and either show or save the image

        :param save: bool, True if you want to save the image to directory, False if you just want to view the image
        :return: line: image with line drawn on
        """
        line = self.img
        line = self.draw_level_line(line)

        if save is True:
            # cv2.imwrite('draw_level.jpg', line)
            pass
        else:
            cv2.imshow('draw_level', line)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return line

    def draw_ref(self, save=False, show=False):
        """
        Draw the reference line on the original image and either show or save the image
        :param save: bool, True if you want to save the image to directory
        :param bool, show: True if you want to display the image
        :return: line: image with line drawn on
        """
        line = self.img
        line = self.draw_ref_line(line)

        if save is True:
            pass
            # cv2.imwrite('draw_ref.jpg', line)
        if show is True:
            cv2.imshow('draw_ref', line)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return line

    def draw_tolerance(self, save):
        """
        Draw lines of the tolerance boundaries for the reference meniscus on the original image and either show or save
        the image
        :param save: bool, True if you want to save the image to directory, False if you just want to view the image
        :return: line: image with line drawn on
        """
        line = self.img
        line = self.draw_tolerance_lines(line)

        if save is True:
            pass
            # cv2.imwrite('draw_tolerance.jpg', line)
        else:
            cv2.imshow('draw_tolerance', line)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return line

    def draw_level_line(self, img):
        """
        Draw the current meniscus on the image
        :param img: image to draw line on
        :return:
        """
        check_top_left = (0, self.row)  # the 'top left' value for the line, really just the left part of the line.
        # check means that it is the determined meniscus water level line
        check_lower_right = (self.ref_img_width, self.row)
        # draw green line for level you just checked
        img = cv2.line(img, check_top_left, check_lower_right, (0, 255, 0))
        cv2.putText(img, 'liquid level', (0, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
        return img

    def draw_ref_line(self, img):
        """
        Draw the reference line on the image
        :param img: image to draw line on
        :return:
        """
        ref_top_left = (0, self.ref_row)
        ref_lower_right = (self.ref_img_width, self.ref_row)
        # draw red line for reference
        img = cv2.line(img, ref_top_left, ref_lower_right, (0, 0, 255))
        cv2.putText(img, 'reference', (0, 15), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
        return img

    def draw_tolerance_lines(self, img):
        """
        Draw lines of the tolerance boundaries for the reference meniscus on the image
        :param img: image to draw line on
        :return:
        """
        tolerance_1_top_left = (0, self.ref_row - int(self.ref_img_height * self.tolerance))
        tolerance_1_lower_right = (self.ref_img_width, self.ref_row - int(self.ref_img_height * self.tolerance))
        tolerance_2_top_left = (0, self.ref_row + int(self.ref_img_height * self.tolerance))
        tolerance_2_lower_right = (self.ref_img_width, self.ref_row + int(self.ref_img_height * self.tolerance))
        # draw blue line for tolerance
        img = cv2.line(img, tolerance_1_top_left, tolerance_1_lower_right, (255, 0, 0))
        img = cv2.line(img, tolerance_2_top_left, tolerance_2_lower_right, (255, 0, 0))
        cv2.putText(img, 'tolerance', (0, 45), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0))
        return img

    def start(self, select_region_of_interest, set_reference, select_tolerance):
        """
        First thing that should be run after making the liquid level instance. This will cause the camera to take a
        picture and allow the user to choose if they want to select a region of interest to look for the meniscus and
        if they want to select a tolerance leve in the image - tolerance mainly used if you want to also use computer
        vision to self correct for meniscus drift

        :param bool, select_region_of_interest: If true, then allow user to select the area to look for the meniscus
        :param bool, set_reference: If True, then find where the most prominent liquid level is and set the reference
            level
        :param bool, select_tolerance: If true, then allow user to select the area that will be the tolerance -
            really only applies for when you have applications where you want to keep the meniscus within a range
            using computer vision
        :return:
        """
        #print(f'start monitoring liquid level - finding the reference meniscus')
        # cam is the camera you want to do everything with
        # set up experiment with the 2 cameras
        # need to have already taken a few pictures and found optimal crop values
        ref_img = self.camera.take_picture()
        # next line can throw NoMeniscusFound exception
        closed = self.load_image_and_select_and_set_parameters(img=ref_img,
                                                               select_region_of_interest=select_region_of_interest,
                                                               set_reference=set_reference,
                                                               select_tolerance=select_tolerance,
                                                               )
        time = datetime.now()
        self.all_images_with_lines.append([time.strftime('%d_%m_%Y_%H_%M_%S'), self.draw_ref(False, False)])
        self.all_images_no_lines.append([time.strftime('%d_%m_%Y_%H_%M_%S'), self.ref_img])
        self.all_images_edge.append([time.strftime('%d_%m_%Y_%H_%M_%S'), closed])

        # while the experiment is still running
        # call self.run(cam) after every cycle of liquid transfer

        return

    def take_photo(self):
        img = self.camera.take_picture()
        time = datetime.now()
        return img, time

    def take_photo_add_to_memory(self):
        img, time = self.take_photo()
        self.add_image_to_memory(img=img,
                                 img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                 array_to_save_to=self.all_images_no_lines,
                                 )
        return img, time
        # self.all_images_no_lines.append([time.strftime('%d_%m_%Y_%H_%M_%S'), img])

    def add_image_to_memory(self, img, img_name, array_to_save_to):
        # add images to one of the arrays used to store images taken in the experiment, organized as
        # [[timestamp, img], [timestamp, img]...]
        array_to_save_to.append([img_name, img])

    def take_photo_find_contour_add_to_memory(self):
        img, time = self.take_photo()
        edge = self.load_and_find_contour(img=img)
        self.add_image_to_memory(img=img,
                                 img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                 array_to_save_to=self.all_images_no_lines,
                                 )
        self.add_image_to_memory(img=edge,
                                 img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                 array_to_save_to=self.all_images_edge,
                                 )
        return img, edge, time

    def take_photo_find_levels_add_to_memory(self):
        img, time = self.take_photo()
        edge = self.load_and_find_level(img=img, show=False)
        self.add_image_to_memory(img=self.img,
                                 img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                 array_to_save_to=self.all_images_no_lines,
                                 )
        self.add_image_to_memory(img=edge,
                                 img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                 array_to_save_to=self.all_images_edge,
                                 )
        self.add_image_to_memory(img=self.draw_lines(show=False, save=False),
                                 img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                 array_to_save_to=self.all_images_with_lines,
                                 )
        return img, edge, time

    def save_drawn_image(self):
        """
        Write the last image with drawn to the computer

        :return:
        """
        # todo need to separate this and have liquid level have its own separate folder
        # img = self.all_images_with_lines[-1]
        date_time_with_line, line_img = self.all_images_with_lines[-1]
        cv2.imwrite(f'{self.camera.raw_images_dir}\\drawn_{date_time_with_line}.jpg', line_img)

    def run(self, show=False, save=False):
        #print(f'find meniscus and check if it is in tolerance')
        # this code is to be run after a cycle of liquid transfer has occurred
        img = self.camera.take_picture()
        # next line can throw NoMeniscusFound exception
        edge = self.load_and_find_level(img, show)
        tolerance_bool, percent_diff = self.in_tolerance()
        time = datetime.now()
        self.all_images_with_lines.append([time.strftime('%d_%m_%Y_%H_%M_%S'), self.draw_lines(show=show, save=save)])
        self.all_images_no_lines.append([time.strftime('%d_%m_%Y_%H_%M_%S'), self.img])
        self.all_images_edge.append([time.strftime('%d_%m_%Y_%H_%M_%S'), edge])

        return tolerance_bool, percent_diff

    def take_picture_find_levels(self):
        # take a picture, find the liquid levels, and return the number of levels that were found
        self.take_photo_find_levels_add_to_memory()
        number_of_levels_found = self.number_of_levels_last_found()

        return number_of_levels_found

    def find_pump_to_pixel_ratio(self,
                                 pump,  # instance of a pump
                                 direction,  # either -1 or 1 for peristaltic, or 0 or 1 for syringe pump. this
                                 # is the direction to move liquid out of the liquid being watched (technically
                                 # direction is called state for a cavro pump)
                                 time_to_pump=None,  # time to pump in seconds; applies only for peristaltic pumps
                                 rpm=None,  # rpm to use to pump; applies only for peristaltic pumps
                                 volume=None,  # volume to pump;  applies only to syringe pumps
                                 flowrate_pull=None,  # flowrate to pump;  applies only to syringe pumps
                                 ):
        # the numbers in should come from after you have found the dead volume for the specfic pump and set up.

        # if using a peristaltic pump, need to put something in for time_to_pump, rpm, and direction when initializing.
        # if using a syringe pump, need to put something in for direction, when initializing.
        # function to automate 'calibration' of a pump. This means create a relation between a pump (rpm and time if it
        # is a peristaltic pump, or volume for a syringe pump) and how far (pixels in an image)
        # pump can either be a type of syringe pump or peristaltic pump (peristaltic pump is controlled by the
        # PeristalticPumpControl class, currently in Gronckle)
        # flowrate_pull is just used so user can specify a slow enough rate to prevent caviation when pulling liquid
        # for this, the liquid in the container needs to be within view of the camera; somewhere in the middle is best

        # todo rename variables in here; the second image should not be called final
        # take a photo, find where the liquid level is at. this will have to be done before the actual peristaltic
        # loop run so this function will use self.start to create a reference image and get the user to select a ROI

        self.start(select_region_of_interest=True, set_reference=True, select_tolerance=False)
        # take a photo, find where the liquid level is; this should be different from the initial
        initial_image = self.camera.take_picture()
        self.load_and_find_level(initial_image)
        initial_image_copy = initial_image.copy()
        initial_image_with_liquid_line_drawn = self.draw_menisci(img=initial_image_copy)
        cv2.imshow("initial image before first pump", initial_image_with_liquid_line_drawn)
        # next line can throw NoMeniscusFound exception
        initial_image_meniscus_row = self.row  # self.row now should be the row where the current meniscus is found at
        print(f'before first pump, liquid level is at {initial_image_meniscus_row} pixel height')

        # pump according to the time and rpm OR volume, in the correct direction; this should be so that vol that
        # gets pumped > dead volume
        if time_to_pump is not None and rpm is not None:  # is using peristaltic pump
            pump.spin_joint(time=time_to_pump,
                            rpm=rpm,
                            direction=direction)
        else:  # if using syringe pump
            pump.set_state(direction)
            pump.start(  # start pump
                volume=volume,  # for target volume
                direction='pull',  # in the pull direction
                flowrate=flowrate_pull,
            )
            direction = 1 - direction  # invert and change state
            pump.set_state(direction, 1.)  # invert state
            pump.empty()  # empty the syringe
        print(f'finish first pump')
        # take a photo, find where the liquid level is; this should be different from the initial
        final_image = self.camera.take_picture()
        self.load_and_find_level(final_image)
        final_image_copy = final_image.copy()
        final_image_with_liquid_line_drawn = self.draw_menisci(img=final_image_copy)
        cv2.imshow("image after first pump", final_image_with_liquid_line_drawn)
        # next line can throw NoMeniscusFound exception
        final_image_meniscus_row = self.row  # self.row now should be the row where the current meniscus is found at
        print(f'after first pump, liquid level is at {final_image_meniscus_row} pixel height')

        # find the difference between the two liquid levels in terms of pixels
        pixel_difference = final_image_meniscus_row - initial_image_meniscus_row  # this should be a positive number since liquid
        # should have been pumped out of the container, and so meniscus travels down
        print(f'pixel height travelled: {pixel_difference}')

        print(f'return liquid moved back to original vial')
        # update the pump_to_pixel_ratio dictionary appropriately depending on the type of pump that was used
        if time_to_pump is not None and rpm is not None:  # is using peristaltic pump
            self.pump_to_pixel_ratio['time'] = time_to_pump
            self.pump_to_pixel_ratio['rpm'] = rpm
            self.pump_to_pixel_ratio['pixels'] = pixel_difference

        else:  # if using syringe pump
            self.pump_to_pixel_ratio['volume'] = volume
            self.pump_to_pixel_ratio['pixels'] = pixel_difference

        # pump at the same time and rpm OR volume in the opposite direction to return the liquid level back to
        # the reference location
        if time_to_pump is not None and rpm is not None:  # is using peristaltic pump
            reverse_direction = -1 * direction
            pump.spin_joint(time=time_to_pump,
                            rpm=rpm,
                            direction=reverse_direction)
        else:  # if using syringe pump
            reverse_direction = 1 - direction  # find the reverse of the first direction
            pump.set_state(reverse_direction)
            pump.start(  # start pump
                volume=volume,  # for target volume
                direction='pull',  # in the pull direction
                flowrate=flowrate_pull,
            )
            reverse_direction = 1 - reverse_direction  # invert and change state
            pump.set_state(reverse_direction, 1.)  # invert state
            pump.empty()  # empty the syringe

        image_after_second_pump = self.camera.take_picture()
        self.load_and_find_level(image_after_second_pump)
        image_after_second_pump_copy = image_after_second_pump.copy()
        image_after_second_pump_with_liquid_line_drawn = self.draw_menisci(img=image_after_second_pump_copy)
        cv2.imshow("image after second pump", image_after_second_pump_with_liquid_line_drawn)
        # next line can throw NoMeniscusFound exception
        after_second_pump_meniscus_row = self.row  # self.row now should be the row where the current meniscus is found at
        print(f'after second pump, liquid level is at {after_second_pump_meniscus_row} pixel height')
        print(f'pixel height travelled: {after_second_pump_meniscus_row - final_image_meniscus_row}')
        print(f'difference between intial and final pixel height row: '
              f'{initial_image_meniscus_row - after_second_pump_meniscus_row}')
        print(f'the pump to pixel ratio is {self.pump_to_pixel_ratio}')
        cv2.waitKeyEx(0)
        cv2.destroyAllWindows()

    def find_ruler(self, image, camera, ruler_size):
        # image is an image
        # camera is a camera instance
        # ruler_size is the length in mm of what you are selecting
        # from an image that has some kind of reference on it, select an area (that reference), and the height of what
        # gets selected is used to find the pixel to mm ratio for that image
        # float values you would need to crop from the edges of the images to get the selected area
        image = image.copy()
        camera.select_frame(img=image)
        # find row and column values for the left, right, top, and bottom of the selected region of interest
        left, right, top, bottom = camera.find_frame(img=image, show=False)
        pixel_to_mm = (bottom - top)/ruler_size
        self.pixel_to_mm_ratio = pixel_to_mm
        return pixel_to_mm

    def draw_frame(self, image, left, right, top, bottom):
        # pixel values for the edges of the region of interest (frame) in terms of rows and columns
        image_with_frame = image.copy()
        # draw frame in green on the image
        image_with_frame = cv2.line(image_with_frame, (left, top), (right, top), (0, 255, 0))
        image_with_frame = cv2.line(image_with_frame, (left, top), (left, bottom), (0, 255, 0))
        image_with_frame = cv2.line(image_with_frame, (left, bottom), (right, bottom), (0, 255, 0))
        image_with_frame = cv2.line(image_with_frame, (right, bottom), (right, top), (0, 255, 0))

        return image_with_frame

    def measure_with_ruler(self, pixel_1, pixel_2, pixel_to_mm=None):
        if pixel_to_mm is None:
            pixel_to_mm = self.pixel_to_mm_ratio
        # pixel one and two are either 2 values for columns in an image, or two rows. The differen between those
        # pixels is then converted to mm. pixel_2 > pixel_1
        dist = pixel_2 - pixel_1
        dist_in_pixels = dist / pixel_to_mm
        return dist_in_pixels

    def select_and_measure_with_ruler(self, image, camera=None, pixel_to_mm=None):
        # let user select frame on an image and let user known distance in mm for the width and height of the box
        # drawn in mm.
        # todo camera is camera instance; for now use that until can isolate a version of select frame that
        # doesnt change self.values of things
        if camera is None:
            camera = self.camera
        if pixel_to_mm is None:
            pixel_to_mm = self.pixel_to_mm_ratio
        image = image.copy()
        camera.select_frame(image)
        left, right, top, bottom = camera.find_frame(img=image, show=False)
        horizontal_distance_in_mm = self.measure_with_ruler(pixel_1=left, pixel_2=right, pixel_to_mm=pixel_to_mm)
        vertical_distance_in_mm = self.measure_with_ruler(pixel_1=top, pixel_2=bottom, pixel_to_mm=pixel_to_mm)
        # draw the frame on the image
        drawn_image = self.draw_frame(image=image, left=left, right=right, top=top, bottom=bottom)
        # write the mm of width and height on image
        cv2.putText(drawn_image, f'width: {horizontal_distance_in_mm} mm', (0, 15),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
        cv2.putText(drawn_image, f'height: {vertical_distance_in_mm} mm', (0, 30),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
        cv2.imshow('measured image', drawn_image)
        return drawn_image

    def interactive_select_and_measure_with_ruler(self, image, camera=None, pixel_to_mm=None):
        if camera is None:
            camera = self.camera
        if pixel_to_mm is None:
            pixel_to_mm = self.pixel_to_mm_ratio

        image_height, image_width, _ = image.shape

        def make_selection(event, x, y, flags, param):

            # if left mouse button clicked, record the starting(x, y) coordinates
            if event is cv2.EVENT_LBUTTONDOWN:
                self.list_of_frame_points_frame_points_tuple = [(x, y)]
            # check if left mouse button was released
            elif event is cv2.EVENT_LBUTTONUP:
                self.list_of_frame_points_frame_points_tuple.append((x, y))

                # draw rectangle around selected region
                cv2.rectangle(image, self.list_of_frame_points_frame_points_tuple[0],
                              self.list_of_frame_points_frame_points_tuple[1],
                              (0, 255, 0), 2)

                # find distance in mm for the width and height of the drawn rectangle
                left = self.list_of_frame_points_frame_points_tuple[0][0]
                right = self.list_of_frame_points_frame_points_tuple[1][0]
                top = self.list_of_frame_points_frame_points_tuple[0][1]
                bottom = self.list_of_frame_points_frame_points_tuple[1][1]
                box_width = right - left
                box_height = bottom - top
                horizontal_distance_in_mm = self.measure_with_ruler(pixel_1=left, pixel_2=right,
                                                                    pixel_to_mm=pixel_to_mm)
                vertical_distance_in_mm = self.measure_with_ruler(pixel_1=top, pixel_2=bottom, pixel_to_mm=pixel_to_mm)
                cv2.putText(image, f'width: {horizontal_distance_in_mm} mm', (0, 15),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
                cv2.putText(image, f'height: {vertical_distance_in_mm} mm', (0, 30),
                            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0))
                cv2.imshow('Select frame', image)

        # clone image and set up cv2 window
        image = image.copy()
        clone = image.copy()
        cv2.namedWindow('Select frame')
        cv2.setMouseCallback('Select frame', make_selection)

        # keep looping until 'q' is pressed
        while True:
            # display image, wait for a keypress
            cv2.imshow('Select frame', image)
            key = cv2.waitKey(1) & 0xFF

            # if 'r' key is pressed, reset the cropping region
            if key == ord('r'):
                image = clone.copy()

            # if 'c' key pressed break from while True loop
            elif key == ord('c'):
                break

        cv2.destroyAllWindows()

    def calculate_smart_pump_time(self, pixels_need_to_self_correct_by):
        # todo right now this only works for if liquid level is for peristaltic pump
        # calculation for pump based on an image to move the liquid level
        # need to find out the time needed for the specific rpm to correct for the identified number of pixels
        rpm_for_pump_to_pixel_ratio = self.pump_to_pixel_ratio['rpm']
        time_for_pump_to_pixel_ratio = self.pump_to_pixel_ratio['time']
        pixels_for_pump_to_pixel_ratio = self.pump_to_pixel_ratio['pixels']

        # to know the time required to do the self correction step is then just a matter of doing:
        # time_to_self_correct = pixels_need_to_self_correct_by * 1/pixels_for_pump_to_pixel_ratio *
        # time_for_pump_to_pixel_ratio
        # then just need to pump at the rpm_for_pump_to_pixel_ratio for time_to_self_correct seconds
        # also to make things simple/work, convert the time from a potential float value into an int
        time_to_self_correct = int(pixels_need_to_self_correct_by * (
                time_for_pump_to_pixel_ratio / pixels_for_pump_to_pixel_ratio))

        return time_to_self_correct, rpm_for_pump_to_pixel_ratio


# camera = Camera(cam=0)
# l = LiquidLevel(camera)
# import ada_peripherals.vision
# import os
# root_dir = os.path.dirname(os.path.abspath(ada_peripherals.__file__))  # root directory of
#             # ada_peripherals
# vision_dir = os.path.join(root_dir, 'vision')
# test_image_path = os.path.join(vision_dir, 'test_image.jpg')
# test_image = cv2.imread(test_image_path)
# l.load_image_and_select_and_set_parameters(img=test_image, select=True)
# l.load_and_find_level(img=test_image, show=True)
# closed = l.find_contour(test_image)
# cv2.imshow('Closed', closed)
# line = l.draw_lines(show=True, save=False)
# cv2.imshow('Video for liquid_level', line)
# # cv2.waitKey(0)

# camera = Camera(1, 'vial')
# l = LiquidLevel(camera=0)
#
# root_dir = os.path.dirname(os.path.abspath(ada_peripherals.__file__))  # root directory of ada_peripherals
#
# vision_folder_name = 'vision'
# vision_folder_path = os.path.join(root_dir, vision_folder_name)

# image_path = os.path.join(vision_folder_path, 'meniscus_test_2.jpg')
#
# l.load_image_and_select_and_set_parameters(img=image_path)
# l.draw_lines(False, True)

def video_run(cam=0):

    camera = Camera(cam=cam,
                    create_save_folder=False)
    l = LiquidLevel(camera=camera,
                    rows_to_count=10,
                    menisci_to_check=0,
                    find_meniscus_minimum=0.1,
                    no_error=True,
                    )

    l.start(select_region_of_interest=True, set_reference=True, select_tolerance=False)

    video_capture = cv2.VideoCapture(cam)
    while True:
        # capture frame-by-frame
        ret, image = video_capture.read()

        if not ret:  # doesn't really matter here, it matters more for reading from an image, but for video doesnt occur
            break

        # do the liquid level stuff - the actual run and identification of the mesnicus
        l.load_and_find_level(image)
        tolerance_bool, percent_diff = l.in_tolerance()

        closed = l.find_contour(image)
        cv2.imshow('Closed', closed)

        line = l.draw_lines(show=False, save=False)

        cv2.imshow('Video for liquid_level', line)

        # if press the q button exit
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    video_capture.release()
    cv2.destroyAllWindows()



class LiquidLevelGUI:
    # example of how to use
    # my_gui = LiquidLevelGUI(root)
    # todo make buttons/textboxes to be able to alter the arguments for finding a meniscus - the actualy algorithm
    # with erode, dilate, blur...
    # todo same as above except have arguments for finding any number of menisci
    def __init__(self,
                 liquid_level,
                 slack_bot=None,
                 ):
        self.liquid_level = liquid_level
        self.slack_bot = slack_bot

        # initialize root window
        self.root = tkinter.Tk()
        self.root_background_colour = 'purple'
        self.root.configure(background=self.root_background_colour)
        self.root.title("Liquid Level GUI")
        self.root_height = self.root.winfo_height()  # the root window height
        self.root_width = self.root.winfo_width()  # the root window height

        # create container frames for the gui
        # all these height values must add to 1
        self.top_frame_height = 0.2  # float, fraction of how much of the entire window size will be for the top frame
        self.center_frame_height = 0.5  # float, fraction of how much of the entire window size will be for the top
        # frame
        self.bottom_frame_height = 0.15  # float, fraction of how much of the entire window size will be for the top
        # frame
        self.bottom_frame_2_height = 0.15  # need another container to put things in the bottom until figure out how
        # to use grid and use a better frame
        self.top_frame = tkinter.Frame(self.root, bg=self.root_background_colour, height=self.root_height*self.top_frame_height,
                                       width=self.root.winfo_width())
        self.center_frame = tkinter.Frame(self.root, bg=self.root_background_colour, height=self.root_height * self.center_frame_height, width=self.root.winfo_width())
        self.bottom_frame = tkinter.Frame(self.root, bg=self.root_background_colour, height=self.root_height * self.bottom_frame_height, width=self.root.winfo_width())
        self.bottom_frame_2 = tkinter.Frame(self.root, bg=self.root_background_colour,
                                          height=self.root_height * self.bottom_frame_2_height,
                                          width=self.root.winfo_width())
        # layout main containers
        self.top_frame.grid(row=0, column=0)
        self.center_frame.grid(row=1, column=0)
        self.bottom_frame.grid(row=2, column=0)
        self.bottom_frame_2.grid(row=3, column=0)

        # initialize other attributes here
        self.show_mask_on_contour = True  # bool, used to control if the mask outline is displayed on the live stream
        #  or not
        self.show_area_in_mask = False  # bool, used to control if when the mask contour is showing, if it should
        # just be an outline, or only showing the part of the image that is allowed by the mask

        # create button to toggle the live video stream on and off
        self.toggle_live_video_on_off_button = tkinter.Button(self.top_frame, text="Stop video live stream",
                                           command=self.toggle_live_video_on_off,)
        self.toggle_live_video_on_off_button.pack(side='left', padx=3, pady=3)

        # create button to let user select the region of interest
        self.select_region_of_interest_button = tkinter.Button(self.top_frame, text="Select region of interest",
                                                              command=self.select_region_of_interest, )
        self.select_region_of_interest_button.pack(side='left', padx=3, pady=3)

        # create button to let user select the tolerance
        self.select_tolerance_button = tkinter.Button(self.top_frame, text="Select tolerance",
                                                               command=self.select_tolerance, )
        self.select_tolerance_button.pack(side='left', padx=3, pady=3)

        # create button to let user display the outline of the contour or not, on both display image panels
        self.toggle_show_mask_contour_on_images_button = tkinter.Button(self.top_frame, text="Show mask contour on "
                                                                                             "images",
                                                               command=self.toggle_show_mask_contour_on_images, )
        self.toggle_show_mask_contour_on_images_button.pack(side='left', padx=3, pady=3)

        # create button to let user display only show whats allowed to be searched in the mask; for this,
        # the mask contour show must be True, and will switch it between showing the mask outline, or only the image
        # within the allowed area in the mask
        self.toggle_show_mask_outline_or_area_button = tkinter.Button(self.top_frame, text='Show area in mask',
                                                                        command=self.toggle_show_mask_outline_or_area, )
        self.toggle_show_mask_outline_or_area_button.pack(side='left', padx=3, pady=3)

        # button to watch until a specified number of liquid levels to appear
        self.watch_liquid_levels_bool = False
        self.found_all_liquid_levels = False
        self.liquid_level.no_error = True
        self.consecutive_number_of_times_found_n_levels_for_success = 15  # number of times must have found n liquid
        # levels in order to say that it has been found; to be more robust to small image fluctuations
        self.track_consecutive_number_of_times_found_n_levels = 0  # to track the number of times the required number
        #  of levels to track was successful
        self.first_image_of_watch_liquid_levels_bool = True  # used so that the starting printout/slack message is only
        # said once when trying to wait for for a specific number of liquid levels to appear
        self.watch_liquid_levels_button = tkinter.Button(self.top_frame, text='Watch for n number '
                                                                                                  'of liquid levels',
                                                                      command=self.toggle_watch_liquid_levels, )
        self.watch_liquid_levels_button.pack(side='right', padx=3, pady=3)

        # # making things using frame - changed back to using label for now because frame way wasn't working well
        # # dictionary to contain the values for when trying to draw a rectangle on an image on image_panel_one
        # self.image_panel_one_rectangle = {
        #     'rectangle': None,
        #     'start_x': 0,
        #     'start_y': 0,
        #     'x_zero': 0,
        #     'y_zero': 0,
        #     'current_x': 0,
        #     'current_y': 0,
        # }
        # initialize first and second image panels
        # self.image_panel_one = tkinter.Canvas(self.center_frame)  # create image panel with the image
        # self.image_panel_one.pack(side="left", padx=5,)
        # # allows you to draw a box on a frame image
        # self.image_panel_one.bind("<ButtonPress-1>", self.on_button_press_rectangle)
        # self.image_panel_one.bind("<B1-Motion>", self.on_move_press_rectangle)
        # self.image_panel_one.bind("<ButtonRelease-1>", self.on_button_release_rectangle)
        # self.image_panel_one.bind("<Button-3>", self.on_right_click_rectangle)  # right click
        # self.image_panel_two = tkinter.Canvas(self.center_frame)  # create image panel with the image
        # self.image_panel_two.pack(side="left", padx=5,)

        # create sliders for altering how find_contour of liquid level instance works
        # self.canny_threshold_1_frame = tkinter.Frame(self.bottom_frame, height=self.bottom_frame.winfo_height())
        # self.canny_threshold_1_label = tkinter.Label(self.canny_threshold_1_frame, text='canny threshold 1')

        # self.image_panel_one = tkinter.Canvas(self.center_frame)  # create image panel with the image
        # self.image_panel_one.pack(side="left", padx=5,)
        # # allows you to draw a box on a frame image
        # self.image_panel_one.bind("<ButtonPress-1>", self.on_button_press_rectangle)
        # self.image_panel_one.bind("<B1-Motion>", self.on_move_press_rectangle)
        # self.image_panel_one.bind("<ButtonRelease-1>", self.on_button_release_rectangle)
        # self.image_panel_one.bind("<Button-3>", self.on_right_click_rectangle)  # right click
        # self.image_panel_two = tkinter.Canvas(self.center_frame)  # create image panel with the image
        # self.image_panel_two.pack(side="left", padx=5,)

        self.image_panel_one = tkinter.Label(self.center_frame)  # create image panel with the image
        self.image_panel_one.pack(side="left", padx=3,)
        self.image_panel_two = tkinter.Label(self.center_frame)  # create image panel with the image
        self.image_panel_two.pack(side="left", padx=3,)
        self.camera_cv_last_image = None  # to keep the most current image from the live stream, before converted
        # into tkinter image

        self.canny_threshold_1_label = tkinter.Label(self.bottom_frame, text='canny threshold 1')
        self.canny_threshold_1_label.pack(side='left', padx=3, pady=3)
        self.canny_threshold_1_scale = tkinter.Scale(self.bottom_frame, from_=0, to=300, orient=tkinter.HORIZONTAL)
        self.canny_threshold_1_scale.set(self.liquid_level.canny_threshold_1)
        # self.canny_threshold_1_scale.grid(row=1, column=0)
        self.canny_threshold_1_scale.pack(side='left', padx=3, pady=3)

        self.canny_threshold_2_scale = tkinter.Scale(self.bottom_frame, from_=0, to=300, orient=tkinter.HORIZONTAL)
        self.canny_threshold_2_scale.set(self.liquid_level.canny_threshold_2)
        # self.canny_threshold_2_scale.grid(row=1, column=1)
        self.canny_threshold_2_label = tkinter.Label(self.bottom_frame, text='canny threshold 2')
        self.canny_threshold_2_label.pack(side='left', padx=3, pady=3)
        self.canny_threshold_2_scale.pack(side='left', padx=3, pady=3)

        # # slider for gaussian blur
        # self.gaussian_x_std_dev_label = tkinter.Label(self.bottom_frame, text='gaussian kernel size x std dev')
        # self.gaussian_x_std_dev_label.pack(side='left', padx=5, pady=5)
        # self.gaussian_x_std_dev_scale = tkinter.Scale(self.bottom_frame, from_=0, to=10,
        #                                                     orient=tkinter.HORIZONTAL)
        # self.gaussian_x_std_dev_scale.set(self.liquid_level.gaussian_x_std_dev)
        # self.gaussian_x_std_dev_scale.pack(side='left', padx=5, pady=5)
        #
        # # slider for morphological dilation kernel size
        # self.morph_dilate_kernel_size_label = tkinter.Label(self.bottom_frame, text='morph dilate kernel size')
        # self.morph_dilate_kernel_size_label.pack(side='left', padx=5, pady=5)
        # self.morph_dilate_kernel_size_scale = tkinter.Scale(self.bottom_frame, from_=1, to=10,
        #                                               orient=tkinter.HORIZONTAL)
        # self.morph_dilate_kernel_size_scale.set(self.liquid_level.morph_dilate_kernel_size[0])
        # self.morph_dilate_kernel_size_scale.pack(side='left', padx=5, pady=5)

        # slider for the number of rows to count to find the meniscus
        self.rows_to_count_label = tkinter.Label(self.bottom_frame, text='number of rows to count')
        self.rows_to_count_label.pack(side='left', padx=5, pady=5)
        self.rows_to_count_scale = tkinter.Scale(self.bottom_frame, from_=1, to=15,
                                                            orient=tkinter.HORIZONTAL)
        self.rows_to_count_scale.set(self.liquid_level.rows_to_count)
        self.rows_to_count_scale.pack(side='left', padx=3, pady=3)

        # slider for the number of menisci to check
        self.menisci_to_check_label = tkinter.Label(self.bottom_frame_2, text='number of menisci to check')
        self.menisci_to_check_label.pack(side='left', padx=3, pady=3)
        self.menisci_to_check_scale = tkinter.Scale(self.bottom_frame_2, from_=0, to=5,
                                                 orient=tkinter.HORIZONTAL)
        self.menisci_to_check_scale.set(self.liquid_level.menisci_to_check)
        self.menisci_to_check_scale.pack(side='left', padx=3, pady=3)

        # slider for the minimum fraction of a slice of of a contour image that needs to be white pixels in order for
        #  that slice to be considered to have/be a liquid level
        self.find_meniscus_minimum_label = tkinter.Label(self.bottom_frame_2, text='fraction of a slice needed to be a '
                                                                                   'liquid level')
        self.find_meniscus_minimum_label.pack(side='left', padx=3, pady=3)
        self.find_meniscus_minimum_scale = tkinter.Scale(self.bottom_frame_2, from_=0, to=100,
                                                    orient=tkinter.HORIZONTAL)
        self.find_meniscus_minimum_scale.set(self.liquid_level.find_meniscus_minimum*100)
        self.find_meniscus_minimum_scale.pack(side='left', padx=3, pady=3)

        # set values and stuff for video streaming to the gui
        self.video_capture = cv2.VideoCapture(self.liquid_level.camera.cam)  # create the video capture instance
        self.thread = None  # need a thread to pool video
        self.stop_event = None  # need a way to know when to stop pooling video
        self.stop_event = threading.Event()
        self.thread = threading.Thread(target=self.video_stream, args=())
        self.thread.start()

        # what to happen when you try to exit out of the gui window
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

    def on_closing(self):
        if tkinter.messagebox.askokcancel("Quit", "Do you want to quit?"):
            if not self.stop_event.is_set():
                self.toggle_live_video_on_off()
            self.root.destroy()

    # todo add button to draw the lines on the image or not

    def video_stream(self):
        try:  # have a try except because sometimes stop event is changed while in the middle which can cause issues
            # to rise because it cannot continue to diplay the image, usually because it hasn't yet been converted to
            #  an appropriate type for tkinter to display
            while not self.stop_event.is_set():  # keep video stream going until stop event if havent told it to stop
                # alter the attributes of liquid level to change the way find_contour works
                self.liquid_level.set_canny_threshold_1(self.canny_threshold_1_scale.get())
                self.liquid_level.set_canny_threshold_2(self.canny_threshold_2_scale.get())
                # self.liquid_level.gaussian_x_std_dev = (self.gaussian_x_std_dev_scale.get())
                # self.liquid_level.morph_dilate_kernel_size = ((self.morph_dilate_kernel_size_scale.get(),
                #                                                self.morph_dilate_kernel_size_scale.get()))
                self.liquid_level.rows_to_count = self.rows_to_count_scale.get()
                self.liquid_level.menisci_to_check = self.menisci_to_check_scale.get()
                self.liquid_level.find_meniscus_minimum = float(self.find_meniscus_minimum_scale.get() / 100)

                _, image = self.video_capture.read()
                self.camera_cv_last_image = image
                # change from BGR to RGB
                image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)  # convert image from BGR to RGB
                contour_image = self.liquid_level.find_contour(image)  # get the contour image

                # raw image in tkinter image format
                image_one_tkinter = ImageTk.PhotoImage(image=Image.fromarray(image_rgb))  # cant pass numpy array
                # image so need to convert it first using Image.fromarray(), and then use that to make a tkinter
                # image, if the liquid level  instance start() function has already been run
                if self.liquid_level.img is not None:  # if there has been an image that has been loaded,
                    # will be True after the ROI was chosen
                    contour_image_to_draw = self.liquid_level.load_and_find_level(image)  # find the contour image and
                    #  get the row where the meniscus is
                    time = datetime.now()
                    self.liquid_level.add_image_to_memory(img=self.liquid_level.img,
                                                          img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                                          array_to_save_to=self.liquid_level.all_images_no_lines,
                                                          )
                    self.liquid_level.add_image_to_memory(img=contour_image_to_draw,
                                                          img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                                          array_to_save_to=self.liquid_level.all_images_edge,
                                                          )
                    self.liquid_level.add_image_to_memory(img=self.liquid_level.draw_lines(show=False, save=False),
                                                          img_name=time.strftime('%d_%m_%Y_%H_%M_%S'),
                                                          array_to_save_to=self.liquid_level.all_images_with_lines,
                                                          )
                    cv2.imwrite(f'{self.liquid_level.camera.raw_images_dir}\\raw_{time.strftime("%d_%m_%Y_%H_%M_%S")}.jpg', self.liquid_level.img)
                    self.liquid_level.save_drawn_image()

                # contour image in tkinter format
                contour_image_tkinter = ImageTk.PhotoImage(image=Image.fromarray(contour_image))
                image_with_lines = self.liquid_level.draw_lines(show=False, save=False, img=image)

                image_with_lines_rgb = cv2.cvtColor(image_with_lines, cv2.COLOR_BGR2RGB)  # convert image_with_lines
                # from BGR to RGB image with liquid lines drawn in tkinter format
                image_with_lines_tkinter = ImageTk.PhotoImage(image=Image.fromarray(image_with_lines_rgb))
                if (self.show_mask_on_contour is True) and (self.liquid_level.mask_to_search_inside is not None):
                    image_with_lines_rgb = self.draw_mask_on_image(image_with_lines_rgb)
                    image_with_lines_tkinter = ImageTk.PhotoImage(image=Image.fromarray(image_with_lines_rgb))
                if (self.show_mask_on_contour is True) and (self.liquid_level.mask_to_search_inside is not None):
                    contour_image = self.draw_mask_on_image(contour_image)
                    contour_image_tkinter = ImageTk.PhotoImage(image=Image.fromarray(contour_image))
                # display the images on the screen
                self.display_image(image_one=image_with_lines_tkinter, image_two=contour_image_tkinter)

                if self.watch_liquid_levels_bool and self.found_all_liquid_levels is False:  # if want to watch for a
                    # certain number of levels to appear
                    if self.liquid_level.img is None:  # if no images have been loaded at all in liquid level,
                        # then take one because there needs to be some kind of reference image
                        self.liquid_level.load_image_and_select_and_set_parameters(img=image,
                                                                                   select_region_of_interest=True,
                                                                                   set_reference=False,
                                                                                   select_tolerance=False)

                    if self.first_image_of_watch_liquid_levels_bool:
                        start_time = datetime.now()
                        print(f'start time to watch for levels: {start_time}')
                        self.first_image_of_watch_liquid_levels_bool = False
                        if self.slack_bot is not None:
                            self.slack_bot.post_slack_message(f'start time to watch for levels: {start_time}')
                            line_image_location = f'{self.liquid_level.camera.camera_images_dir}\\line_image.jpg'
                            cv2.imwrite(line_image_location, image_with_lines)
                            self.slack_bot.post_slack_file(
                                line_image_location,
                                'First image',
                                'First image',
                            )
                            os.remove(path=line_image_location)

                    number_of_levels_found = self.liquid_level.number_of_levels_last_found()
                    if number_of_levels_found == self.liquid_level.menisci_to_check:  # if found the number of
                        # menisci looking for
                        self.track_consecutive_number_of_times_found_n_levels += 1 # increase tracker by 1
                    else:
                        self.track_consecutive_number_of_times_found_n_levels = 0  # reset tracker

                    if self.track_consecutive_number_of_times_found_n_levels == \
                            self.consecutive_number_of_times_found_n_levels_for_success:  # if found liquid level a
                            # certain number of times consecutively, then
                        self.toggle_watch_liquid_levels()

                        end_time = datetime.now()
                        print(f'end time - found all levels: {end_time}')
                        if self.slack_bot is not None:
                            self.slack_bot.post_slack_message(f'end time - found all levels: {end_time}')
                            line_image_location = f'{self.liquid_level.camera.camera_images_dir}\\line_image.jpg'
                            cv2.imwrite(line_image_location, image_with_lines)
                            self.slack_bot.post_slack_file(
                                line_image_location,
                                'Final image',
                                'Final image',
                            )
                            os.remove(path=line_image_location)

        except Exception as e:
            print(e)

    def display_image(self, image_one, image_two):
        self.image_panel_one.configure(image=image_one)
        self.image_panel_one.image = image_one
        self.image_panel_two.configure(image=image_two)
        self.image_panel_two.image = image_two

        # # this will work for frame but not label, as the type for image panels
        # self.image_panel_one.image = image_one
        # self.image_panel_one.create_image(0, 0, image=image_one, anchor=tkinter.NW)
        # self.image_panel_two.image = image_two
        # self.image_panel_two.create_image(0, 0, image=image_two, anchor=tkinter.NW)

    def toggle_live_video_on_off(self):
        # toggle the live stream video on and off
        if self.stop_event.is_set():  # if stop event is set the video stream is currently off, then need to clear
            # stop_event and start video stream again
            self.camera_on()
            self.stop_event.clear()
            # set the thread again and start it
            self.thread = None
            self.thread = threading.Thread(target=self.video_stream, args=())
            self.thread.start()
            # change text on the button to prompt user to start the video stream again
            self.toggle_live_video_on_off_button.configure(text='Stop video live stream')

        else:  # if not set then live stream is currently on, then set stop_event to set to stop the video stream
            self.stop_event.set()
            self.camera_off()  # stop video capture
            # change text on the button to prompt user to start the video stream again
            self.toggle_live_video_on_off_button.configure(text='Start video live stream')

    def camera_on(self):
        # turn the camera on by creating a video capture instance
        self.video_capture = cv2.VideoCapture(self.liquid_level.camera.cam)  # set video capture

    def camera_off(self):
        # turn camera off by releasing the video capture
        self.video_capture.release()

    def select_tolerance(self):
        self.liquid_level.select_tolerance()

    def select_region_of_interest(self):
        try:
            # function to allow user to select the reference liquid level
            # if video live stream is on then it first must be turned off
            if not self.stop_event.is_set():  # if the camera is on live stream, turn it off
                print('turn off camera')
                self.toggle_live_video_on_off()
            print('reset liquid level')
            self.liquid_level.reset()  # reset the liquid level instance
            # set the liquid level attributes for find_contour()  that are from the gui
            self.liquid_level.set_canny_threshold_1(self.canny_threshold_1_scale.get())
            self.liquid_level.set_canny_threshold_2(self.canny_threshold_2_scale.get())
            self.liquid_level.rows_to_count = self.rows_to_count_scale.get()
            self.liquid_level.menisci_to_check = self.menisci_to_check_scale.get()
            self.liquid_level.find_meniscus_minimum = float(self.find_meniscus_minimum_scale.get()/100)
            self.liquid_level.find_meniscus_minimum = float(self.find_meniscus_minimum_scale.get()/100)
            self.liquid_level.find_meniscus_minimum = float(self.find_meniscus_minimum_scale.get()/100)
            # make pop up box for information on how to draw the region of interest to find liquid level
            tkinter.messagebox.showinfo("Select region of interest", "Select the region of interest to look fo a liquid "
                                                                     "level. Left click to draw an outline of the "
                                                                     "region of interest. The last point will be "
                                                                     "connected to the first point in processing. "
                                                                     "Press 'c' to confirm, press 'r' to reset "
                                                                     "drawing.")
            # let user select region of interest
            print('load and set reference')
            contour_image = self.liquid_level.load_image_and_select_and_set_parameters(img=self.camera_cv_last_image,
                                                                                       select_region_of_interest=True,
                                                                                       set_reference=False,
                                                                                       select_tolerance=False)
            time = datetime.now()
            self.liquid_level.all_images_with_lines.append([time.strftime('%d_%m_%Y_%H_%M_%S'),
                                                            self.liquid_level.draw_ref()])
            self.liquid_level.all_images_no_lines.append([time.strftime('%d_%m_%Y_%H_%M_%S'), self.liquid_level.ref_img])
            self.liquid_level.all_images_edge.append([time.strftime('%d_%m_%Y_%H_%M_%S'), contour_image])
            if self.stop_event.is_set():  # if the live stream is off then turn it on
                self.toggle_live_video_on_off()
        except Exception as e:
            # sometimes there is a slight error because where the video live stream was so far, there is some data
            # that was not retrieved, so need to quickly start and stop the video live stream again and then try to
            # select the region of interest again
            print(f'ran in an error {e} but it was handled')
            self.toggle_live_video_on_off()
            self.toggle_live_video_on_off()
            self.select_region_of_interest()

    def toggle_show_mask_contour_on_images(self):
        if self.show_mask_on_contour is False:
            self.show_mask_on_contour = True
            self.toggle_show_mask_contour_on_images_button.configure(text="Show mask contour on images")
        else:
            self.show_mask_on_contour = False
            self.toggle_show_mask_contour_on_images_button.configure(text="Stop showing mask contour on images")

    def toggle_show_mask_outline_or_area(self):
        if self.show_area_in_mask is True:
            self.show_area_in_mask = False
            self.toggle_show_mask_outline_or_area_button.configure(text="Show area in mask")
        else:
            self.show_area_in_mask = True
            self.toggle_show_mask_outline_or_area_button.configure(text="Show outline of mask")

    def draw_mask_on_image(self, cv_image):
        # draw mask either outline or area on the cv image, then return the image
        if cv_image.shape is 2:  # if image is black and white, not rgb
            line_colour = (255, 255, 255)  # make line colour white
        else:
            # cv image is rgb
            line_colour = (0, 255, 0)  # make line colour green
        if self.show_area_in_mask is True:
            line_colour = (255, 255, 255)  # make line colour white
            cv2.drawContours(self.liquid_level.mask_to_search_inside,
                             [self.liquid_level.list_of_frame_points_frame_points_list], -1,
                             line_colour, -1, cv2.LINE_AA)
            # put mask on image, and black out anything not in the area you want to search in
            cv_image = cv2.bitwise_and(cv_image, cv_image, mask=self.liquid_level.mask_to_search_inside)

        else:
            cv2.drawContours(cv_image,
                             [self.liquid_level.list_of_frame_points_frame_points_list], -1,
                             line_colour, 1, cv2.LINE_AA)
        return cv_image

    def toggle_watch_liquid_levels(self):
        if self.watch_liquid_levels_bool is False:
            self.watch_liquid_levels_bool = True
            self.found_all_liquid_levels = False
            self.watch_liquid_levels_button.configure(text=f"Watching for "
                                                           f"{self.liquid_level.menisci_to_check} liquid "
                                                           f"levels")
        else:
            self.first_image_of_watch_liquid_levels_bool = False  # set back to 0 so can do this watch
            # sequence again
            self.found_all_liquid_levels = True  # set to True because have found the number of levels it
            #  was looking for
            self.watch_liquid_levels_bool = False  # set this to False so no longer watching for liquid
            # levels
            self.watch_liquid_levels_button.configure(text=f"Trying to find "
                                                           f"{self.liquid_level.menisci_to_check} levels")

    # # lets you select a rectagle on a frame - not used now because liquid_level.start works
    # def on_button_press_rectangle(self, event):
    #     # press event for image panel one
    #     if self.image_panel_one_rectangle['rectangle']:  # if there was already a rectangle
    #         # then reset everything first
    #         self.image_panel_one.delete(self.image_panel_one_rectangle['rectangle'])
    #         self.image_panel_one_rectangle['rectangle'] = None
    #         self.image_panel_one_rectangle['start_x'] = None
    #         self.image_panel_one_rectangle['start_y'] = None
    #
    #     # save mouse drag start position
    #     self.image_panel_one_rectangle['start_x'] = event.x
    #     self.image_panel_one_rectangle['start_y'] = event.y
    #
    #     # create rectangle if not yet exist
    #     self.image_panel_one_rectangle['rectangle'] = self.image_panel_one.create_rectangle(self.image_panel_one_rectangle['start_x'],
    #                                                                                         self.image_panel_one_rectangle['start_y'], 1, 1, dash=(6, 6),
    #                                                                                         outline='green', width='3')
    #
    # def on_move_press_rectangle(self, event):
    #     # when mouse is moved
    #     self.image_panel_one_rectangle['current_x'], self.image_panel_one_rectangle['current_y'] = (event.x, event.y)
    #
    #     # expand rectangle as you drag the mouse
    #     self.image_panel_one.coords(self.image_panel_one_rectangle['rectangle'],
    #                                 self.image_panel_one_rectangle['start_x'],
    #                                 self.image_panel_one_rectangle['start_y'],
    #                                 self.image_panel_one_rectangle['current_x'],
    #                                 self.image_panel_one_rectangle['current_y'])
    #
    #
    # def on_button_release_rectangle(self, event):
    #     pass
    #
    # def on_right_click_rectangle(self, event):
    #     # clear the box on the frame
    #     if self.image_panel_one_rectangle['rectangle']:  # if there was already a rectangle
    #         # then reset everything first
    #         self.image_panel_one.delete(self.image_panel_one_rectangle['rectangle'])
    #         self.image_panel_one_rectangle['rectangle'] = None
    #         self.image_panel_one_rectangle['start_x'] = None
    #         self.image_panel_one_rectangle['start_y'] = None

from ada_core.dependencies.slackbot import SlackBot
import camera_images

def test_gui():
    CAMERA_NUMBER = 0  # zero if the only webcam is the usb webcam, 1 if computer has its own webcam but you want to
    #  use
    # the usb webcam
    CREATE_SAVE_FOLDER = True  # whether or not to create a folder to save all the images in; these are to save all the
    # raw images taken by the camera
    save_folder_location = os.path.dirname(os.path.abspath(camera_images.__file__))
    ROWS_TO_COUNT = 5  # number of rows of pixels to look for the meniscus choice between 2-5 have worked well in the past

    # create the Camera object
    camera = Camera(cam=CAMERA_NUMBER,
                    create_save_folder=CREATE_SAVE_FOLDER,
                    save_folder_location=save_folder_location,
                    )

    # create the LiquidLevel object
    liquid_level = LiquidLevel(camera,
                               rows_to_count=ROWS_TO_COUNT,
                               menisci_to_check=0,
                               find_meniscus_minimum=0.1,
                               no_error=True,
                               )

    # create slack bot instance
    slack_bot = SlackBot(channel='#gronckleupdates')  # slack channel to post messages to

    my_gui = LiquidLevelGUI(liquid_level=liquid_level,
                            slack_bot=slack_bot,
                            )
    my_gui.root.mainloop()


# test_gui()
