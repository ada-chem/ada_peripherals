import os
import cv2
import shutil
import tkinter
import threading
import numpy as np
from datetime import datetime
import pylab as pl
from PIL import ImageTk, Image
from matplotlib.collections import RegularPolyCollection

_cv = {  # controlled variables for adjusting camera values
    'frame_width': 3,
    'frame_height': 4,
    'brightness': 10,
    'contrast': 11,
    'saturation': 12,
    'exposure': 15,
}


class Camera:
    """
    PhotoBooth class is used for one camera, multiple PhotoBooth instances need to be used if you are using multiple
    cameras. It has one background image that has a scale bar
    and reference box, both of known lengths. You need to know the Location of the corner of the photobooth where the
    arm will bring an item into. Convention for photobooth is that camera 0 is left of camera 1. (number in clockwise
    direction)
    """
    # should all be in mm, need to use UnitValue class later
    def __init__(self,
                 cam=0,
                 create_save_folder=True,
                 save_folder_location=None,
                 naming_prefix=None,
                 ):
        """
        Create instance of camera; this is generally a webcam type of camera not to be moved.

       :param int, cam: 0, or 1 (or a higher number depending on the number of caperas connected). The camera you
            want to use to take a picture with. 0 if its the only camera, 1 if it is a secondary camera.
        :param create_save_folder: bool, if True then create a folder to save the images to
        :param str, save_folder_location: location to the directory where you want images to be saved in. in here,
            if it doesnt already exist, a folder for the camera will be created, and then in there time stamped
            folders will be made for images to be saved in
        :param str, naming_prefix: a string, a prefix you want to append to the start of all images to save
        """

        self.cam = cam  # int, the camera to use
        # create temporary VideoCapture to get the width and height of the images from the camera
        temp_cam = cv2.VideoCapture(self.cam)
        self.cam_width = temp_cam.get(3)  # float
        self.cam_height = temp_cam.get(4)  # float
        temp_cam.release()

        self.frame_points = []  # used for selecting a frame
        self.crop_left = None  # float, how much to crop from the left to get to the region of interest as a fraction
        #  of the entire image
        self.crop_right = None
        self.crop_top = None
        self.crop_bottom = None

        self.naming_prefix = naming_prefix

        self.all_images = []  # list of list, list of [str, np.ndarray], that is
        # the time the picture taken, and the image that was taken
        self.start_time = datetime.now()  # time an instance was made/experiment started
        self.create_save_folder_bool = create_save_folder  # True if you want to save the images of the Camera object
        # initialize arguments for when images will be saved
        self.save_folder_location = save_folder_location  # root folder to save the actual time stamped folder of images
        self.folder = None
        self.root_dir = None
        self.camera_images_dir = None
        self.raw_images_dir = None

        if self.create_save_folder_bool is True:
            self.create_save_folder()

        # initial parameters to be used by reset()
        self.initial_arguments = {
            'cam': self.cam,
            'create_save_folder': self.create_save_folder_bool,
            'naming_prefix': self.naming_prefix,
        }

    def create_save_folder(self):  # todo needs to be updated, especially for when move to install everything through
        #  pip
        if self.naming_prefix is None:
            self.folder = 'raw_' + self.start_time.strftime('%Y_%m_%d_%H_%M_%S')  # the time
        # stamped folder of images
        else:
            self.folder = 'raw_' + self.start_time.strftime('%Y_%m_%d_%H_%M_%S') + self.naming_prefix

        # create folders for images in NAS
        try:
            # self.logger.info('try to connect to NAS')
            self.root_dir = os.getenv('imagefolder')  # this is to connect to the NAS, but only if there is a .env
            # file to indicate where that (the NAS) is
            os.listdir(self.root_dir)  # check if can connect to the NAS

            if self.root_dir is None:  # this is a 'catch' for if there isn't a .env in the script that has the
                # instantiation of a Camera class, in that case, then just set the root directory for saving of
                # images to a local photobooth_images folder in ada_peripherals
                # but this folder should be deleted (manually) as soon as the user has finished using it
                if self.save_folder_location is None:
                    raise Exception("save folder location cannot be none")
                self.root_dir = self.save_folder_location
                # self.logger.info('could not connect to NAS')
                # self.logger.info('instead self.root dir from inside except' + self.root_dir)

        except Exception as e:  # only catches an exception if the .env has 'imagefolder' in it
            # cannot connect to the NAS, so create a folder in ada_peripherals for the photobooth images,
            # but this folder should be deleted (manually) as soon as the user has finished using it
            # self.logger.info('could not connect to NAS')
            if self.save_folder_location is None:
                raise Exception("save folder location cannot be none")
            self.root_dir = self.save_folder_location
            # self.logger.info('instead self.root dir from inside except' + self.root_dir)
            # ada_peripherals

        if f'camera_{self.cam}_images' not in os.listdir(self.root_dir):  # if there is no folder
            # then create one
            # self.logger.info(self.root_dir)
            os.makedirs(os.path.join(
                self.root_dir,
                f'camera_{self.cam}_images'
            ))
        self.camera_images_dir = os.path.join(self.root_dir, f'camera_{self.cam}_images')

        if 'raw_images' not in os.listdir(self.camera_images_dir):  # if there is no folder
            # then create one
            os.makedirs(os.path.join(
                self.camera_images_dir,
                f'raw_images'
            ))

        self.raw_images_dir = os.path.join(self.camera_images_dir, 'raw_images')

        if f'{self.folder}' not in os.listdir(self.raw_images_dir):  # if there is no folder with the time
            # stamp of the start of the run
            os.makedirs(os.path.join(
                self.raw_images_dir,
                self.folder,
            ))

        self.raw_images_dir = os.path.join(self.raw_images_dir, f'{self.folder}')

    def reset(self):
        # if this is called, reset all the initial attributes, and delete the folder of images, if there was one that
        #  will be saved, and create a new folder for saved images

        # first remove the old folder of images from directory
        if self.raw_images_dir is not None:  # if there was a folder that was made for saveing the images
            # then need to remove that folder from disk
            shutil.rmtree(path=self.raw_images_dir)

        # reset all attributes to initial values
        self.cam = self.initial_arguments['cam']
        self.frame_points = []
        self.crop_left = None
        self.crop_right = None
        self.crop_top = None
        self.crop_bottom = None
        self.naming_prefix = self.initial_arguments['naming_prefix']
        self.all_images = []
        self.start_time = datetime.now()
        self.create_save_folder_bool = self.initial_arguments['create_save_folder']
        self.folder = None
        self.root_dir = None
        self.camera_images_dir = None
        self.raw_images_dir = None
        if self.create_save_folder_bool is True:  # recreate a folder of images to store new images that will be taken
            self.create_save_folder()

    def set_naming_prefix(self, prefix):
        # sets the naming prefix name, prefix is a string
        self.naming_prefix = prefix

    def get_cam(self, camera_to_get):
        """
        Returns a VideoCapture object for the camera you want to get

        :param int, camera_to_get: the camera you want to get
        :return: VideoCamera, video
        """
        video = cv2.VideoCapture(camera_to_get)

        return video

    def take_picture(self, save=True):
        """
        Take a picture with a specified camera.
        :param bool, save: True to save images to computer
        :return: frame is a numpy.ndarray, the image taken by the camera as BGR image
        """
        frame = None
        # Code to turn off autofocus and set the focus parameters. but not sure if autofocus is even on right now?
        # cap = cv2.VideoCapture(1)  # my webcam
        # cap.set(3, 1280)  # set the resolution
        # cap.set(4, 720)
        # cap.set(cv2.CAP_PROP_AUTOFOCUS, 0)  # turn the autofocus off

        time = datetime.now()

        # take a picture with a camera
        video = cv2.VideoCapture(self.cam)
        # video.set(3, 1280)
        # video.set(4, 720)
        _, frame = video.read()
        video.release()

        time_as_str = time.strftime('%d_%m_%Y_%H_%M_%S')
        self.all_images.append([time_as_str, frame])

        if self.create_save_folder_bool is True:
            if save is True:
                if self.naming_prefix is None:
                    cv2.imwrite(f'{self.raw_images_dir}\\{time_as_str}.jpg', frame)
                else:
                    cv2.imwrite(f'{self.raw_images_dir}\\{self.naming_prefix}_{time_as_str}.jpg', frame)

        return frame

    def display_all_images(self):
        # display all the images taken

        for time_img_set in self.all_images:
            for time, img in time_img_set:
                cv2.imshow('img', img)
                cv2.waitKey(0)

    def select_frame(self, img):
        """
        Allows user to see an image image, and to draw
        a box on the image and use that as the selected frame of where to look for something. After drawing a box
        you can press 'r' to clear it to reselect a different box or press 'c' to choose the box that will be
        the frame.

        :return: float, the fraction relative to the image size for each side that you would have to crop from to get
            the region of interest.
        """
        # https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and-opencv/
        # have it so that it opens a window so the user can click and drag to define the frame for the image, and then
        # that is the frame that should be set, so it also needs to set crop left, right, top, bottom

        img_height, img_width, _ = img.shape

        def make_selection(event, x, y, flags, param):
            # if left mouse button clicked, record the starting(x, y) coordinates
            if event is cv2.EVENT_LBUTTONDOWN:
                self.frame_points = [(x, y)]
            # check if left mouse button was released
            elif event is cv2.EVENT_LBUTTONUP:
                self.frame_points.append((x, y))

                # draw rectangle around selected region
                cv2.rectangle(image, self.frame_points[0], self.frame_points[1], (0, 255, 0), 2)
                cv2.imshow('Select frame', image)

        # clone image and set up cv2 window
        image = img.copy()
        clone = img.copy()

        cv2.namedWindow('Select frame')
        cv2.setMouseCallback('Select frame', make_selection)

        # keep looping until 'q' is pressed
        while True:
            # display image, wait for a keypress
            cv2.imshow('Select frame', image)
            key = cv2.waitKey(1) & 0xFF

            # if 'r' key is pressed, reset the cropping region
            if key == ord('r'):
                image = clone.copy()

            # if 'c' key pressed break from while True loop
            elif key == ord('c'):
                break

        # if there are 2 reference points then select the region of interest from the image to display
        if len(self.frame_points) == 2:
            cv2.namedWindow('Selected frame - roi')
            roi = clone[self.frame_points[0][1]:self.frame_points[1][1],
                  self.frame_points[0][0]:self.frame_points[1][0]]

            cv2.imshow('Selected frame - roi', roi)
            cv2.waitKey(0)

            # set the self.crop right, left, top, bottom from the selected frame
            self.crop_left = (self.frame_points[0][0] / img_width)
            self.crop_right = ((img_width - self.frame_points[1][0]) / img_width)
            self.crop_top = (self.frame_points[0][1] / img_height)
            self.crop_bottom = ((img_height - self.frame_points[1][1]) / img_height)

        cv2.destroyAllWindows()

        return self.crop_left, self.crop_right, self.crop_top, self.crop_bottom

    def video_run(self):
        # stream what the video sees
        video_capture = cv2.VideoCapture(self.cam)
        while True:
            # capture frame-by-frame
            ret, image = video_capture.read()

            if not ret:  # doesn't really matter here, it matters more for reading from an image, but for video doesnt occur
                break

            cv2.imshow('Live video', image)

            # if press the q button exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        video_capture.release()
        cv2.destroyAllWindows()

    def get_training_images(self):
        # basically take constant stream of pics, and display and save them original image (user can select a region
        # to crop the image and only view and save that cropped portion)

        # first select the frame
        image = self.take_picture(False)
        self.crop_left, self.crop_right, self.crop_top, self.crop_bottom = self.select_frame(img=image)

        video_capture = cv2.VideoCapture(self.cam)

        while True:
            # capture frame-by-frame
            ret, image = video_capture.read()

            if not ret:  # doesn't really matter here, it matters more for reading from an image, but for video
                # doesnt occur
                break

            cropped_image = self.crop_horizontal(img=image, crop_left=self.crop_left, crop_right=self.crop_right)
            cropped_image = self.crop_vertical(img=cropped_image, crop_top=self.crop_top, crop_bottom=self.crop_bottom)

            cv2.imshow('Live cropped video', cropped_image)

            # save the image
            time = datetime.now()
            time_as_str = time.strftime('%d_%m_%Y_%H_%M_%S')
            self.all_images.append([time_as_str, cropped_image])

            if self.create_save_folder is True:
                if self.naming_prefix is None:
                    cv2.imwrite(f'{self.folder_path}\\{time_as_str}.jpg', cropped_image)
                else:
                    cv2.imwrite(f'{self.folder_path}\\{self.naming_prefix}_{time_as_str}.jpg', cropped_image)

            # if press the q button exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

    def find_frame(self, img, show, crop_left=None, crop_right=None, crop_top=None, crop_bottom=None):
        """
        Finds a frame to look for something within a closed image. Also finds it on the image, and has the option
        for user to view the image. What this returns are the row and column values for the region of interest that
        the user selected in self.select_frame; since select_frame returns a float to give the fraction you need to
        move inwards from the 4 edges of the image in order to get the frame, and so this function will give the row
        and column values that correspond to those float values for the image

        The output of this image would be used like this:
        crop_left, crop_right, crop_top, crop_bottom = self.select_frame(image)  # select rectangle on an image,
            gives the fraction to move inside from the edges of the image to get the selected frame (rectangle)
        left, right, top, bottom = self.find_frame(closed, crop_left=crop_left, crop_right=crop_right,
            crop_top=crop_top, crop_bottom=crop_bottom, show)  # gives row values in terms of the image of the row
            and colums (for the pixels) of where the boundaries of the selected area on the image is
        row = self.find_something(image, left, right, top, bottom)

        Where the find_something code iterates through the image according to an algorithm to find something
        or to do something, and the search can be constrained only to the  Example of this is find_liquid_level in
        LiquidLevel.

        Inside of self.find_frame(image, left, right, top, bottom):  # to search within the frame
            rows = range(top, bottom)
            cols = range(left, right)
            ...

        or the output of left, right, top, bottom can also be used to as parameters to crop an image to only get the
        frame.

        :param img: image
        :param float, crop_left: fraction, relative to image length, how much to go in from the left to get to the
            region on interest
        :param float, crop_right: fraction, relative to image length, how much to go in from the right to get to the
            region on interest
        :param float, crop_top: fraction, relative to image height, how much to go in from the top to get to the
            region on interest
        :param float, crop_bottom: fraction, relative to image height, how much to go in from the bottom to get to the
            region on interest
        :param bool, show: True to view the drawn frame on the closed image
        :return: left, right, top, bottom, are all int, that represent the row or column that together define the frame
            that is the region of interest
        """
        if crop_left is None:
            crop_left = self.crop_left
        if crop_right is None:
            crop_right = self.crop_right
        if crop_top is None:
            crop_top = self.crop_top
        if crop_bottom is None:
            crop_bottom = self.crop_bottom

        if len(img.shape) is 3:
            img_height, img_width, channels = img.shape
        else:
            img_height, img_width = img.shape

        left = int(img_width * crop_left)
        right = img_width - int(img_width * crop_right)
        top = int(img_height * crop_top)
        bottom = img_height - int(img_height * crop_bottom)

        if show is True:
            img = img.copy()
            img = cv2.line(img, (left, top), (right, top), (0, 255, 0))
            img = cv2.line(img, (left, top), (left, bottom), (0, 255, 0))
            img = cv2.line(img, (left, bottom), (right, bottom), (0, 255, 0))
            img = cv2.line(img, (right, bottom), (right, top), (0, 255, 0))

            cv2.imshow('Selected frame', img)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

        return left, right, top, bottom

    def crop_horizontal(self, img, crop_left, crop_right):
        """
        Crops image in horizontal direction by the the crop_left fraction on the left and the crop_right fraction on
        the right.

        :param numpy.ndarray, img: image
        :param float, crop_left: value between 0 and 1, is the fraction to crop out from the left part of the image
        :param float, crop_right: value between 0 and 1, is the fraction to crop out from the right part of the image
        :return: numpy.ndarray, cropped, the image but with a fraction of the left and right parts of the image removed
        """
        if len(img.shape) is 3:
            height, width, channels = img.shape
        else:
            height, width = img.shape

        new_left = int(width * crop_left)
        crop_right = 1 - crop_right
        new_right = int(width * crop_right)

        cropped = img[0:height, new_left:new_right]

        return cropped

    def crop_right(self, img, i):
        """
        Crops out a fraction from the left part of the image

        :param img: numpy.ndarray, image
        :param i: float, value between 0 and 1, is the fraction to crop out from the right part of the image
        :return: cropped, a numpy.ndarray of the image but with a fraction of the right part of the image removed
        """
        # crop out i (decimal b/w 0 and 1) fraction from the right part of the image
        if len(img.shape) is 3:
            height, width, channels = img.shape
        else:
            height, width = img.shape
        i = 1 - i
        width_2 = int(width * i)
        cropped = img[0:height, 0:width_2]
        return cropped

    def crop_left(self, img, i):
        """
        Crops out a fraction from the left part of the image

        :param numpy.ndarray, img: image
        :param float, i: value between 0 and 1, is the fraction to crop out from the left part of the image
        :return: numpy.ndarray, cropped, the image but with a fraction of the left part of the image removed
        """
        if len(img.shape) is 3:
            height, width, channels = img.shape
        else:
            height, width = img.shape
        width_2 = int(width*i)
        cropped = img[0:height, width_2:width]
        return cropped

    def crop_bottom_half(self, img):
        """
        Crop the bottom half of the image out

        :param img: numpy.ndarray, image
        :return: cropped is a numpy.ndarray of the top half of the original image
        """
        if len(img.shape) is 3:
            height, width, channels = img.shape
        else:
            height, width = img.shape
        cropped = img[0:height//2, 0:width]
        return cropped

    def crop_vertical(self, img, crop_top, crop_bottom):
        """
        Crops image in horizontal direction by the the crop_left fraction on the left and the crop_right fraction on
        the right.

        :param numpy.ndarray, img: image
        :param float, crop_top: value between 0 and 1, is the fraction to crop out from the top part of the image
        :param float, crop_bottom: value between 0 and 1, is the fraction to crop out from the bottom part of the image
        :return: numpy.ndarray, cropped, the image but with a fraction of the top and bottom parts of the image removed
        """
        if len(img.shape) is 3:
            height, width, channels = img.shape
        else:
            height, width = img.shape

        new_top = int(height * crop_top)
        crop_bottom = 1 - crop_bottom
        new_bottom = int(height * crop_bottom)

        cropped = img[new_top:new_bottom, 0:width]

        return cropped

    def crop_top(self, img, i):
        """
        Crop the bottom half of the image out
        :param img: numpy.ndarray, image
        :return: cropped is a numpy.ndarray of the top half of the original image
        """
        if len(img.shape) is 3:
            height, width, channels = img.shape
        else:
            height, width = img.shape
        height_2 = int(height * i)
        cropped = img[height_2:height, 0:width]
        return cropped

    def crop_bottom(self, img, i):
        """
        Crop the bottom half of the image out
        :param img: numpy.ndarray, image
        :return: cropped is a numpy.ndarray of the top half of the original image
        """
        if len(img.shape) is 3:
            height, width, channels = img.shape
        else:
            height, width = img.shape
        i = 1 - i
        height_2 = int(height * i)
        cropped = img[0:height_2, 0:width]
        return cropped

    def crop_and_resize_image(self,
                              image,
                              crop_left=0,
                              crop_right=0,
                              crop_top=0,
                              crop_bottom=0,
                              ):
        # crop an image but keep it the same size as before, the crop values are fractions (between 0 and 1) to say how
        # much to crop inward into the picture from that edge. After the image gets cropped it will be resized so that
        # its size is the same as the original image
        if len(image.shape) is 3:
            height, width, channels = image.shape
        else:
            height, width = image.shape
        original_image_width = width
        original_image_height = height

        image = self.crop_horizontal(img=image,
                                     crop_left=crop_left,
                                     crop_right=crop_right)
        image = self.crop_vertical(img=image,
                                   crop_bottom=crop_bottom,
                                   crop_top=crop_top)

        cropped_and_resized_image = cv2.resize(image, (original_image_width, original_image_height))

        return cropped_and_resized_image

def crop_horizontal(img, crop_left, crop_right):
    """
    Crops image in horizontal direction by the the crop_left fraction on the left and the crop_right fraction on
    the right.

    :param numpy.ndarray, img: image
    :param float, crop_left: value between 0 and 1, is the fraction to crop out from the left part of the image
    :param float, crop_right: value between 0 and 1, is the fraction to crop out from the right part of the image
    :return: numpy.ndarray, cropped, the image but with a fraction of the left and right parts of the image removed
    """
    if len(img.shape) is 3:
        height, width, channels = img.shape
    else:
        height, width = img.shape

    new_left = int(width * crop_left)
    crop_right = 1 - crop_right
    new_right = int(width * crop_right)

    cropped = img[0:height, new_left:new_right]

    return cropped

def crop_right(img, i):
    """
    Crops out a fraction from the left part of the image

    :param img: numpy.ndarray, image
    :param i: float, value between 0 and 1, is the fraction to crop out from the right part of the image
    :return: cropped, a numpy.ndarray of the image but with a fraction of the right part of the image removed
    """
    # crop out i (decimal b/w 0 and 1) fraction from the right part of the image
    if len(img.shape) is 3:
        height, width, channels = img.shape
    else:
        height, width = img.shape
    i = 1 - i
    width_2 = int(width * i)
    cropped = img[0:height, 0:width_2]
    return cropped

def crop_left(img, i):
    """
    Crops out a fraction from the left part of the image

    :param numpy.ndarray, img: image
    :param float, i: value between 0 and 1, is the fraction to crop out from the left part of the image
    :return: numpy.ndarray, cropped, the image but with a fraction of the left part of the image removed
    """
    if len(img.shape) is 3:
        height, width, channels = img.shape
    else:
        height, width = img.shape
    width_2 = int(width*i)
    cropped = img[0:height, width_2:width]
    return cropped

def crop_vertical(img, crop_top, crop_bottom):
    """
    Crops image in horizontal direction by the the crop_left fraction on the left and the crop_right fraction on
    the right.

    :param numpy.ndarray, img: image
    :param float, crop_top: value between 0 and 1, is the fraction to crop out from the top part of the image
    :param float, crop_bottom: value between 0 and 1, is the fraction to crop out from the bottom part of the image
    :return: numpy.ndarray, cropped, the image but with a fraction of the top and bottom parts of the image removed
    """
    if len(img.shape) is 3:
        height, width, channels = img.shape
    else:
        height, width = img.shape

    new_top = int(height * crop_top)
    crop_bottom = 1 - crop_bottom
    new_bottom = int(height * crop_bottom)

    cropped = img[new_top:new_bottom, 0:width]

    return cropped

def crop_top(img, i):
    """
    Crop the bottom half of the image out
    :param img: numpy.ndarray, image
    :return: cropped is a numpy.ndarray of the top half of the original image
    """
    if len(img.shape) is 3:
        height, width, channels = img.shape
    else:
        height, width = img.shape
    height_2 = int(height * i)
    cropped = img[height_2:height, 0:width]
    return cropped

def crop_and_resize_image(image,
                          crop_left=0,
                          crop_right=0,
                          crop_top=0,
                          crop_bottom=0,
                          ):
    # crop an image but keep it the same size as before, the crop values are fractions (between 0 and 1) to say how
    # much to crop inward into the picture from that edge. After the image gets cropped it will be resized so that
    # its size is the same as the original image
    if len(image.shape) is 3:
        height, width, channels = image.shape
    else:
        height, width = image.shape
    original_image_width = width
    original_image_height = height

    image = crop_horizontal(img=image,
                            crop_left=crop_left,
                            crop_right=crop_right)
    image = crop_vertical(img=image,
                          crop_bottom=crop_bottom,
                          crop_top=crop_top)

    cropped_and_resized_image = cv2.resize(image, (original_image_width, original_image_height))

    return cropped_and_resized_image


# basic camera gui

class CameraGUI:
    def __init__(self,
                 camera,
                 ):
        # self.logger.info('Created instance of CameraGUI')
        self.camera = camera

        # initialize root window
        self.root = tkinter.Tk()
        self.root_background_colour = 'purple'
        self.root.configure(background=self.root_background_colour)
        self.root.title("Camera GUI")
        self.root_height = self.root.winfo_height()  # the root window height
        self.root_width = self.root.winfo_width()  # the root window height

        # create container frames for the gui
        # all these height values must add to 1
        self.top_frame_height = 0.2  # float, fraction of how much of the entire window size will be for the top frame
        self.center_frame_height = 0.5  # float, fraction of how much of the entire window size will be for the top
        # frame
        self.bottom_frame_height = 0.15  # float, fraction of how much of the entire window size will be for the top
        # frame
        self.bottom_frame_2_height = 0.15  # need another container to put things in the bottom until figure out how
        # to use grid and use a better frame
        self.top_frame = tkinter.Frame(self.root, bg=self.root_background_colour,
                                       height=self.root_height*self.top_frame_height,
                                       width=self.root.winfo_width(),
                                       )
        self.center_frame = tkinter.Frame(self.root, bg=self.root_background_colour,
                                          height=self.root_height * self.center_frame_height,
                                          width=self.root.winfo_width(),
                                          )
        self.bottom_frame = tkinter.Frame(self.root, bg=self.root_background_colour,
                                          height=self.root_height * self.bottom_frame_height,
                                          width=self.root.winfo_width(),
                                          )
        self.bottom_frame_2 = tkinter.Frame(self.root, bg=self.root_background_colour,
                                            height=self.root_height * self.bottom_frame_2_height,
                                            width=self.root.winfo_width(),
                                            )
        # layout main containers
        self.top_frame.grid(row=0, column=0)
        self.center_frame.grid(row=1, column=0)
        self.bottom_frame.grid(row=2, column=0)
        self.bottom_frame_2.grid(row=3, column=0)

        # create button to toggle the live video stream on and off
        self.toggle_live_video_on_off_button = tkinter.Button(self.top_frame, text="Stop video live stream",
                                                              command=self.toggle_live_video_on_off,)
        self.toggle_live_video_on_off_button.pack(side='left', padx=3, pady=3)

        # create button to let user select the region of interest
        self.select_region_of_interest_button = tkinter.Button(self.top_frame, text="Select region of interest",
                                                               command=self.select_region_of_interest, )
        self.select_region_of_interest_button.pack(side='left', padx=3, pady=3)

        self.image_panel_one = tkinter.Label(self.center_frame)  # create image panel with the image
        self.image_panel_one.pack(side="left", padx=3,)
        self.image_panel_two = tkinter.Label(self.center_frame)  # create image panel with the image
        self.image_panel_two.pack(side="left", padx=3,)
        self.camera_cv_last_image = None  # to keep the most current image from the live stream, before converted
        # into tkinter image

        # set values and stuff for video streaming to the gui
        self.video_capture = cv2.VideoCapture(self.camera.cam)  # create the video capture instance
        self.thread = None  # need a thread to pool video
        self.stop_event = None  # need a way to know when to stop pooling video
        self.stop_event = threading.Event()
        self.thread = threading.Thread(target=self.video_stream, args=())
        self.thread.start()

        # what to happen when you try to exit out of the gui window
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

    def on_closing(self):
        if tkinter.messagebox.askokcancel("Quit", "Do you want to quit?"):
            if not self.stop_event.is_set():
                self.toggle_live_video_on_off()
            self.root.destroy()

    def video_stream(self):
        try:  # have a try except because sometimes stop event is changed while in the middle which can cause issues
            # to rise because it cannot continue to display the image, usually because it hasn't yet been converted to
            #  an appropriate type for tkinter to display
            while not self.stop_event.is_set():  # keep video stream going until stop event if havent told it to stop
                _, image = self.video_capture.read()
                self.camera_cv_last_image = image
                # change from BGR to RGB
                image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)  # convert image from BGR to RGB
                # raw image in tkinter image format
                image_one_tkinter = ImageTk.PhotoImage( image=Image.fromarray(image_rgb))  # cant pass numpy array
                # image so need to convert it first using Image.fromarray(), and then use that to make a tkinter
                # image, if the liquid level instance start() function has already been run

                # todo can put stuff here to also create a contour image in order to show it in the second image
                # panel. for now just display two identical live streams, one on each panel
                self.display_image(image_one=image_one_tkinter, image_two=image_one_tkinter)
                # not sure yet why need to call update here
        except Exception as e:
            # self.logger.error(e)
            print(e)

    def display_image(self, image_one, image_two):
        self.image_panel_one.configure(image=image_one)
        self.image_panel_one.image = image_one
        self.image_panel_two.configure(image=image_two)
        self.image_panel_two.image = image_two

    def toggle_live_video_on_off(self):
        # toggle the live stream video on and off
        if self.stop_event.is_set():  # if stop event is set the video stream is currently off, then need to clear
            # stop_event and start video stream again
            self.camera_on()
            self.stop_event.clear()
            # set the thread again and start it
            self.thread = None
            self.thread = threading.Thread(target=self.video_stream, args=())
            self.thread.start()
            # change text on the button to prompt user to start the video stream again
            self.toggle_live_video_on_off_button.configure(text='Stop video live stream')

        else:  # if not set then live stream is currently on, then set stop_event to set to stop the video stream
            self.stop_event.set()
            self.camera_off()  # stop video capture
            # change text on the button to prompt user to start the video stream again
            self.toggle_live_video_on_off_button.configure(text='Start video live stream')

    def camera_on(self):
        # turn the camera on by creating a video capture instance
        self.video_capture = cv2.VideoCapture(self.camera.cam)  # set video capture

    def camera_off(self):
        # turn camera off by releasing the video capture
        self.video_capture.release()

    def select_region_of_interest(self):
        try:
            # function to allow user to select the reference liquid level
            # if video live stream is on then it first must be turned off
            if not self.stop_event.is_set():  # if the camera is on live stream, turn it off
                self.toggle_live_video_on_off()
            self.camera.reset()  # reset the camera instance
            # make pop up box for information on how to draw the region of interest to find liquid level
            tkinter.messagebox.showinfo("Select region of interest", "Select the region of interest to look fo a liquid "
                                                                     "level. Press 'c' to confirm, press 'r' to reset ad "
                                                                     "redraw the box. Box must be drawn starting from the "
                                                                     "top left to the bottom right.")
            # todo let user select region of interest... but this is dependent on the specific application,
            # not exactly needed here
            if self.stop_event.is_set():  # if the live stream is off
                # then turn it on
                self.toggle_live_video_on_off()
        except Exception as e:
            # sometimes there is a slight error because where the video live stream was so far, there is some data
            # that was not retrieved, so need to quickly start and stop the video live stream again and then try to
            # select the region of interest again
            print(f'ran in an error {e} but it was handled')
            self.toggle_live_video_on_off()
            self.toggle_live_video_on_off()
            self.select_region_of_interest()

###################################################################################

def read_rgb(reads=5, section=25, webcam=0):
    """
    Reads the center section of a set of images captured by the webcam

    :param reads: number of reads to average
    :param section: the number of pixels of the center section to read (by default this will be 25x25 section)
    :param webcam: the webcam address number to access
    :return: averaged r,g,b value of the center section
    """
    frames = capture_image(
        reads,
        webcam=webcam,
    )
    # height, width
    h = len(frames[0])
    w = len(frames[0][0])

    # snip the desired sections
    snippets = np.asarray([[
        [frame[y][x] for x in range(int(w/2 - section / 2), int(w/2 + section / 2))]
        for y in range(int(h/2 - section / 2), int(h/2 + section / 2))] for frame in frames]
    )
    # flatten snippets and reverse bgr values
    snippets = [[rgb[::-1] for sublist in snippet for rgb in sublist] for snippet in snippets]
    col_lists = [
        [rgb[i] for lst in snippets for rgb in lst] for i in [0, 1, 2]
    ]
    return [int(sum(lst) / len(lst)) for lst in col_lists], frames[0]


def snippet(frame, section=25):
    """
    Extracts a centered snippet from a frame

    :param frame: frame of values
    :return: snippet of the frame
    """
    h = len(frame)
    w = len(frame[0])

    # snip the desired sections
    snippet = np.asarray([
        [frame[y][x] for x in range(int(w / 2 - section / 2), int(w / 2 + section / 2))]
        for y in range(int(h / 2 - section / 2), int(h / 2 + section / 2))]
    )
    return snippet


def average_rgb(frame, incoming='bgr'):
    """
    Determines the average RGB value of a frame

    :param frame: frame of values
    :return: average rgb value
    """
    flat = [rgb for sublist in frame for rgb in sublist]
    col_lists = [
        [rgb[i] for rgb in flat] for i in [0, 1, 2]
    ]
    if incoming.lower() == 'bgr':
        return [int(sum(lst) / len(lst)) for lst in col_lists][::-1]
    if incoming.lower() == 'rgb':
        return [int(sum(lst) / len(lst)) for lst in col_lists]


def plot_colour(colour):
    fig, ax = pl.subplots(
        1,
        # figsize=(9.6, 5.4),
    )
    col = RegularPolyCollection(
        sizes=(1000000,),
        numsides=4,
        facecolors=pl.matplotlib.colors.to_rgb([val / 255 for val in colour]),
        offsets=[0.5, 0.5],
        transOffset=ax.transData
    )
    ax.axis('off')
    ax.add_collection(col)

    pl.show()


def read_and_plot():
    plot_colour(read_rgb())


def capture_image(n=1, webcam=0, show=False, **settings):
    """
    Captures webcam image and overlays the target frame over the image (if specified)

    :param n: number of frames to capture
    :param webcam: webcam index
    :param show: show the captured frame
    :return:
    """
    video = cv2.VideoCapture(webcam)
    # todo fix this after Globe
    if len(settings) == 0:
        video.set(15, -5)
    for setting in settings:
        video.set(_cv[setting], settings[setting])
    video.grab()
    frames = [video.retrieve()[1] for i in range(n)]
    # frames = [video.read()[1] for i in range(n)]
    video.release()
    if show is True:
        show_image(np.hstack(frames))
    if n == 1:
        return frames[0]
    return frames


def box_frame(frame, section=40, linewidth=1):
    """
    Takes a frame and puts a highlight box in the center

    :param frame: frame to manipulate
    :param section: pixel width and height of the section
    :param linewidth: width of the line (pixels)
    :return:
    """
    h = len(frame)
    w = len(frame[0])
    midh = h / 2
    midw = w / 2
    l = int(midw - section / 2 - 1)
    r = int(midw + section / 2 + 1)
    t = int(midh + section / 2 + 1)
    b = int(midh - section / 2 - 1)
    for x in range(l, r):
        for i in range(linewidth):
            frame[t+i][x] = np.array([0, 255, 0], dtype='uint8')
            frame[b-i][x] = np.array([0, 255, 0], dtype='uint8')
    for y in range(b-i, t+i):
        for i in range(linewidth):
            frame[y][l-i] = np.array([0, 255, 0], dtype='uint8')
            frame[y][r+i] = np.array([0, 255, 0], dtype='uint8')
    return np.asarray(frame, dtype='uint8')


def show_image(image):
    cv2.imshow('', image)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()


def get_whitebalance(frame=None):
    """
    Record the white balance values for the current environment in the LAB colour space
    Based on https://stackoverflow.com/questions/46390779/automatic-white-balancing-with-grayworld-assumption

    :param frame: optional frame to hand the function
    :return: scalars for applying white balance
    """
    if frame is None:
        frame = capture_image()
    final = cv2.cvtColor(frame, cv2.COLOR_BGR2LAB)
    avg_a = np.average(final[:, :, 1])
    avg_b = np.average(final[:, :, 2])
    return avg_a, avg_b


def apply_whitebalance(uncorr, a_avg, b_avg, show=False):
    """
    Applies a previous set of white balance parameters to a frame in an efficient manner (using numpy vectorization)

    :param uncorr: uncorrected frame
    :param a_avg: average a value
    :param b_avg: average b value
    :param show: show a comparison of before and after
    :return: corrected frame
    """
    # convert to lab colour space
    labspace = cv2.cvtColor(uncorr, cv2.COLOR_BGR2LAB)

    # extract luminosity
    lorig = labspace[:, :, 0]

    # create base luminosity scalar array for a and b
    l = np.copy(lorig) * (100 / 255.)
    # create scalar arrays
    ascale = np.copy(l) * (a_avg - 128) * 0.011
    bscale = np.copy(l) * (b_avg - 128) * 0.011

    # retrieve and manipulate a and b arrays
    a = labspace[:, :, 1] - ascale
    b = labspace[:, :, 2] - bscale

    # cast to appropriate data types for back conversion
    a = a.astype('uint8')
    b = b.astype('uint8')

    # combine into LAB arrays and reshape to the original form
    modified = np.dstack((lorig, a, b))
    shaped = np.reshape(modified, uncorr.shape)

    # convert back to BGR array and return
    out = cv2.cvtColor(shaped, cv2.COLOR_LAB2BGR)
    if show is True:
        show_image(np.hstack((uncorr, out)))
    return out


# camera = Camera(cam=0)
# camera.get_training_images()



if __name__ == '__main__':
    def test_gui():
        CAMERA_NUMBER = 1 # zero if the only webcam is the usb webcam, 1 if computer has its own webcam but you want to use
        # the usb webcam
        CREATE_SAVE_FOLDER = False  # whether or not to create a folder to save all the images in; these are to save all the
        # raw images taken by the camera

        # create the Camera object
        camera = Camera(cam=CAMERA_NUMBER,
                        create_save_folder=CREATE_SAVE_FOLDER)

        my_gui = CameraGUI(camera=camera)
        my_gui.root.mainloop()


    # test_gui()


