from ada_core.dependencies.interface import controller
from vision.webcam import Camera
from ada_core.dependencies.arm import robotarm as n9
import cv2
import imutils
from datetime import datetime
from ada_core.dependencies.coordinates import Location
from machine_learning.generic_models_and_tests import cnn_image_categorical

# check to see if the arm actually picked something up on the probe or the gripper using visual feedback

"""
Actual functions to use ea ML model to check if something was picked up correctly; but this is only for functions 
that will use the model. and it will bring the arm to the camera, but it will not pick things up; this will need to 
be done in an larger outer sequence (not written out explicitly here), but after something gets picked up the methods here can be used to evaluate what it picked up. and then 
"""

def what_did_i_pick_up_pre_run(camera,
                               model_image_width=None,  # int, the width all training images were resized to
                               model_image_height=None,  # int, the height all training images were resized to
                               # if None is passed for the model image and height, that is because the get training \
                               # images function was run, and no width or height is selected for that. but if this is
                               #  run and width and height is passed, then this is for actual prediction use
                               ):
    # need to find frame
    image = camera.take_picture(False)
    camera.crop_left, camera.crop_right, camera.crop_top, camera.crop_bottom =\
        camera.select_region_of_interest(img=image)

    # need to check if the frame is at least the same if not bigger than the size of the images used to train the
    # model; if smaller, then should pick a bigger frame, so that later when predicting, the image taken can be
    # resized to the same image size as the training images
    left, right, top, bottom = camera.find_frame(img=image)
    region_of_interest_width = right - left
    region_of_interest_height = bottom - top

    # then region_of_interest_width must be => model_image_width, and same with height, if not, make user select new
    # region of interest
    # hopefully dont reach recursion limit by writing the code this way...
    if model_image_height is not None:  # if this function is being run for actual prediction, and not to get training
        # images
        if region_of_interest_height >= model_image_height:
            if region_of_interest_width >= model_image_width:
                # region of interest is at least as big as the size of images used for training, can return
                print('Selection of region of interest works with the machine learning model')
                return
            else:
                # then need to make user select region of interest again
                print("Selection of region of interest doesn't work with the machine learning model. \n "
                      "Select a wider region of interest")
                what_did_i_pick_up_pre_run(camera=camera,
                                           model_image_width=model_image_width,
                                           model_image_height=model_image_height,
                                           )
        else:
            # then need to make user select region of interest again
            print("Selection of region of interest doesn't work with the machine learning model. \n "
                  "Select a taller region of interest")
            what_did_i_pick_up_pre_run(camera=camera,
                                       model_image_width=model_image_width,
                                       model_image_height=model_image_height,
                                       )


def get_training_images(camera):
    # basically take constant stream of pics, and display and save them; both contour and original image.
    # but first only take one pic and let use select the frame, then only save the frame image

    # first select the frame
    what_did_i_pick_up_pre_run(camera=camera)

    video_capture = cv2.VideoCapture(camera.cam)

    print(f'images will be saved into this folder: {camera.folder_path}')  # todo check that this works

    while True:
        # capture frame-by-frame
        ret, image = video_capture.read()
        image_copy = image.copy()
        _, edges = find_contours(image=image, big=True)  # get contours for the image
        image = image_copy

        if not ret:  # doesn't really matter here, it matters more for reading from an image, but for video doesnt occur
            break

        cropped_image = camera.crop_horizontal(img=image, crop_left=camera.crop_left, crop_right=camera.crop_right)
        cropped_image = camera.crop_vertical(img=cropped_image, crop_top=camera.crop_top,
                                             crop_bottom=camera.crop_bottom)

        cropped_edge_image = camera.crop_horizontal(img=edges, crop_left=camera.crop_left, crop_right=camera.crop_right)
        cropped_edge_image = camera.crop_vertical(img=cropped_edge_image, crop_top=camera.crop_top,
                                                  crop_bottom=camera.crop_bottom)

        cv2.imshow('Live cropped video', cropped_image)
        cv2.imshow('Live cropped edge video', cropped_edge_image)

        # save the image
        time = datetime.now()

        time_as_str = time.strftime('%d_%m_%Y_%H_%M_%S')
        camera.all_images.append([time_as_str, cropped_image])

        if camera.naming_prefix is None:
            cv2.imwrite(f'{camera.folder_path}\\{time_as_str}.jpg', cropped_image)
            cv2.imwrite(f'{camera.folder_path}\\edge_{time_as_str}.jpg', cropped_edge_image)
        else:
            cv2.imwrite(f'{camera.folder_path}\\{camera.naming_prefix}_{time_as_str}.jpg', cropped_image)
            cv2.imwrite(f'{camera.folder_path}\\{camera.naming_prefix}_edge_{time_as_str}.jpg', cropped_edge_image)

        # if press the q button exit
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


def what_did_i_pick_up(camera,  # instance of a Camera
                       model_folder_path,  # path ot ml model folder
                       encoding_dictionary,  # dictionary, encoding dictionary used by the model. Must be same as the
                       #  one used to make the model (key is class, value is a number)
                       model_image_width,  # int, the width all training images were resized to. this must be the
                       # same value as was used to create the ML model.
                       model_image_height,  # int, the height all training images were resized to. this must be the
                       # same value as was used to create the ML model.
                       show_prediction=False,  # Whether you want to show the image with prediction on it or not
                       number_of_times_to_image=3,  # int, number of times to image to try to find what is at the end
                       # of the probe. must be an odd number. this is to make things more robust, so there is an
                       # average in case the image gets misclassified the first time the ML model was used.
                       show=False,  # bool, True if you want to display the images to predict
                       ):
    n9.safeheight()  # make sure arm is at safeheight before moving to the camera location
    # function that uses vision to indicate what, if anything, was picked up by the arm

    # but before running this, for the camera instance the user must have already run the
    # self.what_did_i_pick_up_pre_run function and chosen an appropriate region on interest to work with the ML model

    # check if there was something that was theoretically 'picked' up - either on the probe (eg. needle) or the
    # gripper (eg. dispenser).
    if n9.modules.probe.item is not None:
        probe_or_gripper = 'probe'
    else:
        probe_or_gripper = 'gripper'

    keep_track_of_predictions = {}  # dictionary to keep track of the different classes, and how many of each has
    # been predicted

    list_of_images_for_prediction = []  # list to add all the resized images to, since the predict_class funciton
    # takes a list of images as an argument

    for key in encoding_dictionary.keys():  # loop through keys in encoding dictionary to add to the
        # keep_track_of_predictions dictionary
        keep_track_of_predictions[key] = 0  # initialize all keys with count of 0 predictions

    for iteration in range(0, number_of_times_to_image):  # do this for the number of times you want to image
        # move either the probe or gripper to the spot that is indicated by the camera, and take picture
        image = camera.image_target(target=probe_or_gripper)  # moves either the probe or the gripper to the camera
        # location, based on if the arm thinks it has something on the probe or the gripper, then takes a photo

        contours, edges = find_contours(image=image, big=True)  # get contours for the image

        # find where is needed to crop the image; how many pixels inwards from the 4 edges of the frame
        left, right, top, bottom = camera.find_frame(img=image)

        # for now, assume not using edge images, so dont need this code block
        # # crop the edges image
        # edges = camera.crop_horizontal(img=edges, crop_left=left, crop_right=right)
        # edges = camera.crop_vertical(img=edges, crop_top=top, crop_bottom=bottom)

        # crop the original image
        image = camera.crop_horizontal(img=image, crop_left=camera.crop_left, crop_right=camera.crop_right)
        image = camera.crop_vertical(img=image, crop_top=camera.crop_top,
                                             crop_bottom=camera.crop_bottom)

        # resize image to match the size of the training images for the ML model
        image = cv2.resize(image, (model_image_width, model_image_height))

        list_of_images_for_prediction.append(image)

    if show is True:  # if user wants to display image
        for idx, image in enumerate(list_of_images_for_prediction):
            cv2.imshow(f'image_to_predict_{idx}', image)

    # then use model to predict
    # this returns a list of all the predicted classes, but returns the 1-hot encoded values (eg. 0, 1... instead of
    # the string for the class, eg. needle, pipette)
    predicted_encoded_classes_list = cnn_image_categorical.predict_class(model_folder_path=model_folder_path,
                                                                         data_to_test=list_of_images_for_prediction,
                                                                         encoding_dictionary=encoding_dictionary,
                                                                         show_prediction=show_prediction,
                                                                         )

    # iterate through all the predictions from all the images - predictions are the one hot encoded value here
    # for prediction_from_image_as_one_hot_vector in predicted_encoded_classes_list:
    #     prediction_from_image = str(prediction_from_image)
    #     # knowing the prediction, add 1 to the count in the keep_track_of_predictions dictionary
    #     keep_track_of_predictions[prediction_from_image] += 1
    #
    for prediction_from_image_as_one_hot_vector in predicted_encoded_classes_list:
        for label in encoding_dictionary:  # iterate through all the string labels in the encoding dictionary
            if prediction_from_image_as_one_hot_vector == encoding_dictionary[label]:  # if the predicted hot key label is
                # equal to the (hot key label gotten after searching through the encoding dictionary using the
                # string label), then the string associated with the hot key value has been found
                keep_track_of_predictions[label] += 1

                    # find the prediction with highest number of counts of being predicted (still 1 hot encoded value). to do this,
    # zip the values and the keys together, and then find the maximum value (which should be index 0 of the tuple
    # returned from max(zip)) and thus find find the corresponding key (which should be index 1 of the tuple
    # returned from max(zip)) - this key is the encoded value with the maximum counts
    final_prediction = max(zip(keep_track_of_predictions.values(), keep_track_of_predictions.keys()))[1]

    # then final_prediction should be the str form of the label (class)
    return final_prediction

    # # go through the values in the encoding dictionary to find the label that matches with the 1-hot encoded value,
    # # to actually know what the prediciton is (as str, such as needle or pipette)
    # for label in encoding_dictionary:  # iterate through all the string labels in the encoding dictionary
    #     if final_encoded_prediction == encoding_dictionary[label]:  # if the predicted hot key label is
    #         # equal to the (hot key label gotten after searching through the encoding dictionary using the
    #         # string label), then the string associated with the hot key value has been found
    #         return label  # label is the str class that was predicted


# similar to what was used for liquid level, except this one is simplified because dont need to select a specific
# contour, just need to get an image of all the contours
def find_contours(image,
                  big=True,
                  ):
    """
    Finds the contours of the image. Depending on what big is: True sorts largest to smallest, to find
    the largest contour in the image. Returns the contours, and the edges, and the closed for the image. But doesnt
    select and return the largest or smallest contour.

    :param image: numpy.ndarray, image
    :param big: bool, true if you want to sort to find the largest rectangle
    :return: cnts is a list of the rectangle contours as numpy.ndarrays. edges is a list of the rectangle edges as
        numpy.ndarrays. closed is a list of the rectangle closed images as numpy.ndarrays.
    """
    # convert the image to grayscale, blur it, and find edges in the image
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  # assuming image was loaded using opencv; which does things in BGR
    gray = cv2.GaussianBlur(gray, (3, 3), 0)
    # cv2.imshow('Blur', gray)
    #
    # gray_copy = gray.copy()  # todo find good binary threshold image?
    # _, thresh = cv2.threshold(gray_copy, 0, 255, cv2.THRESH_BINARY)

    # TODO OR COULD JUST USE THE EDGES INSTEAD AND JUST ERODE AND DIALATE TO GET RID OF NOISE
    edges = cv2.Canny(gray, 30, 100)

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (7, 7))
    cv2.morphologyEx(edges, cv2.MORPH_OPEN, kernel)  # removes noise

    # print(f'show closed image within find_contours_needle')
    # cv2.imshow('closed in find contours needle', closed)
    # cv2.waitKey(0)
    # _, cnts, _ = cv2.findContours(image=edges, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)
    _, cnts, _ = cv2.findContours(image=edges, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_SIMPLE)

    if big is True:
        cnts = sorted(cnts, key=cv2.contourArea, reverse=True)[:10]  # sorts to find largest rectangle
    else:
        cnts = sorted(cnts, key=cv2.contourArea, reverse=False)[:10]  # sorts to find smallest

    return cnts, edges


# # for testing things up with computer webcam
# camera = Camera(cam=0)
# get_training_images(camera=camera)

"""
should be how things will go:
1. create Camera instance
2. run get_training_images(camera)
3. create ML model - example of this is did_it_pick_up_ml script
4. the values that will need to be passed to the methods in this file will mostly need to be based off the values in 
the script that was used to create the ML models. 
5. need to create a sequence that will use the methods in here. at the beginning of the sequence, need to run 
what_did_i_pick_up_pre_run(camera, width, height), to select the area to use to pass into the model. width and height 
need to be the same as was used to create the ML model. Then need to create a loop to get the arm to pick something 
up, and then call what_did_i_pick_up(...), then based on the result from what_did_i_pick_up(...), either continue on 
and use/remove the item that was picked up, or send an error maybe through slack or a printout statement, to let the 
user know that whatever that wanted to be picked up actually wasnt. The loop really is just if you want to test out 
the use of the model; but the sequence of method calls is what would have to be used for actual integration of using 
the ML model + camera as visual feedback for error avoidance. 
"""


