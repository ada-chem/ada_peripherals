"""
this can be deleted, this is essentially the same as the pipette version, but is outdated
"""
import os
from ada_peripherals import vision
from ada_peripherals.machine_learning.generic_models_and_tests.cnn_image_categorical import ImageCNNModel,  \
    predict_class, load_data_set, load_images_from_folder_no_label, view_dataset, label_data

# this function will iterate through the images in a folder, path of folder needs to be specified, to create a
# machine learning model to identify different things that can be attached to the end of the n9 arm, and then be able
#  to know if there is something attached to it or not

#######################################################################
# # root_dir = os.getenv('imagefolder')  # this is to connect to the NAS, comment out for now because testing with a
# folder of images that isn't in the NAS

vision_folder_name = 'vision'
vision_folder_path = os.path.dirname(os.path.abspath(vision.__file__))

did_it_pick_up_pipette_folder_name = 'did_it_pick_up_VIAL'
did_it_pick_up_pipette_folder_path = os.path.join(vision_folder_path, did_it_pick_up_pipette_folder_name)

data_images_folder_name = 'quality_control_images_VIAL'
data_images_folder_path = os.path.join(did_it_pick_up_pipette_folder_path, data_images_folder_name)

pickle_data_file_name = 'did_it_pick_up_data.pkl'
pickle_data_file_path = os.path.join(did_it_pick_up_pipette_folder_path, pickle_data_file_name)

model_folder_name = 'did_it_pick_up_ml_model'
model_folder_path = os.path.join(did_it_pick_up_pipette_folder_path, model_folder_name)

images_to_test_path = os.path.join(did_it_pick_up_pipette_folder_path, 'test_data')

augment_image_dictionary = {'horizontal_flip': True,
                            'list_of_crop_and_resize_arguments': [{'crop_left': 0.0,
                                                                   'crop_right': 0.0,
                                                                   'crop_top': 0.0,
                                                                   'crop_bottom': 0.0},
                                                                  {'crop_left': 0.01,
                                                                   'crop_right': 0.01,
                                                                   'crop_top': 0.01,
                                                                   'crop_bottom': 0.01},
                                                                  {'crop_left': 0.015,
                                                                   'crop_right': 0.015,
                                                                   'crop_top': 0.015,
                                                                   'crop_bottom': 0.015},
                                                                  {'crop_left': 0.02,
                                                                   'crop_right': 0.02,
                                                                   'crop_top': 0.02,
                                                                   'crop_bottom': 0.02},
                                                                  {'crop_left': 0.025,
                                                                   'crop_right': 0.025,
                                                                   'crop_top': 0.025,
                                                                   'crop_bottom': 0.025},
                                                                  {'crop_left': 0.03,
                                                                   'crop_right': 0.03,
                                                                   'crop_top': 0.03,
                                                                   'crop_bottom': 0.03},
                                                                  {'crop_left': 0.035,
                                                                   'crop_right': 0.035,
                                                                   'crop_top': 0.035,
                                                                   'crop_bottom': 0.035},
                                                                  {'crop_left': 0.04,
                                                                   'crop_right': 0.04,
                                                                   'crop_top': 0.04,
                                                                   'crop_bottom': 0.04},
                                                                  {'crop_left': 0.045,
                                                                   'crop_right': 0.045,
                                                                   'crop_top': 0.045,
                                                                   'crop_bottom': 0.045},
                                                                  {'crop_left': 0.05,
                                                                   'crop_right': 0.05,
                                                                   'crop_top': 0.05,
                                                                   'crop_bottom': 0.05},
                                                                  {'crop_left': 0.075,
                                                                   'crop_right': 0.075,
                                                                   'crop_top': 0.075,
                                                                   'crop_bottom': 0.075},
                                                                  ],
                            }

image_height = 300
image_width = 220
number_of_channels = 3
number_of_classes = 2
batch_size = 10

encoding_dictionary = {'nothing': 0, 'vial': 1}  # dictionary to do one
#  hot encoding

loss_threshold = 0.005
learning_rate = 1e-3

########################################################################

def label_the_data():
    label_data(data_set_path=data_images_folder_path,
               pickle_file_path=pickle_data_file_path,
               img_width=image_width,
               img_height=image_height,
               augment_image_data_arguments=augment_image_dictionary,
               )


def train():
    train_features, train_targets, \
    valid_features, valid_targets, \
    test_features, test_targets = load_data_set(pickle_data_path=pickle_data_file_path,
                                                encoding_dictionary=encoding_dictionary,
                                                img_height=image_height,
                                                img_width=image_width,
                                                number_of_channels=number_of_channels)

    cnn_image_model = ImageCNNModel(batch_size=batch_size,
                                    features_shape=train_features.shape,
                                    targets_shape=train_targets.shape,
                                    number_of_classes=number_of_classes,
                                    model_folder_path=model_folder_path,
                                    loss_threshold=loss_threshold,
                                    learning_rate=learning_rate,
                                    )

    cnn_image_model.set_data(train_features,
                             train_targets,
                             valid_features,
                             valid_targets,
                             test_features,
                             test_targets,
                             )

    cnn_image_model.build_model()


# ########
# # test prediction


def predict(show_prediction=False):
    loaded_images = load_images_from_folder_no_label(path_to_folder=images_to_test_path,
                                                     img_width=image_width,
                                                     img_height=image_height,
                                                     )
    predict_class(model_folder_path=model_folder_path,
                  data_to_test=loaded_images,
                  encoding_dictionary=encoding_dictionary,
                  show_prediction=show_prediction,
                  )

# label_the_data()
# view_dataset(pickle_data_path=pickle_data_file_path)
# train()
predict(show_prediction=True)
