import re
from setuptools import find_packages, setup

NAME = 'ada_peripherals'
DESCRIPTION = 'Python API Ada project related peripherals and third party components'
AUTHOR = 'Lars Yunker'
CLASSIFIERS = [
    'Programming Language :: Python :: 3',
    'Operating System :: Windows',
]
INSTALL_REQUIRES = [
    'flask',
    'zaber.serial',
    'dropbox',
    'opencv_python',
    'imutils',
    'scipy',
    'matplotlib',
    'seaborn',
    'numpy',
    'python-dotenv',
    'pyvisa',
    'pyvisa-py',
    'obs-websocket-py',
    'pandas>=0.23.3',
]
EXCLUDE_PACKAGES = [
    ''
]

with open('LICENSE') as f:
    LICENSE = f.read()

with open('VERSION') as f:
    VERSION = re.sub('^v', '', f.read())

# find packages and prefix them with the main package name
PACKAGES = [NAME] + [f'{NAME}.{package}' for package in find_packages(exclude=EXCLUDE_PACKAGES)]

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    install_requires=INSTALL_REQUIRES,
    packages=PACKAGES,
    # look for modules inside our package namespace in the root directory (normally this is a subdirectory)
    package_dir={NAME: ''},
    license=LICENSE,
    classifiers=CLASSIFIERS
)
