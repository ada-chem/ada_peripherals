from ada_peripherals.hplc.hplc import pull_all_hplc_data_from_folder
from ada_core.dependencies.spreadsheet import XLSX
import os

# folder to search
targetfolder = 'C:\\Temp\\Jordan temp\\JD-EX-110'

#\\N9_MULTI_INJECT 2018-07-11 16-53-30

# wiggle in retention time
wiggle = 0.1

print(f'Extracting data from {targetfolder}')
filenames, targets = pull_all_hplc_data_from_folder(  # pull all the data
    folder=targetfolder,
    wiggle=wiggle,
)

# create excel file in folder
xlfile = XLSX(
    os.path.join(targetfolder, 'extracted data.xlsx'),
    create=True,
)

print('Saving data to %s' % str(xlfile))
for wavelength in targets:  # for each wavelength
    xlfile.add_column(  # add filename column
        f'{wavelength} nm', # sheet name
        'Filename', #print the title (cell A1)
        *[file.split(os.sep)[-2] for file in filenames], #print all the filenames (that's what the star does for dic
        save=False,
    )
    for i in sorted(targets[wavelength]):  # goes through and for each unique retention time (plusmin wiggle)
        xlfile.add_column(
            f'{wavelength} nm',
            f'{targets[wavelength][i].retention_time.specific_prefix("")} {targets[wavelength][i].retention_time.unit}',
            *targets[wavelength][i].areas,
            save=False
        )

xlfile.save()  # save file
print('fin.')
