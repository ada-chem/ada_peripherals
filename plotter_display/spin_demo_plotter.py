"""A plotter window that can be continuously updated with conductivity data
and images from Zippleback"""

import importlib
import matplotlib as mpl
importlib.reload(mpl)
import pickle
import matplotlib.pyplot as plt
plt.ioff()
import pandas as pd
import numpy as np
import os


pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)



class SpinDemoPlotter(object):
    """
    There once was a class that took data,
    But it really was only in beta!
    He pushed it too soon,
    thinking his code was immune,
    Yet now it has periplaneta!

    """

    def __init__(self,
                 plot=True):

        # Create the empty data dataframe
        self.data      = pd.DataFrame(columns=['sample_number', 'position_mm', 'iteration', 'voltage_V', 'current_A'])
        self.stat_data = pd.DataFrame(columns=['mean_A', 'std_A', 'sample_number', 'position', 'voltage_V'])
        self.calc      = pd.DataFrame(columns=['sample_number', 'position_mm', 'slope_ohm', 'slope_err_ohm', 'intercept_A'])
        self.metric    = {'x': [], 'y': []}

        self.ml_figures = {}

        self.once = False
        if plot == True:

            # Create the angle_plot
            mpl.rcParams.update({'font.size': 10})
            self.plot = plt.figure(figsize=(11,8.5))

            # print(mpl.get_backend())
            # mpl.get_backend()
            # wm = plt.get_current_fig_manager()
            # wm.window.wm_geometry("1920x1080-1000-1")
            # wm.window.state('zoomed')

            # self.angle_plot.canvas.manager.window.Move(100,400)

            # Create the subplots
            self.subplots = {}
            for count in range(5):

                position = count + 1

                self.subplots[f'{count}_image'] = self.plot.add_subplot(8, 5, position)
                self.subplots[f'{count}_photo'] = self.plot.add_subplot(8, 5, position + 5)

                # If the first instance of their type
                if count == 0:
                    self.subplots[f'{count}_position'] = self.plot.add_subplot(4, 5, position + 5)
                    self.subplots[f'{count}_voltage'] = self.plot.add_subplot(4, 5, position + 10)

                # Otherwise share axis
                else:
                    self.subplots[f'{count}_position'] = self.plot.add_subplot(4, 5, position + 5, sharex=self.subplots['0_position'], sharey=self.subplots['0_position'])
                    self.subplots[f'{count}_voltage'] = self.plot.add_subplot(4, 5, position + 10)

            self.subplots['metric'] = self.plot.add_subplot(4, 3, 11)


            # Update the angle_plot formatting
            self._format_plot()

    def _format_plot(self):

        # Get the first instances of each type of subplot that will be used for
        # y-axis scaling.

        voltage_scale = self.subplots['0_voltage']
        position_scale = self.subplots['0_position']

        # Set ranges
        for subplot_name, subplot in self.subplots.items():
            if subplot_name.endswith('voltage'):

                subplot.set_xticks([-10, 10])
                subplot.set_xbound(-11, 11)
                subplot.set_xlabel(xlabel='$\it{V}$ (mV)', labelpad=-12)

                # subplot.set_yticks([-100, 100])
                # subplot.set_ybound(-110, 110)
                subplot.set_ylabel(ylabel='$\it{I}$ (nA)')

                # Link the y-axis
                # subplot._shared_y_axes().join(subplot, voltage_scale)

            if subplot_name.endswith('position'):

                subplot.set_xticks([0, 30])
                subplot.set_xbound(-1, 31)
                subplot.set_xlabel(xlabel='$\it{Position}$ (mm)', labelpad=-12)

                # subplot.set_yticks([9, 11])
                # subplot.set_ybound(3, 17)
                subplot.set_ylabel(ylabel='$\it{σ}$ (µS)')

                # Link the y-axis
                # subplot._shared_y_axes().join(subplot, position_scale)

            if subplot_name.endswith('image') or subplot_name.endswith('photo'):
                subplot.axis('off')

            if subplot_name == 'metric':
                subplot.set_xlabel(xlabel='Sample')
                subplot.set_ylabel(ylabel='Metric')
                subplot.set_xbound(0, 0.2)
                subplot.set_xticks([0.165, 0.13, 0.095, 0.06, 0.025])

            # if not the first axis
            if subplot_name.endswith('voltage') or subplot_name.endswith('position'):
                if not int(subplot_name.split('_')[0]) % 5 == 0:
                    subplot.get_yaxis().set_visible(False)


        # Change the spacing
        self.plot.subplots_adjust(
            left=0.05,
            right=0.95,
            top=0.95,
            bottom=0.05,
            wspace=0.03,
            hspace=0.35,
        )

    def update(self,
               data_type=None,
               data_metadata=None,
               data=None):

        # Set some params
        self.current_sample_number = data_metadata['sample_number']
        if data_type == 'data':
            self.current_position_mm = data_metadata['position_mm']
        self.new_data_metadata = data_metadata

        # If data:
        if data_type == 'data':

            # Create an empty dataframe to populate
            self.new_data = pd.DataFrame(columns=['sample_number', 'position_mm', 'iteration', 'voltage_V', 'current_A'])

            # For each iteration
            for count, frame in enumerate(data):

                # Add on the metadata to the data
                frame['sample_number'] = data_metadata['sample_number']
                frame['position_mm'] = data_metadata['position_mm']
                frame['iteration'] = count

                # Append to the growing dataset
                self.new_data = pd.concat([self.new_data, frame], ignore_index=True, sort=False)

            # Append to the root dataset
            self.data = pd.concat([self.data, self.new_data], ignore_index=True, sort=False)

            # Update calculated values
            self._update_calculations()

        if data_type == 'angle_figure':

            # Stash the angle_figure in the class
            self.ml_figures[f'ml_fig_{data_metadata["sample_number"]}'] = data

        if data_type == 'performance_metric':

            # Add the metric data
            self.metric['x'].append(data['x'])
            self.metric['y'].append(data['y'])

        # Clear the old data
        self._clear_plot()

        # update the angle_plot data
        self._update_plot()

        # Update the angle_plot formatting
        self._format_plot()

        # Revisualize the angle_plot
        self._render_plot()

    def _update_calculations(self):

        # Calculate mean and std
        self.new_stat_data = pd.DataFrame(columns=['mean_A', 'std_A', 'sample_number', 'position', 'voltage_V'])
        for voltage in np.unique(self.new_data['voltage_V'].values):

            calc_values = {}
            voltage_point = self.new_data.query(f'voltage_V == {voltage}')
            calc_values['mean_A'] = voltage_point['current_A'].mean()
            calc_values['std_A'] = voltage_point['current_A'].std()

            calc_values['sample_number'] = self.current_sample_number
            calc_values['position'] = self.current_position_mm
            calc_values['voltage_V'] = voltage_point['voltage_V'].iloc[0]
            calc_values_df = pd.DataFrame(data=calc_values, index=[0])
            self.new_stat_data = self.new_stat_data.append(calc_values_df, ignore_index=True, sort=False)
        self.stat_data = self.stat_data.append(self.new_stat_data, ignore_index=True, sort=False)

        # Calculate slope

        fit, cov = np.polyfit(self.new_stat_data['voltage_V'], self.new_stat_data['mean_A'], 1, cov=True)

        # Calculate the uncertainty from the fit
        slope_err_ohm = np.sqrt(np.diag(cov))[0]

        slope, intercept = fit[0], fit[1]
        self.new_calc = pd.DataFrame(data={
            'sample_number': self.current_sample_number,
            'position_mm': self.current_position_mm,
            'slope_ohm': slope,
            'slope_err_ohm': slope_err_ohm,
            'intercept_A': intercept,
        },
            index=[0]
        )

        self.calc = self.calc.append(self.new_calc, ignore_index=True, sort=False)



        # Update some generic conversions
        self.stat_data['voltage_mV'] = self.stat_data['voltage_V'] * 1e3
        self.stat_data['mean_nA'] = self.stat_data['mean_A'] * 1e9
        self.stat_data['std_nA'] = self.stat_data['std_A'] * 1e9
        self.calc['slope_uohm'] = self.calc['slope_ohm'] * 1e6
        self.calc['slope_err_uohm'] = self.calc['slope_err_ohm'] * 1e6
        self.calc['intercept_nA'] = self.calc['intercept_A'] * 1e9

    def _update_plot(self):

        # Calculate the number of positions so as to create color maps
        num_positions = len(np.unique(self.calc['position_mm']))
        pos_cmap = mpl.cm.get_cmap('viridis', num_positions + 1)
        pos_cmap_list = [pos_cmap(i) for i in range(num_positions + 1)]

        # For each sample number observed
        for sample_number in np.unique(self.stat_data['sample_number']):

            # Filter the datasets
            calc_filtered = self.calc.query(f'sample_number == {sample_number}')
            stat_data_filtered = self.stat_data.query(f'sample_number == {sample_number}')

            # Plot the positional data
            self.subplots[f'{sample_number}_position'].plot(
                calc_filtered['position_mm'],
                calc_filtered['slope_uohm'],
                c='lightgrey'
            )

            self.subplots[f'{sample_number}_position'].errorbar(
                    calc_filtered['position_mm'],
                    calc_filtered['slope_uohm'],
                    yerr=calc_filtered['slope_err_uohm'],
                    ls='none',
                    c=pos_cmap_list
            )

            self.subplots[f'{sample_number}_position'].scatter(
                    calc_filtered['position_mm'],
                    calc_filtered['slope_uohm'],
                    c=pos_cmap_list,
                    s=5,
                    zorder=10,
            )

            # Plot the current data
            for count, position in enumerate(np.unique(stat_data_filtered['position'])):
                plot_current_data = stat_data_filtered.query(f'position == {position}')
                calc_current_data = calc_filtered.query(f'position_mm == {position}')
                # Plot current error bars
                self.subplots[f'{sample_number}_voltage'].errorbar(
                    plot_current_data['voltage_mV'],
                    plot_current_data['mean_nA'],
                    yerr=plot_current_data['std_nA'],
                    ls='none',
                    c=pos_cmap(count)
                ,
                )

                # Plot linear fits
                slope_func = lambda x: x * calc_current_data['slope_uohm'].iloc[0] + calc_current_data['intercept_nA'].iloc[0]
                x_i = plot_current_data['voltage_mV'].min()
                x_f = plot_current_data['voltage_mV'].max()

                self.subplots[f'{sample_number}_voltage'].plot(
                    [x_i, x_f],
                    [slope_func(x_i), slope_func(x_f)],
                    c=pos_cmap(count)
                )

        for key, ml_figure in self.ml_figures.items():

            # Get the sample number
            sample_number = int(key.split('_')[2])

            # Get the subplot to write to
            image_subplot = self.subplots[f'{sample_number}_image']
            photo_subplot = self.subplots[f'{sample_number}_photo']

            # Get the unprocessed ML graphic. clean, and save
            ax = ml_figure.gca()
            ax.get_xaxis().set_visible(False)
            ax.get_yaxis().set_visible(False)

            # Make the squares dark
            for child in ax.get_children():
                if type(child).__name__ == 'Rectangle':
                    child.set_alpha(1)

            # Save the solid version
            ml_figure.savefig('temp_1.png', bbox_inches='tight', transparent="True", pad_inches=0)

            # Set the squares to full opacity
            for child in ax.get_children():
                if type(child).__name__ == 'Rectangle':
                    child.set_alpha(0)

            # Save the opaque version
            ml_figure.savefig('temp_0.png', bbox_inches='tight', transparent="True", pad_inches=0)




            # Put in angle_plot
            im_0 = mpl.image.imread('temp_0.png')
            im_1 = mpl.image.imread('temp_1.png')
            image_subplot.imshow(im_0)
            photo_subplot.imshow(im_1)
            # image_subplot.get_xaxis().set_visible(False)
            # image_subplot.get_yaxis().set_visible(False)
            # image_subplot.axis('off')

            # Delete the file
            # os.remove('temp.png')

        self.subplots['metric'].plot(
            self.metric['x'],
            self.metric['y']
        )
        self.subplots['metric'].scatter(
            self.metric['x'],
            self.metric['y']
        )

    # Causes the angle_plot to be rerendered
    def _render_plot(self):

        # self.angle_plot.savefig('status.png')

        plt.pause(0.01)
        # plt.draw()

    def _clear_plot(self):

        # Deletes all the data from the angle_plot to prepare it for replotting
        for ax in self.plot.get_axes():
            ax.clear()

        # plt.clf()


# Passes dummy data to the SpinDemoPlotter() to test the plotting
if __name__ == "__main__":

    # This generates a list of dfs that immitates Current / Votage data from
    # the Keithley for a given sample and position
    def create_dummy_data():

        # List that will be populated with dummy data
        data = []

        # For three iterations at one position, generate three dataframes
        for i in range(3):

            # Make a voltage / current dataframe
            frame = pd.DataFrame(data={
                'voltage_V': np.linspace(-0.01, 0.01, 11),
                'current_A': np.linspace(-1e-7, 1e-7, 11) + (1e-7 * 0.4 * np.random.rand(11)),
            })

            # Append to the list
            data.append(frame)

        # Add some noise to the data
        # TODO Make this a multiple of the same random number such that each position has it's own bias.
        rand = np.random.rand(11)
        for frame in data:
            frame['current_A'] = frame['current_A'] * 0.5 + 1 * rand * frame['current_A']

        # Return the data
        return data

    # Instantiate the plotter window.  Nothing needs to be passed.
    obj = SpinDemoPlotter()

    # For 15 samples:
    for sample in range(5):

        # Clear the metadata dictionary that will be passed to the plotter
        # and add the sample number.
        metadata = {}
        metadata['sample_number'] = sample

        # For each position on this sample:
        for position in range(1, 32, 5):

            # Add the position to the metadata
            metadata['position_mm'] = position

            # Create the dummy data
            data = create_dummy_data()

            # Send the data to the plotter!
            obj.update(data_type='data', data_metadata=metadata, data=data)


        # Import an image that would be generated for each sample.
        with open('example_figure.pickle', 'rb') as f:
            figure = pickle.load(f)
            plt.close(figure)

        # Send this angle_figure to the plotter.
        obj.update(data_type='angle_figure', data_metadata=metadata, data=figure)

        # Add the metric
        # obj.update(data_type='performance_metric', data_metadata={'sample_number': 1}, data={'x': 1, 'y': 2})
        obj.update(data_type='performance_metric', data_metadata={'sample_number': 1}, data={'x': 1, 'y': 2})

    # Ensure that the angle_plot stays open at the end of the script.
    plt.show()
