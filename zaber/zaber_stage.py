"""This class allows for the control of the Zaber T-LST025B linear stages.  1 or 2 Zaber stages can be controlled with
this script.  If two stages are being controlled, the first Zaber stage is plugged into the computer, and the second
stage is plugged into the first stage.

For more information, see https://www.zaber.com/support/docs/api/core-python/0.8.1/examples.html"""

from zaber.serial import *


class ZaberStage(object):
    """
    Create a Zaber stage object for control.  Create an instance of this class for every Zaber stage that you want to
    control.
    """

    def __init__(self,
                 port='COM6',
                 device=1):
        """
        Records the connection parameters for the Zaber stage to the class for future use.  Does some type-checking.

        :param port: Port on the computer to form serial connection.
            For Macs, this looks like:/dev/tty.usbserial-AH0616TK
        :param device: The device number printed on the Zaber stage.
        """

        # Parameter validation tests
        if device not in [1, 2]:
            raise ValueError('Device should be 1 or 2 (int), as indicated on the side of the Zaber stage.')

        if not isinstance(port, str):
            raise ValueError('Port should be a string, pointing to the port where the Zaber stages are connected.')

        # Store these values into the instance of the Zaber stage
        self.port_val = port
        self.device_val = device
        self.homed = False

    def _connect(self):
        """
        Connects to the Zaber stage on the port.
        """

        # Ensure that the port is closed.
        try:
            self._disconnect()
        except:
            pass

        # Get the port location for the Zaber stage.  The kwarg specified is specific to a PC, and should be changed
        # based upon the computer you are using.
        self.port = BinarySerial(self.port_val, timeout=100, inter_char_timeout=0.1) #added a longer inter_char_timeout in an attempt to reduce timeouts which are intermittently occuring

        # Create the device
        self.device = BinaryDevice(self.port, self.device_val)

    def _disconnect(self):
        """
        Disconnect the device from the port.
        """

        self.port.close()

    def move_absolute(self, pos: int):
        """
        Moves the Zaber stage to an absolute position.

        :param pos: Absolute position to move the Zaber stage to.
        """

        # Tests
        if not isinstance(pos, int):
            raise ValueError('The position should be an interger.')
        if pos < 0 or pos > 512000:
            raise ValueError(f'The position passed ({pos}) is not between 0 and 512000.')
        if not self.homed:
            raise ValueError('Device should be homed before absolute movements.')

        # Connect and move the Zaber stage
        self._connect()
        print(f'Moving to position {pos}.')
        self.device.move_abs(pos)
        self._disconnect()

    def move_relative(self, pos: int):
        """
        Moves the Zaber stage a relative amount.

        :param pos: Relative amount to move Zaber stage
        """

        # Tests
        if not self.homed:
            raise ValueError('Device should be homed before relative movements.')

        new_pos = self.get_position() + pos
        if new_pos < 0 or new_pos > 512000:
            raise ValueError(f'New position {new_pos} is outside of 0 and 512,000.')

        # Connect and move
        self._connect()
        print(f'Moving to position {new_pos}.')
        self.device.move_rel(distance=pos)
        self._disconnect()

    def get_position(self):
        """
        Print the device position.
        """

        self._connect()
        reply = self.device.send(60, 0)
        self._disconnect()
        return int(reply.data)

    def home(self):
        """
        Home the device.
        """

        self._connect()
        print('Homing the stage.')
        self.device.home()
        self.homed = True
        self._disconnect()


if __name__ == "__main__":

    # Example movement.
    z2 = ZaberStage(device=2)
    z2.home()
    z2.move_absolute(200000)
    z2.move_relative(312000)
    position = z2.get_position()
    print(f'The current position is {position}.')
    z2.home()



    # TODO Add a mm_to_count method to ZaberStage class.
