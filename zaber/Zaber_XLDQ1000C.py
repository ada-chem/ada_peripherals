

"""This class allows for the control of the Zaber X-???? magnetic linear stages.  1 or 2 Zaber stages can be controlled with
this script.  If two stages are being controlled, the first Zaber stage is plugged into the computer, and the second
stage is plugged into the first stage.

For more information, see https://www.zaber.com/support/docs/api/core-python/0.8.1/examples.html"""

import math
import time
from zaber.serial import *


class NewZaberXLDQ1000C(object):
    """
    Create a Zaber stage object for control.  Create an instance of this class for every Zaber stage that you want to
    control.
    """

    def __init__(self,
                 port='COM6',
                 device=1,
                 simulate=False,
                 simulator=None,
                 simulator_asset=None):
        """
        Records the connection parameters for the Zaber stage to the class for future use.  Does some type-checking.

        :param port: Port on the computer to form serial connection.
            For Macs, this looks like:/dev/tty.usbserial-AH0616TK
        :param device: The device number printed on the Zaber stage.
        """

        # Parameter validation tests
        if device not in [1, 2]:
            raise ValueError('Device should be 1 or 2 (int), as indicated on the side of the Zaber stage.')

        if not isinstance(port, str):
            raise ValueError('Port should be a string, pointing to the port where the Zaber stages are connected.')

        # Store these values into the instance of the Zaber stage
        self.port_val = port
        self.device_val = device
        self.homed = False

        #hardcode device parameters
        self.max_travel = 1000  # mm
        self.max_pos = 1000000000
        self.max_vel = 68719476704
        self.max_possible_velocity = 1500  # mm/s

        # track the last position and velocity (in counts) so we can calculate movement durations
        self.pos = 0
        self.vel = 318967555  # use a decent default velocity for simulation (1000mm/s)

        # simulator integration
        self.simulate = simulate
        self.simulator = simulator
        self.simulator_asset = simulator_asset

    def _connect(self):
        """
        Connects to the Zaber stage on the port.
        """

        if self.simulate:
            return

        # Ensure that the port is closed.
        try:
            self._disconnect()
        except:
            pass

        # Get the port location for the Zaber stage.  The kwarg specified is specific to a PC, and should be changed
        # based upon the computer you are using.
        self.port = AsciiSerial(self.port_val, timeout=100)

        # Create the device
        self.device = AsciiDevice(self.port, self.device_val)

    def _disconnect(self):
        """
        Disconnect the device from the port.
        """

        if self.simulate:
            return

        self.port.close()

    def get_position_in_counts(self,pos: int):
        """

        :param pos: position given in counts
        :return: position in mm
        """
        new_pos = int(pos*self.max_pos/self.max_travel)
        return new_pos

    def get_velocity_in_counts_s(self,vel: int):
        """

        :param vel: velocity as given in counts
        :return: velocity in mm/s
        """
        new_vel = int(self.max_vel * 10 ** (7*(vel / self.max_possible_velocity-1)))
        return new_vel

    def move_absolute(self, pos: int):
        """
        Moves the Zaber stage to an absolute position.

        :param pos: Absolute position to move the Zaber stage to in mm
        """

        # Tests
        position = self.get_position_in_counts(pos)  #gets the position in counts

        if not isinstance(position, int):
            raise ValueError('The position should be an interger.')
        if pos < 0 or pos > self.max_travel:
            raise ValueError(f'The position passed ({pos}) is not between 0 and ({self.max_travel}).')
        if not self.homed:
            raise ValueError('Device should be homed before absolute movements.')

        # Connect and move the Zaber stage
        self._connect()
        print(f'Moving to position {pos} mm.')
        if not self.simulate:
            self.device.move_abs(position)
        self._disconnect()
        # move simulator
        self.move_simulator(position)
        # save the current position
        self.pos = position

    def move_relative(self, pos: int):
        """
        Moves the Zaber stage a relative amount.

        :param pos: Relative amount to move Zaber stage
        """
        position = self.get_position_in_counts(pos)  # gets the position in counts
        # Tests
        if not self.homed:
            raise ValueError('Device should be homed before relative movements.')

        new_pos = self.get_position() + position
        if new_pos < 0 or new_pos > self.max_pos:
            raise ValueError(f'New position is outside of 0 and ({self.max_travel}).')

        # Connect and move
        self._connect()
        print(f'Moving to position {pos} mm.')
        if not self.simulate:
            self.device.move_rel(distance=position)
        self._disconnect()
        # move simulator
        self.move_simulator(new_pos)
        self.pos = new_pos

    def get_position(self):
        """
        Print the device position.
        """

        if self.simulate:
            return self.pos

        self._connect()
        reply = self.device.send("get pos")
        self._disconnect()
        return int(reply.data)

    def home(self):
        """
        Home the device.
        """

        self._connect()
        print('Homing the stage.')
        if not self.simulate:
            self.device.home()
        self.homed = True
        self._disconnect()
        self.move_simulator(0)

    def change_velocity(self, vel: int):
        """
        changes the velocity .
        """

        velocity =  self.get_velocity_in_counts_s(vel)
        print(vel)
        print(velocity)
        if not self.homed:
            raise ValueError('Device should be homed before changing the velocity.')

        if vel < 0 or vel > self.max_possible_velocity:
            raise ValueError(f'New velocity {vel} is outside of 0 and ({self.max_possible_velocity}).')

        self.vel = velocity
        # Connect and move
        self._connect()
        print(f'Change the velocity {vel} mm/s.')
        if not self.simulate:
            self.device.send(f'set maxspeed {velocity}')
        self._disconnect()

    def move_simulator(self, position):
        """
        Updates the position of the Zaber stage in the simulator, if connected.

        :param position: position of the Zaber stage in counts
        """

        if self.simulator_asset is None:
            return

        delta = math.fabs(position - self.pos)
        duration = delta / self.vel

        joint_asset_id = self.simulator_asset.find_child('zaber').id
        self.simulator.move_joint(joint_asset_id, position, duration)

        if self.simulate:
            time.sleep(duration)


#EXAMPLE TEXT BELOW
# z2 = NewZaberXLDQ1000C(device=1)
# z2.home()
# max_speed = 1000
# z2.change_velocity(max_speed)
#
# for  i in range(0, 10):
#     z2.move_absolute(0)
#     z2.move_absolute(1000)
#   #   z2.move_absolute(275)
#   #  z2.move_absolute(275*2)
#    # z2.move_absolute(275)

