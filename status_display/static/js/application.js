

// This socket receives a JSON object from the server, and uses it to update
// the HTML.

var socket = io.connect('http://' + document.domain + ':' + location.port);

// Trigger when emit is received from the server
socket.on('my response', function(JSON_object) {

    // Loop through the JSON provided.
    for (key in JSON_object) {

        // Get the possible element to update
        active_element = document.getElementById(key)

        // If the element already exists
        if (active_element !== null) {

            // Update the element
            active_element.innerHTML = JSON_object[key];

        } else { // If it does not exist

            console.log('The data passed did not match the HTML')
        }

     }

});