import sys, os
INTERP = os.path.join(os.environ['HOME'], 'status.projectada.ca', 'bin', 'python')
if sys.executable != INTERP:
    try:
        os.execl(INTERP, INTERP, *sys.argv)
    except:
        pass
sys.path.append(os.getcwd())


from flask import Flask
application = Flask(__name__)

from flask import Flask, render_template, request
from flask_socketio import SocketIO

application = Flask(__name__)
application.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(application)

@application.route('/')
def index():
    return 'Hello Passenger1'

if __name__ == '__main__':
    socketio.run(application, debug=True)