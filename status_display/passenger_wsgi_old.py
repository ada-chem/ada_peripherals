import sys, os
INTERP = os.path.join(os.environ['HOME'], 'status.projectada.ca', 'bin', 'python')
if sys.executable != INTERP:
    try:
        os.execl(INTERP, INTERP, *sys.argv)
    except:
        pass
sys.path.append(os.getcwd())


from flask import Flask, render_template, request
from flask_socketio import SocketIO

application = Flask(__name__)
application.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(application)

# Serve the main page
@application.route('/')
def serve_page():
    return render_template('index.html')

# Receive a GET update pair
@application.route('/update')
def recieve_get():
    value = request.args
    socketio.emit('my response', value, broadcast=True)
    return 'OK.'

#
# # Emit the update JSON to the page
# @socketio.on('update')
# def emit_json(message):
#     print('The following message was recieved:')
#     print(message)
#     print(type(message))
#     emit('my response', message, broadcast=True)


if __name__ == '__main__':
    socketio.run(application, debug=True)