from machine_learning.liquid_level_ml_categorical_version import *
import vision
from ada_core.dependencies.slackbot import SlackBot

# This is an example of how to use liquid level ML - goes through taking a folder of images of a vessel with liquid
# into it, and then makes an ML model to find the meniscus from the image.

# the main thing to do is to have the paths working correctly. For the variables written in ALL CAPS, there is a
# certain structure already to have everything working. The main thing you need to do to test is to to supply the
# MAIN_FOLDER_PATH. This will be the main folder where all the files associated for the ML model, training,
# and testing are kept. Once you have decided where the main folder is, create the following folders in the main
# folder: drawn_images, model, sliced_image_pickle_files_separated_by_class, testing_images, training_images. Then put
# the images of containers with liquids in them that you want to train the liquid level ML model on into the
# training_images folder, and put images you want to test to see how well the model works in the testing_images
# folder. The number of images you put in the training_images folder must be divisible by 10, or what the value of
# BATCH_SIZE is.

# based on the images used, change IMAGE_WIDTH and IMAGE_HEIGHT to be the width and height of the training and
# testing images.

# # root_dir = os.getenv('imagefolder')  # this is to connect to the NAS, comment out for now because testing with a
# folder of images that isn't in the NAS
# if not connecting to NAS then dont need root_Dir, just need to connect to vision directly, but if using NAS,
# then need change the vision folder path to be connected to the NAS instead
# vision_folder_path = os.path.join(root_dir, vision_folder_name)  # version for if connected to NAS
vision_folder_path = os.path.dirname(os.path.abspath(vision.__file__))

# path to the main folder that contains all the data for testing
MAIN_FOLDER_NAME = 'liquid_level_test_1'
MAIN_FOLDER_PATH = os.path.dirname(os.path.abspath(MAIN_FOLDER_NAME.__file__))

TRAINING_IMAGES_FOLDER_NAME = 'training_images'
TRAINING_IMAGES_FOLDER_PATH = os.path.join(MAIN_FOLDER_PATH, TRAINING_IMAGES_FOLDER_NAME)

DRAWN_IMAGES_FOLDER_NAME = 'drawn_images'
DRAWN_IMAGES_FOLDER_PATH = os.path.join(MAIN_FOLDER_PATH, DRAWN_IMAGES_FOLDER_NAME)

TESTING_IMAGES_FOLDER_NAME = 'testing_images'
TESTING_IMAGES_FOLDER_PATH = os.path.join(MAIN_FOLDER_PATH, TESTING_IMAGES_FOLDER_NAME)

FULL_IMAGE_PICKLE_DATA_SET_NAME = 'full_image_data_set.pkl'
FULL_IMAGE_PICKLE_DATA_SET_PATH = os.path.join(MAIN_FOLDER_PATH, FULL_IMAGE_PICKLE_DATA_SET_NAME)

SLICED_IMAGE_PICKLE_FILES_SEPARATED_BY_CLASS_FOLDER_NAME = 'sliced_image_pickle_files_separated_by_class'
SLICED_IMAGE_PICKLE_FILES_SEPARATED_BY_CLASS_FOLDER_PATH = os.path.join(MAIN_FOLDER_PATH,
                                                                        SLICED_IMAGE_PICKLE_FILES_SEPARATED_BY_CLASS_FOLDER_NAME)

FINAL_MERGED_SLICED_DATA_SET_NAME = 'final_merged_sliced_data_set.pkl'
FINAL_MERGED_SLICED_DATA_SET_PATH = os.path.join(MAIN_FOLDER_PATH, FINAL_MERGED_SLICED_DATA_SET_NAME)

CLASS_ONE_PICKLE_FILE_NAME = 'no_meniscus.pkl'
CLASS_ONE_PICKLE_FILE_PATH = os.path.join(SLICED_IMAGE_PICKLE_FILES_SEPARATED_BY_CLASS_FOLDER_PATH, CLASS_ONE_PICKLE_FILE_NAME)

CLASS_TWO_PICKLE_FILE_NAME = 'meniscus.pkl'
CLASS_TWO_PICKLE_FILE_PATH = os.path.join(SLICED_IMAGE_PICKLE_FILES_SEPARATED_BY_CLASS_FOLDER_PATH, CLASS_TWO_PICKLE_FILE_NAME)

ML_MODEL_FOLDER_NAME = 'model'
ML_MODEL_FOLDER_PATH = os.path.join(MAIN_FOLDER_PATH, ML_MODEL_FOLDER_NAME)

AUGMENT_IMAGE_DICTIONARY = {'horizontal_flip': True,
                            'list_of_crop_and_resize_arguments': [{'crop_left': 0.0,
                                                                   'crop_right': 0.0,
                                                                   'crop_top': 0.0,
                                                                   'crop_bottom': 0.0},
                                                                  {'crop_left': 0.01,
                                                                   'crop_right': 0.01,
                                                                   'crop_top': 0.01,
                                                                   'crop_bottom': 0.01},
                                                                  {'crop_left': 0.015,
                                                                   'crop_right': 0.015,
                                                                   'crop_top': 0.015,
                                                                   'crop_bottom': 0.015},
                                                                  {'crop_left': 0.02,
                                                                   'crop_right': 0.02,
                                                                   'crop_top': 0.02,
                                                                   'crop_bottom': 0.02},
                                                                  {'crop_left': 0.025,
                                                                   'crop_right': 0.025,
                                                                   'crop_top': 0.025,
                                                                   'crop_bottom': 0.025},
                                                                  {'crop_left': 0.03,
                                                                   'crop_right': 0.03,
                                                                   'crop_top': 0.03,
                                                                   'crop_bottom': 0.03},
                                                                  {'crop_left': 0.035,
                                                                   'crop_right': 0.035,
                                                                   'crop_top': 0.035,
                                                                   'crop_bottom': 0.035},
                                                                  # {'crop_left': 0.04,
                                                                  #  'crop_right': 0.04,
                                                                  #  'crop_top': 0.04,
                                                                  #  'crop_bottom': 0.04},
                                                                  # {'crop_left': 0.045,
                                                                  #  'crop_right': 0.045,
                                                                  #  'crop_top': 0.045,
                                                                  #  'crop_bottom': 0.045},
                                                                  # {'crop_left': 0.05,
                                                                  #  'crop_right': 0.05,
                                                                  #  'crop_top': 0.05,
                                                                  #  'crop_bottom': 0.05},
                                                                  # {'crop_left': 0.075,
                                                                  #  'crop_right': 0.075,
                                                                  #  'crop_top': 0.075,
                                                                  #  'crop_bottom': 0.075},
                                                                  ],
                            }

IMAGE_WIDTH = 1280
IMAGE_HEIGHT = 720
IMAGE_SLICE_HEIGHT = 10  # the height of a horizontal slice of an image
ROWS_TO_COUNT = 3  # number of rows to count to find the meniscus
NUMBER_OF_CHANNELS = 3  # number of colour channels; 1 for b/w images and 3 for rgb
NUMBER_OF_CLASSES = 2
ENCODING_DICTIONARY = {'meniscus': 1, 'no_meniscus': 0}

BATCH_SIZE = 10
LEARNING_RATE = 1e-4
LOSS_THRESHOLD = 0.0001  # smaller number means ML model will train until it messes up less compared to a high loss
# threshold

SHOW_PREDICTION = False

try:
    slack_bot = SlackBot()  # create gronckupdate bot
except Exception as e:
    slack_bot = None


# STEP 1
if slack_bot is not None:
    slack_bot.post_slack_message(f'Starting test for liquid level model for {MAIN_FOLDER_NAME}')

# try:
#     run_liquid_level_algorithm_on_folder_of_images(image_folder_path=TRAINING_IMAGES_FOLDER_PATH,
#                                                    pickle_file_path=FULL_IMAGE_PICKLE_DATA_SET_PATH,
#                                                    save_drawn_image_folder_path=DRAWN_IMAGES_FOLDER_PATH,
#                                                    rows_to_count=ROWS_TO_COUNT,
#                                                    augment_image_data_arguments=AUGMENT_IMAGE_DICTIONARY,
#                                                    )
#     if slack_bot is not None:
#         slack_bot.post_slack_message(f'Completed step 1: run_liquid_level_algorithm_on_folder_of_images')
# except Exception as e:
#     print(e)
#     if slack_bot is not None:
#         slack_bot.post_slack_message(f'Encountered error in step 1: run_liquid_level_algorithm_on_folder_of_images')
#         slack_bot.post_slack_message(f'Error encountered: {e}')

# view_dataset(pickle_data_path=FULL_IMAGE_PICKLE_DATA_SET_PATH)

# STEP 2
try:
    extract_classes_into_separate_pickle_files(pickle_file_path=FULL_IMAGE_PICKLE_DATA_SET_PATH,
                                               encoding_dictionary=ENCODING_DICTIONARY,
                                               folder_path_to_put_resulting_pickle_files_in=SLICED_IMAGE_PICKLE_FILES_SEPARATED_BY_CLASS_FOLDER_PATH)

    # view_dataset(pickle_data_path=CLASS_ONE_PICKLE_FILE_PATH)
    # view_dataset(pickle_data_path=CLASS_TWO_PICKLE_FILE_PATH)

    number_of_items_in_meniscus_pickle_file = count_items_in_feature_list_in_pickle_file(
        pickle_data_path=CLASS_TWO_PICKLE_FILE_PATH)
    print(f'number of items in meniscus pickle file is {number_of_items_in_meniscus_pickle_file} at  '
          f'{CLASS_TWO_PICKLE_FILE_PATH}')

    number_of_items_in_no_meniscus_pickle_file = count_items_in_feature_list_in_pickle_file(
        pickle_data_path=CLASS_ONE_PICKLE_FILE_PATH)
    print(f'number of items in meniscus pickle file before getting subset of no_meniscus images from pickle file is '
          f'{number_of_items_in_no_meniscus_pickle_file} at  '
          f'{CLASS_ONE_PICKLE_FILE_PATH}')

    if slack_bot is not None:
        slack_bot.post_slack_message(f'Completed step 2: extract_classes_into_separate_pickle_files')
        slack_bot.post_slack_message(f'number of items in meniscus pickle file is {number_of_items_in_meniscus_pickle_file} at  '
                                     f'{CLASS_TWO_PICKLE_FILE_PATH}')
        slack_bot.post_slack_message(f'number of items in meniscus pickle file before getting subset of no_meniscus images from pickle file is '
                                     f'{number_of_items_in_no_meniscus_pickle_file} at  '
                                     f'{CLASS_ONE_PICKLE_FILE_PATH}')

except Exception as e:
    print(e)
    if slack_bot is not None:
        slack_bot.post_slack_message(f'Encountered error in step 2: extract_classes_into_separate_pickle_files')
        slack_bot.post_slack_message(f'Error encountered: {e}')

number_of_items_in_meniscus_pickle_file = count_items_in_feature_list_in_pickle_file(pickle_data_path=CLASS_TWO_PICKLE_FILE_PATH)
print(f'number of items in meniscus pickle file is {number_of_items_in_meniscus_pickle_file} at  '
      f'{CLASS_TWO_PICKLE_FILE_PATH}')

number_of_items_in_no_meniscus_pickle_file = count_items_in_feature_list_in_pickle_file(
    pickle_data_path=CLASS_ONE_PICKLE_FILE_PATH)
print(f'number of items in meniscus pickle file before getting subset of no_meniscus images from pickle file is '
      f'{number_of_items_in_no_meniscus_pickle_file} at  '
      f'{CLASS_ONE_PICKLE_FILE_PATH}')

# STEP 3
try:
    get_subset_of_data_in_pickle_file(number_of_items_to_get=number_of_items_in_meniscus_pickle_file,  # which is just the number of items in the meniscus
                                      #  pickle file
                                      pickle_file_path=CLASS_ONE_PICKLE_FILE_PATH,
                                      new_pickle_file_path=CLASS_ONE_PICKLE_FILE_PATH,
                                      )

    number_of_items_in_no_meniscus_pickle_file_post_subset = count_items_in_feature_list_in_pickle_file(
        pickle_data_path=CLASS_ONE_PICKLE_FILE_PATH)
    print(f'number of items in meniscus pickle file after getting subset of no_meniscus images from pickle file is '
          f'{number_of_items_in_no_meniscus_pickle_file_post_subset} at  '
          f'{CLASS_ONE_PICKLE_FILE_PATH}')

    if slack_bot is not None:
        slack_bot.post_slack_message(f'number of items in meniscus pickle file after getting subset of no_meniscus images from pickle file is '
              f'{number_of_items_in_no_meniscus_pickle_file_post_subset} at  '
              f'{CLASS_ONE_PICKLE_FILE_PATH}')
        slack_bot.post_slack_message(f'Completed step 3: get_subset_of_data_in_pickle_file')

except Exception as e:
    print(e)
    if slack_bot is not None:
        slack_bot.post_slack_message(f'Encountered error in step 3: get_subset_of_data_in_pickle_file')
        slack_bot.post_slack_message(f'Error encountered: {e}')

# STEP 4
try:
    merge_pickle_files_into_one_file(
        folder_path_with_multiple_pickle_files=SLICED_IMAGE_PICKLE_FILES_SEPARATED_BY_CLASS_FOLDER_PATH,
        path_to_put_resulting_pickle_file_in=FINAL_MERGED_SLICED_DATA_SET_PATH)
    if slack_bot is not None:
        slack_bot.post_slack_message(f'Completed step 4: merge_pickle_files_into_one_file')
except Exception as e:
    print(e)
    if slack_bot is not None:
        slack_bot.post_slack_message(f'Encountered error in step 4: merge_pickle_files_into_one_file')
        slack_bot.post_slack_message(f'Error encountered: {e}')

# view_dataset(pickle_data_path=FINAL_MERGED_SLICED_DATA_SET_PATH)

# STEP 5
try:
    train(pickle_data_path=FINAL_MERGED_SLICED_DATA_SET_PATH,
          encoding_dictionary=ENCODING_DICTIONARY,
          model_folder_path=ML_MODEL_FOLDER_PATH,
          image_slice_height=IMAGE_SLICE_HEIGHT,  # height of the slice, not of the entire image
          image_width=IMAGE_WIDTH,
          number_of_channels=NUMBER_OF_CHANNELS,
          number_of_classes=NUMBER_OF_CLASSES,
          loss_threshold=LOSS_THRESHOLD,
          learning_rate=LEARNING_RATE,
          batch_size=BATCH_SIZE,
          )
    if slack_bot is not None:
        slack_bot.post_slack_message(f'Completed step 5: train')
except Exception as e:
    print(e)
    if slack_bot is not None:
        slack_bot.post_slack_message(f'Encountered error in step 5: train')
        slack_bot.post_slack_message(f'Error encountered: {e}')

# STEP 6
try:
    predict(path_to_folder_of_images_to_test=TESTING_IMAGES_FOLDER_PATH,
            image_width=IMAGE_WIDTH,
            image_height=IMAGE_HEIGHT,
            model_folder_path=ML_MODEL_FOLDER_PATH,
            encoding_dictionary=ENCODING_DICTIONARY,
            rows_to_count=ROWS_TO_COUNT,
            show_prediction=SHOW_PREDICTION,
            )
    if slack_bot is not None:
        slack_bot.post_slack_message(f'Completed step 6: predict')
except Exception as e:
    print(e)
    if slack_bot is not None:
        slack_bot.post_slack_message(f'Encountered error in step 6: predict')
        slack_bot.post_slack_message(f'Error encountered: {e}')

