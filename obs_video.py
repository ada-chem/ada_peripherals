import logging
import time
from obswebsocket import obsws, requests, events
from obswebsocket.exceptions import ConnectionFailure

logger = logging.getLogger(__name__)


class ObsVideoError(Exception):
    pass


class ObsVideoResponseError(ObsVideoError):
    def __init__(self, message, response):
        super().__init__(message)
        self.response = response
        self.response_error = response.datain['error']


class ObsConnectionError(ObsVideoError):
    pass


class ObsVideo:
    def __init__(self, host='localhost', port=4444, password=None, connect=True):
        self.logger = logger.getChild(self.__class__.__name__)
        self.logger.info("OBS: connecting to OBS web socket...")
        self.socket = obsws(host, port, password)
        self.recording_folder = None
        self.connected = False

        if connect:
            self.connect()

    def connect(self, retries=3):
        for _ in range(retries):
            # Keep trying!
            try:
                self.socket.connect()
                self.connected = True
                break
            except ConnectionFailure as err:
                self.logger.info('Failed to connect to OBS. Please check that it is open and press enter to continue.')
                input()
                time.sleep(1)  # Wait a bit before retrying
        else:  # We tried {retries} times and we still were unable to connect. There's gotta be something else at hand.
            # Last try
            try:
                self.socket.connect()
                self.connected = True
            except ConnectionFailure as err:
                raise ObsConnectionError(f'Error connecting to OBS: {err}')

    def disconnect(self):
        self.socket.disconnect()
        self.connected = False

    def request(self, request):
        try:
            response = self.socket.call(request)
        except ConnectionFailure:
            raise ObsConnectionError(f'Error connecting to OBS')

        if not response.status:
            raise ObsVideoResponseError(f'Error sending OBS request: {response}', response)

    def set_recording_folder(self, recording_folder):
        self.recording_folder = recording_folder
        self.request(requests.SetRecordingFolder(recording_folder))

    def start_recording(self, recording_folder=None, retries=1, retry_delay=1):
        if recording_folder:
            self.set_recording_folder(recording_folder)

        try:
            self.request(requests.StartRecording())
        except ObsVideoResponseError as err:
            # if the recording is already active, try stopping it before starting the recording again
            if err.response_error == 'recording already active' and retries > 0:
                self.stop_recording()
                time.sleep(retry_delay)
                self.start_recording(recording_folder, retries - 1, retry_delay)
                time.sleep(retry_delay)
            else:
                raise err

    def stop_recording(self):
        try:
            self.request(requests.StopRecording())
        except ObsVideoResponseError as err:
            # catch response errors, and ignore the error if the recording is already stopped
            if err.response_error == 'recording not active':
                logger.debug("OBS: recording not active, ignoring.")
            else:
                raise err

    def start_streaming(self):
        try:
            self.request(requests.StartStreaming())
        except ObsVideoResponseError as err:
            if err.response_error == 'streaming already active':
                logger.debug("OBS: streaming already active, ignoring.")
            else:
                raise err

    def stop_streaming(self):
        try:
            self.request(requests.StopStreaming())
        except ObsVideoResponseError as err:
            if err.response_error == 'streaming not active':
                logger.debug("OBS: streaming not active, ignoring.")
            else:
                raise err


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    obs_video = ObsVideo()
    obs_video.start_streaming()
