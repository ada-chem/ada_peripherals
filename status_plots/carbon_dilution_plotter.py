"""
A live status angle_plot for the cobalt doping conductivity demo for NRCan
"""

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import pandas as pd
import numpy as np
import pickle
import time
plt.ioff()
#mpl.style.use('fivethirtyeight')
class carbon_dilution_plotter(object):

    def __init__(self):

        # Empty arrays to dump data in
        self._x_list = []
        self._y_list = []
        self._images = {}

        # Empty array for subplots
        self._subplots = {}

        # Create the angle_plot
        mpl.rcParams.update({'font.size': 20})
        self.plot = plt.figure(figsize=(11,8.5))

        # Move the angle_plot to the second screen
        wm = plt.get_current_fig_manager()
        wm.window.wm_geometry("1920x1080-1910-340")
        wm.window.state('zoomed')

        # Create image subplots
        for count in range(5):

            # Create subplots and store
            self._subplots[f'image_{count}'] = self.plot.add_subplot(3, 5, count + 1)

        # Create the big subplot
        self._subplots['experiment'] = self.plot.add_subplot(2, 1, 2)

        # Format the angle_plot
        self._format()

    def _render(self):

        plt.pause(0.01)

    def _format(self):

        # Set x range
        self._subplots['experiment'].set_xlim(-0.25, 1.25)

        # Set y range
        self._subplots['experiment'].set_ylim(bottom=0)

        # Set labels
        self._subplots['experiment'].set_xlabel('Carbon Percentage $\it{(\%)}$')
        self._subplots['experiment'].set_ylabel('Conductance $\it{(Mohm^{-1})}$')

        # Format image plots
        for count in range(5):

            self._subplots[f'image_{count}'].set_axis_off()

        # Spacing
        self.plot.subplots_adjust(
            left=0.06,
            right=0.94,
            top=1,
            bottom=0.10,
            # wspace=0.03,
            # hspace=0.35,
        )

        # Render the angle_plot
        self._render()


    def _update_data(self, type, data):

        if type == 'datapoint':

            # Append the data points to the list
            self._x_list.append(data[0])
            self._y_list.append(data[1] * 10e9)

        if type == 'image':

            # Append data to dict
            self._images[data[0]] = data[1]

    def _update_plot(self):

        # Delete old data
        for ax in self.plot.get_axes():
            ax.clear()

        # Plot new data
        self._subplots['experiment'].scatter(
            self._x_list,
            self._y_list
        )

        for key, value in self._images.items():

            # Plot image
            self._subplots[f'image_{key}'].imshow(value[170:460, 315:1175, :])

    def update(self, type, data):

        # Update the data lists
        self._update_data(type, data )

        # Plot the data
        self._update_plot()

        # Format
        self._format()



        return


if __name__  == "__main__":

    # Prepare some dummy data.  This will be the image from the webcam.
    # BPM may pass this image however he likes.
    img = mpimg.imread('example.png')

    # Initiate the plotter
    plotter = carbon_dilution_plotter()

    # Send dummy data.
    for count in range(1):

        # Send a conductivity datapoint
        plotter.update(type='datapoint', data=[count, np.random.rand() * 1E-6])

        # Send an image.
        plotter.update(type='image', data=[count, img])

        time.sleep(3)

